<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class frontend_controller extends MY_Controller{	

	public $themes;
	public $_vclam;
	public $_aplicare;
	public $_dukcapil;
	public $_logo_mini;
	public $_logo_regular;
	public $_title;
	public $_version;
	public $_footer;
	public $_email;
	public $_cf;
	public $_segs;

	function __construct(){
		parent::__construct();
		global $Cf;
		$this->_cf = $Cf;
		$this->_segs = $this->uri->segment_array();
		
		if($Cf->themes_login=='lucida')
		{
			$menu = 'lucida_helper';
		}
		else
		{
			$menu = 'menu_helper';
		}

		$this->load->model(['Api_model','Configapps_model','Configuration_model']); // load model class Get_model
		$this->load->library(['Site','form_validation','session','mybreadcrumb','Functionother','Pdf','Datatables']); // load library
		$this->load->helper([''.$menu.'','instrument_helper','medic_helper','date']); // load library
		
		$this->site->themes			= 'Lcd';
		$this->site->themes_error	= 'errors';
		
		$this->site->template 		= '';
		$this->site->template_error	= 'html';

		/*
		Config Title Application
		*/
		$apps = $this->Configapps_model->getData(['status'=>1],5,0);
		$this->_logo_mini 		= $apps[0]->name;
		$this->_logo_regular 	= $apps[1]->name;
		$this->_title 			= $apps[2]->name;
		$this->_version 		= $apps[3]->name;
		$this->_footer 			= $apps[4]->name;
	}

}