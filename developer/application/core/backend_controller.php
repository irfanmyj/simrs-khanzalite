<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class backend_controller extends CI_Controller{	

	public $themes;
	public $_vclam;
	public $_vclamtes;
	public $_aplicare;
	public $_dukcapil;
	public $_logo_mini;
	public $_logo_regular;
	public $_title;
	public $_version;
	public $_footer;
	public $_email;
	public $_cf;
	public $_segs;

	function __construct(){
		parent::__construct();
		global $Cf;
		$this->_cf = $Cf;
		$this->_segs = $this->uri->segment_array();
		
		if($this->_cf->themes_login=='lucida')
		{
			$menu = 'lucida_helper';
		}
		else
		{
			$menu = 'menu_helper';
		}

		$this->load->model(['Api_model','Configapps_model','Configuration_model']); // load model class Get_model
		$this->load->library(['Site','form_validation','session','mybreadcrumb','Functionother','Pdf','Datatables']); // load library
		$this->load->helper([''.$menu.'','instrument_helper','medic_helper','date']); // load library
		
		//Config template
		if($this->session->userdata('name_levelaccess'))
		{
			if($this->_cf->themes_login=='lucida')
			{
				$this->themes = $this->_cf->themes_folder[str_replace(' ', '',$this->session->userdata('name_levelaccess'))];
			}
			else
			{
				$this->themes = $this->_cf->themes_folder_default['SuperAdministrator'];
			}
		}
		else
		{
			$this->themes = $this->_cf->themes_login;
		}
		
		$this->site->themes			= $this->themes;
		$this->site->themes_error	= 'errors';
		
		$this->site->template 		= '';
		$this->site->template_error	= 'html';

		// Breadcrumb
		$this->mybreadcrumb->add('Home', base_url('home/index'));
		$this->mybreadcrumb->add(ucwords($this->uri->segment(1)).' - '.ucwords($this->uri->segment(2)), base_url($this->uri->segment(1).'/'.$this->uri->segment(2)));

		// GET CONFIG API BPJS
		$vclaim = $this->Api_model->getData(['id'=>1,'status'=>1],1,0);
		if($vclaim[0]->status==1)
		{
			$this->_vclam =  [
				'cons_id'		=> decrypt_aes($vclaim[0]->api_id),
				'secret_key'	=> decrypt_aes($vclaim[0]->password),
				'base_url' 		=> $vclaim[0]->base_url,
				'service_name' 	=> $vclaim[0]->service_name
			];
		}

		$vclaimtes = $this->Api_model->getData(['id'=>1,'status'=>1],1,0);
		if($vclaimtes[0]->status==1)
		{
			$this->_vclamtes =  [
				'cons_id'		=> decrypt_aes($vclaimtes[0]->api_id),
				'secret_key'	=> decrypt_aes($vclaimtes[0]->password),
				'base_url' 		=> $vclaimtes[0]->base_url,
				'service_name' 	=> $vclaimtes[0]->service_name
			];
		}
		
		$applicare = $this->Api_model->getData(['id'=>2,'status'=>1],1,0);
		if($applicare[0]->status==1)
		{
			$this->_aplicare = [
			    'cons_id'       => decrypt_aes($applicare[0]->api_id),
			    'secret_key'    => decrypt_aes($applicare[0]->password),
			    'base_url'      => $applicare[0]->base_url,
			    'service_name'  => $applicare[0]->service_name
			];
		}
		
		$dukcapil = $this->Api_model->getData(['id'=>3,'status'=>1],1,0);
		if($dukcapil[0]->status==1)
		{
			$this->_dukcapil = [
			    'user_id'       => decrypt_aes($dukcapil[0]->api_id),
			    'password'    	=> decrypt_aes($dukcapil[0]->password),
			    'ip_user'    	=> decrypt_aes($dukcapil[0]->ip)
			];
		}

		/*
		Config Title Application
		*/
		$apps = $this->Configapps_model->getData(['status'=>1],5,0);
		$this->_logo_mini 		= $apps[0]->name;
		$this->_logo_regular 	= $apps[1]->name;
		$this->_title 			= $apps[2]->name;
		$this->_version 		= $apps[3]->name;
		$this->_footer 			= $apps[4]->name;

		/*
		Config Email
		*/
		$email = $this->Configuration_model->getData(['id'=>1]);
		if($email[0]->status==1)
		{
			$this->_email = $this->_cf->configEmail;
		}

	}

    protected function _folder_creators($foldername='',$themes='lucida')
    {
        if($themes)
        {
        	if(empty($foldername)){
        		$foldername = $this->_segs;
        	}

            $folder = FCPATH.'themes/'.$themes.'/'.ucwords($foldername);
            if ( ! file_exists($folder) ) {
                mkdir($folder);
                $this->_file_creators($folder);
                return TRUE;
            }else{
            	return FALSE;
            }
        }    
    }

    protected function _file_creators($foldername=''){
    	$folder = FCPATH.'themes/lucida/Copyfile/';
    	if(is_array($this->_cf->_filename)){
    		foreach ($this->_cf->_filename as $k => $v) {
    			//fopen($foldername.'/'.$v, 'w');
    			copy($folder.'/'.$v, $foldername.'/'.$v);
    		}
    	}
    }

    protected function _file_controller($filename=''){
    	$folder = FCPATH.'developer/application/controllers/';
    	if(!file_exists($folder.ucwords($filename))){
    		//fopen($folder.ucwords($filename).'.php', 'w');
    		copy($folder.'/Copyfile.php', $folder.'/'.ucwords($filename).'.php');
    	}
    }

    protected function _file_model($filename=''){
    	$folder = FCPATH.'developer/application/models/';
    	if(!file_exists($folder.ucwords($filename))){
    		//fopen($folder.ucwords($filename).'.php', 'w');
    		copy($folder.'/Copyfile_model.php', $folder.'/'.ucwords($filename).'_model.php');
    	}
    }

    /*public function _set_timezone(){
		date_default_timezone_set("Asia/Jakarta");
	}*/

}