<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nursingcriteria_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'code';
	protected $_order_by = 'code';
	protected $_order_by_type = '';
	protected $_database = '';

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('nursing_criteria');
	}

	/*
	Cara mengambil data tanpa join
	*/
	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

	
}