<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookingregistrasi_model extends MY_Model{

	protected $_table_name = 'booking_registrasi as a';
	protected $_primary_key = 'no_rkm_medis';
	protected $_order_by = '';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public function __construct(){
		parent::__construct();
	}

	public $rules = array(
		'tanggal_periksa' => [
            'field' => 'tanggal_periksa',
            'label' => 'Tanggal Periksa',
            'rules' => 'trim|required'
		]
	);

	private $field = '
			a.no_reg,
			a.no_rkm_medis,
			a.kd_dokter,
			a.kd_poli,
			a.kd_pj,
			a.no_rujukan,
			a.no_skdp,
			a.no_sep,
			a.tanggal_periksa,
			a.tanggal_booking,
			a.waktu_kunjungan,
			a.jam_booking,
			a.status,
			a.status_bpjs,
			b.nm_pasien,
			IF(b.jk="L","Laki-laki","Perempuan") as jk,
			c.nm_dokter,
			d.nm_poli,
			e.png_jawab';

	private $tbjoin = [
		'dokter as c' => [
			'metode' => 'INNER',
			'relasi' => 'c.kd_dokter=a.kd_dokter'
		],
		'pasien as b' => [
			'metode' => 'INNER',
			'relasi' => 'b.no_rkm_medis=a.no_rkm_medis'
		],
		'poliklinik as d' => [
			'metode' => 'INNER',
			'relasi' => 'd.kd_poli=a.kd_poli'
		],
		'penjab as e' => [
			'metode' => 'INNER',
			'relasi' => 'e.kd_pj=a.kd_pj'
		]
	];

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','tanggal_periksa DESC',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}


	/*
	Select data untuk pembatalam pasien
	*/

	private $field1 = '
			a.kd_dokter,
			a.kd_poli,
			a.tanggal_periksa,
			c.nm_dokter,
			d.nm_poli';

	private $tbjoin1 = [
		'dokter as c' => [
			'metode' => 'INNER',
			'relasi' => 'c.kd_dokter=a.kd_dokter'
		],
		'poliklinik as d' => [
			'metode' => 'INNER',
			'relasi' => 'd.kd_poli=a.kd_poli'
		]
	];

	public function getDataBooking($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin1,$this->field1,$where,'a.tanggal_periksa','','a.tanggal_periksa ASC',$limit,$offset)->result();
	}

	public function getBooking($where=''){
		$field2 = '
			count(*) as jumlah,
			a.tanggal_periksa,
			a.no_reg,
			a.waktu_kunjungan,
			a.tanggal_booking,
			a.jam_booking,
			a.status,b.nm_poli,
			c.nm_dokter,
			d.png_jawab'
		;

		$tbjoin2 = [
			'poliklinik as b' => [
				'metode' => 'INNER',
				'relasi' => 'b.kd_poli=a.kd_poli'
			],
			'dokter as c' => [
				'metode' => 'INNER',
				'relasi' => 'c.kd_dokter=a.kd_dokter'
			],
			'penjab as d' => [
				'metode' => 'INNER',
				'relasi' => 'd.kd_pj=a.kd_pj'
			]
		];

		return $this->getJoin('',$tbjoin2,$field2,$where,'','','a.tanggal_periksa DESC',1)->result();
	}

	private $field_search = '
			a.no_reg,
			a.no_rkm_medis,
			a.kd_dokter,
			a.kd_poli,
			a.kd_pj,
			a.no_rujukan,
			a.no_skdp,
			a.no_sep,
			a.tanggal_periksa,
			a.tanggal_booking,
			a.waktu_kunjungan,
			a.jam_booking,
			a.status,
			a.status_bpjs,
			b.nm_pasien,
			IF(b.jk="L","Laki-laki","Perempuan") as jk,
			c.nm_dokter,
			d.nm_poli,
			e.png_jawab';

	private $tbjoin_search = [
		'dokter as c' => [
			'metode' => 'INNER',
			'relasi' => 'c.kd_dokter=a.kd_dokter'
		],
		'pasien as b' => [
			'metode' => 'INNER',
			'relasi' => 'b.no_rkm_medis=a.no_rkm_medis'
		],
		'poliklinik as d' => [
			'metode' => 'INNER',
			'relasi' => 'd.kd_poli=a.kd_poli'
		],
		'penjab as e' => [
			'metode' => 'INNER',
			'relasi' => 'e.kd_pj=a.kd_pj'
		]
	];

	public function getDataSearch($where=[],$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin_search,$this->field_search,$where,'','','tanggal_periksa DESC',$limit,$offset)->result();
	}

	public function countDataSearch($where=[])
	{
		return $this->countJoin('',$this->tbjoin_search,$where);
	}
}