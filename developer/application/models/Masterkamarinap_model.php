<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterkamarinap_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = 'tgl_masuk';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('kamar_inap');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	kamar_inap.no_rawat,
    	kamar_inap.kd_kamar,
    	kamar_inap.trf_kamar,
    	kamar_inap.diagnosa_awal,
    	kamar_inap.diagnosa_akhir,
    	kamar_inap.tgl_masuk,
    	kamar_inap.jam_masuk,
    	kamar_inap.tgl_keluar,
    	kamar_inap.jam_keluar,
    	kamar_inap.lama,
    	kamar_inap.ttl_biaya,
    	kamar_inap.stts_pulang,
    	bangsal.nm_bangsal,
    	reg_periksa.no_rkm_medis,
    	reg_periksa.umurdaftar,
    	reg_periksa.sttsumur,
    	pasien.nm_pasien,
    	IF(pasien.jk="L","Laki-laki","Perempuan") as jk,
    	pasien.tgl_lahir
	';

	private $tbjoin = array(
		'kamar' => array(
			'metode' => 'INNER',
			'relasi' => 'kamar.kd_kamar=kamar_inap.kd_kamar'
		),
		'bangsal' => array(
			'metode' => 'INNER',
			'relasi' => 'bangsal.kd_bangsal=kamar.kd_bangsal'
		),
		'reg_periksa' => array(
			'metode' => 'INNER',
			'relasi' => 'reg_periksa.no_rawat=kamar_inap.no_rawat'
		),
		'pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','kamar_inap.tgl_masuk DESC',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

	public function getDataSearch($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countDataSearch($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}