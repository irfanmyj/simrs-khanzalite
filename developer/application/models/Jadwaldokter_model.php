<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwaldokter_model extends MY_Model{

	protected $_table_name = 'jadwal';
	protected $_primary_key = 'kd_dokter';
	protected $_order_by = '';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'kd_poli' => [
            'field' => 'kd_poli',
            'label' => 'Kode Poli',
            'rules' => 'trim|required'
		],
		'kd_dokter' => [
            'field' => 'kd_dokter',
            'label' => 'Kode Dokter',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	private $field = '
    	jadwal.kd_poli,
    	jadwal.kd_dokter,
    	jadwal.jam_mulai,
    	jadwal.jam_selesai,
    	jadwal.hari_kerja,
    	dokter.nm_dokter,
    	poliklinik.nm_poli
	';

	private $tbjoin = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=jadwal.kd_dokter'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=jadwal.kd_poli'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','hari_kerja',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

	private $field1 = '
    	jadwal.jam_mulai,
    	jadwal.jam_selesai,
    	jadwal.hari_kerja,
    	dokter.nm_dokter,
    	poliklinik.nm_poli
	';

	private $tbjoin1 = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=jadwal.kd_dokter'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=jadwal.kd_poli'
		)
	);

	public function getJadwal($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin1,$this->field1,$where,'','','',$limit,$offset)->result_array();
	}

}