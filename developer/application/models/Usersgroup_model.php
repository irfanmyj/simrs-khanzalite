<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usersgroup_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = '';
	protected $_database = '';

	public $rules = array(
		'user_id' => [
            'field' => 'user_id',
            'label' => 'User',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('user_group');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	user_group.id,
    	user_group.user_id,
    	user_group.group_id,
    	user_group.create_date,
    	user_group.is_active,
    	users.username,
    	category_group.name as nm_group
	';

	private $tbjoin = array(
		'users' => array(
			'metode' => 'INNER',
			'relasi' => 'users.id_user=user_group.user_id'
		),
		'category_group' => array(
			'metode' => 'INNER',
			'relasi' => 'category_group.id=user_group.group_id'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}