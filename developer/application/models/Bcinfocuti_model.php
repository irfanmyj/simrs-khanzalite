<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bcinfocuti_model extends MY_Model{

	protected $_table_name = 'bc_info_cuti';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'ASC';
	protected $_database = 'dbtwo';

	
	public function __construct(){
		parent::__construct();
	}

}