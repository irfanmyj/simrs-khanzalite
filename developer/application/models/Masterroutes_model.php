<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterroutes_model extends MY_Model{

	protected $_table_name = 'routes';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_database = '';

	public $rules = array(
		'controller' => [
            'field' => 'controller',
            'label' => 'Nama Kontroller',
            'rules' => 'trim|required'
		],
		'routes' => [
            'field' => 'routes',
            'label' => 'Nama Routes',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	/*
	Cara mengambil data tanpa join
	*/
	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

	/*
	Cara mengambil data dengan join
	*/

	/*private $field = '
    	brodcast.id,
    	dokter.nm_dokter,
    	poliklinik.nm_poli
	';

	private $tbjoin = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=brodcast.kd_dokter'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=brodcast.kd_poli'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}*/

}