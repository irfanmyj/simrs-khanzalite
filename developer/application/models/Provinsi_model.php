<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'kd_prop';
	protected $_order_by = 'kd_prop';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nm_prop' => [
            'field' => 'nm_prop',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('propinsi');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	propinsi.kd_prop,
    	propinsi.kd_prop_bpjs,
    	propinsi.nm_prop as nama,
    	propinsi.id_negara,
    	negara.nama as nm_negara
	';

	private $tbjoin = array(
		'negara' => array(
			'metode' => 'INNER',
			'relasi' => 'negara.id=propinsi.id_negara'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}