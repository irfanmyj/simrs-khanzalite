<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Identityfunctionary_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = '';
	protected $_database = '';

	public $rules = array(
		'id_user' => [
            'field' => 'id_user',
            'label' => 'id_user',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('identity_functionary');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	identity_functionary.id,
    	identity_functionary.id_user,
    	identity_functionary.category,
    	identity_functionary.created_at,
    	detail_users.full_name
	';

	private $tbjoin = array(
		'users' => array(
			'metode' => 'INNER',
			'relasi' => 'users.id_user=identity_functionary.id_user'
		),
		'detail_users' => array(
			'metode' => 'INNER',
			'relasi' => 'detail_users.id_detail_user=users.id_user'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}