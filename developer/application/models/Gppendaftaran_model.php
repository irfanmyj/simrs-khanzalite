<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gppendaftaran_model extends MY_Model{

	protected $_table_name = 'reg_periksa';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = 'tgl_registrasi';
	protected $_order_by_type = 'DESC';
	protected $_database = 'dbtwo';
	private   $db2;

	var $table = 'reg_periksa'; //nama tabel dari database
	var $column_order = array('reg_periksa.tgl_registrasi','reg_periksa.jam_reg','reg_periksa.kd_dokter','reg_periksa.kd_poli','reg_periksa.no_reg'); //field yang ada di table user
	var $column_search = array('reg_periksa.no_rkm_medis','reg_periksa.tgl_registrasi','pasien.nm_pasien','dokter.nm_dokter','poliklinik.nm_poli'); //field yang diizin untuk pencarian 
	var $order = array('reg_periksa.tgl_registrasi' => 'desc'); // default order 

	public $rules = array(
		'nik' => [
            'field' => 'nik',
            'label' => 'Nik',
            'rules' => 'trim|required'
		]
	);

	public $rules_upsep = array(
		'no_sep' => [
            'field' => 'no_sep',
            'label' => 'No SEP',
            'rules' => 'trim|required'
		]
	);

	public $rules_reg = array(
		'no_rkm_medis' => [
            'field' => 'no_rkm_medis',
            'label' => 'No RM',
            'rules' => 'trim|required'
		],
		'tanggal_periksa' => [
            'field' => 'tanggal_periksa',
            'label' => 'Tanggal Periksa',
            'rules' => 'trim|required'
		],
		'kd_dokter' => [
            'field' => 'kd_dokter',
            'label' => 'Kode Dokter',
            'rules' => 'trim|required'
		],
		'kd_poli' => [
            'field' => 'kd_poli',
            'label' => 'Kode Poli',
            'rules' => 'trim|required'
		],
		'stts_daftar' => [
            'field' => 'stts_daftar',
            'label' => 'Status Daftar',
            'rules' => 'trim|required'
		]
	);

	public $rules_regigd = array(
		'no_rkm_medis' => [
            'field' => 'no_rkm_medis',
            'label' => 'No RM',
            'rules' => 'trim|required'
		],
		'tanggal_periksa' => [
            'field' => 'tanggal_periksa',
            'label' => 'Tanggal Periksa',
            'rules' => 'trim|required'
		],
		'kd_dokter' => [
            'field' => 'kd_dokter',
            'label' => 'Kode Dokter',
            'rules' => 'trim|required'
		],
		'stts_daftar' => [
            'field' => 'stts_daftar',
            'label' => 'Status Daftar',
            'rules' => 'trim|required'
		]
	);

	public $rules_trf_inap = array(
		'no_rawat' => [
            'field' => 'no_rawat',
            'label' => 'No Rawat',
            'rules' => 'trim|required'
		],
		'trf_kamar' => [
            'field' => 'trf_kamar',
            'label' => 'Harga Kamar',
            'rules' => 'trim|required'
		],
		'kd_kamar' => [
            'field' => 'kd_kamar',
            'label' => 'Kode Kamar',
            'rules' => 'trim|required'
		]
	);

	public $rules_sep = array(
		'no_rkm_medis' => [
            'field' => 'no_rkm_medis',
            'label' => 'No RM',
            'rules' => 'trim|required'
		],
		'no_rawat' => [
            'field' => 'no_rawat',
            'label' => 'No Rawat',
            'rules' => 'trim|required'
		],
		'nm_pasien' => [
            'field' => 'nm_pasien',
            'label' => 'Nama Pasien',
            'rules' => 'trim|required'
		],
		'no_peserta' => [
            'field' => 'no_peserta',
            'label' => 'No Peserta',
            'rules' => 'trim|required'
		],
		'no_rujukan' => [
            'field' => 'no_rujukan',
            'label' => 'No Rujukan',
            'rules' => 'trim|required'
		]
	);

	private $field = '
    	reg_periksa.no_reg,
    	reg_periksa.no_rawat,
    	reg_periksa.no_rkm_medis,
    	reg_periksa.kd_dokter,
    	reg_periksa.kd_poli,
    	reg_periksa.kd_pj,
    	reg_periksa.stts,
    	reg_periksa.tgl_registrasi,
    	reg_periksa.hubunganpj,
    	reg_periksa.p_jawab,
    	reg_periksa.almt_pj,
    	reg_periksa.stts_daftar,
    	reg_periksa.jam_reg,
    	dokter.nm_dokter,
    	pasien.nm_pasien,
    	pasien.tgl_lahir,
    	pasien.no_peserta,
    	pasien.pekerjaan,
    	IF(pasien.jk="L","Laki-laki","Perempuan") as jk,
    	pasien.jk as kode_jk,
    	poliklinik.nm_poli,
    	penjab.png_jawab,
    	bridging_sep.no_sep
	';

	private $tbjoin = [
		'dokter' => [
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=reg_periksa.kd_dokter'
		],
		'pasien' => [
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		],
		'poliklinik' => [
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli'
		],
		'penjab' => [
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
		],
		'bridging_sep' => [
			'metode' => 'LEFT',
			'relasi' => 'bridging_sep.no_rawat=reg_periksa.no_rawat'
		]
	];

	public function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database($this->_database,TRUE);
	}

	public function getData($where='',$limit='',$offset='')
	{
		if($where){
			return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
		}else{
			return $this->getJoin('',$this->tbjoin,$this->field,'','','','',$limit,$offset)->result();
		}
		
	}

	public function countData($where='')
	{
		if($where){
			return $this->countJoin('',$this->tbjoin,$where);
		}else{
			return $this->countJoin('',$this->tbjoin,'');
		}
	}

	/*private $field_search = '
		reg_periksa.no_reg,
    	reg_periksa.no_rawat,
    	reg_periksa.no_rkm_medis,
    	reg_periksa.kd_dokter,
    	reg_periksa.kd_poli,
    	reg_periksa.kd_pj,
    	reg_periksa.stts,
    	reg_periksa.tgl_registrasi,
    	reg_periksa.hubunganpj,
    	reg_periksa.p_jawab,
    	reg_periksa.almt_pj,
    	reg_periksa.stts_daftar,
    	reg_periksa.jam_reg,
    	dokter.nm_dokter,
    	pasien.nm_pasien,
    	pasien.tgl_lahir,
    	pasien.no_peserta,
    	pasien.pekerjaan,
    	IF(pasien.jk="L","Laki-laki","Perempuan") as jk,
    	pasien.jk as kode_jk,
    	poliklinik.nm_poli,
    	penjab.png_jawab
    ';

	private $tbjoin_search = [
		'dokter' => [
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=reg_periksa.kd_dokter'
		],
		'pasien' => [
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		],
		'poliklinik' => [
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli'
		],
		'penjab' => [
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
		]
	];*/

	public function getDataSearch($where=[],$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','tgl_registrasi',$limit,$offset)->result();
	}

	public function countDataSearch($where=[])
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

	private function _get_datatables_query($where=''){
		$this->db2->from($this->table);
		$this->db2->join('pasien','pasien.no_rkm_medis=reg_periksa.no_rkm_medis');
		$this->db2->join('dokter','dokter.kd_dokter=reg_periksa.kd_dokter');
		$this->db2->join('poliklinik','poliklinik.kd_poli=reg_periksa.kd_poli');
		$this->db2->join('penjab','penjab.kd_pj=reg_periksa.kd_pj');

		$wheres = ['reg_periksa.tgl_registrasi'=>date('Y-m-d',strtotime('2019-11-25'))];

		if($where){
			$wheres = $where;
		}

		$this->db2->where($wheres);
		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db2->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db2->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db2->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db2->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db2->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db2->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		
		if($_POST['length'] != -1)
		$this->db2->limit($_POST['length'], $_POST['start']);
		$query = $this->db2->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db2->get();
		return $query->num_rows();
	}

	public function count_all() { 
		$this->_get_datatables_query();
		return $this->db2->count_all_results(); 
	}
}