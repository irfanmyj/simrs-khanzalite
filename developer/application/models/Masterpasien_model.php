<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterpasien_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'no_rkm_medis';
	protected $_order_by = 'tgl_daftar';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nm_pasien' => [
            'field' => 'nm_pasien',
            'label' => 'Nama Pasien',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('pasien');
	}

	/*
	Cara mengambil data tanpa join
	*/
	/*public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}*/

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	pasien.no_rkm_medis,
    	pasien.nm_pasien,
    	pasien.no_ktp,
    	pasien.jk,
    	pasien.tmp_lahir,
    	pasien.tgl_lahir,
    	pasien.nm_ibu,
    	pasien.alamat,
    	pasien.gol_darah,
    	pasien.pekerjaan,
    	pasien.stts_nikah,
    	pasien.agama,
    	pasien.tgl_daftar,
    	pasien.no_tlp,
    	pasien.umur,
    	pasien.keluarga,
    	pasien.namakeluarga,
    	pasien.kd_pj,
    	pasien.no_peserta,
    	pasien.kd_prop,
    	pasien.kd_kab,
    	pasien.kd_kec,
    	pasien.kd_kel,
    	pasien.email,
    	pasien.nip,
    	pasien.suku_bangsa,
    	pasien.cacat_fisik,
    	pasien.bahasa_pasien,
    	pasien.propinsipj,
    	pasien.kabupatenpj,
    	pasien.kecamatanpj,
    	pasien.kelurahanpj,
    	penjab.png_jawab,
    	propinsi.nm_prop,
    	kabupaten.nm_kab,
    	kecamatan.nm_kec,
    	kelurahan.nm_kel
	';

	private $tbjoin = array(
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=pasien.kd_pj'
		),
		'propinsi' => array(
			'metode' => 'INNER',
			'relasi' => 'propinsi.kd_prop=pasien.kd_prop'
		),
		'kabupaten' => array(
			'metode' => 'INNER',
			'relasi' => 'kabupaten.kd_kab=pasien.kd_kab'
		),
		'kecamatan' => array(
			'metode' => 'INNER',
			'relasi' => 'kecamatan.kd_kec=pasien.kd_kec'
		),
		'kelurahan' => array(
			'metode' => 'INNER',
			'relasi' => 'kelurahan.kd_kel=pasien.kd_kel'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','pasien.tgl_daftar DESC',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}