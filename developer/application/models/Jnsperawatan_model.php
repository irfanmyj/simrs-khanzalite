<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jnsperawatan_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'kd_jenis_prw';
	protected $_order_by = 'kd_jenis_prw';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nm_perawatan' => [
            'field' => 'nm_perawatan',
            'label' => 'Nama Perawatan',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('Jns_perawatan');
	}

	/*
	Cara mengambil data tanpa join
	*/
	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	jns_perawatan.*,
    	kategori_perawatan.nm_kategori,
    	poliklinik.nm_poli,
    	penjab.png_jawab
	';

	private $tbjoin = array(
		'kategori_perawatan' => array(
			'metode' => 'INNER',
			'relasi' => 'kategori_perawatan.kd_kategori=jns_perawatan.kd_kategori'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=jns_perawatan.kd_poli'
		),
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=jns_perawatan.kd_pj'
		)
	);

	public function getDataJoin($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

	private $field_search = '
    	jns_perawatan.*,
    	kategori_perawatan.nm_kategori,
    	poliklinik.nm_poli,
    	penjab.png_jawab
	';

	private $tbjoin_search = array(
		'kategori_perawatan' => array(
			'metode' => 'INNER',
			'relasi' => 'kategori_perawatan.kd_kategori=jns_perawatan.kd_kategori'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=jns_perawatan.kd_poli'
		),
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=jns_perawatan.kd_pj'
		)
	);

	public function getDataSearch($where=[],$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin_search,$this->field_search,$where,'','','jns_perawatan.kd_jenis_prw DESC',$limit,$offset)->result();
	}

	public function countDataSearch($where=[])
	{
		return $this->countJoin('',$this->tbjoin_search,$where);
	}

}