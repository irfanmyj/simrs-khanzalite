<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterprofesi_model extends MY_Model{

	protected $_table_name = 'professional';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = '';

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	private $field = '
    	professional.id,
    	professional.name,
    	professional.id_category_professional,
    	professional.created_at,
    	category_professional.name as category
	';

	private $tbjoin = array(
		'category_professional' => array(
			'metode' => 'INNER',
			'relasi' => 'category_professional.id=professional.id_category_professional'
		)
	);

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}
}