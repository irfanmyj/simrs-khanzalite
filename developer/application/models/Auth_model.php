<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends MY_Model{

	protected $_table_name = 'users';
	protected $_primary_key = 'id_user';
	protected $_order_by = 'id_user';
	protected $_order_by_type = 'ASC';

	public $rules = array(
		'username' => array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
		),
		'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
		)
	);

	public function __construct(){
		parent::__construct();
	}

	public function checking($where='')
	{
		return $this->get('','*',$where,'','','',1)->result();
	}

}