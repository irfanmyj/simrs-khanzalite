<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'kd_kel';
	protected $_order_by = 'kd_kel';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nm_kel' => [
            'field' => 'nm_kel',
            'label' => 'Nama Kelurahan',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('Kelurahan');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	kelurahan.kd_kel,
    	kelurahan.nm_kel as nama,
    	kelurahan.kd_kec,
    	kelurahan.id_jenis,
    	kecamatan.nm_kec as nm_kecamatan,
    	kabupaten.nm_kab as nm_kabupaten,
    	propinsi.nm_prop as nm_provinsi
	';

	private $tbjoin = array(
		'kecamatan' => array(
			'metode' => 'INNER',
			'relasi' => 'kecamatan.kd_kec=kelurahan.kd_kec'
		),
		'kabupaten' => array(
			'metode' => 'INNER',
			'relasi' => 'kabupaten.kd_kab=kecamatan.kd_kab'
		),
		'propinsi' => array(
			'metode' => 'INNER',
			'relasi' => 'propinsi.kd_prop=kabupaten.kd_prop'
		),
		'jenis_wilayah' => array(
			'metode' => 'INNER',
			'relasi' => 'jenis_wilayah.id_jenis=kelurahan.id_jenis'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}