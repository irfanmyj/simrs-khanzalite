<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends MY_Model{

	protected $_table_name = 'log_actvity_application';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

}