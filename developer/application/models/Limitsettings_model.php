<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class limitsettings_model extends MY_Model{

	protected $_table_name = 'limit_pasien_online';
	protected $_primary_key = 'id_limit_pasien';
	protected $_order_by = 'id_limit_pasien';
	protected $_order_by_type = 'DESC';
	protected $_database = 'dbtwo';

	public $rules = array(
		'kd_poli' => [
            'field' => 'kd_poli',
            'label' => 'Nama Poliklinik',
            'rules' => 'trim|required'
		],
		'kd_dokter' => [
            'field' => 'kd_dokter',
            'label' => 'Nama Dokter',
            'rules' => 'trim|required'
		],
		'hari_kerja' => [
            'field' => 'hari_kerja[]',
            'label' => 'Hari Kerja',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	private $field = '
    	limit_pasien_online.id_limit_pasien,
    	limit_pasien_online.kd_poli,
    	limit_pasien_online.kd_dokter,
    	limit_pasien_online.hari_kerja,
    	limit_pasien_online.limit_reg,
    	limit_pasien_online.limit_lansia,
    	limit_pasien_online.limit_kedatangan,
    	limit_pasien_online.waktu_praktek,
    	limit_pasien_online.umum,
    	limit_pasien_online.bpjs,
    	limit_pasien_online.masuk_tgl,
    	dokter.nm_dokter,
    	poliklinik.nm_poli
	';

	private $tbjoin = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=limit_pasien_online.kd_dokter'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=limit_pasien_online.kd_poli'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}