<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration_model extends MY_Model{

	protected $_table_name = 'configuration';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'ASC';

	public $rules = array(
		'id_config' => [
            'field' => 'id_config',
            'label' => 'Name',
            'rules' => 'trim|required'
		],
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

}