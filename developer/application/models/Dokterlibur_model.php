<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokterlibur_model extends MY_Model{

	protected $_table_name = 'dokter_libur';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_database = 'dbtwo';

	public function __construct(){
		parent::__construct();
	}

}