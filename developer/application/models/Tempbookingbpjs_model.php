<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tempbookingbpjs_model extends MY_Model{

	protected $_table_name = 'temp_booking_bpjs';
	protected $_primary_key = 'id_booking_bpjs';
	protected $_order_by = 'id_booking_bpjs';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';


	public function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database($this->_database,TRUE);
	}

	
}