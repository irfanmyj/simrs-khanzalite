<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_model extends MY_Model{

	protected $_table_name = 'pasien';
	protected $_primary_key = 'no_rkm_medis';
	protected $_order_by = 'no_rkm_medis';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nik' => [
            'field' => 'nik',
            'label' => 'Nik',
            'rules' => 'trim|required'
		],
		'nm_pasien' => [
            'field' => 'nm_pasien',
            'label' => 'Nama Lengkap',
            'rules' => 'trim|required'
		]
	);

	/*private $field = '
    	patient_data_master.nik,
    	patient_data_master.full_name,
    	patient_data_master.norm_medic,
    	patient_data_master.gender,
    	patient_data_master.date_of_birth,
    	patient_data_master.place_of_birth,
    	patient_data_master.created_at,
    	patient_data_master.language,
    	language.name as name_language,
	';

	private $tbjoin = array(
		'language' => array(
			'metode' => 'INNER',
			'relasi' => 'language.id=patient_data_master.language'
		)
	);*/

	public function __construct(){
		parent::__construct();
	}

	/*public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}*/

}