<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_model extends MY_Model{

	protected $_table_name = 'jadwal';
	protected $_primary_key = '';
	protected $_order_by = 'kd_dokter';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';
	
	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database($this->_database,TRUE);
	}

	private $field = '
		jadwal.kd_poli,
		poliklinik.nm_poli,
		jadwal.jam_mulai,
		jadwal.jam_selesai
	';

	private $tbjoin = array(
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=jadwal.kd_poli'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'jadwal.kd_poli','','jadwal.kd_poli ASC',$limit,$offset);
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

	private $field1 = '
		jadwal.kd_dokter,
		dokter.nm_dokter
	';

	private $tbjoin1 = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=jadwal.kd_dokter'
		)
	);


	public function getDokter($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin1,$this->field1,$where,'jadwal.kd_dokter','','jadwal.kd_poli ASC',$limit,$offset);
	}

}