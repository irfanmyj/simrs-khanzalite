<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends MY_Model{

	protected $_table_name = 'api';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'ASC';

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		],
		'base_url' => [
			'field' => 'base_url',
            'label' => 'Base URL',
            'rules' => 'trim|required'
		],
		'service_name' => [
			'field' => 'service_name',
            'label' => 'Service Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

}