<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nursingdiagnosis_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = '';
	protected $_database = '';

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('Nursing_diagnosis');
	}

	private $field = '
    	nursing_diagnosis.id,
    	nursing_diagnosis.name,
    	nursing_diagnosis.code,
    	nursing_diagnosis.created_at,
    	nursing_diagnosis.code_domain,
    	nursing_diagnosis.code_class,
    	nursing_diagnosis_domain.name as name_domain,
    	nursing_diagnosis_class.name as name_class
	';

	private $tbjoin = array(
		'nursing_diagnosis_class' => array(
			'metode' => 'INNER',
			'relasi' => 'nursing_diagnosis_class.code=nursing_diagnosis.code_class'
		),
		'nursing_diagnosis_domain' => array(
			'metode' => 'INNER',
			'relasi' => 'nursing_diagnosis_domain.code=nursing_diagnosis.code_domain'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}