<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ermpoliklinik_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = 'tgl_registrasi';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('reg_periksa');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	reg_periksa.no_reg,
    	reg_periksa.no_rkm_medis,
    	reg_periksa.no_rawat,
    	reg_periksa.tgl_registrasi,
    	reg_periksa.kd_poli,
    	reg_periksa.kd_dokter,
    	reg_periksa.kd_pj,
    	reg_periksa.stts,
    	dokter.nm_dokter,
    	pasien.nm_pasien,
    	pasien.tgl_lahir,
    	pasien.jk,
    	poliklinik.nm_poli,
    	penjab.png_jawab
	';

	private $tbjoin = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=reg_periksa.kd_dokter'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=reg_periksa.kd_poli'
		),
		'pasien' => array(
			'metode' => 'INNER',
			'relasi' => 'pasien.no_rkm_medis=reg_periksa.no_rkm_medis'
		),
		'penjab' => array(
			'metode' => 'INNER',
			'relasi' => 'penjab.kd_pj=reg_periksa.kd_pj'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}