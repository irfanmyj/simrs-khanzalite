<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userdetail_model extends MY_Model{

	protected $_table_name = 'detail_users';
	protected $_primary_key = 'id_detail_user';
	protected $_order_by = 'id_detail_user';
	protected $_order_by_type = '';

	public $rules = array(
		'username' => [
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
		],
		'password' => [
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
		],
		'level_access' => [
            'field' => 'level_access',
            'label' => 'Level Akses',
            'rules' => 'trim|required'
		],
		'is_active' => [
            'field' => 'is_active',
            'label' => 'Status Aktiv',
            'rules' => 'trim|required'
		]
	);

	/*private $field = '
    	users.id_user,
    	users.username,
    	users.level_access,
    	users.is_active,
    	users.created_at,
    	level_access.name as access
	';

	private $tbjoin = array(
		'level_access' => array(
			'metode' => 'INNER',
			'relasi' => 'level_access.id=users.level_access'
		)
	);*/

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('',$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}