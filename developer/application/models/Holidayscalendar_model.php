<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Holidayscalendar_model extends MY_Model{

	protected $_table_name = 'libur_nasional';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_database = 'dbtwo';

	public $rules = array(
		'judul' => [
            'field' => 'judul',
            'label' => 'Judul',
            'rules' => 'trim|required'
		],
		'tanggal' => [
            'field' => 'tanggal',
            'label' => 'Tanggal',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

}