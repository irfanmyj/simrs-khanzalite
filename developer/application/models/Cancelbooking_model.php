<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cancelbooking_model extends MY_Model{

	protected $_table_name = 'brodcast';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'ASC';
	protected $_database = 'dbtwo';

	public $rules = array(
		'kd_poli' => [
            'field' => 'kd_poli',
            'label' => 'Kode Poli',
            'rules' => 'trim|required'
		],
		'kd_dokter' => [
            'field' => 'kd_dokter',
            'label' => 'Kode Dokter',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	private $field = '
    	brodcast.id,
    	brodcast.kd_poli,
    	brodcast.kd_dokter,
    	brodcast.tgl_periksa,
    	brodcast.judul,
    	brodcast.keterangan,
    	brodcast.kategori,
    	brodcast.masuk_tgl,
    	brodcast.masuk_oleh,
    	brodcast.status,
    	brodcast.waktu_masuk,
    	dokter.nm_dokter,
    	poliklinik.nm_poli
	';

	private $tbjoin = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=brodcast.kd_dokter'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=brodcast.kd_poli'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}