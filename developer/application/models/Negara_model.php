<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Negara_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nama' => [
            'field' => 'nama',
            'label' => 'nama',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('negara');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	negara.id,
    	negara.nama,
    	negara.id_jenis,
    	jenis_wilayah.nama as nm_jeniswilayah
	';

	private $tbjoin = array(
		'jenis_wilayah' => array(
			'metode' => 'INNER',
			'relasi' => 'jenis_wilayah.id_jenis=negara.id_jenis'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}