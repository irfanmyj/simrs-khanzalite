<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modules_model extends MY_Model{

	protected $_table_name = 'modules';
	protected $_primary_key = 'module_id';
	protected $_order_by = '';
	protected $_order_by_type = '';

	public $rules = array(
		'name_module' => [
            'field' => 'name_module',
            'label' => 'Nama Module',
            'rules' => 'trim|required'
		],
		'link' => [
            'field' => 'link',
            'label' => 'Link',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','position ASC',$limit,$offset)->result();
	}

}