<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter_model extends MY_Model{

	protected $_table_name = 'dokter';
	protected $_primary_key = 'kd_dokter';
	protected $_order_by = 'kd_dokter';
	protected $_order_by_type = 'DESC';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nm_dokter' => [
            'field' => 'nm_dokter',
            'label' => 'Nama Dokter',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	private $field = '
    	jadwal_libur.id,
    	jadwal_libur.id_bc_info_cuti,
    	jadwal_libur.kd_dokter,
    	jadwal_libur.tgl_awal,
    	jadwal_libur.tgl_akhir,
    	jadwal_libur.created_at,
    	dokter.nm_dokter
	';

	private $tbjoin = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=jadwal_libur.kd_dokter'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}