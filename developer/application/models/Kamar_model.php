<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = '';
	protected $_order_by = '';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('kamar');
	}

	/*
	Cara mengambil data tanpa join
	*/
	/*public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}*/

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	kamar.kd_kamar,
    	kamar.kd_bangsal,
    	kamar.trf_kamar,
    	kamar.status,
    	kamar.kelas,
    	kamar.statusdata,
    	bangsal.kd_bangsal,
    	bangsal.nm_bangsal
	';

	private $tbjoin = array(
		'bangsal' => array(
			'metode' => 'INNER',
			'relasi' => 'bangsal.kd_bangsal=kamar.kd_bangsal'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}