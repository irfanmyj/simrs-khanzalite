<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends MY_Model{

	protected $_table_name = 'reg_periksa';
	protected $_primary_key = 'no_rawat';
	protected $_order_by = '';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';
	private $db2; 

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		],
		'base_url' => [
			'field' => 'base_url',
            'label' => 'Base URL',
            'rules' => 'trim|required'
		],
		'service_name' => [
			'field' => 'service_name',
            'label' => 'Service Name',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->db2 = $this->load->database($this->_database,TRUE);
	}

	public function getData($where='',$limit='',$offset='')
	{
		$this->db2->select('*');
		$this->db2->where($where);
		$this->db2->group_by('');
		$this->db2->order_by('jam_reg','ASC');
		$this->db2->limit($limit);
		$this->db2->offset($offset);
		$res = $this->db2->get($this->_table_name)->result();
		return $res;
	}

	public function getVisitorYear($where=''){
		
		$wheres = ['YEAR(tgl_registrasi)'=>date('Y')];
		if($where){
			$wheres = $where;
		}

		$this->db2->select('count(no_rawat) as visitor,MONTH(tgl_registrasi) as bulan, YEAR(tgl_registrasi) as tahun');
		$this->db2->where($wheres);
		$this->db2->group_by('MONTH(tgl_registrasi),YEAR(tgl_registrasi)');
		$this->db2->order_by('tgl_registrasi',$this->_order_by_type);
		$res = $this->db2->get($this->_table_name);
		return $res;
	}

	public function getVisitorDay(){
		$this->db2->select('count(no_rawat) as visitor');
		$this->db2->where(['tgl_registrasi'=>date('Y-m-d'),'status_lanjut'=>'Ralan']);
		$res = $this->db2->get($this->_table_name)->result();
		return $res;
	}

}