<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ward_model extends MY_Model{

	protected $_table_name = 'ward';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'ASC';

	public $rules = array(
		'name' => [
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required'
		],
		'status' => [
			'field' => 'status',
            'label' => 'Status',
            'rules' => 'trim|required|numeric'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

}