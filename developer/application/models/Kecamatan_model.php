<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'kd_kec';
	protected $_order_by = 'kd_kec';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nm_kec' => [
            'field' => 'nm_kec',
            'label' => 'Nama Kecamatan',
            'rules' => 'trim|required'
		],
		'kd_kab' => [
            'field' => 'kd_kab',
            'label' => 'Nama Kabupaten',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('Kecamatan');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	kecamatan.kd_kec,
    	kecamatan.nm_kec as nama,
    	kecamatan.kd_kab,
    	propinsi.nm_prop as nm_provinsi,
    	kabupaten.nm_kab as nm_kabupaten
	';

	private $tbjoin = array(
		'kabupaten' => array(
			'metode' => 'INNER',
			'relasi' => 'kabupaten.kd_kab=kecamatan.kd_kab'
		),
		'propinsi' => array(
			'metode' => 'INNER',
			'relasi' => 'propinsi.kd_prop=kabupaten.kd_prop'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

	public function getSearch($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset);
	}

}