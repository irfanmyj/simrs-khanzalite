<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detailusers_model extends MY_Model{

	protected $_table_name = 'detail_users';
	protected $_primary_key = 'id_detail_user';
	protected $_order_by = 'id_detail_user';
	protected $_order_by_type = 'ASC';

	private $field = '
    	detail_users.id_detail_user,
    	detail_users.id_users,
    	detail_users.id_position,
    	detail_users.professional,
    	detail_users.title_first,
    	detail_users.title_last,
    	detail_users.full_name,
    	detail_users.nickname,
    	detail_users.gender,
    	detail_users.religion,
    	detail_users.place_of_birth,
    	detail_users.birthday,
    	detail_users.blood_type,
    	detail_users.marital_status,
    	detail_users.email,
    	detail_users.telp,
    	detail_users.mobile1,
    	detail_users.mobile2,
    	detail_users.upload_image,
    	detail_users.size_upload,
    	detail_users.state as idstate,
    	detail_users.province as idprovince,
    	detail_users.regencies as idregencies,
    	detail_users.districts as iddistricts,
    	detail_users.villages as idvillages,
    	detail_users.address,
    	detail_users.created_at,
    	state.name as state,
    	provinces.name as provinces,
    	regencies.name as regencies,
    	regencies.name as regencies,
    	districts.name as districts,
    	villages.name as villages
	';

	private $tbjoin = [
		'state' => [
			'metode' => 'LEFT',
			'relasi' => 'state.id=detail_users.state'
		],
		'provinces' => [
			'metode' => 'LEFT',
			'relasi' => 'provinces.id=detail_users.province'
		],
		'regencies' => [
			'metode' => 'LEFT',
			'relasi' => 'regencies.id=detail_users.regencies'
		],
		'districts' => [
			'metode' => 'LEFT',
			'relasi' => 'districts.id=detail_users.districts'
		],
		'villages' => [
			'metode' => 'LEFT',
			'relasi' => 'villages.id=detail_users.villages'
		]
	];

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

}