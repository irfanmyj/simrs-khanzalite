<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logactvityapplication_model extends MY_Model{

	protected $_table_name = 'log_actvity_application';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';
	protected $_order_by_type = 'ASC';
	protected $_database = '';

	public $rules = array(
		'id_user' => [
            'field' => 'id_user',
            'label' => 'ID User',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	/*
	Cara mengambil data tanpa join
	*/
	/*public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}*/

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	log_actvity_application.id,
    	log_actvity_application.user_id,
    	log_actvity_application.controller,
    	log_actvity_application.method,
    	log_actvity_application.description,
    	log_actvity_application.created_at,
    	users.username
	';

	private $tbjoin = array(
		'users' => array(
			'metode' => 'INNER',
			'relasi' => 'users.id_user=log_actvity_application.user_id'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}