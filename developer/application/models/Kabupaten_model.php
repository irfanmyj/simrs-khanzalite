<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten_model extends MY_Model{

	protected $_table_name = '';
	protected $_primary_key = 'kd_kab';
	protected $_order_by = 'kd_kab';
	protected $_order_by_type = '';
	protected $_database = 'dbtwo';

	public $rules = array(
		'nm_kab' => [
            'field' => 'nm_kab',
            'label' => 'Nama',
            'rules' => 'trim|required'
		],
		'kd_prop' => [
            'field' => 'kd_prop',
            'label' => 'Provinsi',
            'rules' => 'trim|required'
		],
		'id_jenis' => [
            'field' => 'id_jenis',
            'label' => 'Jenis Wilayah',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
		$this->_table_name = strtolower('kabupaten');
	}

	/*
	Cara mengambil data dengan join
	*/

	private $field = '
    	kabupaten.kd_kab,
    	kabupaten.kd_kab_bpjs,
    	kabupaten.nm_kab as nama,
    	kabupaten.kd_prop,
    	kabupaten.id_jenis,
    	propinsi.nm_prop as nm_provinsi,
    	jenis_wilayah.nama as nm_jeniswilayah
	';

	private $tbjoin = array(
		'propinsi' => array(
			'metode' => 'INNER',
			'relasi' => 'propinsi.kd_prop=kabupaten.kd_prop'
		),
		'jenis_wilayah' => array(
			'metode' => 'INNER',
			'relasi' => 'jenis_wilayah.id_jenis=kabupaten.id_jenis'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

}