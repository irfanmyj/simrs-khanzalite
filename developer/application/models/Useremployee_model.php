<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Useremployee_model extends MY_Model{

	protected $_table_name = 'users';
	protected $_primary_key = 'id_user';
	protected $_order_by = 'id_user';
	protected $_order_by_type = '';

	public $rules = array(
		'username' => [
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
		],
		'password' => [
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
		],
		'level_access' => [
            'field' => 'level_access',
            'label' => 'Level Akses',
            'rules' => 'trim|required'
		],
		'is_active' => [
            'field' => 'is_active',
            'label' => 'Status Aktiv',
            'rules' => 'trim|required'
		]
	);

	public $rules1 = array(
		'username' => [
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
		],
		'level_access' => [
            'field' => 'level_access',
            'label' => 'Level Akses',
            'rules' => 'trim|required'
		],
		'is_active' => [
            'field' => 'is_active',
            'label' => 'Status Aktiv',
            'rules' => 'trim|required'
		]
	);

	private $field = '
    	users.id_user,
    	users.username,
    	users.level_access,
    	users.is_active,
    	users.created_at,
    	users.menu_utama,
    	users.menu_report,
    	users.menu_master,
    	users.code_doctor,
    	level_access.name as access
	';

	private $tbjoin = array(
		'level_access' => array(
			'metode' => 'INNER',
			'relasi' => 'level_access.id=users.level_access'
		)
	);

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}

	public function getDoctors($where=''){
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','','','');
	}
}