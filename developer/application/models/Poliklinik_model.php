<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poliklinik_model extends MY_Model{

	protected $_table_name = 'poliklinik';
	protected $_primary_key = 'kd_poli';
	protected $_order_by = 'kd_poli';
	protected $_order_by_type = 'DESC';
	protected $_database = 'dbtwo';

	public $rules = array(
		'kd_poli' => [
            'field' => 'kd_poli',
            'label' => 'Kode Poli',
            'rules' => 'trim|required'
		],
		'nm_poli' => [
            'field' => 'nm_poli',
            'label' => 'Nama Poli',
            'rules' => 'trim|required'
		]
	);

	public $rules_update = array(
		'nm_poli' => [
            'field' => 'nm_poli',
            'label' => 'Nama Poli',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	/*
	Cara mengambil data tanpa join
	*/
	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

	/*
	Cara mengambil data dengan join
	*/

	/*private $field = '
    	brodcast.id,
    	dokter.nm_dokter,
    	poliklinik.nm_poli
	';

	private $tbjoin = array(
		'dokter' => array(
			'metode' => 'INNER',
			'relasi' => 'dokter.kd_dokter=brodcast.kd_dokter'
		),
		'poliklinik' => array(
			'metode' => 'INNER',
			'relasi' => 'poliklinik.kd_poli=brodcast.kd_poli'
		)
	);

	public function getData($where='',$limit='',$offset='')
	{
		return $this->getJoin('',$this->tbjoin,$this->field,$where,'','','',$limit,$offset)->result();
	}

	public function countData($where='')
	{
		return $this->countJoin('',$this->tbjoin,$where);
	}*/

}