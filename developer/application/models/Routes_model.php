<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes_model extends MY_Model{

	protected $_table_name = 'routes';
	protected $_primary_key = 'id';
	protected $_order_by = 'position';
	protected $_order_by_type = 'ASC';

	public $rules = array(
		'controller' => [
            'field' => 'controller',
            'label' => 'Nama Controller',
            'rules' => 'trim|required'
		]
	);

	public function __construct(){
		parent::__construct();
	}

	public function getData($where='',$limit='',$offset='')
	{
		return $this->get('','*',$where,'','','',$limit,$offset)->result();
	}

}