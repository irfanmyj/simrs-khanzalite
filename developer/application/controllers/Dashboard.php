<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Dashboard_model'));
		$this->site->is_logged_in();
	}
	
	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = str_replace(' ', '',strtolower($this->session->userdata('name_levelaccess')));

	  switch ($method) {
	    case null;
	    case false;
	    case 'superadministrator':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'dokterpoliklinik':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->dokterpoliklinik();
	      	break;
	    case 'dokterigd':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->DokterIGD();
	      	break;
	    case 'dokterrawatinap':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->DokterRawatinap();
	      	break;
	    case 'perawatrawatinap':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->PerawatRawatinap();
	      	break;
	    default:
	      show_404();
	      break;
	  }
	}

	public function index()
	{	
		$segs 		= $this->_segs;
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;

		$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		$file = $this->_cf->dashbord_files[$this->session->userdata('level_access')];

		/*
		Ambil data pengunjungan berdasarkan tahun saat ini.
		*/
		$visitor = [];
		$res_visitor = $this->Dashboard_model->getVisitorYear()->result();
		if($res_visitor){
			foreach ($res_visitor as $k => $v) {
				$visitor[] = $v->visitor;
			}
		}
		

		/*
		Ambil data pengunjungn berdasakan hari sekarang.
		*/
		$res_visitor_month = $this->Dashboard_model->getVisitorDay();

		/*
		Ambil data pengunjun pasien yang terdaftar hari ini.
		*/
		$pasien_aktiv = $this->Dashboard_model->getData(['tgl_registrasi'=>date('Y-m-d')],$limit,$limit_start);

		$datas = array(
			'title_table1' => 'Data Pengunjung Pasien Hari ini.',
			'file' => $file,
			'visitor' => $visitor,
			'visitor_month' => $res_visitor_month,
			'pasien_aktiv' => $pasien_aktiv,
			'folder' => ucwords($segs[1])
		);
		$this->site->view('inc',$datas);
	}

	public function dokterpoliklinik(){
		$segs 		= $this->_segs;
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;

		$file = $this->_cf->dashbord_files[$this->session->userdata('level_access')];

		/*
		Ambil data pengunjungan berdasarkan tahun saat ini.
		*/
		$visitor = [];
		$res_visitor = $this->Dashboard_model->getVisitorYear()->result();
		if($res_visitor){
			foreach ($res_visitor as $k => $v) {
				$visitor[] = $v->visitor;
			}
		}
		

		/*
		Ambil data pengunjungn berdasakan hari sekarang.
		*/
		$res_visitor_month = $this->Dashboard_model->getVisitorDay();

		/*
		Ambil data pengunjun pasien yang terdaftar hari ini.
		*/
		$pasien_aktiv = $this->Dashboard_model->getData(['tgl_registrasi'=>date('Y-m-d')],$limit,$limit_start);

		$datas = array(
			'title_table1' => 'Data Pengunjung Pasien Hari ini.',
			'visitor' => $visitor,
			'visitor_month' => $res_visitor_month,
			'pasien_aktiv' => $pasien_aktiv,
			'file' => $file,
			'folder' => ucwords($segs[1])
		);
		$this->site->view('inc',$datas);
	}

	public function DokterIGD(){
		$segs 		= $this->_segs;
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;

		$file = $this->_cf->dashbord_files[$this->session->userdata('level_access')];

		/*
		Ambil data pengunjungan berdasarkan tahun saat ini.
		*/
		$visitor = [];
		$res_visitor = $this->Dashboard_model->getVisitorYear()->result();
		if($res_visitor){
			foreach ($res_visitor as $k => $v) {
				$visitor[] = $v->visitor;
			}
		}
		

		/*
		Ambil data pengunjungn berdasakan hari sekarang.
		*/
		$res_visitor_month = $this->Dashboard_model->getVisitorDay();

		/*
		Ambil data pengunjun pasien yang terdaftar hari ini.
		*/
		$pasien_aktiv = $this->Dashboard_model->getData(['tgl_registrasi'=>date('Y-m-d')],$limit,$limit_start);

		$datas = array(
			'title_table1' => 'Data Pengunjung Pasien Hari ini.',
			'visitor' => $visitor,
			'visitor_month' => $res_visitor_month,
			'pasien_aktiv' => $pasien_aktiv,
			'file' => $file,
			'folder' => ucwords($segs[1])
		);
		$this->site->view('inc',$datas);
	}

	public function DokterRawatinap(){
		$segs 		= $this->_segs;
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;

		$file = $this->_cf->dashbord_files[$this->session->userdata('level_access')];

		/*
		Ambil data pengunjungan berdasarkan tahun saat ini.
		*/
		$visitor = [];
		$res_visitor = $this->Dashboard_model->getVisitorYear()->result();
		if($res_visitor){
			foreach ($res_visitor as $k => $v) {
				$visitor[] = $v->visitor;
			}
		}
		

		/*
		Ambil data pengunjungn berdasakan hari sekarang.
		*/
		$res_visitor_month = $this->Dashboard_model->getVisitorDay();

		/*
		Ambil data pengunjun pasien yang terdaftar hari ini.
		*/
		$pasien_aktiv = $this->Dashboard_model->getData(['tgl_registrasi'=>date('Y-m-d')],$limit,$limit_start);

		$datas = array(
			'title_table1' => 'Data Pengunjung Pasien Hari ini.',
			'visitor' => $visitor,
			'visitor_month' => $res_visitor_month,
			'pasien_aktiv' => $pasien_aktiv,
			'file' => $file,
			'folder' => ucwords($segs[1])
		);
		$this->site->view('inc',$datas);
	}

	public function PerawatRawatinap(){
		$segs 		= $this->_segs;
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;

		$file = $this->_cf->dashbord_files[$this->session->userdata('level_access')];

		/*
		Ambil data pengunjungan berdasarkan tahun saat ini.
		*/
		$visitor = [];
		$res_visitor = $this->Dashboard_model->getVisitorYear()->result();
		if($res_visitor){
			foreach ($res_visitor as $k => $v) {
				$visitor[] = $v->visitor;
			}
		}
		

		/*
		Ambil data pengunjungn berdasakan hari sekarang.
		*/
		$res_visitor_month = $this->Dashboard_model->getVisitorDay();

		/*
		Ambil data pengunjun pasien yang terdaftar hari ini.
		*/
		$pasien_aktiv = $this->Dashboard_model->getData(['tgl_registrasi'=>date('Y-m-d')],$limit,$limit_start);

		$datas = array(
			'title_table1' => 'Data Pengunjung Pasien Hari ini.',
			'visitor' => $visitor,
			'visitor_month' => $res_visitor_month,
			'pasien_aktiv' => $pasien_aktiv,
			'file' => $file,
			'folder' => ucwords($segs[1])
		);
		$this->site->view('inc',$datas);
	}
}
