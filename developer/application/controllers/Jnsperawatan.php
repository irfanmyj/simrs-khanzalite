<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jnsperawatan extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Jnsperawatan_model','Kategoriperawatan_model']);
		$this->site->is_logged_in();
	}

	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'getcode':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->getcode();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	    default:
	      show_404();
	      break;
	  }
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= '';

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where = 'jns_perawatan.nm_perawatan like \'%'.$search.'%\' OR poliklinik.nm_poli like \'%'.$search.'%\'  OR kategori_perawatan.nm_kategori like \'%'.$search.'%\'';
			}
		}

		$res_count	= $this->Jnsperawatan_model->countData($where);
		$res_data 	= $this->Jnsperawatan_model->getDataJoin($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		$res_kat = $this->Kategoriperawatan_model->get('','kd_kategori,nm_kategori')->result_array();
		$split_kat = splitRecursiveArray($res_kat,'kd_kategori','nm_kategori');
		
		unset($_SESSION['kd_poli']);
		unset($_SESSION['kd_kategori']);
		unset($_SESSION['kd_pj']);

		$data = array(
			'title' => 'Halaman Master Jenis Perawatan',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->status_field,
			'kategori' => $split_kat,
			'poliklinik' => splitRecursiveArray($this->functionother->get_poliklinik(),'kd_poli','nm_poli'),
			'penjab' => splitRecursiveArray($this->functionother->penjab(),'kd_pj','png_jawab'),
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	public function search(){
		$post = _post();
		$this->session->set_userdata($post);
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= [];

		if(!empty($post['kd_poli']) || !empty($_SESSION['kd_poli'])){
			$where['poliklinik.kd_poli'] = isset($post['kd_poli']) ? $post['kd_poli'] : $_SESSION['kd_poli'];
		}

		if(!empty($post['kd_kategori']) || !empty($_SESSION['kd_kategori'])){
			$where['kategori_perawatan.kd_kategori'] = isset($post['kd_kategori']) ? $post['kd_kategori'] : $_SESSION['kd_kategori'];
		}

		if(!empty($post['kd_pj']) || !empty($_SESSION['kd_pj'])){
			$where['penjab.kd_pj'] = isset($post['kd_pj']) ? $post['kd_pj'] : $_SESSION['kd_pj'];
		}

		$res_count	= $this->Jnsperawatan_model->countDataSearch($where);
		$res_data 	= $this->Jnsperawatan_model->getDataSearch($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination_search(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count,'search');

		$res_kat = $this->Kategoriperawatan_model->get('','kd_kategori,nm_kategori')->result_array();
		$split_kat = splitRecursiveArray($res_kat,'kd_kategori','nm_kategori');
		
		$data = array(
			'title' => 'Halaman Master Jenis Perawatan',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->status_field,
			'kategori' => $split_kat,
			'poliklinik' => splitRecursiveArray($this->functionother->get_poliklinik(),'kd_poli','nm_poli'),
			'penjab' => splitRecursiveArray($this->functionother->penjab(),'kd_pj','png_jawab'),
			'file' => 'index_search',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Jnsperawatan_model->rules;
		$this->form_validation->set_rules($rules);

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);

	        	if(!$this->Jnsperawatan_model->insert('',$post,FALSE))
	        	{
	        		throw new Exception('Data <b>berhasil</b> tersimpan.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> tersimpan.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    /*public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }*/

    public function update()
    {
        $post = $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        
        $rules 	= $this->Jnsperawatan_model->rules;
        $this->form_validation->set_rules($rules);
       	
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);

	        	if($this->Jnsperawatan_model->update('',$post,array('kd_jenis_prw' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);

        foreach ($explode as $key => $value) {

			if(!$this->Jnsperawatan_model->delete('',array('kd_jenis_prw' => $value)))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b>gagal</b> dihapus';
			}
			else
			{
				$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data <b>berhasil</b> dihapus.';
			}
        } 
    	
    	//echo base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

    public function download(){
    	$post = _post();
        if(!empty($post['kd_poli'])){
			$where['poliklinik.kd_poli'] = isset($post['kd_poli']) ? $post['kd_poli'] : '';
		}

		if(!empty($post['kd_kategori'])){
			$where['kategori_perawatan.kd_kategori'] = isset($post['kd_kategori']) ? $post['kd_kategori'] : '';
		}

		if(!empty($post['kd_pj'])){
			$where['penjab.kd_pj'] = isset($post['kd_pj']) ? $post['kd_pj'] : '';
		}

		$res_count	= $this->Jnsperawatan_model->countDataSearch($where);
		$res_data 	= $this->Jnsperawatan_model->getDataSearch($where);

		header('Content-Type: application/force-download');
		header("Content-Disposition: attachment; filename=pasien_booking".date('dmyHis').".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
    	$html = '<table class="table table-hover table-striped">';
    		$html .= '<thead class="l-blush text-white">';
    			$html .= '<tr>';
					$html .= '<th style="width: 30px;" class="text-center">No</th>';
					$html .= '<th style="width: 100px;">Kode</th>';
					$html .= '<th>Nama Perawatan</th>';
					$html .= '<th>Nama Kategori</th>';
					$html .= '<th>Poliklinik</th>';
					$html .= '<th>Cara Bayar</th>';
					$html .= '<th>Material</th>';
					$html .= '<th>BHP</th>';
					$html .= '<th>Tarif Tindakan Dr</th>';
					$html .= '<th>Tarif Tindakan Pr</th>';
					$html .= '<th>Kso</th>';
					$html .= '<th>Menejemen</th>';
					$html .= '<th>Total Bayar Dr</th>';
					$html .= '<th>Total Bayar Pr</th>';
					$html .= '<th>Total Bayar Dr/Pr</th>';
    			$html .= '</tr>';
    		$html .= '</thead>';
    		$html .= '<tbody>';
    		$no = 1;
    		foreach ($res_data as $k => $val) {
	    		$html .= '<tr>';
					$html .= '<td>'.($no++).'</td>';
					$html .= '<td>'.$val->kd_jenis_prw.'</td>';
					$html .= '<td>'.$val->nm_perawatan.'</td>';
					$html .= '<td>'.$val->nm_kategori.'</td>';
					$html .= '<td>'.$val->nm_poli.'</td>';
					$html .= '<td>'.$val->png_jawab.'</td>';
					$html .= '<td>'.$val->material.'</td>';
					$html .= '<td>'.$val->bhp.'</td>';
					$html .= '<td>'.$val->tarif_tindakandr.'</td>';
					$html .= '<td>'.$val->tarif_tindakanpr.'</td>';
					$html .= '<td>'.$val->kso.'</td>';
					$html .= '<td>'.$val->menejemen.'</td>';
					$html .= '<td>'.$val->total_byrdr.'</td>';
					$html .= '<td>'.$val->total_byrpr.'</td>';
					$html .= '<td>'.$val->total_byrdrpr.'</td>';
    			$html .= '</tr>';
	    	}
    		$html .= '</tbody>';
    	$html .= '</table>';

    	echo $html;
    }

    public function getcode(){
    	$data 	= $this->functionother->getCodeJnsperawatan()[0]['kd_jenis_prw'];
    	$res 	= substr($data,2) + 1;
    	echo 'RJ'.$res;
    }
}