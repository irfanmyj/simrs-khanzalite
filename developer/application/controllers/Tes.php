<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tes extends CI_Controller {

	private $refrensi;
	private $peserta;
	private $rujukan;
	private $sep;
	private $CI;

	private $_conf1	= [
		'cons_id'		=> 3362,
		'secret_key'	=> '1jY72ED457',
		'base_url' 		=> 'https://new-api.bpjs-kesehatan.go.id:8080',
		'service_name' 	=> 'new-vclaim-rest'
	];

	//'https://new-api.bpjs-kesehatan.go.id',
	private $_conf2 = [
	    'cons_id'       => 3362,
	    'secret_key'    => '1jY72ED457',
	    'base_url'      => 'https://dvlp.bpjs-kesehatan.go.id:8888',
	    'service_name'  => 'aplicaresws'
	];

	private $_conftes	= [
		'cons_id'		=> 3362,
		'secret_key'	=> '1jY72ED457',
		'base_url' 		=> 'https://dvlp.bpjs-kesehatan.go.id',
		'service_name' 	=> 'vclaim-rest'
	];

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('medic_helper');
		$this->load->library(['Functionother']);
		$this->CI =& get_instance();
		$this->refrensi = new Nsulistiyawan\Bpjs\VClaim\Referensi($this->CI->_conftes);
		$this->peserta = new Nsulistiyawan\Bpjs\VClaim\Peserta($this->CI->_conftes);
		$this->rujukan = new Nsulistiyawan\Bpjs\VClaim\Rujukan($this->CI->_conftes);
		$this->sep = new Nsulistiyawan\Bpjs\VClaim\Sep($this->CI->_conftes);
		$this->aplicare = new Nsulistiyawan\Bpjs\Aplicare\KetersediaanKamar($this->CI->_conf2);
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function enc($password='',$nilai='')
	{
		global $Cf;
		$res = $Cf->encrypt_aes_256_cbc($password);
		//$res = bCrypt($password,$nilai);
		echo $res;
	}

	public function norawat($tanggal=''){
		$res = $this->functionother->getnorawatlast(date('Y-m-d',strtotime($tanggal)));
		print_r($res);
	}

	public function datetime(){
		echo date_default_timezone_get();
		echo date('Y-m-d H:i:s');
	}

	public function propinsi(){
		$res = $this->refrensi->propinsi();
		echo '<pre>';
		print_r($res);
	}

	// Propinis 11 belum
	public function kabupaten($kdprop='11'){
		$res = $this->refrensi->kabupaten($kdprop);
		echo '<pre>';
		print_r($res);
	}

	public function cariSEP($nosep=''){
		$res = $this->sep->cariSEP($nosep);
		print_r($res);
	}
}