<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Useremployee extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Useremployee_model','Levelaccess_model','Userdetail_model','Kamar_model']);
		$this->site->is_logged_in();
	}

	/*
	Catatan : 
	1. formulir dokter rawatinap
	2. formulir perawat poli/ranap
	3. edit data
	*/

	public function _remap()
	{
		$segs = $this->_segs;
		$method = isset($segs[6]) ? $segs[6] : '';
		switch ($method) {
		  	case null;
		  	case false;
		  	case '':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		      	$this->index();
		      break;
		  	case 'search':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
		      	$this->search();
		      break;
		  	case 'store':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
		      	$this->store();
		      break;
		  	case 'update':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->update();
		      break;
		  	case 'edit':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->edit();
		      break;
		  	case 'destroy':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
		    	$this->destroy();
		    break;
		  	case 'print':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
		    	$this->print();
		    break;
		    case 'checking_username':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		    	$this->checking_username();
		    break;
		  default:
		    	show_404();
		    break;
		}
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['users.deleted_at' => NULL];

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where = 'users.username like \'%'.$search.'%\' OR level_access.name like \'%'.$search.'%\' and users.deleted_at IS NULL';
			}
		}

		$res_count	= $this->Useremployee_model->countData($where);
		$res_data 	= $this->Useremployee_model->getData($where,$limit,$limit_start);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		/*
		Data pendukung
		*/
		$rlevelaccess = $this->Levelaccess_model->get('','id,name',array('deleted_at'=>NULL),'','','id')->result_array();
		$access = splitRecursiveArray($rlevelaccess,'id','name');

		$res_kamar = $this->Kamar_model->getData(['bangsal.status'=>'1']);
		$room_ranap = datasToArray($res_kamar,'kd_kamar','kd_kamar','nm_bangsal');

		$data = array(
			'title' => 'Halaman Pendaftaran Karyawan',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'access' => $access,
			'pagination' => $pagination,
			'status' => $this->_cf->statusdata,
			'dokter' => splitRecursiveArray($this->functionother->get_dokter(),'kd_dokter','nm_dokter'),
			'poliklinik' => splitRecursiveArray($this->getPoli(),'kd_poli','nm_poli'),
			'ranap' => $room_ranap,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan form pengkajian pasien.
	* Menampilkan data riwayat kunjungan pasien.
	* Menampilkan 
	* @ second load method 
	*/
	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Useremployee_model->rules;
        $this->form_validation->set_rules($rules);
        $post['password'] = sha1($post['password'] .'-'. $post['username']);
        $search = $this->Useremployee_model->get('','username',['username'=>$post['username']])->result();

        if(!empty($post['id_room1'])){
        	$post['id_room1'] = implode(',', $post['id_room1']);
        }

        if(!empty($post['id_room2'])){
        	$post['id_room2'] = implode(',', $post['id_room2']);
        }

        if(!empty($post['code_doctor'])){
        	$post['code_doctor'] = $post['code_doctor'];
        }else{
        	$post['code_doctor'] = '-';
        }


        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);	        	
	        	if($search)
	        	{
	        		$error = 1;
	        		throw new Exception('Terdapat duplikasi data. Data ini <b>'. $search[0]->username .'</b> sudah tersedia di dalam tabel database.');
	        	}
	        	else
	        	{
	        		$res = $this->Useremployee_model->insert('',$post);
	        		if($res)
		        	{
		        		$data['id_users']= $res;
		        		$this->Userdetail_model->insert('',$data);
		        		throw new Exception('Data <b>berhasil</b> tersimpan.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> tersimpan.');
		        	}
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        $post['updated_at'] = date('Y-m-d H:i:s');
        $rules 	= $this->Useremployee_model->rules1;
        $this->form_validation->set_rules($rules);

        if(!empty($post['id_room1'])){
        	$post['id_room1'] = implode(',', $post['id_room1']);
        }

        if(!empty($post['id_room2'])){
        	$post['id_room2'] = implode(',', $post['id_room2']);
        }

        if(!empty($post['code_doctor'])){
        	$post['code_doctor'] = $post['code_doctor'];
        }else{
        	$post['code_doctor'] = '-';
        }

        if($post['password'])
        {
        	$post['password'] = sha1($post['password'].'-'.$post['username']);
        }
        else
        {
        	unset($post['password']);
        }
        
        $post['menu_utama'] = !empty($post['menu_utama']) ? $post['menu_utama'] : 'N';
        $post['menu_report'] = !empty($post['menu_report']) ? $post['menu_report'] : 'N';
        $post['menu_master'] = !empty($post['menu_master']) ? $post['menu_master'] : 'N';

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);
	        	if($this->Useremployee_model->update('',$post,array('id_user' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);
        //print_r($explode);
        foreach ($explode as $key => $value) {
        	if($this->Useremployee_model->update('',array('deleted_at'=>date('Y-m-d H:i:s')),array('id'=>$value))){
        		$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data telah berhasil dihapus';
        	}else{
        		$_SESSION['error'] = 1;
        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
        	} 	
        } 
    	//print_r($explode);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/'.$segs[3].'/'.$segs[4].'/1'));
    }

    public function checking_username()
    {
    	$post = _post();
    	$res = $this->Useremployee_model->get('','username',$post)->result_array();
    	
    	$error = '';
    	$msg = '';
    	if($res){
    		$error = 'Ada';
    		$msg = 'Mohon maaf username yang anda input sudah digunakan...';
    	}

    	echo json_encode(['error'=>$error,'msg'=>$msg]);	
    }

    private function getPoli(){
    	$res = $this->functionother->get_poliklinik();
    	return $res;
    }

}