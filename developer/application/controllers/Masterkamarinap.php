<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Masterkamarinap extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model([
			'Masterkamarinap_model',
			'Kamar_model'
		]);
		$this->site->is_logged_in();
	}

	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'updatelamainap':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->updatelamainap();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	   	case 'countTotalBiayaInap':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->countTotalBiayaInap();
	    	break;
	    default:
	      show_404();
	      break;
	  }
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['kamar_inap.stts_pulang'=>'-','kamar.status'=>'ISI'];

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where = 'pasien.nm_pasien like \'%'.$search.'%\' and kamar_inap.stts_pulang="-" and kamar.status="ISI"';
			}
		}

		$res_count	= $this->Masterkamarinap_model->countData($where);
		$res_data 	= $this->Masterkamarinap_model->getData($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		$status_pulang = ['Sehat'=>'Sehat','Rujuk'=>'Rujuk','APS'=>'APS','+'=>'+','Meninggal'=>'Meninggal','Sembuh'=>'Sembuh','Membaik'=>'Membaik','Pulang Paksa'=>'Pulang Paksa','-'=>'-','Pindah Kamar'=>'Pindah Kamar','Status Belum Lengkap'=>'Status Belum Lengkap'];

		$data = array(
			'title' => 'Halaman Master Pasien Rawatinap',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->statusdata,
			'kamar' => $this->_kamar(),
			'status_pulang' => $status_pulang,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	public function search(){
		$post = $this->input->post();
		$this->session->set_userdata($post);
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= [];

		if(!empty($post['kd_kamar']) || !empty($_SESSION['kd_kamar'])){
			$where['kamar_inap.kd_kamar'] = isset($post['kd_kamar']) ? $post['kd_kamar'] : $_SESSION['kd_kamar'];
		}

		if(!empty($post['stts_pulang']) || !empty($_SESSION['stts_pulang'])){
			$where['kamar_inap.stts_pulang'] = isset($post['stts_pulang']) ? $post['stts_pulang'] : $_SESSION['stts_pulang'];
		}

		if(!empty($post['cara_bayar']) || !empty($_SESSION['cara_bayar'])){
			$where['reg_periksa.kd_pj'] = isset($post['cara_bayar']) ? $post['cara_bayar'] : $_SESSION['cara_bayar'];
		}

		if(!empty($post['no_rkm_medis']) || !empty($_SESSION['no_rkm_medis'])){
			$where['reg_periksa.no_rkm_medis'] = isset($post['no_rkm_medis']) ? $post['no_rkm_medis'] : $_SESSION['no_rkm_medis'];
		}

		if(!empty($post['tgl_masuk_awal']) && !empty($post['tgl_masuk_akhir']) || !empty($_SESSION['tgl_masuk_awal']) && !empty($_SESSION['tgl_masuk_akhir'])){
			$where['kamar_inap.tgl_masuk >= '] = isset($post['tgl_masuk_awal']) ? date('Y-m-d',strtotime($post['tgl_masuk_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_masuk_awal']));	
			$where['kamar_inap.tgl_masuk <= '] = isset($post['tgl_masuk_akhir']) ? date('Y-m-d',strtotime($post['tgl_masuk_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_masuk_akhir']));	
		}else if(!empty($post['tgl_masuk_awal']) && empty($post['tgl_masuk_akhir'])){
			$where['kamar_inap.tgl_masuk'] = isset($post['tgl_masuk_awal']) ? date('Y-m-d',strtotime($post['tgl_masuk_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_masuk_awal']));	
		}else if(empty($post['tgl_masuk_awal']) && !empty($post['tgl_masuk_akhir'])){
			$where['kamar_inap.tgl_masuk'] = isset($post['tgl_masuk_akhir']) ? date('Y-m-d',strtotime($post['tgl_masuk_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_masuk_akhir']));
		}

		if(!empty($post['tgl_keluar_awal']) && !empty($post['tgl_keluar_akhir']) || !empty($_SESSION['tgl_keluar_awal']) && !empty($_SESSION['tgl_keluar_akhir'])){
			$where['kamar_inap.tgl_keluar >= '] = isset($post['tgl_keluar_awal']) ? date('Y-m-d',strtotime($post['tgl_keluar_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_keluar_awal']));	
			$where['kamar_inap.tgl_keluar <= '] = isset($post['tgl_keluar_akhir']) ? date('Y-m-d',strtotime($post['tgl_keluar_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_keluar_akhir']));	
		}else if(!empty($post['tgl_keluar_awal']) && empty($post['tgl_keluar_akhir'])){
			$where['kamar_inap.tgl_keluar'] = isset($post['tgl_keluar_awal']) ? date('Y-m-d',strtotime($post['tgl_keluar_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_keluar_awal']));	
		}else if(empty($post['tgl_keluar_awal']) && !empty($post['tgl_keluar_akhir'])){
			$where['kamar_inap.tgl_keluar'] = isset($post['tgl_keluar_akhir']) ? date('Y-m-d',strtotime($post['tgl_keluar_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_keluar_akhir']));
		}

		if(!empty($post['jk']) || !empty($_SESSION['jk'])){
			$where['b.jk'] = isset($post['jk']) ? $post['jk'] : $_SESSION['jk'];
		}


		$res_count	= $this->Masterkamarinap_model->countDataSearch($where);
		$res_data 	= $this->Masterkamarinap_model->getDataSearch($where,$limit,$limit_start);
		$url 		= base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination_search(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count,'search');

		$status_pulang = ['Sehat'=>'Sehat','Rujuk'=>'Rujuk','APS'=>'APS','+'=>'+','Meninggal'=>'Meninggal','Sembuh'=>'Sembuh','Membaik'=>'Membaik','Pulang Paksa'=>'Pulang Paksa','-'=>'-','Pindah Kamar'=>'Pindah Kamar','Status Belum Lengkap'=>'Status Belum Lengkap'];

		$data = array(
			'title' => 'Halaman Master Pasien Rawatinap',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->statusdata,
			'kamar' => $this->_kamar(),
			'status_pulang' => $status_pulang,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);

	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Masterkamarinap_model->rules;
		$this->form_validation->set_rules($rules);

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);

	        	if(!$this->Masterkamarinap_model->insert('',$post,FALSE))
	        	{
	        		throw new Exception('Data <b>berhasil</b> tersimpan.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> tersimpan.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
        $post = $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        
        $rules 	= $this->Masterkamarinap_model->rules;
        $this->form_validation->set_rules($rules);
       	
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);

	        	if($this->Masterkamarinap_model->update('',$post,array('id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function updatelamainap(){
    	$post = _post();
    	print_r($post);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);

        foreach ($explode as $key => $value) {

			if(!$this->Masterkamarinap_model->delete('',array('id' => $value)))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b>gagal</b> dihapus';
			}
			else
			{
				$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data <b>berhasil</b> dihapus.';
			}
        } 
    	
    	//echo base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

    private function _kamar(){
    	$res = $this->Kamar_model->getData();
    	$data = datasToArray($res,'kd_kamar','nm_bangsal');
    	return $data;
    }

    public function countTotalBiayaInap(){
    	$post = _post();

    	if($post){
    		$data['jumlah_hari'] = countDate(date('Y-m-d'),$post['tgl_masuk']);
    		$data['tarif'] = $post['trf_kamar'];
    		$data['total'] = ($data['tarif']*$data['jumlah_hari']);
    	}else{
    		$data['jumlah_hari'] = '';
    		$data['tarif'] = '';
    		$data['total'] = '';
    	}

    	echo json_encode($data);
    }
}