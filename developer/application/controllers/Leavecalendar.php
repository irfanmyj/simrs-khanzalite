<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Leavecalendar extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Leavecalendar_model','Dokter_model','Dokterlibur_model']); //jadwal_libur
		$this->site->is_logged_in();
	}

	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	    default:
	      show_404();
	      break;
	  }
	}
	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['deleted_at' => NULL];

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where = 'dokter.nm_dokter like \'%'.$search.'%\' OR jadwal_libur.tgl_awal like \'%'.date('Y-m-d',strtotime($search)).'%\'  OR jadwal_libur.tgl_akhir like \'%'.date('Y-m-d',strtotime($search)).'%\' and deleted_at IS NULL';
			}
		}

		$res_count	= $this->Leavecalendar_model->countData('',$where);
		$res_data 	= $this->Leavecalendar_model->getData($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);
		
		$res_dokter = $this->Dokter_model->get('','kd_dokter,nm_dokter',['status!='=>0])->result_array();
		$split_dokter = splitRecursiveArray($res_dokter,'kd_dokter','nm_dokter');

		/*echo '<pre>';
		print_r($res_dokter);
		exit();*/
		$data = array(
			'title' => 'Kalender Jadwal Cuti Dokter',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->statusdata,
			'dokter' => $split_dokter,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan form pengkajian pasien.
	* Menampilkan data riwayat kunjungan pasien.
	* Menampilkan 
	* @ second load method 
	*/

	/*public function create()
	{

	}*/

	public function store()
    {
        $post 	= _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Leavecalendar_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);
	        	unset($post['tglAwal']);
	        	unset($post['tglAkhir']);

	        	$tanggalAwal  = isset($post['tgl_awal']) ? $post['tgl_awal'] : '';
				$tanggalAkhir = isset($post['tgl_akhir']) ? $post['tgl_akhir'] : '';

				while (strtotime($tanggalAwal) <= strtotime($tanggalAkhir)) {
				    $datas[] = array(
				    	'kd_dokter' => $post['kd_dokter'],
				    	'tanggal' => $tanggalAwal
				    );
				    $tanggalAwal = date("Y-m-d", strtotime("+1 days", strtotime($tanggalAwal)));
				}

	        	if($this->Leavecalendar_model->insert('',$post))
	        	{
	        		$this->Dokterlibur_model->insert('',$datas,true);
	        		throw new Exception('Data <b>berhasil</b> tersimpan.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> tersimpan.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
        $post = _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        $post['updated_at'] = date('Y-m-d H:i:s');
        $rules 	= $this->Leavecalendar_model->rules;
        $this->form_validation->set_rules($rules);
        
        $tanggalAwal  = isset($post['tgl_awal']) ? $post['tgl_awal'] : '';
		$tanggalAkhir = isset($post['tgl_akhir']) ? $post['tgl_akhir'] : '';
		
		$wheredel = ['kd_dokter'=>$post['kd_dokter'],'date(tanggal)>='=>$post['tglAwal'],'date(tanggal) <='=>$post['tglAkhir']];
		
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);
	        	unset($post['tglAkhir']);
	        	unset($post['tglAwal']);

	        	$tanggalAwal  = isset($post['tgl_awal']) ? $post['tgl_awal'] : '';
				$tanggalAkhir = isset($post['tgl_akhir']) ? $post['tgl_akhir'] : '';

				$this->Dokterlibur_model->delete('',$wheredel);

				while (strtotime($tanggalAwal) <= strtotime($tanggalAkhir)) {
				    $datas[] = array(
				    	'kd_dokter' => $post['kd_dokter'],
				    	'tanggal' => $tanggalAwal
				    );
				    $tanggalAwal = date("Y-m-d", strtotime("+1 days", strtotime($tanggalAwal)));
				}

	        	if($this->Leavecalendar_model->update('',$post,array('id' => $id)))
	        	{
	        		$this->Dokterlibur_model->insert('',$datas,true);
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);

        foreach ($explode as $key => $value) {
        	$det = $this->Leavecalendar_model->get('','kd_dokter,tgl_awal,tgl_akhir','id=\''.$value.'\'')->result();
			$this->Dokterlibur_model->delete('','kd_dokter=\''.$det[0]->kd_dokter.'\' and date(tanggal) >= \''.$det[0]->tgl_awal.'\' and date(tanggal) <= \''.$det[0]->tgl_akhir.'\'',2);

			if(!$this->Leavecalendar_model->delete('',array('id' => $value)))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b>gagal</b> dihapus';
			}
			else
			{
				$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data <b>berhasil</b> dihapus.';
			}
        } 
    	
    	//echo base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

}