<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Patientbooking extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$model = [
			'Bookingregistrasi_model',
			'Jadwal_model',
			'Holidayscalendar_model',
			'Limitsettings_model',
			'Dokterlibur_model',
			'Dokter_model',
			'Poliklinik_model',
			'Pasien_model',
			'Gppendaftaran_model',
			'Tempbookingbpjs_model'
		];
		$this->load->model($model);
		$this->site->is_logged_in();
		$this->CI =& get_instance();
	}

	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	    case 'getpasien':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->getpasien();
	    	break;
	    case 'get_validasi':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->get_validasi();
	    	break;
	    case 'get_dokter':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->get_dokter();
	    	break;
	    case 'cek_info_dokter':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->cek_info_dokter();
	    case 'cek_info_carabayar':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->cek_info_carabayar();
	    	break;
	    case 'cek_rujukan_bpjs':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->cek_rujukan_bpjs();
	    	break;
	    default:
	      show_404();
	      break;
	  }
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['a.tanggal_booking' => date('Y-m-d')];

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{	
				if(is_numeric($search)){
					$where = 'a.no_rkm_medis=\''.$search.'\' and date(tanggal_booking)=\''.date('Y-m-d').'\'';
				}else if(is_string($search)){
					$where = 'b.nm_pasien LIKE \'%'.$search.'%\' and date(tanggal_booking)=\''.date('Y-m-d').'\'';
				}
			}
		}

		$res_count	= $this->Bookingregistrasi_model->countData($where);
		$res_data 	= $this->Bookingregistrasi_model->getData($where,$limit,$limit_start);
		$url 		= base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		$api = [1=>'Dukcapil',2 => 'Bpjs'];

		$poliklinik = splitRecursiveArray($this->functionother->get_poliklinik(),'kd_poli','nm_poli');
		$dokter = splitRecursiveArray($this->functionother->get_dokter(),'kd_dokter','nm_dokter');

		unset($_SESSION['poliklinik']);
		unset($_SESSION['dokter']);
		unset($_SESSION['cara_bayar']);
		unset($_SESSION['no_rkm_medis']);
		unset($_SESSION['nm_pasien']);
		unset($_SESSION['tgl_awal']);
		unset($_SESSION['tgl_akhir']);
		unset($_SESSION['jk']);

		$data = array(
			'title' => 'Halaman Data Pasien Booking',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status_field' => $this->_cf->status_field,
			'poliklinik' => $poliklinik,
			'dokter' => $dokter,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	public function search(){
		$post = _post();
		$this->session->set_userdata($post);
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= [];

		if(!empty($post['poliklinik']) || !empty($_SESSION['poliklinik'])){
			$where['a.kd_poli'] = isset($post['poliklinik']) ? $post['poliklinik'] : $_SESSION['poliklinik'];
		}

		if(!empty($post['dokter']) || !empty($_SESSION['dokter'])){
			$where['a.kd_dokter'] = isset($post['dokter']) ? $post['dokter'] : $_SESSION['dokter'];
		}

		if(!empty($post['cara_bayar']) || !empty($_SESSION['cara_bayar'])){
			$where['a.kd_pj'] = isset($post['cara_bayar']) ? $post['cara_bayar'] : $_SESSION['cara_bayar'];
		}

		if(!empty($post['no_rkm_medis']) || !empty($_SESSION['no_rkm_medis'])){
			$where['a.no_rkm_medis'] = isset($post['no_rkm_medis']) ? $post['no_rkm_medis'] : $_SESSION['no_rkm_medis'];
		}

		if(!empty($post['nm_pasien']) || !empty($_SESSION['nm_pasien'])){
			$where['b.nm_pasien'] = isset($post['nm_pasien']) ? $post['nm_pasien'] : $_SESSION['nm_pasien'];
		}

		if(!empty($post['tgl_awal']) && !empty($post['tgl_akhir']) || !empty($_SESSION['tgl_awal']) && !empty($_SESSION['tgl_akhir'])){
			$where['a.tanggal_periksa >= '] = isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_awal']));	
			$where['a.tanggal_periksa <= '] = isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_akhir']));	
		}else if(!empty($post['tgl_awal']) && empty($post['tgl_akhir'])){
			$where['a.tanggal_periksa'] = isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_awal']));	
		}else if(empty($post['tgl_awal']) && !empty($post['tgl_akhir'])){
			$where['a.tanggal_periksa'] = isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_akhir']));
		}

		if(!empty($post['jk']) || !empty($_SESSION['jk'])){
			$where['b.jk'] = isset($post['jk']) ? $post['jk'] : $_SESSION['jk'];
		}


		$res_count	= $this->Bookingregistrasi_model->countDataSearch($where);
		$res_data 	= $this->Bookingregistrasi_model->getDataSearch($where,$limit,$limit_start);
		$url 		= base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination_search(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count,'search');

		$api = [1=>'Dukcapil',2 => 'Bpjs'];

		$poliklinik = splitRecursiveArray($this->functionother->get_poliklinik(),'kd_poli','nm_poli');
		$dokter = splitRecursiveArray($this->functionother->get_dokter(),'kd_dokter','nm_dokter');

		/*echo '<pre>';
		print_r($res_data);
		exit();*/
		$data = array(
			'title' => 'Halaman Data Pasien Booking',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status_field' => $this->_cf->status_field,
			'poliklinik' => $poliklinik,
			'dokter' => $dokter,
			'where' => $post,
			'file' => 'index_search',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan form pengkajian pasien.
	* Menampilkan data riwayat kunjungan pasien.
	* Menampilkan 
	* @ second load method 
	*/

	/*public function create()
	{

	}*/

	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Bookingregistrasi_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	$no_rkm_medis	= isset($post['no_rkm_medis']) ? $post['no_rkm_medis'] : '';
				$kd_dokter 		= isset($post['kd_dokter']) ? $post['kd_dokter'] : '';
				$kd_poli 		= isset($post['kd_poli']) ? $post['kd_poli'] : '';
				$tgl_registrasi = isset($post['tanggal_periksa']) ? date('Y-m-d',strtotime($post['tanggal_periksa'])) : '';
				$tgl_norawat	= date('Y/m/d',strtotime($tgl_registrasi));
				$kd_pj 			= isset($post['kd_pj']) ? $post['kd_pj'] : '';
				$hari_kerja		= indoDays($tgl_registrasi);
				$dNow			= date('Y-m-d');
				$tNow 			= date('H:i:s');

				$getPasien = $this->Pasien_model->get('','*',['no_rkm_medis'=>$no_rkm_medis])->result();
				$usia_pasien = getAge(date('Y-m-d'),$getPasien[0]->tgl_lahir,['year'])['year'];
				/*
				Info 	: Query get jumlah pendaftar
				Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
				*/
				$wh1 = [
					'no_rkm_medis'=>$no_rkm_medis,
					'tanggal_periksa'=>$tgl_registrasi,
					'kd_poli'=>$kd_poli,
					'kd_dokter'=>$kd_dokter,
					'kd_pj'=>$kd_pj,
					'limit_reg'=>1,
					'status'=>'Belum'
				];

				$getJumPendaftar= $this->Bookingregistrasi_model->get('','count(limit_reg) as total',$wh1)->result();
				
				$wh2 = [
					'kd_dokter'=>$kd_dokter,
					'kd_poli'=>$kd_poli,
					'hari_kerja'=>$hari_kerja
				];

				$Getlimit 	= $this->Limitsettings_model->get('','*',$wh2)->result();
				if($usia_pasien >= 65){
					$info_msg = 'lansia';
					$limit 	= (@$Getlimit[0]->limit_lansia) ? $Getlimit[0]->limit_lansia : 0;
				}else{
					$info_msg = '';
					$limit 	= (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;
				}
				
				/*
				Info 	: Query get dokter
				*/
				$getDokter 		= $this->Dokter_model->get('','kd_dokter,nm_dokter',['kd_dokter'=>$kd_dokter])->result();
				$getPoli 		= $this->Poliklinik_model->get('','kd_poli,nm_poli',['kd_poli'=>$kd_poli])->result();
				
			
				// Get No Reg Terbesar Pada tabel booking_registrasi
				$getNoReg1	= $this->Bookingregistrasi_model->get('','max(no_reg) as no_reg',['tanggal_periksa'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli])->result();

				// Get No Reg Terbesar Pada tabel reg_periksa
				$getNoReg2	= $this->Gppendaftaran_model->get('','max(no_reg) as no_reg',['tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli])->result();

				// Mengambil nomor registrasi yang paling terbesar dari kedua tabel reg_periksa dan booking_registrasi
				$NoReg 		= getNoReg($getNoReg1[0]->no_reg,$getNoReg2[0]->no_reg);
				$conNoReg 	= substr($NoReg, 0, 3);
				$NoRegNow	= sprintf('%03s', ($conNoReg + 1));

				// Ambil waktu pengurangan jam praktek dan waktu praktek dari tabel limit_pasien_online
		        $getLimitDokter		= $this->Limitsettings_model->get('','limit_kedatangan, waktu_praktek',['kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja])->result(); 
		        $limit_kedatangan 	= $getLimitDokter[0]->limit_kedatangan;
		        $waktu_praktek 		= $getLimitDokter[0]->waktu_praktek;

		       // get Jam Praktek
		        $getJamPraktek		= $this->Jadwal_model->get('','jam_mulai',array('kd_dokter'=>$kd_dokter,'hari_kerja'=>$hari_kerja))->result();

		        // Perkalian limit dan no registrasi
		        $menitKedatangan = $limit_kedatangan * $NoRegNow;

		        // Menyatukan Tanggal Registrasi dengan Jam mulai prkatek
				$MergeTanggal = date('Y-m-d H:i:s', strtotime($tgl_registrasi . $getJamPraktek[0]->jam_mulai));

				// Pengurangan 30 menit sebelum praktek
				$ConTglKedatangan = date_create($MergeTanggal); // Jadikan format data menjadi tanggal
				date_add($ConTglKedatangan, date_interval_create_from_date_string('-'. $waktu_praktek .' minutes'));
				$TglKedatangan =  date_format($ConTglKedatangan, 'Y-m-d H:i:s');

				// Penambahan waktu/menit setiap pasien
				$TglPengurangan = date_create($TglKedatangan);
				date_add($TglPengurangan, date_interval_create_from_date_string('+'. $menitKedatangan . ' minutes'));
				$waktu_datang = date_format($TglPengurangan, 'Y-m-d H:i:s');

				$tanggal_datang = date('Y-m-d',strtotime($waktu_datang));
				$jam_datang = date('H:i:s',strtotime($waktu_datang));
				$waktu_format_indonesia = tanggal_indo($tanggal_datang,true);

				if($getJumPendaftar[0]->total >= $limit)
				{
					$error = 1;
					$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli.' kuota '. $info_msg .' sudah penuh.';
	        		throw new Exception($msg);
				}else{

					if(@$post['asal_rujukan']=='faskes1'){
						$datas['no_rujukan'] = isset($post['no_rujukan']) ? $post['no_rujukan'] : '';
					}else{
						$datas['no_sep'] = isset($post['no_rujukan']) ? $post['no_rujukan'] : '';
					}

					$datas['tanggal_booking'] = $dNow;
					$datas['jam_booking']= $tNow;
					$datas['no_rkm_medis']= $no_rkm_medis;
					$datas['tanggal_periksa']= $tgl_registrasi;
					$datas['kd_dokter']= $kd_dokter;
					$datas['kd_poli']= $kd_poli;
					$datas['no_reg']= $NoRegNow;
					$datas['kd_pj']= $kd_pj;
					$datas['limit_reg']= 1;
					$datas['waktu_kunjungan']= $waktu_datang;
					$datas['no_skdp']= isset($post['no_skdp']) ? $post['no_skdp'] : '';
					$datas['status']= 'Belum';

					if(!empty($post['no_rujukan']) && !empty($post['asal_rujukan'])){
						
						$this->load->library('Bpjs');

						if($post['asal_rujukan']=='faskes1'){
							$res = $this->bpjs->cariByNoRujukan('',$post['no_rujukan']);
						}else{
							$res = $this->bpjs->cariByNoRujukan('RS',$post['no_rujukan']);
						}

						$rw = $res['response']['rujukan'];

						$temp['no_rkm_medis'] = $post['no_rkm_medis']; 
						$temp['no_rujukan'] = $post['no_rujukan']; 
						$temp['tanggal_periksa'] = $tgl_registrasi; 
						$temp['nokartu'] = $rw['peserta']['noKartu']; 
						$temp['kdDiagnosis'] = $rw['diagnosa']['kode']; 
						$temp['nmDiagnosis'] = $rw['diagnosa']['nama']; 
						$temp['kdPelayanan'] = $rw['pelayanan']['kode']; 
						$temp['nmPelayanan'] = $rw['pelayanan']['nama']; 
						$temp['keteranganHakKls'] = $rw['peserta']['hakKelas']['keterangan']; 
						$temp['kdHakKls'] = $rw['peserta']['hakKelas']['kode']; 
						$temp['keteranganJnsPeserta'] = $rw['peserta']['jenisPeserta']['keterangan']; 
						$temp['keteranganstatusPeserta'] = $rw['peserta']['statusPeserta']['keterangan']; 
						$temp['nmAsuransiCob'] = $rw['peserta']['cob']['nmAsuransi']; 
						$temp['noAsuransiCob'] = $rw['peserta']['cob']['noAsuransi']; 
						$temp['tglTATCob'] = $rw['peserta']['cob']['tglTAT']; 
						$temp['tglTMTCob'] = $rw['peserta']['cob']['tglTMT']; 
						$temp['kdJnsPeserta'] = $rw['peserta']['jenisPeserta']['kode']; 
						$temp['kdPoliRujukan'] = $rw['poliRujukan']['kode']; 
						$temp['kdProviderPerujuk'] = $rw['provPerujuk']['kode']; 
						$temp['nmProviderPerujuk'] = $rw['provPerujuk']['nama']; 
						$temp['tglKunjungan'] = $rw['tglKunjungan'];
						$temp['keluhan'] = $rw['keluhan'];
						$temp['noTelpMr'] = $rw['peserta']['mr']['noTelepon'];

						$this->Bookingregistrasi_model->insert('booking_registrasi',$datas,false);
						if($this->Tempbookingbpjs_model->insert('',$temp,false)){
							$error = '';
							throw new Exception('Data temp booking bpjs berhasil di simpan');
						}else{
							$error = 1;
							throw new Exception('Data temp booking bpjs gagal di simpan.');
						}
					}else{
						if(!$this->Bookingregistrasi_model->insert('booking_registrasi',$datas,false)){
							$error = '';
							throw new Exception('Data booking berhasil di simpan.');
						}else{
							$error = 1;
							throw new Exception('Data booking gagal di simpan.');
						}
					}
				}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

       
        echo json_encode([
        	'error' => $error,
        	'msg' => $e->getMessage(),
        	'url' => $url
        ]);
    }

    /*public function show($id)
    {
        //
    }

    */

    public function update()
    {
        $post = _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        $rules 	= $this->Masterprofesi_model->rules;
        $post['updated_at'] = date('Y-m-d H:i:s');
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);
	        	if($this->Masterprofesi_model->update('',$post,array('id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function download()
    {
    	$post = _post();
        if(!empty($post['poliklinik'])){
			$where['a.kd_poli'] = isset($post['poliklinik']) ? $post['poliklinik'] : '';
		}

		if(!empty($post['dokter'])){
			$where['a.kd_dokter'] = isset($post['dokter']) ? $post['dokter'] : '';
		}

		if(!empty($post['cara_bayar'])){
			$where['a.kd_pj'] = isset($post['cara_bayar']) ? $post['cara_bayar'] : '';
		}

		if(!empty($post['no_rkm_medis'])){
			$where['a.no_rkm_medis'] = isset($post['no_rkm_medis']) ? $post['no_rkm_medis'] : '';
		}

		if(!empty($post['nm_pasien'])){
			$where['b.nm_pasien'] = isset($post['nm_pasien']) ? $post['nm_pasien'] : '';
		}

		if(!empty($post['tgl_awal']) && !empty($post['tgl_akhir'])){
			$where['a.tanggal_periksa >= '] = isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : '';	
			$where['a.tanggal_periksa <= '] = isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : '';	
		}else if(!empty($post['tgl_awal']) && empty($post['tgl_akhir'])){
			$where['a.tanggal_periksa'] = isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : '';	
		}else if(empty($post['tgl_awal']) && !empty($post['tgl_akhir'])){
			$where['a.tanggal_periksa'] = isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : '';
		}

		if(!empty($post['jk'])){
			$where['b.jk'] = isset($post['jk']) ? $post['jk'] : '';
		}

		
		$res_count	= $this->Bookingregistrasi_model->countDataSearch($where);
		$res_data 	= $this->Bookingregistrasi_model->getDataSearch($where);

		header('Content-Type: application/force-download');
		header("Content-Disposition: attachment; filename=pasien_booking".date('dmyHis').".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
    	$html = '<table class="table table-hover table-striped">';
    		$html .= '<thead class="l-blush text-white">';
    			$html .= '<tr>';
    				$html .= '<th>No</th>';
    				$html .= '<th>No Reg</th>';
    				$html .= '<th>No RM</th>';
    				$html .= '<th>No Rujukan</th>';
    				$html .= '<th>Nama Pasien</th>';
    				$html .= '<th>Jenis Kelamin</th>';
    				$html .= '<th>Dokter</th>';
    				$html .= '<th>Poliklinik</th>';
    				$html .= '<th>Cara Bayar</th>';
    				$html .= '<th>Tanggal Kunjungan</th>';
    				$html .= '<th>Status</th>';
    			$html .= '</tr>';
    		$html .= '</thead>';
    		$html .= '<tbody>';
    		$no = 1;
    		foreach ($res_data as $k => $v) {
	    		$html .= '<tr>';
    				$html .= '<td>'.($no++).'</td>';
    				$html .= '<td>"'.$v->no_reg.'</td>';
    				$html .= '<td>'.$v->no_rkm_medis.'</td>';
    				$html .= '<td>'.$v->no_rujukan.'</td>';
    				$html .= '<td>'.$v->nm_pasien.'</td>';
    				$html .= '<td>'.$v->jk.'</td>';
    				$html .= '<td>'.$v->nm_dokter.'</td>';
    				$html .= '<td>'.$v->nm_poli.'</td>';
    				$html .= '<td>'.$v->png_jawab.'</td>';
    				$html .= '<td>'.tanggal_indo($v->tanggal_periksa).'</td>';
    				$html .= '<td>'.$v->status.'</td>';
    			$html .= '</tr>';
	    	}
    		$html .= '</tbody>';
    	$html .= '</table>';

    	echo $html;
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);
   
        foreach ($explode as $key => $value) {
        	$exp = explode('mer', $value);
        	$where = [
        		'no_rkm_medis'=>$exp[0],
        		'DATE(tanggal_periksa)'=>date('Y-m-d',strtotime($exp[1])),
        		'kd_poli'=>$exp[2],
        		'kd_dokter'=>$exp[3],
        		'kd_pj'=>$exp[4]
        	];

        	if(!empty($exp[5])){
        		$where_temp = [
        			'no_rkm_medis' => $exp[0],
        			'DATE(tanggal_periksa)'=>date('Y-m-d',strtotime($exp[1])),
        			'no_rujukan' => $exp[5]
        		];
        		$this->delTemp($where_temp);
        	}else if(!empty($exp[6])){
        		$where_temp = [
        			'no_rkm_medis' => $exp[0],
        			'DATE(tanggal_periksa)'=>date('Y-m-d',strtotime($exp[1])),
        			'no_rujukan' => $exp[6]
        		];
        		$this->delTemp($where_temp);
        	}
        	
        	if($this->Bookingregistrasi_model->delete('booking_registrasi',$where)){
        		
        		$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data telah berhasil dihapus';
        	}else{
        		$_SESSION['error'] = 1;
        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
        	} 	
        } 
    	//print_r($explode);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

    private function delTemp($where=''){
    	return $this->Tempbookingbpjs_model->delete('',$where);
    }

    public function print(){
    	$this->load->library('Pdf');
    	$segs = $this->_segs;
    	$code = explode('mer', $segs[7]);
    	$where = [
    		'a.no_rkm_medis' 		=> $code[0],
    		'a.tanggal_periksa' 	=> $code[1],
    		'a.kd_poli' 			=> $code[2],
    		'a.kd_dokter' 		=> $code[3],
    		'a.kd_pj' 			=> $code[4]
    	];

    	$get = $this->Bookingregistrasi_model->getData($where);
    	
    	if($get){
    		//Create a new PDF file
			$pdf = new FPDF('P','mm',array(125,176)); //L For Landscape / P For Portrait
			$pdf->AddPage();

			//Menambahkan Gambar
			//$pdf->Image('../foto/logo.png',10,10,-175);

			$pdf->SetFont('Arial','B',13);
			$pdf->Cell(40);
			$pdf->Cell(30,0,'RSUD KOTA DEPOK',0,0,'C');
			$pdf->SetY(12);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(8);
			$pdf->Cell(90,5,'Jl. Raya Muchtar No.99, Sawangan Lama, Sawangan',0,0,'C');
			$pdf->Ln();
			$pdf->SetY(15);
			$pdf->Cell(8);
			$pdf->Cell(90,5,'Kota Depok, Jawa Barat 16511',0,0,'C');
			$pdf->Ln();
			$pdf->SetY(18);
			$pdf->Cell(8);
			$pdf->Cell(90,5,'Telp : (0251) 8602514',0,0,'C');
			$pdf->Ln();

			//Fields Name position
			$Y_Fields_Name_position = 35;

			//First create each Field Name
			//Gray color filling each Field Name box
			$pdf->SetFillColor(110,180,230);
			//Bold Font for Field Name
			$pdf->SetFont('Arial','B',10);
			$pdf->SetY($Y_Fields_Name_position);
			$pdf->SetX(1);
			$pdf->Cell(40,8,'Bukti Pendaftaran Online', 0 ,0 , 'L' , 0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8, 'No Urut Booking', 1 ,0 , 'C' , 1);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',15);
			$pdf->SetX(2);
			$pdf->Cell(40,8, $get[0]->no_reg, 1 ,0 , 'C' , 1);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Nomor Rekam Medik',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$get[0]->no_rkm_medis,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Nama Lengkap',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$get[0]->nm_pasien,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Tanggal Booking',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8, tanggal_indo($get[0]->tanggal_booking,true) .'  '. $get[0]->jam_booking,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Tanggal Periksa',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8, tanggal_indo($get[0]->tanggal_periksa,true),1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Waktu Datang',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8, tanggal_indo($get[0]->waktu_kunjungan,true) ,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Poliklinik Tujuan',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$get[0]->nm_poli,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Dokter',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$get[0]->nm_dokter,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Cara Bayar',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$get[0]->png_jawab,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',8);
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Di Cetak Tanggal : ' . tanggal_indo(date('Y-m-d')), 0 ,0 , 'L' , 0);
			$pdf->SetY(128);
			$pdf->SetX(2);
			$pdf->Cell(25,6,'Catatan : Silahkan datang sesuai pada jam waktu datang yang telah ditentukan...',0,'L');

			$pdf->SetY(145);
			$pdf->SetFont('Arial','I',8);
			$pdf->Cell(0,10,'http://www.rsudreg.depok.go.id ',0,0,'C');
			$pdf->Output();
    	}else{
    		echo 'Data tidak bisa tercetak.';
    	}
    }

    public function getpasien(){
    	$this->_datapasien();
    }

    private function _datapasien(){
    	$search 	= $_GET['term'];
				
		if(isset($search))
		{
			$where = 'no_rkm_medis like \'%'.$search.'%\' OR nm_pasien like \'%'.$search.'%\'';
			$res = $this->functionother->get_tb_pasien($where);
			if(count($res) > 0)
			{
				$res_data = array();
				$no=0;
				foreach($res as $key => $value)
				{
					$no++;
					$jk = ($value->jk=='L') ? 'Laki-laki' : 'Perempuan';
					$res_data[] = array(
						'id' => $value->no_rkm_medis,
						'value' => $value->no_rkm_medis.' - '. $value->nm_pasien . ' - '. tanggal_indo($value->tgl_lahir,true) .' - '. $jk 
					);
				}
			}
			else
			{
				$res_data[] = array(
					'id' => 'No',
					'value' => 'Data tidak ditemukan.'
				);
			}	
			
		}
		echo json_encode($res_data);
    }

    private function get_poliklinik($where=''){   	
    	$res_poli 	= $this->Jadwal_model->getData($where)->result_array();
		
		$data = [];
		if($res_poli){
			$data = splitRecursiveArray($res_poli,'kd_poli','nm_poli');
		}

		return $data;
    }

    private function get_carabayar(){
    	$post = _post();
    	$this->load->model('Penjab_model');
    	$res = $this->Penjab_model->get('','kd_pj,png_jawab')->result_array();
    	$carabayar 	= splitRecursiveArray($res,'kd_pj','png_jawab');
		return $carabayar;
    }

    public function get_dokter(){
    	$post = $this->input->post();
    	
    	$where['hari_kerja'] = indoDays(date('Y-m-d'));

    	if(!empty($post['tanggal_periksa'])){
    		$where['hari_kerja'] = indoDays($post['tanggal_periksa']);
    	}
    	    	
    	if(!empty($post['kd_poli'])){
    		$where['kd_poli'] = isset($post['kd_poli']) ? $post['kd_poli'] : '';
    	}

    	$res_dokter 	= $this->Jadwal_model->getDokter($where)->result_array();
    	$splitdokter = [];
		if($res_dokter){
			$splitdokter 	= splitRecursiveArray($res_dokter,'kd_dokter','nm_dokter');
		}

		echo htmlSelectFromArray($splitdokter, 'name="kd_dokter" id="kd_dokter" class="form-control input-lg"', true);
    }

    public function get_validasi(){
    	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$post 			= _post();
			$tgl_registrasi	= date('Y-m-d',strtotime($post['tanggal_periksa']));
			$no_rkm_medis	= isset($post['no_rkm_medis']) ? explode('-', $post['no_rkm_medis']) : '';
			$dNow 			= date('Y-m-d');
			$tNow 			= date('H:i:s');
			$tgl_kedepan 	= AddDate($dNow,$this->_cf->_hari_daftar,'days');
			$where 	= ['hari_kerja'=>indoDays($tgl_registrasi)];

			// Info Pasien
			$res_pasien = $this->Pasien_model->get('','tgl_lahir',['no_rkm_medis'=>trim($no_rkm_medis[0])],'','','',1)->result();
			$usia_pasien = getAge(date('Y-m-d'),$res_pasien[0]->tgl_lahir,['year'])['year'];
			$respoliklinik = $this->get_poliklinik($where);

			if($usia_pasien <= 17){
				$poliklinik = $respoliklinik;
			}else{
				unset($respoliklinik['ANA']);
				$poliklinik = $respoliklinik;
			}

			$cek_pasien_aktiv= $this->Bookingregistrasi_model->getBooking(array('a.no_rkm_medis'=>trim($no_rkm_medis[0]),'a.tanggal_periksa'=>$tgl_registrasi,'a.limit_reg'=>1));

			$getLiburNasional= $this->Holidayscalendar_model->get('','*',['tanggal'=>$tgl_registrasi],'','',1)->result();
			
			if($tgl_registrasi == $dNow && $tNow > $this->_cf->_limit_jam)
			{
				$error = 1;
				$msg = 'Mohon maaf pendaftaran sudah selesai';
			}
			else if($tgl_registrasi < $dNow)
			{
				$error = 1;
				$msg = 'Mohon maaf tanggal yang anda pilih kurang dari tanggal saat ini.';
			}
			else if($tgl_registrasi > $tgl_kedepan)
			{
				$error = 1;
				$msg = 'Mohon maaf tanggal yang anda pilih melebihi batas limit pendaftaran, silahkan mundurkan tanggal kunjungan anda.';
			}
			else if($cek_pasien_aktiv[0]->status == 'Belum')
			{
				$error = 1;
				$msg = 'Anda sudah terdaftar '. tanggal_indo($cek_pasien_aktiv[0]->tanggal_periksa,true) . ', pada Dokter '. $cek_pasien_aktiv[0]->nm_dokter . ' ke '. $cek_pasien_aktiv[0]->nm_poli . ' dengan waktu kunjungan '. tanggal_indo($cek_pasien_aktiv[0]->waktu_kunjungan,true);
			}
			else if($cek_pasien_aktiv[0]->status == 'Batal')
			{
				$error = 1;
				$msg = 'Mohon maaf anda tidak bisa mendaftar pada tanggal yang sama, tercatat anda sudah mendaftar pada tanggal sekarang kemudian membatalkan.';
			}
			else if($getLiburNasional)
			{
				$error = 1;
				$msg = 'Mohon maaf pada '. tanggal_indo($getLiburNasional[0]->tanggal) . ' '. $getLiburNasional[0]->keterangan;
			}
			else
			{
				$error = 2;
				$msg = htmlSelectFromArray($poliklinik, 'name="kd_poli" id="kd_poli" class="form-control input-lg"', true);
			}

			$data = array(
				'error' => $error,
				'msg' => $msg
			);

			echo json_encode($data);
		}
    }

    /*
    ----------------------------------------------------------
	| INFO DOKTER
	----------------------------------------------------------
	| 1. Info dokter tidak praktik.
	| 2. Info ceking pasien sudah terdaftar
    */
    public function cek_info_dokter(){
    	$post = _post();
    	$kd_dokter 		= isset($post['kd_dokter']) ? $post['kd_dokter'] : '';
		$kd_poli 		= isset($post['kd_poli']) ? $post['kd_poli'] : '';
		$tgl_registrasi = isset($post['tanggal_periksa']) ? $post['tanggal_periksa'] : date('Y-m-d');
		$hari_kerja		= indoDays($tgl_registrasi);
		$norkmmedis		= isset($post['no_rkm_medis']) ? explode('-', $post['no_rkm_medis']) : '';
		$no_rkm_medis	= trim($norkmmedis[0]);
		$dNow			= date('Y-m-d');
		$tNow 			= date('H:i:s');
		$tgl_kedepan 	= AddDate($dNow,$this->_cf->_hari_daftar,'days');
		$jumlah_hari	= countDate($dNow,$tgl_kedepan);

		/*
		Info 	: Info ceking ke dalam tabel booking apakah pasien sudah terdaftar.
		Manfaat : untuk cek apakah pasien sudah terdaftar pada tanggal yang dia pilih atau belum, karena dalam satu hari yang sama pasien tidak bisa booking ke dua tempat.
		*/
		$getCekBk = $this->Bookingregistrasi_model->getBooking(['a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'a.tanggal_periksa'=>$tgl_registrasi,'a.kd_poli'=>$kd_poli,'a.kd_dokter'=>$kd_dokter,'a.kd_pj'=>'UMU','a.status'=>'Belum']);

		/*
		Info 	: Info ambil data dokter yang tidak praktik berdasarkan waktu sekarang. 
		Manfaat	: Untuk memunculkan informasi kepada pasien bahwa dokter sedang tidak praktik.
		*/
		$getDokCuti		= $this->Dokterlibur_model->get('','count(*) as total, tanggal',array('kd_dokter'=>$kd_dokter,'tanggal'=>$tgl_registrasi),'','','',1)->result();

		/*
		Info 	: Query get dokter
		*/
		$getDokter 		= $this->Dokter_model->get('','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter))->result();
		$getPoli 		= $this->Poliklinik_model->get('','kd_poli,nm_poli',array('kd_poli'=>$kd_poli))->result();

		if($kd_dokter=='')
		{
			$sts = 1;
			$msg = 'Dokter tujuan anda masih kosong.';
		}
		else if($getDokCuti[0]->total > 0)
		{
			$sts = 1;
			$msg = 'Mohon maaf Dokter : '. $getDokter[0]->nm_dokter .' sedang tidak praktik pada  '. tanggal_indo($getDokCuti[0]->tanggal,true) .'';
		}
		else if($getCekBk[0]->tanggal_periksa)
		{
			$sts = 1;
			$msg = 'Mohon maaf anda sudah terdaftar pada : '.tanggal_indo($tgl_registrasi,true).' , Tujuan : '. $getCekBk[0]->nm_poli .' , Dokter : '. $getCekBk[0]->nm_dokter .'';
		}
		else
		{
			$sts = 2;
			$msg = 'Silahakn pilih cara bayar yang ingin anda gunakan.';
		}
		
		$d = array(
			'sts' => $sts,
			'msg' => $msg
		);
		//.' sisa kuota : '. $sisikuota .' -> limit : '. $limit .' - total pendaftara : '. $getJumPendaftar[0]->total .' - jum batal : ' .$getJumBatal[0]->total
		echo json_encode($d);
    }

    /*
    ----------------------------------------------------------
	| INFO CARA BAYAR
	----------------------------------------------------------
	| 1. Info sisa kuota berdasarkan cara bayar.
	| 1. Info sisa kuota berdasarkan usia.
	| 2. Info ceking pasien sudah terdaftar
    */
    public function cek_info_carabayar(){
		$post 			= _post();
		$kd_dokter 		= isset($post['kd_dokter']) ? $post['kd_dokter'] : '';
		$kd_poli 		= isset($post['kd_poli']) ? $post['kd_poli'] : '';
		$tgl_registrasi = isset($post['tanggal_periksa']) ? $post['tanggal_periksa'] : date('Y-m-d');
		$kd_pj 			= isset($post['kd_pj']) ? $post['kd_pj'] : '';
		$hari_kerja		= indoDays($tgl_registrasi);
		$norkmmedis		= isset($post['no_rkm_medis']) ? explode('-', $post['no_rkm_medis']) : '';
		$no_rkm_medis	= trim($norkmmedis[0]);
		$dNow			= date('Y-m-d');
		$tNow 			= date('H:i:s');
		$tgl_kedepan 	= AddDate($dNow,$this->_cf->_hari_daftar,'days');
		$jumlah_hari	= countDate($dNow,$tgl_kedepan);

		// Info Pasien
		$res_pasien = $this->Pasien_model->get('','tgl_lahir',['no_rkm_medis'=>$no_rkm_medis],'','','',1)->result();
		$usia_pasien = getAge(date('Y-m-d'),$res_pasien[0]->tgl_lahir,['year'])['year'];
	
		/*
		Info 	: Block Area Perhitungan info sisa kuota
		Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
		*/
		$where = 'tanggal_periksa=\''.$tgl_registrasi.'\' and kd_poli=\''.$kd_poli.'\' and kd_dokter=\''.$kd_dokter.'\' and limit_reg=1';

		$getJumPendaftar= $this->Bookingregistrasi_model->get('','COUNT(*) as total',$where)->result();

		$getJumBatal= $this->Bookingregistrasi_model->get('','COUNT(*) as total',array('tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'limit_reg'=>1,'status'=>'Batal'))->result();
		$text = '';

		if($usia_pasien >= 60){
			$Getlimit = $this->Limitsettings_model->get('','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja))->result();
			$limit_kuota = (@$Getlimit[0]->limit_lansia) ? $Getlimit[0]->limit_lansia : 0;
			$text = 'lanisa';
		}else{
			$Getlimit = $this->Limitsettings_model->get('','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja))->result();
			$limit_kuota = (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;

		}
		
		$limit 			= $limit_kuota;
		//$total_daftar   =
		$sisakuota		= $limit - ($getJumPendaftar[0]->total - $getJumBatal[0]->total);
		
		/*
		Info 	: Query get dokter
		*/
		$getDokter 		= $this->Dokter_model->get('','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter))->result();
		$getPoli 		= $this->Poliklinik_model->get('','kd_poli,nm_poli',array('kd_poli'=>$kd_poli))->result();

		if($sisakuota == 0 || $sisakuota < 0)
		{
			$sts = 1;
			$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli .' kuota '.$text.' sudah penuh.';
		}else{
			$sts = '';
			$msg = 'Sisa kuota yang tersedia : '. $sisakuota;
		}

		echo json_encode(['sts'=>$sts,'msg'=>$msg]);
    }

    public function cek_rujukan_bpjs(){
    	$this->load->library('Bpjs');
    	$post = _post();

    	if($post['asal_rujukan']=='faskes1'){
    		$res = $this->bpjs->cariByNoRujukan('',$post['no_rujukan']);
    	}else{
    		$res = $this->bpjs->cariByNoRujukan('RS',$post['no_rujukan']);
    	}

    	if($res['metaData']['code'] == 201 || $res['metaData']['code'] == '' || $res=='')
		{
			$error = 1;
			$msg = 'Mohon maaf kami tidak menemukan data dengan no rujukan ini.'. $post['no_rujukan'];
		}
		else if($res['response']['rujukan'])
		{		
			$error = 2;	
			$msg = 'No Rujukan Aktif.';
		}
		else
		{
			$error = 1;
			$msg = 'Mohon maaf koneksi dari bpjs sedang tidak terhubung.';
		}
    	
    	echo json_encode(['error'=>$error,'msg'=>$msg]);
    }
}