<?php defined('BASEPATH') OR exit('No direct script access allowed');

class limitsettings extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Limitsettings_model','Jadwal_model']);
		$this->site->is_logged_in();
	}

	public function _remap()
	{
		$segs = $this->_segs;
		$method = isset($segs[6]) ? $segs[6] : '';
		switch ($method) {
		  	case null;
		  	case false;
		  	case '':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		      	$this->index();
		      break;
		  	case 'search':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
		      	$this->search();
		      break;
		  	case 'store':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
		      	$this->store();
		      break;
		  	case 'update':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->update();
		      break;
		  	case 'edit':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->edit();
		      break;
		  	case 'destroy':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
		    	$this->destroy();
		    break;
		  	case 'print':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
		    	$this->print();
		    break;
			case 'get_limit':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		    	$this->get_limit();
		    break;
		    case 'get_dokter':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		    	$this->get_dokter();
		    break;
		  default:
		    	show_404();
		    break;
		}
	}
	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= '';

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where = 'dokter.nm_dokter like \'%'.$search.'%\' OR poliklinik.nm_poli like \'%'.$search.'%\' OR limit_pasien_online.hari_kerja like \'%'.$search.'%\'';
			}
		}

		$res_count	= $this->Limitsettings_model->countData($where);
		$res_data 	= $this->Limitsettings_model->getData($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		/*
		Ambil kode poli dari tabel jadwal
		*/
		$res_poli = $this->Jadwal_model->getData()->result_array();
		$split_poli = splitRecursiveArray($res_poli,'kd_poli','nm_poli');

		$data = array(
			'title' => 'Halaman pengaturan limit pendaftaran online',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->statusdata,
			'poli' => $split_poli,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Limitsettings_model->rules;
		$this->form_validation->set_rules($rules);

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);

	        	$datas = [];
				if(!empty($post['kd_poli']) && !empty($post['kd_dokter'])){	
					foreach ($post['hari_kerja'] as $k => $v) {
						$datas[] = [
							'kd_poli' => $post['kd_poli'],
							'kd_dokter' => $post['kd_dokter'],
							'hari_kerja' => $v,
							'limit_reg' => $post['limit_reg'][$k],
							'bpjs' => $post['bpjs'][$k],
							'umum' => $post['umum'][$k],
							'limit_lansia' => $post['limit_lansia'][$k],
							'limit_kedatangan' => $post['limit_kedatangan'][$k],
							'waktu_praktek' => $post['waktu_praktek'][$k],
							'masuk_tgl' => date('Y-m-d H:i:s'),
							'masuk_oleh' => $this->session->userdata('id_user')
						];
					}

					if($this->Limitsettings_model->insert('',$datas,TRUE))
		        	{
		        		throw new Exception('Data <b>berhasil</b> tersimpan.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> tersimpan.');
		        	}
				}else{
					$error = 1;
		        	throw new Exception('Data <b>gagal</b> tersimpan kode poli dan kode dokter tidak ada.');
				}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
        $post = $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        
        $rules 	= $this->Limitsettings_model->rules;
        $this->form_validation->set_rules($rules);


        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);

	        	foreach ($post['hari_kerja'] as $k => $v) {
					$datas = [
						'limit_reg' => $post['limit_reg'][$k],
						'bpjs' => $post['bpjs'][$k],
						'umum' => $post['umum'][$k],
						'limit_lansia' => $post['limit_lansia'][$k],
						'limit_kedatangan' => $post['limit_kedatangan'][$k],
						'waktu_praktek' => $post['waktu_praktek'][$k]
					];
				}

	        	if($this->Limitsettings_model->update('',$datas,array('id_limit_pasien' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);

        foreach ($explode as $key => $value) {

			if(!$this->Limitsettings_model->delete('',array('id_limit_pasien' => $value)))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b>gagal</b> dihapus';
			}
			else
			{
				$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data <b>berhasil</b> dihapus.';
			}
        } 
    	
    	//echo base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

    public function edit(){
    	global $Cf;
    	$post 		= $this->input->post();
    	$hari_kerja = $this->_cf->dayschedule;
    	
    	$datas = [];
    	if(!empty($post['kd_poli']) && !empty($post['kd_dokter']) && !empty($post['hari_kerja'])){
    		$res = $this->Jadwal_model->get('','hari_kerja,jam_mulai,jam_selesai',['jadwal.kd_poli'=>$post['kd_poli'],'jadwal.kd_dokter'=>$post['kd_dokter']],'','','')->result_array();

    		$field ='
					hari_kerja,
					limit_reg,
					limit_lansia,
					limit_kedatangan,
					waktu_praktek,
					umum,
					bpjs';
    		$limit = $this->Limitsettings_model->get('',$field,['kd_poli'=>$post['kd_poli'],'kd_dokter'=>$post['kd_dokter']])->result_array();

    		$ket = '<b>Jadwal Belum Terinput.</b>';

    		$empty['jam_mulai'] = $ket;
			$empty['jam_selesai'] = '';
			$empty['limit_reg'] = '';
			$empty['limit_lansia'] = '';
			$empty['limit_kedatangan'] = '';
			$empty['waktu_praktek'] = '';
			$empty['umum'] = '';
			$empty['bpjs'] = '';

    		$html = '<table class="table table-responsive table-hover table-striped">';
    		$html .= '<thead class="l-blush text-white">';
    			$html .= '<tr>';
    				$html .= '<th>Hari</th>';
    				$html .= '<th class="text-center">Jam M/S</th>';
    				$html .= '<th class="text-center">LPO</th>';
    				$html .= '<th class="text-center">LBB</th>';
    				$html .= '<th class="text-center">LBU</th>';
    				$html .= '<th class="text-center">LPL</th>';
    				$html .= '<th class="text-center">LI</th>';
    				$html .= '<th class="text-center">JPP</th>';
    			$html .= '</tr>';
    		$html .= '</thead>';
    			$html .= '<tbody>';
    			if($res){
    				foreach ($res as $k => $v) {
	    				$datas1[$v['hari_kerja']] = $v;		
	    			}

	    			foreach ($limit as $k => $v) {
	    				$datas2[$v['hari_kerja']] = $v;
	    			}

	    			foreach ($datas1 as $k => $v) {

	    				if($k!=$post['hari_kerja']){
	    					$attr = ' disabled="disabled"';
	    				}else{
	    					$attr = '';
	    				}

	    				$datas[$k]['hari_kerja'] = '<input type="hidden" name="kd_poli" value="'.$post['kd_poli'].'"><input type="hidden" name="kd_dokter" value="'.$post['kd_dokter'].'"><input type="hidden" name="hari_kerja[]" value="'.$k.'" "'.$attr.'">'.$k;
	    				$datas[$k]['jam_mulai'] = $v['jam_mulai'];
	    				$datas[$k]['jam_selesai'] = $v['jam_selesai'];
	    				if(!empty($v['jam_mulai']) && !empty($v['jam_selesai'])){
		    				$datas[$k]['limit_reg'] = isset($datas2[$k]['limit_reg']) ? '<input type="number" name="limit_reg[]" value="'.$datas2[$k]['limit_reg'].'" class="form-control text-center" "'.$attr.'">' : '<input type="number" name="limit_reg[]" value="" class="form-control text-center" "'.$attr.'">';

		    				$datas[$k]['bpjs'] = isset($datas2[$k]['bpjs']) ? '<input type="number" name="bpjs[]" value="'.$datas2[$k]['bpjs'].'" class="form-control text-center" "'.$attr.'">' : '<input type="number" name="bpjs[]" value="" class="form-control text-center" "'.$attr.'">';

		    				$datas[$k]['umum'] = isset($datas2[$k]['umum']) ? '<input type="number" name="umum[]" value="'.$datas2[$k]['umum'].'" class="form-control text-center" "'.$attr.'">' : '<input type="number" name="umum[]" value="" class="form-control text-center" "'.$attr.'">';

		    				$datas[$k]['limit_lansia'] = isset($datas2[$k]['limit_lansia']) ? '<input type="number" name="limit_lansia[]" value="'.$datas2[$k]['limit_lansia'].'" class="form-control text-center" "'.$attr.'">' : '<input type="number" name="limit_lansia[]" value="" class="form-control text-center" "'.$attr.'">';

		    				$datas[$k]['limit_kedatangan'] = isset($datas2[$k]['limit_kedatangan']) ? '<input type="number" name="limit_kedatangan[]" value="'.$datas2[$k]['limit_kedatangan'].'" class="form-control text-center" "'.$attr.'">' : '<input type="number" name="limit_kedatangan[]" value="" class="form-control text-center" "'.$attr.'">';

		    				$datas[$k]['waktu_praktek'] = isset($datas2[$k]['waktu_praktek']) ? '<input type="number" name="waktu_praktek[]" value="'.$datas2[$k]['waktu_praktek'].'" class="form-control" "'.$attr.'">' : '<input type="number" name="waktu_praktek[]" value="" class="form-control text-center" "'.$attr.'">';
	    				}else{
	    					$datas[$k]['limit_reg'] = '';
	    					$datas[$k]['bpjs'] = '';
	    					$datas[$k]['umum'] = '';
	    					$datas[$k]['limit_lansia'] = '';
	    					$datas[$k]['limit_kedatangan'] = '';
	    					$datas[$k]['waktu_praktek'] = '';
	    				}
	    			}

	    			foreach ($hari_kerja as $k => $v) {
	    				$empty['hari_kerja'] = strtoupper($k);
			    		$data[strtoupper($k)] = !empty($datas[strtoupper($v)]) ? $datas[strtoupper($v)] : $empty;	
					}

					foreach ($data as $k => $v) {

						if(!empty($v['jam_mulai']) && !empty($v['jam_selesai'])){
							$jms = $v['jam_mulai'].' - '.$v['jam_selesai'];
						}else{
							$jms = $v['jam_mulai'];
						}

		    			$html .= '<tr>';
		    				$html .= '<td>'.$v['hari_kerja'].'</td>';
		    				$html .= '<td class="text-center">'.$jms.'</td>';
		    				$html .= '<td class="text-center">'.@$v['limit_reg'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['bpjs'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['umum'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['limit_lansia'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['limit_kedatangan'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['waktu_praktek'].'</td>';
		    			$html .= '</tr>';
		    		}
    			}
    			$html .= '</tbody>';
    		$html .= '</table>';

    		echo $html;
    	}
    }

    public function get_limit(){
    	global $Cf;
    	$hari_kerja = $this->_cf->dayschedule;

    	$post = $this->input->post();
    	$datas = [];
    	if(!empty($post['kd_poli']) && !empty($post['kd_dokter'])){
    		$res = $this->Jadwal_model->get('','hari_kerja,jam_mulai,jam_selesai',['jadwal.kd_poli'=>$post['kd_poli'],'jadwal.kd_dokter'=>$post['kd_dokter']],'','','')->result_array();
    		
    		$field ='
					hari_kerja,
					limit_reg,
					limit_lansia,
					limit_kedatangan,
					waktu_praktek,
					umum,
					bpjs';
    		$limit = $this->Limitsettings_model->get('',$field,['kd_poli'=>$post['kd_poli'],'kd_dokter'=>$post['kd_dokter']])->result_array();
    		$ket = '<b>Jadwal Belum Terinput.</b>';

    		$empty['jam_mulai'] = $ket;
			$empty['jam_selesai'] = '';
			$empty['limit_reg'] = '';
			$empty['limit_lansia'] = '';
			$empty['limit_kedatangan'] = '';
			$empty['waktu_praktek'] = '';
			$empty['umum'] = '';
			$empty['bpjs'] = '';

			$html = '<table class="table table-responsive table-hover table-striped">';
    		$html .= '<thead class="l-blush text-white">';
    			$html .= '<tr>';
    				$html .= '<th>Hari</th>';
    				$html .= '<th class="text-center">Jam M/S</th>';
    				$html .= '<th class="text-center">LPO</th>';
    				$html .= '<th class="text-center">LBB</th>';
    				$html .= '<th class="text-center">LBU</th>';
    				$html .= '<th class="text-center">LPL</th>';
    				$html .= '<th class="text-center">LI</th>';
    				$html .= '<th class="text-center">JPP</th>';
    			$html .= '</tr>';
    		$html .= '</thead>';
    			$html .= '<tbody>';
	    		if($res){
	    			foreach ($res as $k => $v) {
	    				$datas1[$v['hari_kerja']] = $v;		
	    			}

	    			foreach ($limit as $k => $v) {
	    				$datas2[$v['hari_kerja']] = $v;
	    			}

	    			foreach ($datas1 as $k => $v) {

	    				$datas[$k]['hari_kerja'] = '<input type="hidden" name="hari_kerja[]" value="'.$k.'">'.$k;
	    				$datas[$k]['jam_mulai'] = $v['jam_mulai'];
	    				$datas[$k]['jam_selesai'] = $v['jam_selesai'];
	    				if(!empty($v['jam_mulai']) && !empty($v['jam_selesai'])){
		    				$datas[$k]['limit_reg'] = isset($datas2[$k]['limit_reg']) ? '<input type="number" name="limit_reg[]" value="'.$datas2[$k]['limit_reg'].'" class="form-control text-center">' : '<input type="number" name="limit_reg[]" value="" class="form-control text-center">';

		    				$datas[$k]['bpjs'] = isset($datas2[$k]['bpjs']) ? '<input type="number" name="bpjs[]" value="'.$datas2[$k]['bpjs'].'" class="form-control text-center">' : '<input type="number" name="bpjs[]" value="" class="form-control text-center">';

		    				$datas[$k]['umum'] = isset($datas2[$k]['umum']) ? '<input type="number" name="umum[]" value="'.$datas2[$k]['umum'].'" class="form-control text-center">' : '<input type="number" name="umum[]" value="" class="form-control text-center">';

		    				$datas[$k]['limit_lansia'] = isset($datas2[$k]['limit_lansia']) ? '<input type="number" name="limit_lansia[]" value="'.$datas2[$k]['limit_lansia'].'" class="form-control text-center">' : '<input type="number" name="limit_lansia[]" value="" class="form-control text-center">';

		    				$datas[$k]['limit_kedatangan'] = isset($datas2[$k]['limit_kedatangan']) ? '<input type="number" name="limit_kedatangan[]" value="'.$datas2[$k]['limit_kedatangan'].'" class="form-control text-center">' : '<input type="number" name="limit_kedatangan[]" value="" class="form-control text-center">';

		    				$datas[$k]['waktu_praktek'] = isset($datas2[$k]['waktu_praktek']) ? '<input type="number" name="waktu_praktek[]" value="'.$datas2[$k]['waktu_praktek'].'" class="form-control">' : '<input type="number" name="waktu_praktek[]" value="" class="form-control text-center">';
	    				}else{
	    					$datas[$k]['limit_reg'] = '';
	    					$datas[$k]['bpjs'] = '';
	    					$datas[$k]['umum'] = '';
	    					$datas[$k]['limit_lansia'] = '';
	    					$datas[$k]['limit_kedatangan'] = '';
	    					$datas[$k]['waktu_praktek'] = '';
	    				}
	    			}

	    			foreach ($hari_kerja as $k => $v) {
	    				$empty['hari_kerja'] = strtoupper($k);
			    		$data[strtoupper($k)] = !empty($datas[strtoupper($v)]) ? $datas[strtoupper($v)] : $empty;	
					}

					foreach ($data as $k => $v) {

						if(!empty($v['jam_mulai']) && !empty($v['jam_selesai'])){
							$jms = $v['jam_mulai'].' - '.$v['jam_selesai'];
						}else{
							$jms = $v['jam_mulai'];
						}

		    			$html .= '<tr>';
		    				$html .= '<td>'.$v['hari_kerja'].'</td>';
		    				$html .= '<td class="text-center">'.$jms.'</td>';
		    				$html .= '<td class="text-center">'.@$v['limit_reg'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['bpjs'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['umum'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['limit_lansia'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['limit_kedatangan'].'</td>';
		    				$html .= '<td class="text-center">'.@$v['waktu_praktek'].'</td>';
		    			$html .= '</tr>';
		    		}	
	    		}else{
	    			$html .= '<tr><td colspan="8"><b>Silahkan buat jadwal dokter terlebih dahulu.</b></td></tr>';
	    		}
	    		$html .= '</tbody>';
		    $html .= '</table>';
		    
	    	echo $html;
    	}else{
    		echo 'Data tidak tersedia.';
    	}
    }

    public function get_dokter(){
    	$post = $this->input->post();
    	if(!empty($post['kd_poli']) && empty($post['kd_dokter'])){
    		$res = $this->Jadwal_model->getDokter(['kd_poli'=>$post['kd_poli']])->result_array();
    		$split_dokter = splitRecursiveArray($res,'kd_dokter','nm_dokter');
    		echo select($split_dokter,'name="kd_dokter" id="kd_dokter" class="form-control"  style="width:100%;"',TRUE);
    	}else if(!empty($post['kd_poli']) && !empty($post['kd_dokter'])){
    		$res = $this->Jadwal_model->getDokter(['kd_poli'=>$post['kd_poli'],'jadwal.kd_dokter'=>$post['kd_dokter']])->result_array();
    		$split_dokter = splitRecursiveArray($res,'kd_dokter','nm_dokter');
    		echo select($split_dokter,'name="kd_dokter" id="kd_dokter" class="form-control"  style="width:100%;"',TRUE);
    	}else{
    		echo select([],'name="kd_dokter" id="kd_dokter" class="form-control"  style="width:100%;"',FALSE);
    	}
    }

}