<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwaldokter extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Jadwaldokter_model','Dokter_model','Poliklinik_model']);
		$this->site->is_logged_in();
	}

	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	    case 'get_jadwal':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_jadwal();
	    	break;
	    case 'edit_jadwal':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->edit_jadwal();
	    	break;
	    case 'get_dokter':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_dokter();
	    	break;
	    default:
	      show_404();
	      break;
	  }
	}
	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= '';

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where = 'dokter.nm_dokter like \'%'.$search.'%\' OR poliklinik.nm_poli like \'%'.$search.'%\' OR jadwal.hari_kerja like \'%'.$search.'%\'';
			}
		}

		$res_count	= $this->Jadwaldokter_model->countData($where);
		$res_data 	= $this->Jadwaldokter_model->getData($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		$res_dokter = $this->Dokter_model->get('','kd_dokter,nm_dokter',['status!='=>0])->result_array();
		$split_dokter = splitRecursiveArray($res_dokter,'kd_dokter','nm_dokter');

		$res_poli = $this->Poliklinik_model->get('','kd_poli,nm_poli')->result_array();
		$split_poli = splitRecursiveArray($res_poli,'kd_poli','nm_poli');

		$data = array(
			'title' => 'Halaman pengaturan jadwal dokter',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->statusdata,
			'hari_kerja' => $this->_cf->dayschedule,
			'dokter' => $split_dokter,
			'poli' => $split_poli,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Jadwaldokter_model->rules;
		$this->form_validation->set_rules($rules);

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);

	        	if(!empty($post['kd_poli']) && !empty($post['kd_dokter'])){
	        		if(isset($post['jam_mulai'])){
	        			foreach ($post['jam_mulai'] as $k => $v) {
							if(!empty($v)){
								$datas[] = [
									'hari_kerja' => isset($post['hari_kerja'][$k]) ? $post['hari_kerja'][$k] : '',
									'kd_poli' => isset($post['kd_poli']) ? $post['kd_poli'] : '',
									'kd_dokter' => isset($post['kd_dokter']) ? $post['kd_dokter'] : '',
									'jam_mulai' => isset($v) ? date('H:i:s',strtotime($v)) : '',
									'jam_selesai' => isset($post['jam_selesai'][$k]) ? date('H:i:s',strtotime($post['jam_selesai'][$k])) : ''
								];
							}
						}

						if(!$this->Jadwaldokter_model->insert('',$datas,TRUE))
			        	{
			        		throw new Exception('Data <b>berhasil</b> tersimpan.');
			        	}
			        	else
			        	{
			        		$error = 1;
			        		throw new Exception('Data <b>gagal</b> tersimpan.');
			        	}
	        		}else{
	        			$error = 1;
			        	throw new Exception('Data <b>tidak bisa</b> tersimpan.');
	        		}
	        	}else{
	        		$error = 1;
		        	throw new Exception('Poliklinik dan Dokter <b>tidak boleh kosong</b>.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
        $post = $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        
        $rules 	= $this->Jadwaldokter_model->rules;
        $this->form_validation->set_rules($rules);

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);

	        	if(!empty($post['kd_poli']) && !empty($post['kd_dokter'])){
	        		if(isset($post['jam_mulai'])){
	        			foreach ($post['jam_mulai'] as $k => $v) {
							if(!empty($v)){
								$datas = [
									'jam_mulai' => isset($v) ? date('H:i:s',strtotime($v)) : '',
									'jam_selesai' => isset($post['jam_selesai'][$k]) ? date('H:i:s',strtotime($post['jam_selesai'][$k])) : ''
								];

								$this->Jadwaldokter_model->update('',$datas,['kd_poli' => $post['kd_poli'],'kd_dokter'=>$post['kd_dokter'],'hari_kerja'=>$post['hari_kerja'][$k]]);
								
							}
						}
						throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        		}else{
	        			$error = 1;
			        	throw new Exception('Data <b>tidak bisa</b> tersimpan.');
	        		}
	        	}else{
	        		$error = 1;
		        	throw new Exception('Poliklinik dan Dokter <b>tidak boleh kosong</b>.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $exp1 	= explode('del',$segs[7]);

        foreach ($exp1 as $key => $value) {
			$exp2	= explode('P', $value);

			$where = [
				'kd_dokter' => $exp2[0],
				'kd_poli' => $exp2[1],
				'hari_kerja' => $exp2[2]
			];

			if(!$this->Jadwaldokter_model->delete('',$where))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b>gagal</b> dihapus';
			}
			else
			{
				$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data <b>berhasil</b> dihapus.';
			}
        } 
    	
    	//echo base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

    public function get_jadwal(){
    	$post = _post();
    	$datas = [];
    	$res = $this->Jadwaldokter_model->getJadwal(['jadwal.kd_poli'=>$post['kd_poli'],'jadwal.kd_dokter'=>$post['kd_dokter']]);

    	$empty['jam_mulai'] = '<input type="time" name="jam_mulai[]" class="form-control">';
		$empty['jam_selesai'] = '<input type="time" name="jam_selesai[]" class="form-control">';
		/*$empty['nm_dokter'] = $this->DataDokter();
		$empty['nm_poli'] = $this->DataPoli();*/

    	$html = '<table class="table table-hover table-striped">';
    		$html .= '<thead class="l-blush text-white">';
    			$html .= '<tr>';
    				$html .= '<th>Hari</th>';
    				$html .= '<th class="text-center">Jam Mulai</th>';
    				$html .= '<th class="text-center">Jam Selesai</th>';
    			$html .= '</tr>';
    		$html .= '</thead>';
    		$html .= '<tbody>';
	    	if($res){
	    		foreach ($res as $k => $v) {
		    		$datas[$v['hari_kerja']] = $v;
		    	}

		    	foreach ($Cf->dayschedule as $k => $v) {
		    		$empty['hari_kerja'] = strtoupper($k).'<input type="hidden" name="hari_kerja[]" class="form-control" value="'.strtoupper($k).'">';
		    		$data[strtoupper($k)] = !empty($datas[strtoupper($v)]) ? $datas[strtoupper($v)] : $empty;
		    	}

	    		foreach ($data as $k => $v) {
	    			$html .= '<tr>';
	    				$html .= '<td>'.$v['hari_kerja'].'</td>';
	    				$html .= '<td class="text-center">'.$v['jam_mulai'].'</td>';
	    				$html .= '<td class="text-center">'.$v['jam_selesai'].'</td>';
	    			$html .= '</tr>';
	    		}
	    	}else{
		    	foreach ($this->_cf->dayschedule as $k => $v) {
		    		$empty['hari_kerja'] = strtoupper($k);
		    		$data[strtoupper($k)] = !empty($datas[strtoupper($v)]) ? $datas[strtoupper($v)] : $empty;
		    	}

	    		foreach ($data as $k => $v) {
	    			$html .= '<tr>';
	    				$html .= '<td><input type="hidden" name="hari_kerja[]" class="form-control" value="'.$k.'">'.$k.'</td>';
	    				$html .= '<td class="text-center">'.$v['jam_mulai'].'</td>';
	    				$html .= '<td class="text-center">'.$v['jam_selesai'].'</td>';
	    			$html .= '</tr>';
	    		}
	    	}
    		$html .= '</tbody>';
	    $html .= '</table>';

    	echo $html;
    }

    public function edit_jadwal(){
    	$post = _post();
    	$datas = [];
    	$res = $this->Jadwaldokter_model->getJadwal(['jadwal.kd_poli'=>$post['kd_poli'],'jadwal.kd_dokter'=>$post['kd_dokter']]);

    	$empty['jam_mulai'] 	= '';
		$empty['jam_selesai'] 	= '';
		/*$empty['nm_dokter'] = $this->DataDokter();
		$empty['nm_poli'] = $this->DataPoli();*/

    	$html = '<table class="table table-hover table-striped">';
    		$html .= '<thead class="l-blush text-white">';
    			$html .= '<tr>';
    				$html .= '<th>Hari</th>';
    				$html .= '<th class="text-center">Jam Mulai</th>';
    				$html .= '<th class="text-center">Jam Selesai</th>';
    			$html .= '</tr>';
    		$html .= '</thead>';
    		$html .= '<tbody>';
	    	if($res){
	    		foreach ($res as $k => $v) {
		    		$datas[$v['hari_kerja']] = $v;
		    	}

		    	foreach ($this->_cf->dayschedule as $k => $v) {
		    		$empty['hari_kerja'] = strtoupper($k);
		    		$data[strtoupper($k)] = !empty($datas[strtoupper($v)]) ? $datas[strtoupper($v)] : $empty;
		    	}

	    		foreach ($data as $k => $v) {
	    			$jam_mulai = !empty($v['jam_mulai']) ? "<input type='time' name='jam_mulai[]' class='form-control' value='".$v['jam_mulai']."'>" :  '<b>Tidak ada jadwal.</b>';

					$jam_selesai = !empty($v['jam_selesai']) ? "<input type='time' name='jam_selesai[]' class='form-control' value='".$v['jam_selesai']."'>" :  '<b>Tidak ada jadwal.</b>';

	    			$html .= '<tr>';
	    				$html .= '<td><input type="hidden" name="hari_kerja[]" class="form-control" value="'.$v['hari_kerja'].'">'.$v['hari_kerja'].'</td>';
	    				$html .= '<td class="text-center">'. $jam_mulai.'</td>';
	    				$html .= '<td class="text-center">'. $jam_selesai.'</td>';
	    			$html .= '</tr>';
	    		}
	    	}else{
		    	foreach ($this->_cf->dayschedule as $k => $v) {
		    		$empty['hari_kerja'] = strtoupper($k);
		    		$data[strtoupper($k)] = !empty($datas[strtoupper($v)]) ? $datas[strtoupper($v)] : $empty;
		    	}

	    		foreach ($data as $k => $v) {
	    			$jam_mulai = isset($v['jam_mulai']) ? date('H:i:s',strtotime($v['jam_mulai'])) : '';
	    			$jam_selesai = isset($v['jam_selesai']) ? date('H:i:s',strtotime($v['jam_selesai'])) : '';
	    			$html .= '<tr>';
	    				$html .= '<td><input type="hidden" name="hari_kerja[]" class="form-control" value="'.$k.'">'.$k.'</td>';
	    				$html .= '<td class="text-center">'.$jam_mulai.'</td>';
	    				$html .= '<td class="text-center">'.$jam_selesai.'</td>';
	    			$html .= '</tr>';
	    		}
	    	}
    		$html .= '</tbody>';
	    $html .= '</table>';

    	echo $html;
    }

    public function download(){
    	header('Content-Type: application/force-download');
		header("Content-Disposition: attachment; filename=jadwal_dokter_".date('dmyHis').".xls");
		header("Pragma: no-cache");
		header("Expires: 0");
    	$html = '<table class="table table-hover table-striped">';
    		$html .= '<thead class="l-blush text-white">';
    			$html .= '<tr>';
    				$html .= '<th>Hari</th>';
    				$html .= '<th>Nama Poliklinik</th>';
    				$html .= '<th>Nama Dokter</th>';
    				$html .= '<th class="text-center">Jam Mulai</th>';
    				$html .= '<th class="text-center">Jam Selesai</th>';
    			$html .= '</tr>';
    		$html .= '</thead>';
    		$html .= '<tbody>';
	    	foreach ($this->get_excel() as $k => $v) {
	    		$html .= '<tr>';
    				$html .= '<td>'.$v->hari_kerja.'</td>';
    				$html .= '<td>'.$v->nm_poli.'</td>';
    				$html .= '<td>'.$v->nm_dokter.'</td>';
    				$html .= '<td class="text-center">'.$v->jam_mulai.'</td>';
    				$html .= '<td class="text-center">'.$v->jam_selesai.'</td>';
    			$html .= '</tr>';
	    	}
	    	$html .= '</tbody>';
    	$html .= '</table>';

    	echo $html;
    }

    private function get_excel(){
    	$res = $this->Jadwaldokter_model->getData();
    	return $res;
    }

    private function DataDokter(){
    	$res_dokter = $this->Dokter_model->get('','kd_dokter,nm_dokter',['status!='=>0])->result_array();
		$split_dokter = splitRecursiveArray($res_dokter,'kd_dokter','nm_dokter');
		return select($split_dokter,'name="kd_dokter[]" class="form-control kd_dokter" style="width:100%;"',TRUE);
    }

    private function DataPoli(){
		$res_poli = $this->Poliklinik_model->get('','kd_poli,nm_poli')->result_array();
		$split_poli = splitRecursiveArray($res_poli,'kd_poli','nm_poli');

		return select($split_poli,'name="kd_poli[]" class="form-control kd_poli" style="width:100%;"',TRUE);
    }

    public function get_dokter(){
    	$res_dokter = $this->Dokter_model->get('','kd_dokter,nm_dokter',['status!='=>0])->result_array();
		$split_dokter = splitRecursiveArray($res_dokter,'kd_dokter','nm_dokter');
		return $split_dokter;
    }
}