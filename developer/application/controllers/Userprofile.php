<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Userprofile extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Detailusers_model','Position_model','Professional_model','State_model']);
		$this->site->is_logged_in();
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		global $Cf;
		$segs 		= $this->uri->segment_array();
		$this->site->is_access(acl($segs[2]),'view');
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $Cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['detail_users.deleted_at' => NULL,'detail_users.id_users'=>$this->session->userdata('id_user')];

		$res_data 	= $this->Detailusers_model->getData($where);
		$url = base_url($segs[1].'/'.$id_module.'/search=');

		// 
		$rposition = $this->Position_model->get('','id,name',['deleted_at' => NULL,'id'=>@$res_data[0]->id_position])->result_array();
		$position = splitRecursiveArray($rposition,'id','name');

		$rprofessional = $this->Professional_model->get('','id,name',['deleted_at' => NULL])->result_array();
		$professional = splitRecursiveArray($rprofessional,'id','name');

		$rstate = $this->State_model->get('','id,name',['deleted_at'=>NULL])->result_array();
		$dstate = splitRecursiveArray($rstate,'id','name'); // Negara

		$data = array(
			'title' => 'Profil Pengguna',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $Cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'state' => $dstate,
			'status_field' => $Cf->status_field,
			'status_marital' => $Cf->status_marital,
			'gender' => $Cf->gender,
			'position' => $position,
			'professional' => $professional,
			'religion' => $this->get_religion(),
			'bloodtype' => $this->get_bloodtype(),
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

    public function update()
    {
    	$segs = $this->uri->segment_array();
        $post = _post();
        $target_file = isset($_FILES["upload_image"]["name"]) ? $_FILES["upload_image"]["name"] :'';
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $file   = $this->session->userdata('username');
        $error 	= '';
        $url 	= base_url($segs[1].'/'.$segs[2].'/'.$segs[3].'/'.$segs[4].'/'.$segs[5]);
        $id 	= isset($post['id']) ? $post['id'] : '';
        $post['updated_at'] = date('Y-m-d H:i:s');
        
        /*echo '<pre>';
        print_r($post);*/
        if($_FILES['upload_image']['name'])
        {
        	unlink($this->_folder_creator().'/'.$file.'.'.$imageFileType);
        	$this->_uploadImage($file);
        	$post['upload_image'] = $file.'.'.$imageFileType;
        }
        
        try {
        	unset($post['id']);
        	if($this->Detailusers_model->update('',$post,array('id_detail_user' => $id)))
        	{
        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
        	}
        	else
        	{
        		$error = 1;
        		throw new Exception('Data <b>gagal</b> diperbaharui.');
        	}

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        redirect($url);
    }

    public function destroy()
    {
        $segs 		= $this->uri->segment_array();
        $explode 	= explode('del',$segs[7]);
        //print_r($explode);
        foreach ($explode as $key => $value) {
        	if($this->Categoryallergy_model->delete('',array('id'=>$value))){
        		$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data telah berhasil dihapus';
        	}else{
        		$_SESSION['error'] = 1;
        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
        	} 	
        } 
    	//print_r($explode);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/'.$segs[3].'/'.$segs[4].'/'.$segs[5]));
    }


    protected function _folder_creator()
    {
		$folder = FCPATH.'uploads/'.$this->session->userdata('username').'/img_profile';
		if (!is_dir($folder)) {
		    mkdir($folder,0777, true);
		}
        return $folder;        
    }

    private function _uploadImage($file)
	{
		$path = $this->_folder_creator();
	    $config['upload_path']          = $path.'/';
	    $config['allowed_types']        = 'gif|GIF|jpg|JPG|png|PNG';
	    $config['file_name']            = $file;
	    $config['overwrite']			= true;
	    $config['max_size']             = 1024; // 1MB
	    // $config['max_width']            = 1024;
	    // $config['max_height']           = 768;

	    $this->load->library('upload', $config);

	    if ($this->upload->do_upload('upload_image')) {
	        return $this->upload->data("file_name");
	    }
	    
	    return "default.jpg";
	}

    private function get_religion()
    {
    	$this->load->model('Religion_model');
    	$res = $this->Religion_model->get('','id,name',['deleted_at'=>NULL])->result_array();
    	return splitRecursiveArray($res,'id','name');
    }

    private function get_bloodtype()
    {
    	$this->load->model('Bloodtype_model');
    	$res = $this->Bloodtype_model->get('','id,name',['deleted_at'=>NULL])->result_array();
    	return splitRecursiveArray($res,'id','name');
    }
}