<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categoryprofesi extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Categoryprofesi_model');
		$this->site->is_logged_in();
	}

	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	    default:
	      show_404();
	      break;
	  }
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['deleted_at' => NULL];

		// Cek akses untuk fitur pencarian
		if(@acl($segs[2])['search']=='Y')
		{
			if($search)
			{
				$where = 'name like \'%'.$search.'%\' and deleted_at IS NULL';
			}
		}

		$res_count	= $this->Categoryprofesi_model->count('',$where);
		$res_data 	= $this->Categoryprofesi_model->getData($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);
		
		$data = array(
			'title' => 'Halaman Data Master Kategori Profesi',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status_field' => $this->_cf->status_field,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan form pengkajian pasien.
	* Menampilkan data riwayat kunjungan pasien.
	* Menampilkan 
	* @ second load method 
	*/

	/*public function create()
	{

	}*/

	public function store()
    {
        $post 	= _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Categoryprofesi_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);
	        	if($this->Categoryprofesi_model->insert('',$post))
	        	{
	        		throw new Exception('Data <b>berhasil</b> tersimpan.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> tersimpan.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
        $post = _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        $post['updated_at'] = date('Y-m-d H:i:s');
        $rules 	= $this->Categoryprofesi_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);
	        	if($this->Categoryprofesi_model->update('',$post,array('id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);
        //print_r($explode);
        foreach ($explode as $key => $value) {
        	if($this->Categoryprofesi_model->delete('',array('id'=>$value))){
        		$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data telah berhasil dihapus';
        	}else{
        		$_SESSION['error'] = 1;
        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
        	} 	
        } 
    	//print_r($explode);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

}