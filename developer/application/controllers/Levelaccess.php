<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Levelaccess extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Levelaccess_model','Modules_model']);
		$this->site->is_logged_in();
	}

  public function _remap()
  {
    $segs = $this->_segs;
    $method = isset($segs[6]) ? $segs[6] : '';
    switch ($method) {
      case null;
      case false;
      case '':
        $this->site->is_access(acl(decrypt_aes($segs[2])),'view');
          $this->index();
          break;
      case 'search':
        $this->site->is_access(acl(decrypt_aes($segs[2])),'search');
          $this->search();
          break;
      case 'store':
        $this->site->is_access(acl(decrypt_aes($segs[2])),'add');
          $this->store();
          break;
      case 'update':
        $this->site->is_access(acl(decrypt_aes($segs[2])),'update');
          $this->update();
          break;
      case 'edit':
        $this->site->is_access(acl(decrypt_aes($segs[2])),'update');
          $this->edit();
          break;
      case 'destroy':
        $this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
        $this->destroy();
        break;
      case 'print':
        $this->site->is_access(acl(decrypt_aes($segs[2])),'print');
        $this->print();
        break;
      default:
        show_404();
        break;
    }
  }
	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= decodeUrl($segs[3]);

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['deleted_at' => NULL];

		// Cek akses untuk fitur pencarian
		if(@acl($segs[2])['search']=='Y')
		{
			if($search)
			{
				$where = 'name like \'%'.$search.'%\' and deleted_at IS NULL';
			}
		}

		$res_count	= $this->Levelaccess_model->count('',$where);
		$res_data 	= $this->Levelaccess_model->getData($where,$limit,$limit_start);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		/*
		Data pendukung
		*/
		$rmodules = $this->Modules_model->get('','*',array('deleted_at'=>NULL,'status'=>'Y'),'','','module_id ASC')->result_array();	
		$result = buildTree($rmodules,0);

		
		/*echo '<pre>';
		print_r($result);
		exit();*/
		$data = array(
			'title' => 'Halaman Data Master Level Akses',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'modules' => $result,
			'status_field' => $this->_cf->status_field,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan form pengkajian pasien.
	* Menampilkan data riwayat kunjungan pasien.
	* Menampilkan 
	* @ second load method 
	*/

	/*public function create()
	{

	}*/

	public function store()
  {
        $post 	= $this->input->post();
        $error 	= '';
        $post['access'] = serialize($post['akses']);
        
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Levelaccess_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);
        		unset($post['akses']);
	        	if($this->Levelaccess_model->insert('',$post))
	        	{
	        		throw new Exception('Data <b>berhasil</b> tersimpan.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> tersimpan.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
    		$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
  }

    /*public function show($id)
    {
        //
    }*/

    public function edit()
    {
        $id = $this->input->post('id');
        $res = $this->Levelaccess_model->get('','access',['id'=>$id])->result_array();
        $akses = unserialize($res[0]['access']);

        $rmodules = $this->Modules_model->get('','*',array('deleted_at'=>NULL,'status'=>'Y'),'','','module_id ASC')->result_array();	
		    $result = buildTree($rmodules,0);
        /*echo '<pre>';
		    print_r($result);*/
		    foreach ($result as $k => $v) {
           echo '<div class="form-group row">';
           echo '<label class="col-sm-3"><strong>'.$v['name_module'].'</strong></label>';
           if($v['view'] == 'Y')
              {
                echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][view]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['view'])
                {
                	echo ' checked';
                }
                echo '> &nbsp;</span>';
              }

              if($v['add'] == 'Y')
              {
                echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][add]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['add'])
                {
                	echo ' checked';
                }
                echo '> </span>';
              }

              if($v['update'] == 'Y')
              {
                echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][update]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['update'])
                {
                	echo ' checked';
                }
                echo '> </span>';
              }

              if($v['delete'] == 'Y')
              {
                echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][delete]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['delete'])
                {
                	echo ' checked';
                }
                echo '> </span>';
              }

              if($v['search'] == 'Y')
              {
                echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][search]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['search'])
                {
                	echo ' checked';
                }
                echo '> </span>';
              }

              if($v['filter'] == 'Y')
              {
                echo '<span>Filter &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][filter]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['filter'])
                {
                  echo ' checked';
                }
                echo '> </span>';
              }

              if($v['upload'] == 'Y')
              {
                echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][upload]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['upload'])
                {
                	echo ' checked';
                }
                echo '> </span>';
              }

              if($v['download'] == 'Y')
              {
                echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][download]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['download'])
                {
                	echo ' checked';
                }
                echo '> </span>';
              } 

              if($v['print'] == 'Y')
              {
                echo '<span>Print &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][print]" value="Y" data-val="Y" ';
                if(@$akses[$v['module_id']]['print'])
                {
                  echo ' checked';
                }
                echo '> </span>';
              }
              

           echo '<div class="col-sm-9">';
            if(@$v['children'])
            {
              foreach (@$v['children'] as $k1 => $v1) {
                  echo '<label><strong>'.$v1['name_module'].'</strong></label><br>';
                  if(@$v2['children'])
                  {
                    foreach (@$v1['children'] as $k2 => $v2) {
                      if($v2['view'] == 'Y')
                      {
                        echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][view]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['view'])
                        {
                        	echo ' checked';
                        }
                        echo '> &nbsp;</span>';
                      }

                      if($v2['add'] == 'Y')
                      {
                        echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][add]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['add'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v2['update'] == 'Y')
                      {
                        echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][update]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['update'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v2['delete'] == 'Y')
                      {
                        echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][delete]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['delete'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v2['search'] == 'Y')
                      {
                        echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][search]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['search'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v2['filter'] == 'Y')
                      {
                        echo '<span>Filter &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][filter]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['filter'])
                        {
                          echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v2['upload'] == 'Y')
                      {
                        echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][upload]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['upload'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v2['download'] == 'Y')
                      {
                        echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][download]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['download'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v2['print'] == 'Y')
                      {
                        echo '<span>Print &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][print]" value="Y" data-val="Y" ';
                        if(@$akses[$v2['module_id']]['print'])
                        {
                          echo ' checked';
                        }
                        echo '> </span>';
                      }  
                      echo '<br>';                   
                    }
                    
                  }
                  else
                  {
                    if($v1['view'] == 'Y')
                      {
                        echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][view]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['view'])
                        {
                        	echo ' checked';
                        }
                        echo '> &nbsp;</span>';
                      }

                      if($v1['add'] == 'Y')
                      {
                        echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][add]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['add'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v1['update'] == 'Y')
                      {
                        echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][update]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['update'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v1['delete'] == 'Y')
                      {
                        echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][delete]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['delete'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v1['search'] == 'Y')
                      {
                        echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][search]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['search'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v1['filter'] == 'Y')
                      {
                        echo '<span>Filter &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][filter]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['filter'])
                        {
                          echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v1['upload'] == 'Y')
                      {
                        echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][upload]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['upload'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v1['download'] == 'Y')
                      {
                        echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][download]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['download'])
                        {
                        	echo ' checked';
                        }
                        echo '> </span>';
                      }

                      if($v1['print'] == 'Y')
                      {
                        echo '<span>Print &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][print]" value="Y" data-val="Y" ';
                        if(@$akses[$v1['module_id']]['print'])
                        {
                          echo ' checked';
                        }
                        echo '> </span>';
                      } 
                    echo '<br>';
                  }                 
                echo '<br>';
              }
            }
           echo '</div>';
           echo '</div>';
        }
    }

    public function update()
    {
        $post = $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        $post['access'] = serialize($post['akses']);
        $post['updated_at'] = date('Y-m-d H:i:s');
        $rules 	= $this->Levelaccess_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);
	        	unset($post['akses']);

	        	if($this->Levelaccess_model->update('',$post,array('id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);
        //print_r($explode);
        foreach ($explode as $key => $value) {
        	if($this->Levelaccess_model->update('',array('deleted_at'=>date('Y-m-d H:i:s')),array('id'=>$value))){
        		$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data telah berhasil dihapus';
        	}else{
        		$_SESSION['error'] = 1;
        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
        	} 	
        } 
    	//print_r($explode);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/'.$segs[3].'/'.$segs[4].'/1'));
    }

}