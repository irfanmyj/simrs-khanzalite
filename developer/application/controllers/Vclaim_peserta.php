<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Vclaim_peserta extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->site->is_logged_in();
		$this->load->library(['Bpjs']);
	}

	public function _remap()
	{
		$segs = $this->_segs;
		$method = isset($segs[6]) ? $segs[6] : '';
		switch ($method) {
		  	case null;
		  	case false;
		  	case '':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		      	$this->index();
		      break;
		  	case 'get_peserta':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		      	$this->search();
		      break;
		  	case 'store':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
		      	$this->store();
		      break;
		  	case 'update':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->update();
		      break;
		  	case 'edit':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->edit();
		      break;
		  	case 'destroy':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
		    	$this->destroy();
		    break;
		  	case 'print':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
		    	$this->print();
		    break;
		    case 'checking_username':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		    	$this->checking_username();
		    break;
		  default:
		    	show_404();
		    break;
		}
	}

	public function get_peserta(){
		$post = _post();
		$aksi = 'kartu';
		
		$res = $this->bpjs->Peserta($post['no_kartu'],date('Y-m-d'),$aksi);
		$data['nama'] = $res['response']['peserta']['nama'];
		$data['nik'] = $res['response']['peserta']['nik'];
		$data['tgl_lahir'] = $res['response']['peserta']['tglLahir'];
		$data['no_telp'] = $res['response']['peserta']['mr']['noTelepon'];
		echo '<pre>';
		print_r($data);
	}
}