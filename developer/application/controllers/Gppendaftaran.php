<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gppendaftaran extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model([
			'Gppendaftaran_model',
			'Poliklinik_model',
			'Penjab_model',
			'Sukubangsa_model',
			'Bahasapasien_model',
			'Cacatfisik_model',
			'Kecamatan_model',
			'Kelurahan_model',
			'Kabupaten_model',
			'Provinsi_model',
			'Pasien_model',
			'Jadwal_model',
			'Holidayscalendar_model',
			'Bookingregistrasi_model',
			'Bridgingsep_model',
			'Kamar_model',
			'Masterpenyakit_model',
			'Masterkamarinap_model'
		]);
		$this->load->library(['Dukcapil','Bpjs']);
		$this->site->is_logged_in();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function _remap()
	{
	  $segs = $this->uri->segment_array();
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'getpasien':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->getpasien();
	    	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'storepasien':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->storepasien();
	      	break;
	    case 'storepasienigd':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->storepasienigd();
	      	break;
	    case 'createsep':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->createsep();
	      	break;
	    case 'stroretrftranap':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->stroretrftranap();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'updatesep':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->updatesep();
	      	break;
	    case 'updatetglsep':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->updatetglsep();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'deletesep':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->deletesep();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	    case 'cetaksep':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->cetaksep();
	    	break;
	    case 'getDatas':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getDatas();
	    	break;
	    case 'get_normmedic':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_normmedic();
	    	break;
	    case 'getapi':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getapi();
	    	break;
	    case 'get_kab':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_kab();
	    	break;
	    case 'get_kec':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_kec();
	    	break;
	    case 'get_kel':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_kel();
	    	break;
	    case 'checkNik':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->checkNik();
	    	break;
	    case 'getInfoKartu':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getInfoKartu();
	    	break;
	    case 'get_poli':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_poli();
	    	break;
	    case 'get_dokter':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_dokter();
	    	break;
	    case 'get_validasi':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_validasi();
	    	break;
	    case 'cekRujukan':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->cekRujukan();
	    	break;
	    case 'getdiagnosabpjs':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getdiagnosabpjs();
	    	break;
	    case 'getdiagnosa':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getdiagnosa();
	    	break;
	    case 'getpolibpjs':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getpolibpjs();
	    	break;
	    case 'getdokterdpjpbpjs':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getdokterdpjpbpjs();
	    	break;
	    case 'getKabBpjs':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getKabBpjs();
	    	break;
	    case 'getKecBpjs':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getKecBpjs();
	    	break;
	    case 'getbarcode':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getbarcode();
	    	break;
	    case 'getBridgingSep':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getBridgingSep();
	    	break;
	    case 'getDataBooking':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->getDataBooking();
	    	break;
	    case 'kamarinap':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->kamarinap();
	    	break;
	    default:
	      show_404();
	      break;
	  }
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->uri->segment_array();
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['reg_periksa.tgl_registrasi' => date('Y-m-d')];

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{	
				if(is_numeric($search)){
					$where = 'reg_periksa.no_rkm_medis=\''.$search.'\'';
				}else if(is_string($search)){
					$where = 'pasien.nm_pasien=\''.$search.'\'';
				}
			}
		}

		$res_data 	= $this->Gppendaftaran_model->getData($where,$limit,$limit_start);
		$res_count	= $this->Gppendaftaran_model->countData($where);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		/*
		| Area Get data poliklinik
		*/
		$respoli = $this->Poliklinik_model->get('','kd_poli,nm_poli',['status'=>"1"])->result_array();
		$splitpoli = splitRecursiveArray($respoli,'kd_poli','nm_poli');

		/*
		| Area Get data penjamin
		*/
		$splitpenjab = $this->penjab();

		/*
		| Area Get data Suku Bangsa
		*/
		$splitsukubangsa = $this->sukubangsa();

		/*
		| Area Get data Bahasa Pasien
		*/
		$splitbahasapasien = $this->bahasapasien();

		/*
		| Area Get data Cacat Fisik
		*/
		$splitcacatfisik = $this->cacatfisik();

		/*
		| Area Get data Provinsi
		*/
		$splitprovinsi = $this->getProvinsi();
		$splitprovinsibpjs = $this->getProvinsiBpjs();

		$dokter = splitRecursiveArray($this->functionother->get_dokter(),'kd_dokter','nm_dokter');

		$api = ['dukcapil'=>'Dukcapil','bpjs' => 'Bpjs'];

		unset($_SESSION['poliklinik']);
		unset($_SESSION['dokter']);
		unset($_SESSION['cara_bayar']);
		unset($_SESSION['no_rkm_medis']);
		unset($_SESSION['nm_pasien']);
		unset($_SESSION['tgl_awal']);
		unset($_SESSION['tgl_akhir']);
		unset($_SESSION['jk']);

		/*echo '<pre>';
		print_r();
		exit();*/
		$data = array(
			'title' => 'Halaman Registrasi Pasien',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status_field' => $this->_cf->status_field,
			'golongan_darah' => $this->_cf->_golongan_darah,
			'pendidikan' => $this->_cf->_pendidikan,
			'agama' => $this->_cf->_agama,
			'sts_nikah' => $this->_cf->_sts_nikah,
			'status_family' => $this->_cf->status_family,
			'bpjs' => $this->_cf->_bpjs,
			'poliklinik' => $splitpoli,
			'penjab' => $splitpenjab,
			'sukubangsa' => $splitsukubangsa,
			'bahasapasien' => $splitbahasapasien,
			'cacatfisik' => $splitcacatfisik,
			'provinsi' => $splitprovinsi,
			'provinsibpjs' => $splitprovinsibpjs,
			'dokter' => $dokter,
			'api' => $api,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan form pengkajian pasien.
	* Menampilkan data riwayat kunjungan pasien.
	* Menampilkan 
	* @ second load method 
	*/

	public function search()
	{
		$post = _post();
		$this->session->set_userdata($post);
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= [];

		if(!empty($post['poliklinik']) || !empty($_SESSION['poliklinik'])){
			$where['reg_periksa.kd_poli'] = isset($post['poliklinik']) ? $post['poliklinik'] : $_SESSION['poliklinik'];
		}

		if(!empty($post['dokter']) || !empty($_SESSION['dokter'])){
			$where['reg_periksa.kd_dokter'] = isset($post['dokter']) ? $post['dokter'] : $_SESSION['dokter'];
		}

		if(!empty($post['cara_bayar']) || !empty($_SESSION['cara_bayar'])){
			$where['reg_periksa.kd_pj'] = isset($post['cara_bayar']) ? $post['cara_bayar'] : $_SESSION['cara_bayar'];
		}

		if(!empty($post['no_rkm_medis']) || !empty($_SESSION['no_rkm_medis'])){
			$where['reg_periksa.no_rkm_medis'] = isset($post['no_rkm_medis']) ? $post['no_rkm_medis'] : $_SESSION['no_rkm_medis'];
		}

		if(!empty($post['nm_pasien']) || !empty($_SESSION['nm_pasien'])){
			$where['pasien.nm_pasien'] = isset($post['nm_pasien']) ? $post['nm_pasien'] : $_SESSION['nm_pasien'];
		}

		if(!empty($post['tgl_awal']) && !empty($post['tgl_akhir']) || !empty($_SESSION['tgl_awal']) && !empty($_SESSION['tgl_akhir'])){
			$where['reg_periksa.tgl_registrasi >= '] = isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_awal']));	
			$where['reg_periksa.tgl_registrasi <= '] = isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_akhir']));	
		}else if(!empty($post['tgl_awal']) && empty($post['tgl_akhir'])){
			$where['reg_periksa.tgl_registrasi'] = isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_awal']));	
		}else if(empty($post['tgl_awal']) && !empty($post['tgl_akhir'])){
			$where['reg_periksa.tgl_registrasi'] = isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_akhir']));
		}

		if(!empty($post['jk']) || !empty($_SESSION['jk'])){
			$where['pasien.jk'] = isset($post['jk']) ? $post['jk'] : $_SESSION['jk'];
		}

		$res_count	= $this->Gppendaftaran_model->countDataSearch($where);
		$res_data 	= $this->Gppendaftaran_model->getDataSearch($where,$limit,$limit_start);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination_search(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count,'search');

		/*
		| Area Get data poliklinik
		*/
		$respoli = $this->Poliklinik_model->get('','kd_poli,nm_poli',['status'=>"1"])->result_array();
		$splitpoli = splitRecursiveArray($respoli,'kd_poli','nm_poli');

		/*
		| Area Get data penjamin
		*/
		$splitpenjab = $this->penjab();

		/*
		| Area Get data Suku Bangsa
		*/
		$splitsukubangsa = $this->sukubangsa();

		/*
		| Area Get data Bahasa Pasien
		*/
		$splitbahasapasien = $this->bahasapasien();

		/*
		| Area Get data Cacat Fisik
		*/
		$splitcacatfisik = $this->cacatfisik();

		/*
		| Area Get data Provinsi
		*/
		$splitprovinsi = $this->getProvinsi();

		$dokter = splitRecursiveArray($this->functionother->get_dokter(),'kd_dokter','nm_dokter');

		$api = ['dukcapil'=>'Dukcapil','bpjs' => 'Bpjs'];

		$data = array(
			'title' => 'Halaman Registrasi Pasien',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status_field' => $this->_cf->status_field,
			'golongan_darah' => $this->_cf->_golongan_darah,
			'pendidikan' => $this->_cf->_pendidikan,
			'agama' => $this->_cf->_agama,
			'sts_nikah' => $this->_cf->_sts_nikah,
			'status_family' => $this->_cf->status_family,
			'poliklinik' => $splitpoli,
			'penjab' => $splitpenjab,
			'sukubangsa' => $splitsukubangsa,
			'bahasapasien' => $splitbahasapasien,
			'cacatfisik' => $splitcacatfisik,
			'provinsi' => $splitprovinsi,
			'dokter' => $dokter,
			'api' => $api,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	public function download(){
		$post = _post();

		if(!empty($post['poliklinik']) || !empty($_SESSION['poliklinik'])){
			$where['reg_periksa.kd_poli'] = isset($post['poliklinik']) ? $post['poliklinik'] : $_SESSION['poliklinik'];
		}

		if(!empty($post['dokter']) || !empty($_SESSION['dokter'])){
			$where['reg_periksa.kd_dokter'] = isset($post['dokter']) ? $post['dokter'] : $_SESSION['dokter'];
		}

		if(!empty($post['cara_bayar']) || !empty($_SESSION['cara_bayar'])){
			$where['reg_periksa.kd_pj'] = isset($post['cara_bayar']) ? $post['cara_bayar'] : $_SESSION['cara_bayar'];
		}

		if(!empty($post['no_rkm_medis']) || !empty($_SESSION['no_rkm_medis'])){
			$where['reg_periksa.no_rkm_medis'] = isset($post['no_rkm_medis']) ? $post['no_rkm_medis'] : $_SESSION['no_rkm_medis'];
		}

		if(!empty($post['nm_pasien']) || !empty($_SESSION['nm_pasien'])){
			$where['pasien.nm_pasien'] = isset($post['nm_pasien']) ? $post['nm_pasien'] : $_SESSION['nm_pasien'];
		}

		if(!empty($post['tgl_awal']) && !empty($post['tgl_akhir']) || !empty($_SESSION['tgl_awal']) && !empty($_SESSION['tgl_akhir'])){
			$where['reg_periksa.tgl_registrasi >= '] = isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_awal']));	
			$where['reg_periksa.tgl_registrasi <= '] = isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_akhir']));	
		}else if(!empty($post['tgl_awal']) && empty($post['tgl_akhir'])){
			$where['reg_periksa.tgl_registrasi'] = isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : date('Y-m-d',strtotime($_SESSION['tgl_awal']));	
		}else if(empty($post['tgl_awal']) && !empty($post['tgl_akhir'])){
			$where['reg_periksa.tgl_registrasi'] = isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : date('Y-m-d',strtotime($_SESSION['tgl_akhir']));
		}

		if(!empty($post['jk']) || !empty($_SESSION['jk'])){
			$where['pasien.jk'] = isset($post['jk']) ? $post['jk'] : $_SESSION['jk'];
		}

		$res_count	= $this->Gppendaftaran_model->countDataSearch($where);
		$res_data 	= $this->Gppendaftaran_model->getDataSearch($where);

		header('Content-Type: application/force-download');
		header("Content-Disposition: attachment; filename=pasien_regperiksa".date('dmyHis').".xls");
		header("Pragma: no-cache");
		header("Expires: 0");

		$html = '<table class="table table-hover table-striped">';
			$html .= '<thead class="l-blush text-white">';
			    $html .= '<tr>';
			      $html .= '<th style="width: 30px;" class="text-center">No</th>';
			      $html .= '<th>No Reg</th>';
			      $html .= '<th>No Rekam Medis</th>';
			      $html .= '<th>Nama Pasien</th>';
			      $html .= '<th>Dokter</th>';
			      $html .= '<th>Poliklinik</th>';
			      $html .= '<th>Cara Bayar</th>';
			      $html .= '<th style="width: 200px;">Dibuat Tanggal</th>';
			    $html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			$no=1; 
			foreach ($res_data as $key => $val) {
				$html .= '<tr style="cursor:pointer">';
			      $html .= '<td>'.($no++).'</td>';
			      $html .= '<td>"'.$val->no_reg.'</td>';
			      $html .= '<td>"'.$val->no_rkm_medis.'</td>';
			      $html .= '<td>'.$val->nm_pasien.'</td>';
			      $html .= '<td>'.$val->nm_dokter.'</td>';
			      $html .= '<td>'.$val->nm_poli.'</td>';
			      $html .= '<td>'.$val->png_jawab.'</td>';
			      $html .= '<td>'.$val->tgl_registrasi.'</td>';
			    $html .= '</tr>';
			}
			$html .= '</tbody>';
		$html .= '</table>';
		echo $html;
	}

	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Pasien_model->rules;
        $this->form_validation->set_rules($rules);

        $nik = $this->_checkNik($post['nik']);

        $no_rkm_medis = json_decode($this->get_normmedic(true),TRUE)['no_rkm_medis'];
        if($post['no_rkm_medis']>=$no_rkm_medis){
        	$no_rkm_medis = $post['no_rkm_medis'];
        }

        $usia = getAge(date('Y-m-d'),$post['tgl_lahir'],['year','month','day']);
        
        $datas['no_rkm_medis'] = $no_rkm_medis;
        $datas['no_ktp'] = !empty($post['nik']) ? $post['nik'] : '';
        $datas['nm_pasien'] = !empty($post['nm_pasien']) ? $post['nm_pasien'] : '';
        $datas['tmp_lahir'] = !empty($post['tmp_lahir']) ? $post['tmp_lahir'] : '';
        $datas['tgl_lahir'] = !empty($post['tgl_lahir']) ? date('Y-m-d',strtotime($post['tgl_lahir'])) : '';       
        $datas['jk'] = !empty($post['jk']) ? $post['jk'] : '-';
        $datas['umur'] = $usia['year'] .' Th '. $usia['month'] .' Bln '. $usia['day'] .' Hari';
        $datas['gol_darah'] = !empty($post['gol_darah']) ? $post['gol_darah'] : '-';
        $datas['kd_pj'] = !empty($post['kd_pj']) ? $post['kd_pj'] : '-';
        $datas['no_peserta'] = !empty($post['no_peserta']) ? $post['no_peserta'] : '-';
        $datas['kd_prop'] = !empty($post['kd_prop']) ? $post['kd_prop'] : '99';
        $datas['kd_kab'] = !empty($post['kd_kab']) ? $post['kd_kab'] : '9999';
        $datas['kd_kec'] = !empty($post['kd_kec']) ? $post['kd_kec'] : '999999';
        $datas['kd_kel'] = !empty($post['kd_kel']) ? $post['kd_kel'] : '9999999999';
        $datas['nm_ibu'] = !empty($post['nm_ibu']) ? $post['nm_ibu'] : '-';
        $datas['pekerjaan'] = !empty($post['pekerjaan']) ? $post['pekerjaan'] : '-';
        $datas['pnd'] = !empty($post['pnd']) ? $post['pnd'] : '-';
        $datas['no_tlp'] = !empty($post['no_tlp']) ? $post['no_tlp'] : '-';
        $datas['email'] = !empty($post['email']) ? $post['email'] : '-';
        $datas['suku_bangsa'] = !empty($post['suku_bangsa']) ? $post['suku_bangsa'] : 1;
        $datas['bahasa_pasien'] = !empty($post['bahasa_pasien']) ? $post['bahasa_pasien'] : 5;
        $datas['cacat_fisik'] = !empty($post['cacat_fisik']) ? $post['cacat_fisik'] : 1;
        $datas['alamat'] = !empty($post['alamat']) ? $post['alamat'] : '-';
        $datas['perusahaan_pasien'] = !empty($post['perusahaan_pasien']) ? $post['perusahaan_pasien'] : '-';
        $datas['nip'] = !empty($post['nip']) ? $post['nip'] : '-';
        $datas['keluarga'] = !empty($post['keluarga']) ? $post['keluarga'] : '';
        $datas['namakeluarga'] = !empty($post['namakeluarga']) ? $post['namakeluarga'] : '';
        $datas['propinsipj'] = !empty($post['propinsipj']) ? $post['propinsipj'] : '99';
        $datas['kabupatenpj'] = !empty($post['kabupatenpj']) ? $post['kabupatenpj'] : '9999';
        $datas['kecamatanpj'] = !empty($post['kecamatanpj']) ? $post['kecamatanpj'] : '999999';
        $datas['kelurahanpj'] = !empty($post['kelurahanpj']) ? $post['kelurahanpj'] : '9999999999';
        $datas['alamatpj'] = !empty($post['alamatpj']) ? $post['alamatpj'] : '-';
        $datas['tgl_daftar'] = date('Y-m-d');
        
        /*echo '<pre>';
        print_r($nik);
        exit();*/
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);
	        	unset($post['nik']);
	        	if(!empty($nik[0]->no_ktp)){
	        		$error = 1;
	        		throw new Exception("Mohon maaf nik yang anda masukan sudah terdaftar.");
	        	}else{
	        		if(!$this->Pasien_model->insert('',$datas))
		        	{
		        		throw new Exception('Data <b>berhasil</b> tersimpan.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> tersimpan.');
		        	}
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
			$msg = $e->getMessage();
        }

        echo json_encode([
        	'url' => $url,
        	'error' => $error,
        	'msg' => $msg,
        	'no_rkm_medis' => $no_rkm_medis,
        	'keluarga' => $datas['keluarga'],
        	'namakeluarga' => $datas['namakeluarga'],
        	'stts_daftar' => (!empty($this->_stts_daftar($no_rkm_medis))) ? 'Lama' : 'Baru',
        	'alamatpj' => $datas['alamatpj'],
        	'nm_pasien' => $datas['nm_pasien'],
        	'tgl_lahir' => $datas['tgl_lahir']
        ]);
    }

    public function storepasien(){
    	$post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Gppendaftaran_model->rules_reg;
        $this->form_validation->set_rules($rules);
        $tanggal_periksa = date('Y-m-d',strtotime($post['tanggal_periksa']));
        $tgl_lahir = $post['tgl_lahir'];

        $post['no_reg'] = $this->_noreg($tanggal_periksa,$post['kd_poli'],$post['kd_dokter']);
        $post['jam_reg'] = date('H:i:s');
        $post['no_rawat'] = $this->functionother->getnorawatlast($tanggal_periksa);
        $post['tgl_registrasi'] = isset($tanggal_periksa) ? $tanggal_periksa : date('Y-m-d');
        $post['stts'] = 'Belum'; 
        $post['biaya_reg'] = 0; 
        $post['status_lanjut'] = 'Ralan'; 
        $post['umurdaftar'] = $this->_cekAge($tgl_lahir)['usia']; 
        $post['sttsumur'] = $this->_cekAge($tgl_lahir)['status']; 
        $post['status_bayar'] = 'Belum Bayar'; 
        $post['status_poli'] = $this->_stts_poli($post['no_rkm_medis'],$post['kd_poli'])['status']; 

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
		        unset($post['tgl_lahir']);
		        unset($post['url']);
		        unset($post['asal_rujukan']);
		        unset($post['tanggal_periksa']);
	        	if($this->_stts_poli($post['no_rkm_medis'],$post['kd_poli'])['status']==''){
	        		$error = 1;
	        		throw new Exception('Mohon maaf data tidak bisa diproses, karean status poli kosong.');
	        	}else{
	        		if(!$this->Gppendaftaran_model->insert('',$post))
		        	{
		        		throw new Exception('Data <b>berhasil</b> tersimpan.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> tersimpan.');
		        	}
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
			$msg = $e->getMessage();
        }

        echo json_encode([
        	'url' => $url,
        	'error' => $error,
        	'msg' => $msg
        ]);    
    }

    public function storepasienigd(){
    	$post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Gppendaftaran_model->rules_regigd;
        $this->form_validation->set_rules($rules);
        $tanggal_periksa = date('Y-m-d',strtotime($post['tanggal_periksa']));
        $tgl_lahir = $post['tgl_lahir'];

        $post['no_reg'] = $this->_noregigd($tanggal_periksa,$post['kd_dokter']);
        $post['jam_reg'] = date('H:i:s');
        $post['no_rawat'] = $this->functionother->getnorawatlast($tanggal_periksa);
        $post['tgl_registrasi'] = isset($tanggal_periksa) ? $tanggal_periksa : date('Y-m-d');
        $post['stts'] = 'Belum'; 
        $post['biaya_reg'] = 0; 
        $post['status_lanjut'] = 'Ralan'; 
        $post['umurdaftar'] = $this->_cekAge($tgl_lahir)['usia']; 
        $post['sttsumur'] = $this->_cekAge($tgl_lahir)['status']; 
        $post['status_bayar'] = 'Belum Bayar'; 
        $post['status_poli'] = $this->_stts_poli($post['no_rkm_medis'],'IGDK')['status']; 
        $post['kd_poli'] = 'IGDK';

        /*echo '<pre>';
        print_r($post);
        exit();*/
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
		        unset($post['tgl_lahir']);
		        unset($post['url']);
		        unset($post['asal_rujukan']);
		        unset($post['tanggal_periksa']);
	        	
	        	if($this->_stts_poli($post['no_rkm_medis'],'IGDK')['status']==''){
	        		$error = 1;
	        		throw new Exception('Mohon maaf data tidak bisa diproses, karean status poli kosong.');
	        	}else{
	        		if(!$this->Gppendaftaran_model->insert('',$post))
		        	{
		        		throw new Exception('Data <b>berhasil</b> tersimpan.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> tersimpan.');
		        	}
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
			$msg = $e->getMessage();
        }

        echo json_encode([
        	'url' => $url,
        	'error' => $error,
        	'msg' => $msg
        ]);    
    }

    public function stroretrftranap(){
    	$post = $this->input->post();
    	$error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Gppendaftaran_model->rules_trf_inap;
        //$status_lanjut = ;

        $this->form_validation->set_rules($rules);
        $post['tgl_masuk'] = !empty($post['tgl_masuk']) ? date('Y-m-d',strtotime($post['tgl_masuk'])) : date('Y-m-d');
        $post['jam_masuk'] = date('H:i:s');

        $transfer = $this->_updatetransfer_inap(['no_rawat'=>$post['no_rawat']],$post['status_lanjut']);

        $getkamar = $this->Masterkamarinap_model->get('','*',['no_rawat'=>$post['no_rawat'],'kd_kamar'=>$post['kd_kamar']],'','','',1)->result_array();

    	try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
		        unset($post['url']);
		        unset($post['no_rkm_medis']);
		        unset($post['status_lanjut']);
		        unset($post['nm_pasien']);

		        if($getkamar){
		        	$error = 1;
		        	throw new Exception('Mohon maaf pasien sudah terdaftar di kode kamar.'. $post['kd_kamar']);
		        }else{
		        	if($transfer['status']=='Not'){
		        		$error = 1;
		        		throw new Exception($transfer['data']);
		        	}else{
		        		if(!$this->Masterkamarinap_model->insert('',$post))
			        	{
			        		$this->_updatekamar(['kd_kamar'=>$post['kd_kamar']],'ISI');
			        		throw new Exception('Data <b>berhasil</b> tersimpan.');
			        	}
			        	else
			        	{
			        		$error = 1;
			        		throw new Exception('Data <b>gagal</b> tersimpan.');
			        	}
		        	}
		        }
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
			$msg = $e->getMessage();
        }

        echo json_encode([
        	'url' => $url,
        	'error' => $error,
        	'msg' => $msg
        ]);   
    }

    private function _cekAge($tgl_lahir=''){
    	$res = $this->_getAge($tgl_lahir);
    	if(!empty($res['year'])){
    		$data = ['usia'=>$res['year'],'status'=>'Th'];
    	}else if(!empty($res['month'])){
    		$data = ['usia'=>$res['month'],'status'=>'Bl'];
    	}else if(!empty($res['day'])){
    		$data = ['usia'=>$res['day'],'status'=>'Hr'];
    	}else if(!empty($res['hour'])){
    		$data = ['usia'=>1,'status'=>'Hr'];
    	}else{
    		$data = ['usia'=>1,'status'=>'Hr'];
    	}

    	return $data;
    }

    private function _noreg($tgl_registrasi='',$kd_poli='',$kd_dokter=''){
    	// Get No Reg Terbesar Pada tabel booking_registrasi
		$getNoReg1	= $this->Bookingregistrasi_model->get('','max(no_reg) as no_reg',['tanggal_periksa'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli])->result();

		// Get No Reg Terbesar Pada tabel reg_periksa
		$getNoReg2	= $this->Gppendaftaran_model->get('','max(no_reg) as no_reg',['tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli])->result();

		// Mengambil nomor registrasi yang paling terbesar dari kedua tabel reg_periksa dan booking_registrasi
		$NoReg 		= getNoReg($getNoReg1[0]->no_reg,$getNoReg2[0]->no_reg);
		$conNoReg 	= substr($NoReg, 0, 3);
		$NoRegNow	= sprintf('%03s', ($conNoReg + 1));
		return $NoRegNow;
    }

    private function _noregigd($tgl_registrasi='',$kd_dokter=''){
    	// Get No Reg Terbesar Pada tabel reg_periksa
		$getNoReg2	= $this->Gppendaftaran_model->get('','max(no_reg) as no_reg',['tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter])->result();

		if(!empty($getNoReg2[0]->no_reg)){
			$noreg = $getNoReg2[0]->no_reg;
		}else{
			$noreg = 000; 
		}

		$conNoReg 	= substr($noreg, 0, 3);
		$NoRegNow	= sprintf('%03s', ($conNoReg + 1));
		return $NoRegNow;
    }	

    private function _stts_poli($no_rkm_medis='',$kd_poli=''){
    	if(!empty($no_rkm_medis)){
    		$res = $this->Gppendaftaran_model->get('','*',['no_rkm_medis'=>$no_rkm_medis,'kd_poli'=>$kd_poli],'','','',1)->result();
    		if($res){
    			$data = ['status'=>'Lama'];
    		}else{
    			$data = ['status'=>'Baru'];
    		}
    	}else{
    		$data = ['status'=>''];
    	}
    	return $data;
    }

    public function update()
    {
        $post = _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        $rules 	= $this->Gppendaftaran_model->rules;
        $post['updated_at'] = date('Y-m-d H:i:s');
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);
	        	if($this->Gppendaftaran_model->update('',$post,array('id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    private function _updatetransfer_inap($where=[],$data=''){
    	if(is_array($where)){
    		$res = $this->Gppendaftaran_model->update('',['status_lanjut'=>$data],$where);
    		if($res){
    			$datas['status'] = 'Ok';
    			$datas['data'] = 'Sukses update';
    		}else{
    			$datas['status'] = 'Not';
    			$datas['data'] = 'Gagal update';
    		}
    	}else{
    		$datas['status'] = 'Not';
    		$datas['data'] = '';
    	}

    	return $datas;
    }

    private function _updatekamar($where=[],$data=''){
    	if(is_array($where)){
    		$res = $this->Kamar_model->update('',['status'=>$data],$where);
    		if($res){
    			$datas['status'] = 'Ok';
    			$datas['data'] = 'Sukses update';
    		}else{
    			$datas['status'] = 'Not';
    			$datas['data'] = 'Gagal update';
    		}
    	}else{
    		$datas['status'] = 'Not';
    		$datas['data'] = '';
    	}

    	return $datas;
    }

    public function destroy()
    {
        $segs 		= $this->uri->segment_array();
        $explode 	= explode('del',$segs[7]);
        print_r($explode);
        //print_r($explode);
        foreach ($explode as $key => $value) {
        	if($this->Gppendaftaran_model->delete('',array('no_rawat'=>str_replace('-', '/', $value)))){
        		$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data telah berhasil dihapus';
        	}else{
        		$_SESSION['error'] = 1;
        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
        	} 	
        } 
    	//print_r($explode);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

    public function getDatas(){
    	$list = $this->Gppendaftaran_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row['no'] = $no;
			$row['no_rkm_medis'] = $field->no_rkm_medis;
			$row['nm_pasien'] = $field->nm_pasien;
			$row['nm_dokter'] = $field->nm_dokter;
			$row['nm_poli'] = $field->nm_poli;
			$row['png_jawab'] = $field->png_jawab;
			$row['tgl_registrasi'] = date('d M Y',strtotime($field->tgl_registrasi));

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Gppendaftaran_model->count_all(),
			"recordsFiltered" => $this->Gppendaftaran_model->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
    }

    public function get_normmedic($cetak='')
    {
    	$data = 0;
    	if($this->functionother->getnormmedic()){
    		$data = $this->functionother->getnormmedic();
    	}

    	if($cetak){
    		return json_encode(['no_rkm_medis'=>$data]);
    	}else{
    		echo json_encode(['no_rkm_medis'=>$data]);
    	}
    	
    }

    public function getapi(){
    	$post = _post();
    	$nik = isset($post['nik']) ? $post['nik'] : '';
    	$cat = isset($post['getapi']) ? $post['getapi'] : '';

    	if($cat=='dukcapil'){
    		$postdukcapil = ['nik'=>$nik]+$this->_dukcapil;
			$res = $this->dukcapil->get($postdukcapil,'');
			
			$datas['msg'] = '';
			if($res['error']=='Connected'){
				$dukcapil = $res['data']['content'][0];
				$responcode = isset($dukcapil['RESPONSE_CODE']) ? $dukcapil['RESPONSE_CODE'] : '';
				if($responcode==''){
					
					if($dukcapil['STATUS_KAWIN']=='KAWIN'){
						$sts_nikah = 'MENIKAH';
					}else if($dukcapil['STATUS_KAWIN']=='BELUM KAWIN'){
						$sts_nikah = 'TIDAK MENIKAH';
					}else{
						$sts_nikah = '';
					}

					$kd_prop = $this->provinsi($dukcapil['PROP_NAME']);

					$alamat = $dukcapil['ALAMAT'] .", RT : ".$dukcapil['NO_RT'].", RW : ".$dukcapil['NO_RW'];

					$datas['api'] = [
						'nm_pasien' => $dukcapil['NAMA_LGKP'],
						'tgl_lahir' => $dukcapil['TGL_LHR'],
						'agama' 	=> $dukcapil['AGAMA'],
						'no_rw' 	=> $dukcapil['NO_RW'],
						'no_rt' 	=> $dukcapil['NO_RT'],
						'alamat'	=> $alamat,
						'provinsi' 	=> $this->provinsi($dukcapil['PROP_NAME']),
						'kab' 		=> $this->kabupaten($kd_prop,$dukcapil['KAB_NAME']),
						'kec' 		=> $dukcapil['KEC_NAME'],
						'kel' 		=> $dukcapil['KEL_NAME'],
						'pekerjaan' => $dukcapil['JENIS_PKRJN'],
						'tmp_lahir' => $dukcapil['TMPT_LHR'],
						'pnd'		=> $dukcapil['PDDK_AKH'],
						'sts_nikah' => $sts_nikah,
						'nm_ibu' 	=> $dukcapil['NAMA_LGKP_IBU'],
						'jk' 		=> ($dukcapil['JENIS_KLMIN']=='Laki-Laki') ? 'L' : 'P',
						'gol_darah' => ($dukcapil['GOL_DARAH']!='TIDAK TAHU') ? $dukcapil['GOL_DARAH'] : '-',
						'suku_bangsa' => 1,
						'bahasa_pasien' => 1,
						'cacat_fisik' => 1, 
						'penjab' => '-', 
						'kd_pj' => '-' 
						
					];
					$datas['status'] = 'active';
				}else{
					$datas['error'] = $res['error'];
					$datas['data'] = $dukcapil['RESPONSE_DESC'];
					$datas['status'] = 'expired';
				}
			}else{
				$datas['error'] = $res['error'];
				$datas['data'] = 'Mohon maaf system tidak bisa terhubung dengan server DUKCAPIL.';
				$datas['status'] = 'expired';
			}
    	}else if($cat=='bpjs'){
    		$post = ['nik'=>$nik];
			$res = $this->bpjs->Peserta($nik,date('Y-m-d'),'nik');

			$datas['msg'] = '';
			if($res['error']=='Connected'){
				$bpjs = $res['data']['response']['peserta'];
				$datas['api'] = [
						'nm_pasien' => ($bpjs['nama']) ? $bpjs['nama'] : 'Tidak ada.',
						'no_peserta' => $bpjs['noKartu'],
						'tgl_lahir' => $bpjs['tglLahir'],
						'no_telp' 	=> $bpjs['mr']['noTelepon'],
						'hakKelasKode' => $bpjs['hakKelas']['kode'],
						'hakKelasKet' => ($bpjs['hakKelas']['keterangan']) ? $bpjs['hakKelas']['keterangan'] : 'Tidak ada.',
						'stskode' 		=> $bpjs['statusPeserta']['kode'],
						'stsketerangan' => ($bpjs['statusPeserta']['keterangan']) ? $bpjs['statusPeserta']['keterangan'] : 'Tidak Terdaftar',
						'pekerjaan' => $bpjs['jenisPeserta']['keterangan'],
						'sts_nikah' => '',
						'jk' 		=> ($bpjs['sex']) ? ($bpjs['sex']=='L') ? 'L' : 'P' : '',
						'gol_darah' => '-',
						'suku_bangsa' => 1,
						'bahasa_pasien' => 1,
						'cacat_fisik' => 1, 
						'penjab' => '-',
						'kd_pj' => ($bpjs['nama']) ? 'BPJ' : '' 
					];
			}else{
				$datas['error'] = $res['error'];
				$datas['data'] = 'Mohon maaf system tidak bisa terhubung dengan server BPJS.';
				$datas['status'] = 'expired';
			}
    	}else{
    		$datas['error'] = 'error';
			$datas['data'] = 'Mohon maaf system tidak bisa terhubung dengan semua server.';
			$datas['status'] = 'expired';
    	}
    	
    	//print_r($datas);
		echo json_encode($datas);
    }

    private function provinsi($data=''){
    	$get = $this->Provinsi_model->get('','kd_prop',['nm_prop'=>$data],'','','',1)->result();
    	if($get){
    		$res = $get[0]->kd_prop;
    	}else{
    		$res = 99;
    	}
    	return $res;
    }

    private function kabupaten($kd_prop='',$data=''){

    	if(!empty($kd_prop) && !empty($data)){
    		$where = 'kd_prop=\''.$kd_prop.'\' and nm_kab LIKE \'%'.$data.'%\'';
    	}else{
    		$where = 'kd_prop=\''.$kd_prop.'\'';
    	}
    	
    	$get = $this->Kabupaten_model->get('','kd_kab,nm_kab as nama',$where,'','','')->result_array();
    	if(count($get) > 1){
    		$split = splitRecursiveArray($get,'kd_kab','nama');
    		$res = htmlSelectFromArray($split, 'name="kd_kab" id="kd_kab" class="form-control select2" style="width:100%;"', true);
    	}else{
    		$split = splitRecursiveArray($get,'kd_kab','nama');
    		$res = htmlSelectFromArray($split, 'name="kd_kab" id="kd_kab" class="form-control select2" style="width:100%;"', true);
    	}
    	return $res;
    }

    private function kabupatenpj($kd_prop='',$data=''){

    	if(!empty($kd_prop) && !empty($data)){
    		$where = 'kd_prop=\''.$kd_prop.'\' and nm_kab LIKE \'%'.$data.'%\'';
    	}else{
    		$where = 'kd_prop=\''.$kd_prop.'\'';
    	}
    	
    	$get = $this->Kabupaten_model->get('','kd_kab,nm_kab as nama',$where,'','','')->result_array();
    	if(count($get) > 1){
    		$split = splitRecursiveArray($get,'kd_kab','nama');
    		$res = htmlSelectFromArray($split, 'name="kabupatenpj" id="kabupatenpj" class="form-control select2" style="width:100%;"', true);
    	}else{
    		$split = splitRecursiveArray($get,'kd_kab','nama');
    		$res = htmlSelectFromArray($split, 'name="kabupatenpj" id="kabupatenpj" class="form-control select2" style="width:100%;"', true);
    	}
    	return $res;
    }

    public function get_kab(){
    	$post = $this->input->post();
    	if(@$post['pj']){
    		echo json_encode(['data'=>$this->kabupatenpj($post['kd_prop'])]);
    	}else{
    		echo json_encode(['data'=>$this->kabupaten($post['kd_prop'])]);
    	}
    	
    }

    private function kecamatan($kd_kab=''){
    	$where = '';
    	if(!empty($kd_kab)){
    		$where = 'kd_kab=\''.$kd_kab.'\'';
    	}

    	$get = $this->Kecamatan_model->get('','kd_kec,nm_kec as nama',$where,'','','')->result_array();
    	if(count($get) > 1){
    		$split = splitRecursiveArray($get,'kd_kec','nama');
    		$res = htmlSelectFromArray($split, 'name="kd_kec" id="kd_kec" class="form-control select2" style="width:100%;"', true);
    	}else{
    		$split = splitRecursiveArray($get,'kd_kec','nama');
    		$res = htmlSelectFromArray($split, 'name="kd_kec" id="kd_kec" class="form-control select2" style="width:100%;"', true,@$get[0]['kd_kec']);
    	}
    	return $res;
    }

    private function kecamatanpj($kd_kab=''){
    	$where = '';
    	if(!empty($kd_kab)){
    		$where = 'kd_kab=\''.$kd_kab.'\'';
    	}

    	$get = $this->Kecamatan_model->get('','kd_kec,nm_kec as nama',$where,'','','')->result_array();
    	if(count($get) > 1){
    		$split = splitRecursiveArray($get,'kd_kec','nama');
    		$res = htmlSelectFromArray($split, 'name="kecamatanpj" id="kecamatanpj" class="form-control select2" style="width:100%;"', true);
    	}else{
    		$split = splitRecursiveArray($get,'kd_kec','nama');
    		$res = htmlSelectFromArray($split, 'name="kecamatanpj" id="kd_kecamatanpjkec" class="form-control select2" style="width:100%;"', true,@$get[0]['kd_kec']);
    	}
    	return $res;
    }

    public function get_kec(){
    	$post = $this->input->post();
    	if(@$post['pj']){
    		echo json_encode(['data'=>$this->kecamatanpj($post['kd_kab'])]);
    	}else{
    		echo json_encode(['data'=>$this->kecamatan($post['kd_kab'])]);
    	}
    	
    }

    private function kelurahan($kd_kec=''){
    	$where = '';
    	if(!empty($kd_kec)){
    		$where = 'kd_kec=\''.$kd_kec.'\'';
    	}

    	$get = $this->Kelurahan_model->get('','kd_kel,nm_kel as nama',$where,'','','')->result_array();
    	if(count($get) > 1){
    		$split = splitRecursiveArray($get,'kd_kel','nama');
    		$res = htmlSelectFromArray($split, 'name="kd_kel" id="kd_kel" class="form-control select2" style="width:100%;"', true);
    	}else{
    		$split = splitRecursiveArray($get,'kd_kel','nama');
    		$res = htmlSelectFromArray($split, 'name="kd_kel" id="kd_kel" class="form-control select2" style="width:100%;"', true,@$get[0]['kd_kel']);
    	}
    	return $res;
    }

    private function kelurahanpj($kd_kec=''){
    	$where = '';
    	if(!empty($kd_kec)){
    		$where = 'kd_kec=\''.$kd_kec.'\'';
    	}

    	$get = $this->Kelurahan_model->get('','kd_kel,nm_kel as nama',$where,'','','')->result_array();
    	if(count($get) > 1){
    		$split = splitRecursiveArray($get,'kd_kel','nama');
    		$res = htmlSelectFromArray($split, 'name="kelurahanpj" id="kelurahanpj" class="form-control select2" style="width:100%;"', true);
    	}else{
    		$split = splitRecursiveArray($get,'kd_kel','nama');
    		$res = htmlSelectFromArray($split, 'name="kelurahanpj" id="kelurahanpj" class="form-control select2" style="width:100%;"', true,@$get[0]['kd_kel']);
    	}
    	return $res;
    }

    public function get_kel(){
    	$post = $this->input->post();
    	if(@$post['pj']){
    		echo json_encode(['data'=>$this->kelurahanpj($post['kd_kec'])]);
    	}else{
    		echo json_encode(['data'=>$this->kelurahan($post['kd_kec'])]);
    	}
    	
    }

    private function penjab(){
		return $this->functionother->penjab(true);
	}

	private function sukubangsa(){
		return $this->functionother->sukubangsa(true);
	}

	private function bahasapasien(){
		return $this->functionother->bahasapasien(true);
	}

	private function cacatfisik(){
		return $this->functionother->cacatfisik(true);
	}

	private function getProvinsi(){
		return $this->functionother->getProvinsi(true);
	}

	private function getProvinsiBpjs(){
		return $this->functionother->getProvinsiBpjs(true);
	}

	public function checkNik($nik='',$type='json'){
		$post = _post();
		if(@$post['nik']){
			$data = $this->_checkNik($post['nik']);
			if($data){
				if($type=='json'){
					echo json_encode($data[0]);
				}else{
					return $data;
				}
			}else{
				if($type=='json'){
					echo json_encode(['no_ktp'=>'']);
				}else{
					return ['no_ktp'=>''];
				}
			}
		}else{
			$data = $this->_checkNik($nik);
			if($data){
				if($type=='json'){
					echo json_encode($data[0]);
				}else{
					return $data;
				}
			}else{
				if($type=='json'){
					echo json_encode(['no_ktp'=>'']);
				}else{
					return ['no_ktp'=>''];
				}
			}
		}
	}

	private function _checkNik($nik=''){
		$res = $this->Pasien_model->get('','no_ktp',['no_ktp'=>$nik],'','','',1)->result();
		return $res;
	}

	public function getpasien(){
    	$this->_datapasien();
    }

    private function _datapasien(){
    	$search 	= $_GET['term'];
				
		if(isset($search))
		{
			$where = 'no_rkm_medis like \'%'.$search.'%\' OR nm_pasien like \'%'.$search.'%\'';
			$res = $this->functionother->get_tb_pasien($where);
			if(count($res) > 0)
			{
				$res_data = array();
				$no=0;
				foreach($res as $key => $value)
				{
					$tgl_lahir = (@$value->tgl_lahir!='0000-00-00') ? tanggal_indo($value->tgl_lahir, true) : '';
					$no++;
					$jk = ($value->jk=='L') ? 'Laki-laki' : 'Perempuan';
					$res_data[] = array(
						'id' => $value->no_rkm_medis,
						'value' => $value->no_rkm_medis.' - '. $value->nm_pasien . ' - '. $tgl_lahir .' - '. $jk,
						'keluarga' => $value->keluarga,
						'namakeluarga' => $value->namakeluarga,
						'kd_pj' => $value->kd_pj,
						'alamatpj' => $value->alamatpj,
						'stts_daftar' => (!empty($this->_stts_daftar($value->no_rkm_medis))) ? 'Lama' : 'Baru',
						'tgl_lahir' => $value->tgl_lahir 
					);
				}
			}
			else
			{
				$res_data[] = array(
					'id' => 'No',
					'value' => 'Data tidak ditemukan.',
					'keluarga' => '',
					'namakeluarga' => '',
					'kd_pj' => '',
					'alamatpj' => '',
					'stts_daftar' => ''
				);
			}	
			
		}
		echo json_encode($res_data);
    }

    private function _stts_daftar($no_rkm_medis=''){
    	if($no_rkm_medis){
    		$res = $this->Gppendaftaran_model->get('','stts_daftar',['no_rkm_medis'=>$no_rkm_medis],'','',1)->result();
    		if($res){
    			$data = $res[0]->stts_daftar;
    		}else{
    			$data = '';
    		}
    	}else{
    		$data = ''; 
    	}

    	return $data;
    }

    public function get_poli(){
    	return $this->_get_poli();
    }

    private function _get_poli(){
    	$post 			= _post();
    	if($post){
    		$where 			= ['hari_kerja'=>indoDays(date('Y-m-d',strtotime($post['tanggal_periksa'])))];
	    	$res_poli 		= $this->Jadwal_model->getData($where)->result_array();
			
			$data = [];
			if($res_poli){
				$poliklinik = splitRecursiveArray($res_poli,'kd_poli','nm_poli');
				$res = htmlSelectFromArray($poliklinik, 'name="kd_poli" id="kd_poli_reg" class="form-control input-lg" style="width:100%;"', true);
				$data = ['status'=>'Tersedia','data'=>$res];
			}else{
				$data = ['status'=>'Kosong','data'=>'Mohon maaf tidak terdapat jdwal pada hari '. indoDays($post['tanggal_periksa'])];
			}
    	}else{
    		$data = ['status'=>'Kosong','data'=>'Mohon maaf tanggal periksa masih kosong.'];
    	}

		echo json_encode($data);
    }

    public function get_dokter(){
    	return $this->_get_dokter();
    }

    private function _get_dokter(){
    	$post = $this->input->post();
    	
    	$where['hari_kerja'] = indoDays(date('Y-m-d'));

    	if(!empty($post['tanggal_periksa'])){
    		$where['hari_kerja'] = indoDays($post['tanggal_periksa']);
    	}
    	    	
    	if(!empty($post['kd_poli'])){
    		$where['kd_poli'] = isset($post['kd_poli']) ? $post['kd_poli'] : '';
    	}

    	$res_dokter 	= $this->Jadwal_model->getDokter($where)->result_array();
    	$splitdokter = [];
		if($res_dokter){
			$splitdokter 	= splitRecursiveArray($res_dokter,'kd_dokter','nm_dokter');
			$res =  htmlSelectFromArray($splitdokter, 'name="kd_dokter" id="kd_dokter" class="form-control input-lg"', true);
			$data = ['status'=>'Tersedia','data'=>$res];
		}else{
			$data = ['status'=>'Kosong','data'=>'Mohon maaf dokter tidak tersedia.'];
		}

		echo json_encode($data);
    }

    public function get_validasi(){
    	$this->_get_validasi();
    }

    private function _get_validasi(){
    	$post = _post();
    	if($post){
    		$tgl_registrasi	= date('Y-m-d',strtotime($post['tanggal_periksa']));
			$no_rkm_medis	= isset($post['no_rkm_medis']) ? explode('-', $post['no_rkm_medis']) : '';
			$dNow 			= date('Y-m-d');
			$tNow 			= date('H:i:s');
			$tgl_kedepan 	= AddDate($dNow,$this->_cf->_hari_daftar,'days');
			$where 	= ['hari_kerja'=>indoDays($tgl_registrasi)];

			// Info Pasien
			$res_pasien = $this->Pasien_model->get('','tgl_lahir',['no_rkm_medis'=>trim($no_rkm_medis[0])],'','','',1)->result();
			$usia_pasien = getAge(date('Y-m-d'),$res_pasien[0]->tgl_lahir,['year'])['year'];
			$respoliklinik = $this->get_poliklinik($where);

			if($usia_pasien <= 17){
				$poliklinik = $respoliklinik;
			}else{
				unset($respoliklinik['ANA']);
				$poliklinik = $respoliklinik;
			}

			$getLiburNasional= $this->Holidayscalendar_model->get('','*',['tanggal'=>$tgl_registrasi],'','',1)->result();
			
			if($tgl_registrasi < $dNow)
			{
				$error = 1;
				$msg = 'Mohon maaf tanggal yang anda pilih kurang dari tanggal saat ini.';
			}
			else if($tgl_registrasi > $tgl_kedepan)
			{
				$error = 1;
				$msg = 'Mohon maaf tanggal yang anda pilih melebihi batas limit pendaftaran, silahkan mundurkan tanggal kunjungan anda.';
			}
			else if($getLiburNasional)
			{
				$error = 1;
				$msg = 'Mohon maaf pada '. tanggal_indo($getLiburNasional[0]->tanggal) . ' '. $getLiburNasional[0]->keterangan;
			}
			else
			{
				$error = 2;
				$msg = htmlSelectFromArray($poliklinik, 'name="kd_poli" id="kd_poli_reg" class="form-control input-lg"', true);
			}

			$data = array(
				'error' => $error,
				'msg' => $msg
			);

			echo json_encode($data);
    	}
    }

    private function get_poliklinik($where=''){   	
    	$res_poli 	= $this->Jadwal_model->getData($where)->result_array();
		
		$data = [];
		if($res_poli){
			$data = splitRecursiveArray($res_poli,'kd_poli','nm_poli');
		}

		return $data;
    }

    private function _getAge($tgl_lahir=''){
    	if($tgl_lahir){
    		$dNow = date('Y-m-d');
    		$res = getAge($dNow,$tgl_lahir,['year','month','day','hour']);
    	}else{
    		$res = ['year'=>'','month'=>'','day'=>'','hour'=>''];
    	}

    	return $res;
    }

    public function getInfoKartu(){
    	$post = _post();
    	$nik = isset($post['no_peserta']) ? $post['no_peserta'] : '';
    	if($post['kd_pj']=='BPJ'){
    		$res = $this->bpjs->Peserta($nik,date('Y-m-d'),'kartu');
			if($res['error']=='Connected'){
				$bpjs = $res['data']['response']['peserta'];
				if($bpjs['nama']){
					$datas['stskode'] = $bpjs['statusPeserta']['kode'];
					$datas['info'] = $bpjs['nama'] .' - '. $bpjs['tglLahir'] .' - '. $bpjs['statusPeserta']['keterangan'];
					$datas['status'] = $bpjs['statusPeserta']['keterangan'];
				}else{
					$datas['stskode'] = $bpjs['statusPeserta']['kode'];
					$datas['info'] = $bpjs['nama'] .' - '. $bpjs['tglLahir'] .' - '. $bpjs['statusPeserta']['keterangan'];
					$datas['status'] = 'PASIEN TIDAK TERDAFTAR';
				}
			}else{
				$datas['stskode'] = '1';
				$datas['info'] = 'Server tidak bisa terhubung ke BPJS.';
				$datas['status'] = '';
			}
    	}else{
    		$datas['stskode'] = '1';
			$datas['info'] = 'Mohon maaf asuransi anda bukan JAMKESNAS.';
			$datas['status'] = '';
    	}

    	echo json_encode($datas);
    }

    public function cekRujukan(){
    	$post = _post();
    	if($post['no_rujukan']){
    		$res = $this->bpjs->cariByNoRujukan('',$post['no_rujukan']);
    		if($res['error']=='Connected'){
    			$bpjs = $res['data']['response']['rujukan'];
    			$peserta = $bpjs['peserta'];
    			if($peserta['nama']){
    				$data['info'] = 'Ok';
    				$data['kode_diagnosa'] = $bpjs['diagnosa']['kode'];
    				$data['nama_diagnosa'] = $bpjs['diagnosa']['nama'];
    				$data['keluhan'] = $bpjs['keluhan'];
    				$data['no_rujukan'] = $bpjs['noKunjungan'];
    				$data['kode_pelayanan'] = $bpjs['pelayanan']['kode'];
    				$data['nama_pelayanan'] = $bpjs['pelayanan']['nama'];
    				$data['cob_nmasuransi'] = $peserta['cob']['nmAsuransi'];
    				$data['cob_noasuransi'] = $peserta['cob']['noAsuransi'];
    				$data['cob_tgltat'] = $peserta['cob']['tglTAT'];
    				$data['cob_tgltmt'] = $peserta['cob']['tglTMT'];
    				$data['hakkelas_keterangan'] = $peserta['hakKelas']['keterangan'];
    				$data['hakkelas_kode'] = $peserta['hakKelas']['kode'];
    				$data['informasi_dinsos'] = $peserta['informasi']['dinsos'];
    				$data['informasi_nosktm'] = $peserta['informasi']['noSKTM'];
    				$data['informasi_prolanisprb'] = $peserta['informasi']['prolanisPRB'];
    				$data['jenispeserta_keterangan'] = $peserta['jenisPeserta']['keterangan'];
    				$data['jenispeserta_kode'] = $peserta['jenisPeserta']['kode'];
    				$data['mr_nomr'] = $peserta['mr']['noMR'];
    				$data['mr_notelp'] = $peserta['mr']['noTelepon'];
    				$data['nama'] = $peserta['nama'];
    				$data['nik'] = $peserta['nik'];
    				$data['nokartu'] = $peserta['noKartu'];
    				$data['pisa'] = $peserta['pisa'];
    				$data['provumum_kdprovider'] = $peserta['provUmum']['kdProvider'];
    				$data['provumum_nmprovider'] = $peserta['provUmum']['nmProvider'];
    				$data['sex'] = $peserta['sex'];
    				$data['statuspeserta_keterangan'] = $peserta['statusPeserta']['keterangan'];
    				$data['statuspeserta_kode'] = $peserta['statusPeserta']['kode'];
    				$data['polirujukan_kode'] = $bpjs['poliRujukan']['kode'];
    				$data['polirujukan_nama'] = $bpjs['poliRujukan']['nama'];
    				$data['provperujuk_kode'] = $bpjs['provPerujuk']['kode'];
    				$data['provperujuk_nama'] = $bpjs['provPerujuk']['nama'];
    				$data['tglkunjungan'] = $bpjs['tglKunjungan'];
    				$data['msg']	= '';
    			}else{
    				$data['info'] 	= 'Not';
    				$data['msg']	= 'No Rujukan yang Anda Masukan Tidak Ditemukan.';
    			}
    		}else{
    			$data['info'] 	= 'Not';
    			$data['msg']	= 'Mohon maaf koneksi sedang terputus.';
    		}
    	}else{
    		$data['info'] 	= 'Not';
    		$data['msg']	= 'No Rujukan Tidak Boleh Kosong.';
    	}    	
    	
    	echo json_encode($data);
    }

    public function getdiagnosabpjs(){
		$search 	= $_GET['term'];
		if(!empty($search))
		{
			$res = $this->bpjs->diagnosa($search);
			if($res['error']=='Connected'){
				$data = $res['data']['response']['diagnosa'];
				if(count($data) > 0){
					foreach($data as $k => $v){
						$nama = explode('-', $v['nama']);
						$res_data[] = array(
							'id' => $v['kode'],
							'value' => $v['nama'],
							'name' => trim($nama[1])
						);
					}
				}else{
					$res_data[] = array(
						'id' => '',
						'value' => 'Data Tidak Ditemukan.',
						'nama' => ''
					);
				}
			}else{
				$res_data[] = array(
					'id' => '',
					'value' => 'Data Tidak Ditemukan.',
					'nama' => ''
				);
			}
		}else{
			$res_data[] = array(
				'id' => '',
				'value' => 'Silahkan isi formulir pencarian diagnosa.',
				'nama' => ''
			);
		}
		echo json_encode($res_data);
    }

    public function getdiagnosa(){
		$search 	= $_GET['term'];
		if(!empty($search))
		{
			$where = 'kd_penyakit like \'%'.$search.'%\' OR nm_penyakit like \'%'.$search.'%\'';
			$res = $this->Masterpenyakit_model->get('','kd_penyakit,nm_penyakit',$where,'','','',10)->result_array();
			if($res){
				foreach($res as $k => $v){
					$res_data[] = array(
						'id' => $v['kd_penyakit'],
						'value' => $v['nm_penyakit']
					);
				}
			}else{
				$res_data[] = array(
					'id' => '',
					'value' => 'Data Tidak Ditemukan.'
				);
			}
		}else{
			$res_data[] = array(
				'id' => '',
				'value' => 'Silahkan isi formulir pencarian diagnosa.'
			);
		}
		echo json_encode($res_data);
    }

    public function getpolibpjs(){
		$search 	= $_GET['term'];
		if(!empty($search))
		{
			$res = $this->bpjs->poli($search);
			if($res['error']=='Connected'){
				$data = $res['data']['response']['poli'];
				if(count($data) > 0){
					foreach($data as $k => $v){
						$res_data[] = array(
							'id' => $v['kode'],
							'value' => $v['kode'] .' - '. $v['nama'],
							'name' => $v['nama']
						);
					}
				}else{
					$res_data[] = array(
						'id' => '',
						'value' => 'Data Tidak Ditemukan.',
						'name' => ''
					);
				}
			}else{
				$res_data[] = array(
					'id' => '',
					'value' => 'Data Tidak Ditemukan.',
					'name' => ''
				);
			}
		}else{
			$res_data[] = array(
				'id' => '',
				'value' => 'Silahkan isi formulir pencarian poli.',
				'name' => ''
			);
		}
		echo json_encode($res_data);
    }

    public function getdokterdpjpbpjs(){
		//$search 	= $_GET['term'];
		$post = _post();
		if(!empty($post))
		{
			$res = $this->bpjs->dokterdpjp($post['jnspelayanan_sep'],date('Y-m-d',strtotime($post['tglpelayanan_sep'])),$post['search']);
			if($res['error']=='Connected'){
				$data = $res['data']['response']['list'];
				if(count($data) > 0){
					foreach($data as $k => $v){
						$res_data[] = array(
							'id' => $v['kode'],
							'value' => $v['kode'] .' - '. $v['nama'],
							'name' => $v['nama']
						);
					}
				}else{
					$res_data[] = array(
						'id' => '',
						'value' => 'Data Tidak Ditemukan.',
						'name' => $v['nama']
					);
				}
			}else{
				$res_data[] = array(
					'id' => '',
					'value' => 'Data Tidak Ditemukan.',
					'name' => $v['nama']
				);
			}
		}else{
			$res_data[] = array(
				'id' => '',
				'value' => 'Silahkan isi formulir pencarian poli.',
				'name' => $v['nama']
			);
		}
		echo json_encode($res_data);
    }

    public function getKabBpjs(){
    	$post = _post();

    	if($post){
    		$res = $this->bpjs->kabupaten($post['kd_prop']);
    		if($res['error']=='Connected'){
				$data = $res['data']['response']['list'];
				$split = splitRecursiveArray($data,'kode','nama');
				if(count($data) > 0){
					$data['status'] = '';
					$data['data'] = htmlSelectFromArray($split, 'name="kdkabupaten" id="kdkabupaten_sep" class="form-control input-lg"', true);
				}else{
					$data['status'] = '1';
					$data['data'] = 'Data Tidak Tersedia.';
				}
			}else{
				$data['status'] = '1';
				$data['data'] = 'Data Tidak Tersedia.';
			}
    	}else{
    		$data['status'] = '1';
			$data['data'] = 'Data Tidak Tersedia.';
    	}

    	echo json_encode($data);
    }

    public function getKecBpjs(){
    	$post = _post();

    	if($post){
    		$res = $this->bpjs->kecamatan($post['kd_kab']);
    		if($res['error']=='Connected'){
				$data = $res['data']['response']['list'];
				$split = splitRecursiveArray($data,'kode','nama');
				if(count($data) > 0){
					$data['status'] = '';
					$data['data'] = htmlSelectFromArray($split, 'name="kdkecamatan" id="kdkecamatan_sep" class="form-control input-lg"', true);
				}else{
					$data['status'] = '1';
					$data['data'] = 'Data Tidak Tersedia.';
				}
			}else{
				$data['status'] = '1';
				$data['data'] = 'Data Tidak Tersedia.';
			}
    	}else{
    		$data['status'] = '1';
			$data['data'] = 'Data Tidak Tersedia.';
    	}

    	echo json_encode($data);
    }

    /*
	AREA BPJS
    */
    public function createsep(){
    	$post = $this->input->post();
    	$error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Gppendaftaran_model->rules_sep;
        $this->form_validation->set_rules($rules);

        $tgl_kll = '0000-00-00';
        if($post['laka_lantas']){
        	$tgl_kll = date('Y-m-d',strtotime($post['tgl_kll']));
        }

        $createsep = [
        	'request' => [
                't_sep' => [
                    'noKartu' => !empty($post['no_peserta']) ? $post['no_peserta'] : "",
                    'tglSep' => !empty($post['tgl_sep']) ? date('Y-m-d',strtotime($post['tgl_sep'])) : "",
                    'ppkPelayanan' => !empty($post['kodeppk']) ? $post['kodeppk'] : "1027R007",
                    'jnsPelayanan' => !empty($post['jnspelayanan']) ? $post['jnspelayanan'] : "",
                    'klsRawat' => !empty($post['klsrawat']) ? $post['klsrawat'] : "",
                    'noMR' => !empty($post['no_rkm_medis']) ? $post['no_rkm_medis'] : "",
                    'rujukan' => [
                        'asalRujukan' => !empty($post['asal_rujukan']) ? $post['asal_rujukan'] : "",
                        'tglRujukan' => !empty($post['tgl_rujukan']) ? date('Y-m-d',strtotime($post['tgl_rujukan'])) : "",
                        'noRujukan' => !empty($post['no_rujukan']) ? $post['no_rujukan'] : "",
                        'ppkRujukan' => !empty($post['kodeppkrujukan']) ? $post['kodeppkrujukan'] : ""
                    ],
                    'catatan' => !empty($post['catatan']) ? $post['catatan'] : "",
                    'diagAwal' => !empty($post['kodediagnosa']) ? $post['kodediagnosa'] : "",
                    'poli' => [
                        'tujuan' => !empty($post['politujuan']) ? $post['politujuan'] : "",
                        'eksekutif' => !empty($post['eksekutif']) ? (string)$post['eksekutif'] : "0"
                    ],
                    'cob' => [
                        'cob' => !empty($post['cob']) ? $post['cob'] : "0"
                    ],
                    'katarak' => [
                        'katarak' => !empty($post['katarak']) ? $post['katarak'] : "0"
                    ],
                    'jaminan' => [
                        'lakaLantas' => !empty($post['laka_lantas']) ? $post['laka_lantas'] : "0",
                        'penjamin' => [
                            'penjamin' => !empty($post['penjamin_lakalantas']) ? $post['penjamin_lakalantas'] : "",
                            'tglKejadian' => $tgl_kll,
                            'keterangan' => !empty($post['keterangan']) ? $post['keterangan'] : "",
                            'suplesi' => [
                                'suplesi' => !empty($post['suplesi']) ? $post['suplesi'] : "0",
                                'noSepSuplesi' => !empty($post['nosepsuplesi']) ? $post['nosepsuplesi'] : "",
                                'lokasiLaka' => [
                                    'kdPropinsi' => !empty($post['kdpropinsi']) ? $post['kdpropinsi'] : "",
                                    'kdKabupaten' => !empty($post['kdkabupaten']) ? $post['kdkabupaten'] : "",
                                    'kdKecamatan' => !empty($post['kdkecamatan']) ? $post['kdkecamatan'] : "",
                                ],
                            ],
                        ],
                    ],
                    'skdp' => [
                        'noSurat' => !empty($post['no_skdp']) ? $post['no_skdp'] : "",
                        'kodeDPJP' => !empty($post['dokterdpjp']) ? $post['dokterdpjp'] : ''
                    ],
                    'noTelp' => !empty($post['no_telp']) ? $post['no_telp'] : "",
                    'user' => $this->session->userdata('username')
                ]
            ]
        ];
        $result_sep = $this->_insertsep($createsep);

        $bridgingsep = [
        	'no_sep' => @$result_sep['data'],
    		'no_rawat' => !empty($post['no_rawat']) ? $post['no_rawat'] : "",
    		'tglsep' => !empty($post['tgl_sep']) ? date('Y-m-d',strtotime($post['tgl_sep'])) : "",
    		'tglrujukan' => !empty($post['tgl_rujukan']) ? date('Y-m-d',strtotime($post['tgl_rujukan'])) : "",
    		'no_rujukan' => !empty($post['no_rujukan']) ? $post['no_rujukan'] : "",
    		'kdppkrujukan' => !empty($post['kodeppkrujukan']) ? $post['kodeppkrujukan'] : "",
    		'nmppkrujukan' => !empty($post['namappkrujukan']) ? $post['namappkrujukan'] : "",
    		'kdppkpelayanan' => !empty($post['kodeppk']) ? $post['kodeppk'] : "1027R007",
    		'nmppkpelayanan' => "RSUD KOTA DEPOK",
    		'jnspelayanan' => !empty($post['jnspelayanan']) ? $post['jnspelayanan'] : "",
    		'catatan' => !empty($post['catatan']) ? $post['catatan'] : "",
    		'diagawal' => !empty($post['kodediagnosa']) ? $post['kodediagnosa'] : "",
    		'nmdiagnosaawal' => !empty($post['namadiagnosa']) ? $post['namadiagnosa'] : "",
    		'kdpolitujuan' => !empty($post['politujuan']) ? $post['politujuan'] : "",
    		'nmpolitujuan' => !empty($post['namapolitujuan']) ? $post['namapolitujuan'] : "",
    		'klsrawat' => !empty($post['klsrawat']) ? $post['klsrawat'] : "",
    		'lakalantas' => !empty($post['laka_lantas']) ? $post['laka_lantas'] : "0",
    		'user' => $this->session->userdata('username'),
    		'nomr' => !empty($post['no_rkm_medis']) ? $post['no_rkm_medis'] : "",
    		'nama_pasien' => !empty($post['nm_pasien']) ? $post['nm_pasien'] : "",
    		'tanggal_lahir' => !empty($post['tgl_lahir']) ? date('Y-m-d',strtotime($post['tgl_lahir'])) : "",
    		'peserta' => !empty($post['pekerjaan']) ? $post['pekerjaan'] : "",
    		'jkel' => !empty($post['jk']) ? $post['jk'] : "",
    		'no_kartu' => !empty($post['no_peserta']) ? $post['no_peserta'] : "",
    		'tglpulang' => "0000-00-00 00:00:00",
    		'asal_rujukan' => !empty($post['asal_rujukan']) ? $this->_cf->_bpjs['asal_rujukan'][$post['asal_rujukan']] : "",
    		'eksekutif' => !empty($post['eksekutif']) ? $this->_cf->_bpjs['eksekutif'][$post['eksekutif']] : $this->_cf->_bpjs['eksekutif'][0],
    		'cob' => !empty($post['cob']) ? $this->_cf->_bpjs['cob'][$post['cob']] : $this->_cf->_bpjs['cob'][0],
    		'penjamin' => "0",
    		'notelep' => !empty($post['no_telp']) ? $post['no_telp'] : "",
    		'katarak' => !empty($post['katarak']) ? $this->_cf->_bpjs['katarak'][$post['katarak']] : $this->_cf->_bpjs['katarak'][0],
    		'tglkkl' => $tgl_kll,
    		'keterangankkl' => !empty($post['keterangan']) ? $post['keterangan'] : "",
    		'suplesi' => !empty($post['suplesi']) ? $this->_cf->_bpjs['suplesi'][$post['suplesi']] : $this->_cf->_bpjs['suplesi'][0],
    		'no_sep_suplesi' => isset($post['nosepsuplesi']) ? $post['nosepsuplesi'] : "",
    		'kdprop' => !empty($post['kdpropinsi']) ? $post['kdpropinsi'] : "",
    		'nmprop' => !empty($post['nmpropinsi']) ? $post['nmpropinsi'] : "",
    		'kdkab' => !empty($post['kdkabupaten']) ? $post['kdkabupaten'] : "",
    		'nmkab' => !empty($post['nmkabupaten']) ? $post['nmkabupaten'] : "",
    		'kdkec' => !empty($post['kdkecamatan']) ? $post['kdkecamatan'] : "",
    		'nmkec' => !empty($post['nmkecamatan']) ? $post['nmkecamatan'] : "",
    		'noskdp' => !empty($post['no_skdp']) ? $post['no_skdp'] : '-',
    		'kddpjp' => !empty($post['dokterdpjp']) ? $post['dokterdpjp'] : '',
    		'nmdpdjp' => !empty($post['namadokterdpjp']) ? $post['namadokterdpjp'] : ''
        ];

        //print_r($result_sep);
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);
	        	unset($post['nik']);
	        	
	        	if($result_sep['status']=='Ok'){
	        		if(!$this->Bridgingsep_model->insert('',$bridgingsep))
		        	{
		        		throw new Exception('Data <b>berhasil</b> tersimpan.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> tersimpan.');
		        	}
	        	}else{
	        		$error = 1;
		        	throw new Exception($result_sep['message']);	
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
			$msg = $e->getMessage();
        }

        echo json_encode([
        	'url' => $url,
        	'error' => $error,
        	'msg' => $msg
        ]);
    }

    private function _insertsep($data=[]){
    	if(is_array($data)){
    		$res = $this->bpjs->insertSEP($data);
    		if($res['error']=='Connected'){
    			$data = $res['data']['response']['sep'];
    			if($data['noSep']){
    				$datas['status'] = 'Ok';
    				$datas['message'] = $res['data']['metaData']['message'];
    				$datas['data'] = $data['noSep'];
    			}else{
    				$datas['status'] = 'Not';
    				$datas['message'] = $res['data']['metaData']['message'];
    				$datas['data'] = '';
    			}
    		}else{
    			$datas['status'] = 'Not';
    			$datas['message'] = 'Mohon maaf koneksi sedang terputus.';
    			$datas['data'] = '';
    		}
    	}else{
    		$datas['status'] = 'Not';
    		$datas['message'] = 'Mohon data yang terkirim tidak dalam bentuk array.';
    		$datas['data'] = '';
    	}

    	return $datas;
    }

    public function updatesep(){
    	$post = $this->input->post();
    	$error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Gppendaftaran_model->rules_sep;
        $this->form_validation->set_rules($rules);

        $tgl_kll = '0000-00-00';
        if($post['laka_lantas']){
        	$tgl_kll = date('Y-m-d',strtotime($post['tgl_kll']));
        }

        $updatesep = [
        	'request' => [
                't_sep' => [
                    'noSep' => !empty($post['no_sep']) ? $post['no_sep'] : "",
                    'klsRawat' => !empty($post['klsrawat']) ? $post['klsrawat'] : "",
                    'noMR' => !empty($post['no_rkm_medis']) ? $post['no_rkm_medis'] : "",
                    'rujukan' => [
                        'asalRujukan' => !empty($post['asal_rujukan']) ? $post['asal_rujukan'] : "",
                        'tglRujukan' => !empty($post['tgl_rujukan']) ? date('Y-m-d',strtotime($post['tgl_rujukan'])) : "",
                        'noRujukan' => !empty($post['no_rujukan']) ? $post['no_rujukan'] : "",
                        'ppkRujukan' => !empty($post['kodeppkrujukan']) ? $post['kodeppkrujukan'] : ""
                    ],
                    'catatan' => !empty($post['catatan']) ? $post['catatan'] : "",
                    'diagAwal' => !empty($post['kodediagnosa']) ? $post['kodediagnosa'] : "",
                    'poli' => [
                        'eksekutif' => !empty($post['eksekutif']) ? (string)$post['eksekutif'] : "0"
                    ],
                    'cob' => [
                        'cob' => !empty($post['cob']) ? $post['cob'] : "0"
                    ],
                    'katarak' => [
                        'katarak' => !empty($post['katarak']) ? $post['katarak'] : "0"
                    ],
                    'skdp' => [
                        'noSurat' => !empty($post['no_skdp']) ? $post['no_skdp'] : "",
                        'kodeDPJP' => !empty($post['dokterdpjp']) ? $post['dokterdpjp'] : ''
                    ],
                    'jaminan' => [
                        'lakaLantas' => !empty($post['laka_lantas']) ? $post['laka_lantas'] : "0",
                        'penjamin' => [
                            'penjamin' => !empty($post['penjamin_lakalantas']) ? $post['penjamin_lakalantas'] : "",
                            'tglKejadian' => $tgl_kll,
                            'keterangan' => !empty($post['keterangan']) ? $post['keterangan'] : "",
                            'suplesi' => [
                                'suplesi' => !empty($post['suplesi']) ? $post['suplesi'] : "0",
                                'noSepSuplesi' => !empty($post['nosepsuplesi']) ? $post['nosepsuplesi'] : "",
                                'lokasiLaka' => [
                                    'kdPropinsi' => !empty($post['kdpropinsi']) ? $post['kdpropinsi'] : "",
                                    'kdKabupaten' => !empty($post['kdkabupaten']) ? $post['kdkabupaten'] : "",
                                    'kdKecamatan' => !empty($post['kdkecamatan']) ? $post['kdkecamatan'] : "",
                                ],
                            ],
                        ],
                    ],
                    'noTelp' => !empty($post['no_telp']) ? $post['no_telp'] : "",
                    'user' => $this->session->userdata('username')
                ]
            ]
        ];
        $result_sep = $this->_updatesep($updatesep);

        $bridgingsep = [
    		'tglsep' => !empty($post['tgl_sep']) ? date('Y-m-d',strtotime($post['tgl_sep'])) : "",
    		'tglrujukan' => !empty($post['tgl_rujukan']) ? date('Y-m-d',strtotime($post['tgl_rujukan'])) : "",
    		'no_rujukan' => !empty($post['no_rujukan']) ? $post['no_rujukan'] : "",
    		'kdppkrujukan' => !empty($post['kodeppkrujukan']) ? $post['kodeppkrujukan'] : "",
    		'nmppkrujukan' => !empty($post['namappkrujukan']) ? $post['namappkrujukan'] : "",
    		'kdppkpelayanan' => !empty($post['kodeppk']) ? $post['kodeppk'] : "1027R007",
    		'nmppkpelayanan' => "RSUD KOTA DEPOK",
    		'jnspelayanan' => !empty($post['jnspelayanan']) ? $post['jnspelayanan'] : "",
    		'catatan' => !empty($post['catatan']) ? $post['catatan'] : "",
    		'diagawal' => !empty($post['kodediagnosa']) ? $post['kodediagnosa'] : "",
    		'nmdiagnosaawal' => !empty($post['namadiagnosa']) ? $post['namadiagnosa'] : "",
    		'kdpolitujuan' => !empty($post['politujuan']) ? $post['politujuan'] : "",
    		'nmpolitujuan' => !empty($post['namapolitujuan']) ? $post['namapolitujuan'] : "",
    		'klsrawat' => !empty($post['klsrawat']) ? $post['klsrawat'] : "",
    		'lakalantas' => !empty($post['laka_lantas']) ? $post['laka_lantas'] : "0",
    		'user' => $this->session->userdata('username'),
    		'nomr' => !empty($post['no_rkm_medis']) ? $post['no_rkm_medis'] : "",
    		'nama_pasien' => !empty($post['nm_pasien']) ? $post['nm_pasien'] : "",
    		'tanggal_lahir' => !empty($post['tgl_lahir']) ? date('Y-m-d',strtotime($post['tgl_lahir'])) : "",
    		'peserta' => !empty($post['pekerjaan']) ? $post['pekerjaan'] : "",
    		'jkel' => !empty($post['jk']) ? $post['jk'] : "",
    		'no_kartu' => !empty($post['no_peserta']) ? $post['no_peserta'] : "",
    		'tglpulang' => "0000-00-00 00:00:00",
    		'asal_rujukan' => !empty($post['asal_rujukan']) ? $this->_cf->_bpjs['asal_rujukan'][$post['asal_rujukan']] : "",
    		'eksekutif' => !empty($post['eksekutif']) ? $this->_cf->_bpjs['eksekutif'][$post['eksekutif']] : $this->_cf->_bpjs['eksekutif'][0],
    		'cob' => !empty($post['cob']) ? $this->_cf->_bpjs['cob'][$post['cob']] : $this->_cf->_bpjs['cob'][0],
    		'penjamin' => "0",
    		'notelep' => !empty($post['no_telp']) ? $post['no_telp'] : "",
    		'katarak' => !empty($post['katarak']) ? $this->_cf->_bpjs['katarak'][$post['katarak']] : $this->_cf->_bpjs['katarak'][0],
    		'tglkkl' => $tgl_kll,
    		'keterangankkl' => !empty($post['keterangan']) ? $post['keterangan'] : "",
    		'suplesi' => !empty($post['suplesi']) ? $this->_cf->_bpjs['suplesi'][$post['suplesi']] : $this->_cf->_bpjs['suplesi'][0],
    		'no_sep_suplesi' => isset($post['nosepsuplesi']) ? $post['nosepsuplesi'] : "",
    		'kdprop' => !empty($post['kdpropinsi']) ? $post['kdpropinsi'] : "",
    		'nmprop' => !empty($post['nmpropinsi']) ? $post['nmpropinsi'] : "",
    		'kdkab' => !empty($post['kdkabupaten']) ? $post['kdkabupaten'] : "",
    		'nmkab' => !empty($post['nmkabupaten']) ? $post['nmkabupaten'] : "",
    		'kdkec' => !empty($post['kdkecamatan']) ? $post['kdkecamatan'] : "",
    		'nmkec' => !empty($post['nmkecamatan']) ? $post['nmkecamatan'] : "",
    		'noskdp' => !empty($post['no_skdp']) ? $post['no_skdp'] : '-',
    		'kddpjp' => !empty($post['dokterdpjp']) ? $post['dokterdpjp'] : '',
    		'nmdpdjp' => !empty($post['namadokterdpjp']) ? $post['namadokterdpjp'] : ''
        ];

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);
	        	unset($post['nik']);
	        	
	        	if($result_sep['status']=='Ok'){
	        		if($this->Bridgingsep_model->update('',$bridgingsep,['no_sep'=>$post['no_sep']]))
		        	{
		        		throw new Exception('Data <b>berhasil</b> diupdate.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> diupdate.');
		        	}
	        	}else{
	        		$error = 1;
		        	throw new Exception($result_sep['message']);	
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
			$msg = $e->getMessage();
        }

        echo json_encode([
        	'url' => $url,
        	'error' => $error,
        	'msg' => $msg
        ]);
    }

    private function _updatesep($data=[]){
    	if(is_array($data)){
    		$res = $this->bpjs->updateSEP($data);
    		if($res['error']=='Connected'){
    			$data = $res['data']['response'];
    			if($data){
    				$datas['status'] = 'Ok';
    				$datas['message'] = $res['data']['metaData']['message'];
    				$datas['data'] = $data;
    			}else{
    				$datas['status'] = 'Not';
    				$datas['message'] = $res['data']['metaData']['message'];
    				$datas['data'] = '';
    			}
    		}else{
    			$datas['status'] = 'Not';
    			$datas['message'] = 'Mohon maaf koneksi sedang terputus.';
    			$datas['data'] = '';
    		}
    	}else{
    		$datas['status'] = 'Not';
    		$datas['message'] = 'Mohon data yang terkirim tidak dalam bentuk array.';
    		$datas['data'] = '';
    	}

    	return $datas;
    }

    public function updatetglsep(){
    	$post = $this->input->post();
    	$error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Gppendaftaran_model->rules_upsep;
        $this->form_validation->set_rules($rules);

        $updatesep = [
        	'request' => [
                't_sep' => [
                    'noSep' => !empty($post['no_sep']) ? $post['no_sep'] : "",
                    'tglPulang' => !empty($post['tglpulang']) ? date('Y-m-d H:i:s',strtotime($post['tglpulang'])) : "",
                    'user' => $this->session->userdata('username')
                ]
            ]
        ];
        $result_sep = $this->_updatetglsep($updatesep);

        $bridgingsep = [
    		'tglpulang' => !empty($post['tglpulang']) ? date('Y-m-d H:i:s',strtotime($post['tglpulang'])) : "0000-00-00 00:00:00"
        ];

        //print_r($result_sep);
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);
	        	unset($post['nik']);
	        	
	        	if($result_sep['status']=='Ok'){
	        		if($this->Bridgingsep_model->update('',$bridgingsep,['no_sep'=>$post['no_sep']]))
		        	{
		        		throw new Exception('Data <b>berhasil</b> diupdate.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> diupdate.');
		        	}
	        	}else{
	        		$error = 1;
		        	throw new Exception($result_sep['message']);	
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
			$msg = $e->getMessage();
        }

        echo json_encode([
        	'url' => $url,
        	'error' => $error,
        	'msg' => $msg
        ]);
    }

    private function _updatetglsep($data=[]){
    	if(is_array($data)){
    		$res = $this->bpjs->updateTglPlg($data);
    		if($res['error']=='Connected'){
    			$data = $res['data']['response'];
    			if($data){
    				$datas['status'] = 'Ok';
    				$datas['message'] = $res['data']['metaData']['message'];
    				$datas['data'] = $data;
    			}else{
    				$datas['status'] = 'Not';
    				$datas['message'] = $res['data']['metaData']['message'];
    				$datas['data'] = '';
    			}
    		}else{
    			$datas['status'] = 'Not';
    			$datas['message'] = 'Mohon maaf koneksi sedang terputus.';
    			$datas['data'] = '';
    		}
    	}else{
    		$datas['status'] = 'Not';
    		$datas['message'] = 'Mohon data yang terkirim tidak dalam bentuk array.';
    		$datas['data'] = '';
    	}

    	return $datas;
    }

    public function deletesep(){
    	$segs = $this->_segs;
    	$no_sep = decrypt_aes($segs[7]);
    	
    	$deletesep = [
    		'request' => [
    			't_sep' => [
    				'noSep' => $no_sep,
    				'user' => $this->session->userdata('username')
    			]
    		]
    	];

    	$result_sep = $this->_deletesep($deletesep);

    	try {
        	if($result_sep['status']=='Ok'){
        		$this->Bridgingsep_model->delete('',['no_sep'=>$no_sep]);
	        	throw new Exception('Data <b>berhasil</b> terhapus.');
        	}else{
        		$error = 1;
	        	throw new Exception($result_sep['data']);	
        	}
        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));     
    }

    private function _deletesep($data=[]){
    	if(is_array($data)){
    		$res = $this->bpjs->deleteSEP($data);
    		if($res['error']=='Connected'){
    			$data = $res['data']['response']['sep'];
    			if($data){
    				$datas['status'] = 'Ok';
    				$datas['data'] = $data;
    			}else{
    				$datas['status'] = 'Not';
    				$datas['data'] = 'Mohon maaf sep gagal di hapus.';
    			}
    		}else{
    			$datas['status'] = 'Not';
    			$datas['data'] = 'Mohon maaf koneksi sedang terputus.';
    		}
    	}else{
    		$datas['status'] = 'Not';
    		$datas['data'] = 'Mohon data yang terkirim tidak dalam bentuk array.';
    	}

    	return $datas;
    }

    public function cetaksep(){
    	$segs = $this->_segs;
    	$data_uri = explode('-', decrypt_aes($segs[7]));
    	$url = base_url($segs[1].'/'.$segs[2].'/search=');

    	$label = [
    		'v1' => 'print_sep_v1',
    		'v2' => 'print_sep_v2'
    	];

    	if($data_uri){
    		$res = $this->Bridgingsep_model->get('','*',['no_sep'=>$data_uri[0]]);
    		if($data_uri[1]=='v1'){
    			$value = $res->result();
    		}else if($data_uri[1]=='v2'){
    			$value = $res->result_array();
    		}
    	}



    	$data = array(
			'title' => 'Cetak Surat SEP',
			'url' => $url,
			'r' => $value,
			'file' => $label[$data_uri[1]],
			'folder' => 'Print/SEP'
		);
		
		$this->site->view('incReport',$data);
    }

    public function getbarcode()
    {
    	$segs = $this->_segs;
        $this->load->library('Zend');
        $this->zend->load('Zend/Barcode');
        //generate barcode
        Zend_Barcode::render('code39', 'image', array('text'=>$segs[7],'barHeight'=>30,'drawText'=>true), array());
    }

    public function getBridgingSep(){
    	$post = _post();
    	if($post['no_sep']!='Sukses'){
    		$res = $this->Bridgingsep_model->get('','*',['no_sep'=>$post['no_sep']])->result_array();
    	}

    	if($res){
    		$data['status']  = 'Ok';
    		$data['data']  = $res;
    	}else{
    		$data['status']  = 'Not';
    		$data['data']  = 'Mohon maaf system tidak bisa merespon.';
    	}

    	echo json_encode($data);
    }

    /*
	Area Booking
    */

    public function getDataBooking(){
    	$post = _post();

    	if($post){
    		$res = $this->Bookingregistrasi_model->get('','no_rujukan,no_sep,no_skdp',['no_rkm_medis'=>$post['no_rkm_medis'],'kd_dokter'=>$post['kd_dokter'],'kd_poli'=>$post['kd_poli'],'tanggal_periksa'=>$post['tgl_registrasi']])->result();
    	}

    	if($res){
    		$data['status'] = 'Ok';
    		$data['info'] = !empty($res[0]->no_rujukan) ? 'No Rujukan' : 'No Sep';
    		$data['data'] = !empty($res[0]->no_rujukan) ? $res[0]->no_rujukan : $res[0]->no_sep;
    	}else{
    		$data['status'] = 'Not';
    		$data['info'] = '';
    		$data['data'] = 'Data tidak tersedia.';
    	}

    	echo json_encode($data);
    }

    public function kamarinap(){
    	$this->_kamarinap();
    }

    private function _kamarinap(){
    	$search 	= $_GET['term'];
		if(!empty($search))
		{
			$where = 'kd_kamar like \'%'.$search.'%\' OR nm_bangsal like \'%'.$search.'%\' and kamar.status="KOSONG"';
			$res = $this->Kamar_model->getData($where);
			if($res){
				foreach($res as $k => $v){
					$res_data[] = array(
						'id' => $v->kd_kamar,
						'value' => $v->kd_kamar .' - '. $v->kd_bangsal .' - '. $v->nm_bangsal .' - '. $v->status,
						'nilai' => $v->trf_kamar,
						'total' => (1*$v->trf_kamar)
					);
				}
			}else{
				$res_data[] = array(
					'id' => '',
					'value' => 'Data Tidak Ditemukan.',
					'nilai' => '0',
					'total' => '0'
				);
			}
		}else{
			$res_data[] = array(
				'id' => '',
				'value' => 'Silahkan isi formulir pencarian diagnosa.',
				'nilai' => '0',
				'total' => '0'
			);
		}
		echo json_encode($res_data);
    }
}