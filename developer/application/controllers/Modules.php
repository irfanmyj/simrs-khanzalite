<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Modules extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Modules_model','Routes_model']);
		$this->site->is_logged_in();
	}

	public function _remap()
	{
		$segs = $this->_segs;
		$method = isset($segs[6]) ? $segs[6] : '';
		switch ($method) {
		  	case null;
		  	case false;
		  	case '':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		      	$this->index();
		      break;
		  	case 'search':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
		      	$this->search();
		      break;
		  	case 'store':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
		      	$this->store();
		      break;
		  	case 'update':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->update();
		      break;
		    case 'position':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->updateposition();
		      break;
		  	case 'edit':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->edit();
		      break;
		  	case 'destroy':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
		    	$this->destroy();
		    break;
		  	case 'print':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
		    	$this->print();
		    break;
		  default:
		    	show_404();
		    break;
		}
	}
	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		global $Cf;
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['deleted_at' => NULL];

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where = 'name_module like \'%'.$search.'%\' OR link like \'%'.$search.'%\' and deleted_at IS NULL';
			}
		}

		$res_count	= $this->Modules_model->count('',$where);
		$res_data 	= $this->Modules_model->getData($where,$limit,$limit_start,$offset);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		/*
		Data pendukung
		*/
		$rmodules = $this->Modules_model->get('','module_id,name_module',array('deleted_at'=>NULL,'parent_id'=>0),'','','module_id ASC')->result_array();
		$dmodules = splitRecursiveArray($rmodules,'module_id','name_module');
		/*echo '<pre>';
		print_r($dmodules);
		exit();*/
		$data = array(
			'title' => 'Halaman Data Master Modules',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'modules' => $dmodules,
			'akses' => $this->_cf->modules,
			'status' => $this->_cf->statusdata,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan form pengkajian pasien.
	* Menampilkan data riwayat kunjungan pasien.
	* Menampilkan 
	* @ second load method 
	*/

	/*public function create()
	{

	}*/

	public function store()
    {
        $post 	= _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Modules_model->rules;
        $this->form_validation->set_rules($rules);

        /*
		* Proses pencarian apakah ada nama yang sama pada tabel dengan post yang dikirim oleh user.
        */
		$parent = $this->Modules_model->get('','position','','','','position DESC',1)->result();
		$post['position'] = $parent[0]->position + 1;

        $where = array('link' => $post['link']);
        $search = $this->Modules_model->get('','link',$where)->result();

        $routes = [
        	1 => [
        		'controller' => $post['link'].'/index',
        		'routes' => $post['link'].'/(:any)',
        	],
        	2 => [
        		'controller' => $post['link'].'/index',
        		'routes' => $post['link'].'/(:any)/(:any)',
        	],
        	3 => [
        		'controller' => $post['link'].'/index',
        		'routes' => $post['link'].'/(:any)/(:any)/(:any)',
        	],
        	4 => [
        		'controller' => $post['link'].'/index',
        		'routes' => $post['link'].'/(:any)/(:any)/(:any)/(:any)',
        	],
        	5 => [
        		'controller' => $post['link'].'/destroy',
        		'routes' => $post['link'].'/(:any)/(:any)/(:any)/(:any)/destroy/(:any)',
        	],
        	6 => [
        		'controller' => $post['link'].'/store',
        		'routes' => $post['link'].'/(:any)/(:any)/(:any)/(:any)/store',
        	],
        	7 => [
        		'controller' => $post['link'].'/update',
        		'routes' => $post['link'].'/(:any)/(:any)/(:any)/(:any)/update',
        	],
        	8 => [
        		'controller' => $post['link'].'/edit',
        		'routes' => $post['link'].'/(:any)/(:any)/(:any)/(:any)/edit',
        	],
        	9 => [
        		'controller' => $post['link'].'/show',
        		'routes' => $post['link'].'/(:any)/(:any)/(:any)/(:any)/show',
        	]

        ];


        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['module_id']);
	        	if($search)
	        	{
	        		$error = 1;
	        		throw new Exception('Terdapat duplikasi data. Data ini <b>'. $search[0]->link .'</b> sudah tersedia di dalam tabel database.');
	        	}
	        	else
	        	{
	        		if($this->Modules_model->insert('',$post))
		        	{
		        		if(!empty($post['parent_id']))
		        		{
		        			$this->Routes_model->insert('',$routes,TRUE);
		        			$this->_folder_creators($post['link'],'lucida');
		        			$this->_file_controller($post['link']);
		        			$this->_file_model($post['link']);
		        		}
		        		
		        		throw new Exception('Data <b>berhasil</b> tersimpan.');
		        	}
		        	else
		        	{
		        		$error = 1;
		        		throw new Exception('Data <b>gagal</b> tersimpan.');
		        	}
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    /*public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }*/

    public function update()
    {
        $post = _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['module_id']) ? $post['module_id'] : '';
        $post['updated_at'] = date('Y-m-d H:i:s');
        $rules 	= $this->Modules_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['module_id']);
	        	if($this->Modules_model->update('',$post,array('module_id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);
        //print_r($explode);
        foreach ($explode as $key => $value) {
        	if($this->Modules_model->update('',array('deleted_at'=>date('Y-m-d H:i:s')),array('module_id'=>$value))){
        		$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data telah berhasil dihapus';
        	}else{
        		$_SESSION['error'] = 1;
        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
        	} 	
        } 
    	//print_r($explode);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/'.$segs[3].'/'.$segs[4].'/1'));
    }

    public function updateposition()
    {
		$post = $this->input->post();
		$module_id = explode(',', $post['module_id']);

		$i = 1;
		foreach ($module_id as $k => $v) {
			$data['position'] = $i; 
			$this->Modules_model->update('',$data,['module_id'=>$v]);
			$i++;
		}

		$error = '';

		$datas = [
			'error' => $error,
			'url' => $post['url']
		]; 

		echo json_encode($datas);
    }

}