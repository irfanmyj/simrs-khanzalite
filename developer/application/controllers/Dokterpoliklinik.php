<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokterpoliklinik extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Dashboard_model'));
		$this->site->is_logged_in();
	}
	
	public function index()
	{	
		global $Cf;
		$segs 		= $this->uri->segment_array();
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $Cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;

		$this->site->is_access(acl(decrypt_aes($segs[2])),'view');

		/*
		Ambil data pengunjungan berdasarkan tahun saat ini.
		*/
		$res_visitor = $this->Dashboard_model->getVisitorYear()->result();
		foreach ($res_visitor as $k => $v) {
			$visitor[] = $v->visitor;
		}

		/*
		Ambil data pengunjungn berdasakan hari sekarang.
		*/
		$res_visitor_month = $this->Dashboard_model->getVisitorDay();

		/*
		Ambil data pengunjun pasien yang terdaftar hari ini.
		*/
		$pasien_aktiv = $this->Dashboard_model->getData(['tgl_registrasi'=>date('Y-m-d')],$limit,$limit_start);

		/*echo '<pre>';
		print_r($res_visitor_month);
		exit();*/
		
		$datas = array(
			'title_table1' => 'Data Pengunjung Pasien Hari ini.',
			'visitor' => $visitor,
			'visitor_month' => $res_visitor_month,
			'pasien_aktiv' => $pasien_aktiv,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		$this->site->view('inc',$datas);
	}

	public function cek()
	{
		require APPPATH . 'controllers/Testing.php';
		$tes = new Testing();

		print_r($tes->get());
	}
}
