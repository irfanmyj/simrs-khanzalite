<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Auth_model','Levelaccess_model','Jadwaldokter_model']);
		$this->load->library('email');
	}
	
	public function index()
	{
		redirect(base_url('login'));
	}

	public function login()
	{
		global $Cf;
		$size = $Cf->size_length_token[indoDays(date('Y-m-d'))];
		$datas = array(
			'size' => $size,
			'file' => 'login_index',
			'folder' => ucwords($this->uri->segment(1))
		);
		$this->site->view('inc',$datas);
	}

	public function checking()
	{
		try {
			global $Cf;
			$fields = array(
				'username',
				'password',
				'ci_csrf_token'
			);
			$datas = _post($fields);
			$rules = $this->Auth_model->rules;
			$size = $Cf->size_length_token[indoDays(date('Y-m-d'))];
			$this->form_validation->set_rules($rules);
			
			if($this->form_validation->run() == FALSE)
			{
				$url 	= base_url('auth/login');
				$error 	= 1;
				throw new Exception(validation_errors());
			}
			else
			{
				if(strlen($datas['ci_csrf_token']) == $size)
				{
					$password = sha1($datas['password'].'-'.$datas['username']);
					$get_user = $this->Auth_model->checking(array('username'=>$datas['username'],'password'=>$password));					
					if(!empty($get_user)){
						if($get_user[0]->is_active=='Y' && $get_user[0]->deleted_at=='')
						{		
							$levelakses = $this->Levelaccess_model->get('','access,name',['id'=>$get_user[0]->level_access])->result_array();
							$access = unserialize($levelakses[0]['access']);

							$hari = indoDays(date('Y-m-d'));
							$get_jawdal = $this->Jadwaldokter_model->get('','kd_poli',array('kd_dokter'=>$get_user[0]->code_doctor,'hari_kerja'=>$hari))->result();
							
							$login_data['id_user'] = $get_user[0]->id_user;
							$login_data['username'] = $get_user[0]->username;
							$login_data['level_access'] = $get_user[0]->level_access;
							$login_data['name_levelaccess'] = $levelakses[0]['name'];
							$login_data['code_doctor'] = $get_user[0]->code_doctor;
							$login_data['id_room1'] = $get_user[0]->id_room1;
							$login_data['id_room2'] = $get_user[0]->id_room2;
							$login_data['menu_utama'] = $get_user[0]->menu_utama;
							$login_data['menu_report'] = $get_user[0]->menu_report;
							$login_data['menu_master'] = $get_user[0]->menu_master;
							$login_data['access'] = $access;
							$login_data['status'] =  $get_user[0]->is_active;
							$login_data['kd_poli'] = $get_jawdal;
							$login_data['logged_in'] = TRUE;
							
							$this->session->set_userdata($login_data);

							$url 	= base_url('dashboard/'.encrypt_aes('1'));
							$error = '';
							throw new Exception('Selamat Anda Telah Berhasil Login.');
						}		
						else
						{
							$url 	= base_url('login');
							$error = 1;
							throw new Exception('Kata kunci tidak valid1.');
						}
					}else{
						$url 	= base_url('login');
						$error = 1;
						throw new Exception('Mohon maaf akun yang anda masukan salah.');
					}
				}
			}

		} catch (Exception $e) {
			$_SESSION['error'] = $error;
			$_SESSION['msg'] = $e->getMessage();
		}

		//echo $url;
		generateReturn($url);
	}

	public function lupapassword()
	{
		try {
			global $Cf;
				$post = $this->input->post();
				$fields = array(
					'kd_dokter',
					'email'
				);

				$datas = $this->instrument->_post($fields);
				$data['code'] = isset($datas['kd_dokter']) ? sha1($datas['kd_dokter'].date('Y-m-d H:i:s')) : '';	
				$data['kd_dokter'] = isset($datas['kd_dokter']) ? $datas['kd_dokter'] : '';	
				$data['date'] = date('Y-m-d H:i:s');
				$data['sts_reset'] = 'N';

				$link = base_url('auth/validasi/'.$data['code']);

			    $this->email->initialize($Cf->configEmail);


			    $this->email->from('rsuddepokinfo@gmail.com', 'Rsud Depok Informasi');
			    $this->email->to($datas['email']); 

			    // Lampiran email, isi dengan url/path file
		        $this->email->attach('http://rsud.depok.go.id/wp-content/uploads/2017/09/pp.jpg');

			    // Subject email
		        $this->email->subject('ALAMAT LINK RESET PASSWORD APLIKASI DOKTER RSUD DEPOK');
		        // Isi email
		        $this->email->message('Ini adalah email informasi reset password yang dikirm aplikasi dokter rsud depok.<br><br> Untuk memulai membuat password baru silahkan klik link ini : <strong><a href='.$link.' target="_blank" rel="noopener">disini</a></strong> untuk melihat tutorialnya.');

		        if ($this->email->send()) {
		            $error = '';
					if($this->Reset_model->insert('',$data,false))
					{
						$error = 1;
						throw new Exception('Reset <b> PASSWORD </b> gagal.');
					}
					else
					{
						$error = '';
						throw new Exception('Link reset password telah dikirm ke email anda.');
					}
		        } 
		        else 
		        {
		            $error = 1;
					throw new Exception('Email tidak terkirim '. $this->email->print_debugger());
		        }
			} catch (Exception $e) {
				$ds['error'] = $error;
				$ds['msg'] = $e->getMessage();
			}	

			$dt = array(
				'error' => $ds['error'],
				'msg' => $ds['msg']
			);
			echo json_encode($dt);
			//$this->instrument->generateReturn(base_url($this->uri->segment(1).'/login'));
	}

	public function sendemail($toemail='',$link='')
	{
		$config['protocol']    	= 'smtp';
	    $config['smtp_host']    = 'ssl://smtp.googlemail.com';
	    $config['smtp_port']    = 465;
	    $config['smtp_timeout'] = '7';
	    $config['smtp_user']    = 'irfanmyj@gmail.com';
	    $config['smtp_pass']    = 'bismilahirahmanirahim';
	    $config['charset']    	= 'utf-8';
	    $config['newline']    	= "\r\n";
	    $config['mailtype'] 	= 'html'; // or html
	    //$config['validation'] 	= TRUE; // bool whether to validate email or not      
	    $this->load->library('email');

	    $this->email->initialize($config);


	    $this->email->from('irfanmyj@gmail.com', 'irfanmyj');
	    $this->email->to($toemail); 

	    // Lampiran email, isi dengan url/path file
        $this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

	    // Subject email
        $this->email->subject('ALAMAT LINK RESET PASSWORD APLIKASI DOKTER RSUD DEPOK');
        // Isi email
        $this->email->message("Ini adalah contoh email CodeIgniter yang dikirim menggunakan SMTP email Google (Gmail).<br><br> Klik <strong><a href='https://masrud.com/post/kirim-email-dengan-smtp-gmail' target='_blank' rel='noopener'>disini</a></strong> untuk melihat tutorialnya.");
  

	    // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            $error = '';
			throw new Exception('Email terkirim.');
        } else {
            $error = 1;
			throw new Exception('Email tidak terkirim '. $this->email->print_debugger());
        }

	    

	     //$this->load->view('email_view');
	}


	public function logout(){
		//$this->Get_model->createHistory('Akun ini berhasil logout :'.$this->session->userdata('nm_lengkap').'',$this->session->userdata('id_user'));
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}