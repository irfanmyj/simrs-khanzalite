<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Master_api extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Api_model');
		$this->site->is_logged_in();
	}

	public function _remap()
	{
		$segs = $this->_segs;
		$method = isset($segs[6]) ? $segs[6] : '';
		switch ($method) {
		  	case null;
		  	case false;
		  	case '':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
		      	$this->index();
		      break;
		  	case 'search':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
		      	$this->search();
		      break;
		  	case 'store':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
		      	$this->store();
		      break;
		  	case 'update':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->update();
		      break;
		  	case 'edit':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
		      	$this->edit();
		      break;
		  	case 'destroy':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
		    	$this->destroy();
		    break;
		  	case 'print':
		    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
		    	$this->print();
		    break;
		  default:
		    	show_404();
		    break;
		}
	}

	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= ['deleted_at' => NULL];

		// Cek akses untuk fitur pencarian
		if(@acl($segs[2])['search']=='Y')
		{
			if($search)
			{
				$where = 'name like \'%'.$search.'%\' and deleted_at IS NULL';
			}
		}

		$res_count	= $this->Api_model->count('',$where);
		$res_data 	= $this->Api_model->getData($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);
		
		$data = array(
			'title' => 'Halaman Master API',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->status_field,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	public function store()
    {
    	global $Cf;
        $post 	= _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Api_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	$post['api_id'] = $this->_cf->encrypt_aes_256_cbc($post['api_id']);
	        	$post['password'] = $this->_cf->encrypt_aes_256_cbc($post['password']);

	        	if($this->Api_model->insert('',$post))
	        	{
	        		throw new Exception('Data <b>berhasil</b> tersimpan.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> tersimpan.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
    	global $Cf;
        $post = _post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        $post['updated_at'] = date('Y-m-d H:i:s');
        $rules 	= $this->Api_model->rules;
        $this->form_validation->set_rules($rules);
        
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);
	        	$post['api_id'] = $this->_cf->encrypt_aes_256_cbc($post['api_id']);
	        	$post['password'] = $this->_cf->encrypt_aes_256_cbc($post['password']);
	        	
	        	if($this->Api_model->update('',$post,array('id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);
        //print_r($explode);
        foreach ($explode as $key => $value) {
        	if($this->Api_model->delete('',array('id'=>$value))){
        		$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data telah berhasil dihapus';
        	}else{
        		$_SESSION['error'] = 1;
        		$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
        	} 	
        } 
    	//print_r($explode);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/'.$segs[3].'/'.$segs[4].'/'.$segs[5]));
    }

}