<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cancelbooking extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Cancelbooking_model','Dokter_model','Poliklinik_model','Bcinfocuti_model','Leavecalendar_model','Bookingregistrasi_model']); //jadwal_libur
		$this->site->is_logged_in();
	}

	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	    case 'get_poli':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_poli();
	    	break;
	    case 'get_pasien_booking':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	    	$this->get_pasien_booking();
	    	break;
	    default:
	      show_404();
	      break;
	  }
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$where 		= '';

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where = 'dokter.nm_dokter like \'%'.$search.'%\' OR poliklinik.nm_poli like \'%'.$search.'%\' OR brodcast.masuk_tgl like \'%'.date('Y-m-d',strtotime($search)).'%\'';
			}
		}

		$res_count	= $this->Cancelbooking_model->countData($where);
		$res_data 	= $this->Cancelbooking_model->getData($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);
		
		$res_dokter = $this->Dokter_model->get('','kd_dokter,nm_dokter',['status!='=>0])->result_array();
		$split_dokter = splitRecursiveArray($res_dokter,'kd_dokter','nm_dokter');

		/*$res_poli = $this->Poliklinik_model->get('','kd_poli,nm_poli')->result_array();
		$split_poli = splitRecursiveArray($res_poli,'kd_poli','nm_poli');*/

		$data = array(
			'title' => 'Pesan Brodcast',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->statusdata,
			'dokter' => $split_dokter,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan form pengkajian pasien.
	* Menampilkan data riwayat kunjungan pasien.
	* Menampilkan 
	* @ second load method 
	*/

	/*public function create()
	{

	}*/


	/*
	|------------------------------------------------------------
	| List tabel yang digunakan di methode store Cancelbooking
	|------------------------------------------------------------
	| 1. booking_registrasi
	| 2. jadwal_libur
	| 3. bc_info_cuti
	| 4. brodcast
	*/
	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Cancelbooking_model->rules;
		$this->form_validation->set_rules($rules);

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);

	        	// Update batch booking_registrasi
				$u['status'] = 'Dokter Berhalangan';
				
				// Insert tabel bc info cuti
				$bc['kd_dokter']	= isset($post['kd_dokter']) ? $post['kd_dokter'] : '';
				$bc['kd_poli']		= isset($post['kd_poli']) ? $post['kd_poli'] : '';

				// Insert data ke tabel bc_info_cuti untuk mendapatkan id
		        $id_bc_info_cuti 		= $this->Bcinfocuti_model->insert('',$bc);

		        // Insert tabel jadwal cuti
				$c['id_bc_info_cuti'] 	= $id_bc_info_cuti;//id dari bc_info_cuti
				$c['kd_dokter']			= isset($post['kd_dokter']) ? $post['kd_dokter'] : '';
				$c['tgl_awal']			= isset($post['tgl_awal']) ? date('Y-m-d',strtotime($post['tgl_awal'])) : '';
				$c['tgl_akhir']			= isset($post['tgl_akhir']) ? date('Y-m-d',strtotime($post['tgl_akhir'])) : '';
				$c['status']			= 'Y';

		        foreach ($post['tglperiksa'] as $k => $v) {
					
					// Where update booking
					$wh_update_booking = ['kd_dokter'=>$post['kd_dokter'], 'kd_poli'=>$post['kd_poli'], 'tanggal_periksa'=>$v];

					// Insert batch tabel brodcast
					$d[] = 	[
								'kd_poli' => isset($post['kd_poli']) ? $post['kd_poli'] : '',
								'kd_dokter' => isset($post['kd_dokter']) ? $post['kd_dokter'] : '',
								'judul' => isset($post['judul']) ? $post['judul'] : '',
								'keterangan' => isset($post['keterangan']) ? $post['keterangan'] : '',
								'masuk_tgl' => date('Y-m-d H:i:s'),
								'masuk_oleh' => $this->session->userdata('id_user'),
								'status' => 'Y',
								'tgl_periksa' => $v
							];					

					$this->Bookingregistrasi_model->update('',$u,$wh_update_booking);		

				}

	        	if($this->Cancelbooking_model->insert('',$d,TRUE))
	        	{
	        		$this->Leavecalendar_model->insert('',$c);
	        		throw new Exception('Data <b>berhasil</b> tersimpan.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> tersimpan.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
        $post = $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        
        $rules 	= $this->Cancelbooking_model->rules;
        $this->form_validation->set_rules($rules);
       	
       	$d =[
				'kd_poli' => isset($post['kd_poli']) ? $post['kd_poli'] : '',
				'kd_dokter' => isset($post['kd_dokter']) ? $post['kd_dokter'] : '',
				'judul' => isset($post['judul']) ? $post['judul'] : '',
				'keterangan' => isset($post['keterangan']) ? $post['keterangan'] : '',
				'masuk_tgl' => date('Y-m-d H:i:s'),
				'masuk_oleh' => $this->session->userdata('id_user'),
				'status' => 'Y'
			];
		
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);

	        	if($this->Cancelbooking_model->update('',$d,array('id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);

        foreach ($explode as $key => $value) {
        	$det = $this->Leavecalendar_model->get('','kd_dokter,tgl_awal,tgl_akhir','id=\''.$value.'\'')->result();
			$this->Dokterlibur_model->delete('','kd_dokter=\''.$det[0]->kd_dokter.'\' and date(tanggal) >= \''.$det[0]->tgl_awal.'\' and date(tanggal) <= \''.$det[0]->tgl_akhir.'\'',2);

			if(!$this->Leavecalendar_model->delete('',array('id' => $value)))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b>gagal</b> dihapus';
			}
			else
			{
				$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data <b>berhasil</b> dihapus.';
			}
        } 
    	
    	//echo base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

    public function get_poli(){
    	$post = _post();

    	if($post['kd_dokter']){
    		echo $this->poliklinik($post['kd_dokter']);
    	}
    }

    private function poliklinik($data=''){
    	$this->load->model('Jadwal_model');
    	$res = $this->Jadwal_model->getData(['jadwal.kd_dokter'=>$data])->result_array();
		$split_poli = splitRecursiveArray($res,'kd_poli','nm_poli');

		return htmlSelectFromArray($split_poli, 'name="kd_poli" id="kd_poli" style="width:100%;" class="form-control"', true);
    }

    public function get_pasien_booking(){
    	$post = _post();
    	if(!empty($post['kd_poli']) && !empty($post['kd_dokter'])){
    		$this->data_pasien_booking($post);
    	}
    }

    private function data_pasien_booking($data=''){
    	$this->load->model('Bookingregistrasi_model');

    	$dNow 			= date('Y-m-d');
		$tgl_kedepan 	= AddDate($dNow,07,'days');

		$where = array(
			'a.kd_dokter'=> $data['kd_dokter'],
			'a.tanggal_periksa >='=>$dNow,
			'a.tanggal_periksa <='=>$tgl_kedepan,
			'a.kd_poli'=>$data['kd_poli'],
			'a.status' => 'Belum'
		);

    	$res = $this->Bookingregistrasi_model->getDataBooking($where);
    	if($res){
    		echo '<table class="table table-hover table-bordered table-striped">';
				echo '<thead class="dark">';
					echo '<tr>';
						echo '<th style="width: 20px;">Centang</th>';
						echo '<th style="width: 150px;">Tanggal Periksa</th>';
						echo '<th style="width: 250px;">Nama Poli</th>';
						echo '<th>Nama Dokter</th>';
					echo '</tr>';
				echo '</thead>';
				echo '<body>';
				foreach ($res as $k => $v) {
					echo '<tr>';
						echo '<td><input type="checkbox" id="tglperiksa" name="tglperiksa[]" value="'.$v->tanggal_periksa.'"></td>';
						echo '<td>'.$v->tanggal_periksa.'</td>';
						echo '<td>'.$v->nm_poli.'</td>';
						echo '<td>'.$v->nm_dokter.'</td>';
					echo '</tr>';
				}
				echo '</body>';
			echo '</table>';
    	}else{
    		echo '<table class="table table-hover table-bordered table-striped">';
				echo '<thead class="dark">';
					echo '<tr>';
						echo '<th style="width: 20px;">Centang</th>';
						echo '<th style="width: 150px;">Tanggal Periksa</th>';
						echo '<th style="width: 250px;">Nama Poli</th>';
						echo '<th>Nama Dokter</th>';
					echo '</tr>';
				echo '</thead>';
				echo '<body>';
					echo '<tr>';
						echo '<td colspan="4" style="text-align:center;"><b>Tidak ada pasien booking.</b></td>';
					echo '</tr>';
				echo '</body>';
			echo '</table>';
    	}	
    }

}