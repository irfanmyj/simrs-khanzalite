<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ermpoliklinik extends backend_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Ermpoliklinik_model']);
		$this->site->is_logged_in();
	}

	public function _remap()
	{
	  $segs = $this->_segs;
	  $method = isset($segs[6]) ? $segs[6] : '';
	  switch ($method) {
	    case null;
	    case false;
	    case '':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->index();
	      	break;
	    case 'search':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'search');
	      	$this->search();
	      	break;
	    case 'store':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'add');
	      	$this->store();
	      	break;
	    case 'update':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'update');
	      	$this->update();
	      	break;
	    case 'download':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'download');
	      	$this->download();
	      	break;
	    case 'destroy':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'delete');
	    	$this->destroy();
	    	break;
	    case 'print':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'print');
	    	$this->print();
	    	break;
	    case 'start':
	    	$this->site->is_access(acl(decrypt_aes($segs[2])),'view');
	      	$this->start();
	      	break;
	    default:
	      show_404();
	      break;
	  }
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function index()
	{
		$segs 		= $this->_segs;
		$find_text 	= trim(str_replace('-',' ',substr($segs[3],(strpos($segs[3],"=")+1))));

		//
		$id_module 	= isset($segs[2]) 	? $segs[2] : '';
		$search		= isset($find_text) ? $find_text : '' ;
		$limit		= isset($segs[4]) 	? $segs[4] : $this->_cf->default_limit ;
		$offset		= isset($segs[5]) 	? $segs[5] : 0 ;
		$limit_start= ($offset>1) ? ($offset * $limit) - $limit : 0;
		$dNow 		= '2019-11-01';//date('Y-m-d');
		$time 	= date('H');
		$kdpoli = ($this->session->userdata('kd_poli')) ? getKdPoli(@$this->session->userdata('kd_poli'),$time) : [];

		
		$where['reg_periksa.status_lanjut'] = 'Ralan';
		$where['reg_periksa.tgl_registrasi'] = date('Y-m-d',strtotime($dNow));
		if($this->session->userdata('level_access')!=1){
			$where['reg_periksa.kd_dokter'] = $this->session->userdata('code_doctor');
			$where['reg_periksa.kd_poli'] = $kdpoli;
		}
		

		$dtkdpol = ($this->session->userdata('kd_poli')) ? $this->session->userdata('kd_poli') : [];
		foreach ($dtkdpol as $k => $v) {
            $ls[$v->kd_poli] = $v->kd_poli;
        }

		// Cek akses untuk fitur pencarian
		if(@acl(decrypt_aes($segs[2]))['search']=='Y')
		{
			if($search)
			{
				$where['reg_periksa.no_rkm_medis'] = $search;
			}
		}

		$res_count	= $this->Ermpoliklinik_model->countData($where);
		$res_data 	= $this->Ermpoliklinik_model->getData($where,$limit,$limit_start,$res_count);
		$url = base_url($segs[1].'/'.$id_module.'/search=');
		$pagination = lucida_pagination(base_url($segs[1].'/'.$id_module.'/search='.$search.'/'),$limit,$offset,$res_count);

		$data = array(
			'title' => 'Dokter E-RM Poliklinik',
			'url' => $url,
			'limit' => $limit,
			'limit_rows' => $this->_cf->limit_rows,
			'offset' => $offset,
			'search' => $search,
			'data' => $res_data,
			'res_count' => $res_count,
			'pagination' => $pagination,
			'status' => $this->_cf->statusdata,
			'file' => 'index',
			'folder' => ucwords($segs[1])
		);
		
		$this->site->view('inc',$data);
	}

	/**
	* Menampilkan list pasien saat ini yang sedang aktif.
	* Pencarian berdasarkan tanggal kunjungan
	* 
	* @ default load method
	*/
	public function store()
    {
        $post 	= $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $rules 	= $this->Ermpoliklinik_model->rules;
		$this->form_validation->set_rules($rules);

        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['id']);
	        	unset($post['url']);

	        	if(!$this->Ermpoliklinik_model->insert('',$post,FALSE))
	        	{
	        		throw new Exception('Data <b>berhasil</b> tersimpan.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> tersimpan.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function update()
    {
        $post = $this->input->post();
        $error 	= '';
        $url 	= isset($post['url']) ? $post['url'] : '';
        $id 	= isset($post['id']) ? $post['id'] : '';
        
        $rules 	= $this->Ermpoliklinik_model->rules;
        $this->form_validation->set_rules($rules);
       	
        try {
        	if($this->form_validation->run() == FALSE)
	        {
	        	$error = 1;
	        	throw new Exception(validation_errors());
	        }else{
	        	unset($post['url']);
	        	unset($post['id']);

	        	if($this->Ermpoliklinik_model->update('',$post,array('id' => $id)))
	        	{
	        		throw new Exception('Data <b>berhasil</b> diperbaharui.');
	        	}
	        	else
	        	{
	        		$error = 1;
	        		throw new Exception('Data <b>gagal</b> diperbaharui.');
	        	}
	        }

        } catch (Exception $e) {
        	$_SESSION['error'] = $error;
			$_SESSION['msg'] 	= $e->getMessage();
        }

        echo json_encode([
        	'url' => $url
        ]);
    }

    public function destroy()
    {
        $segs 		= $this->_segs;
        $explode 	= explode('del',$segs[7]);

        foreach ($explode as $key => $value) {

			if(!$this->Ermpoliklinik_model->delete('',array('id' => $value)))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b>gagal</b> dihapus';
			}
			else
			{
				$_SESSION['error'] = '';
            	$_SESSION['msg']   = 'Data <b>berhasil</b> dihapus.';
			}
        } 
    	
    	//echo base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]);
    	generateReturn(base_url($segs[1].'/'.$segs[2].'/search=/'.$segs[4].'/'.$segs[5]));
    }

    public function start(){
    	$segs = $this->_segs;
    	$parameter 	= explode('-', decrypt_aes($segs[7]));
    	$no_rawat 	= $parameter[0];
    	$kd_poli 	= $parameter[1];
    	$kd_dokter 	= $parameter[2];
    	$no_reg 	= $parameter[3];
    	$no_rkm_medis = $parameter[4];
    	$nm_pasien 	= $parameter[5];
    	$jk 		= $parameter[6];
    	$tgl_lahir 	= $parameter[7];

    	print_r($parameter);
    }
}