<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Version extends CI_Migration {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper('db_helper');
        }

        public function up()
        {
                $this->dbforge->add_field([
                        'id_user' => [
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE        
                        ],
                        'username' => [
                                'type' => 'VARCHAR',
                                'constraint' => '50'
                        ],
                        'password' => [
                                'type' => 'VARCHAR',
                                'constraint' => '100',   
                        ],
                        'is_active' => [
                                'type' => 'CHAR',
                                'constraint' => '1',   
                        ],
                        'level_access' => [
                                'type' => 'INT',
                                'constraint' => '3',   
                        ],
                        'created_at' => [
                                'type' => 'TIMESTAMP'
                        ],
                        'updated_at' => [
                                'type' => 'TIMESTAMP',
                                'null' => true
                        ],
                        'deleted_at' => [
                                'type' => 'TIMESTAMP',
                                'null' => true
                        ]
                ]);
                       
                $this->dbforge->add_key('id_user', TRUE);
                $this->dbforge->create_table('blog');
                $this->db->query(add_foreign_key('blog', 'level_access', 'level_access(id)', 'CASCADE', 'RESTRICT'));
        }

        public function down()
        {
                $this->dbforge->drop_table('blog');
        }
}