<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Version extends CI_Migration {

	protected $tb_routes = 'routes';
	protected $tb_modules = 'modules';
	protected $tb_level_access = 'level_access';
	protected $tb_users = 'users';

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('db_helper');
	}

	public function up(){
		/*
		| Created table modules into database
		| Relation : no
		*/
       	$this->tables_modules($this->tb_modules);
        /* End table modules */

        /*
		| Created table routes into database
		| Relation : no
		*/
		
       	$this->tables_routes($this->tb_routes);
       	$this->insert_routes($this->tb_routes);
        /* End table routes */

		/*
		| Created table level_access into database
		| Relation : no
		*/
       	$this->tables_level_access($this->tb_level_access);
        /* End table level_access */

        /*
		| Created table users into database
		| Relation : users(level_access) -> level_access(id) -> CASCADE -> RESTRICT
		*/
		$this->tables_users($this->tb_users,$this->tb_level_access);
        /* End table users*/

	}

	public function down()
    {
    	$this->dbforge->drop_table($this->tb_users);
    	$this->dbforge->drop_table($this->tb_level_access);
    	$this->dbforge->drop_table($this->tb_modules);
    	$this->dbforge->drop_table($this->tb_routes);
    }

    private function tables_users($tables1,$tables2)
    {
    	$this->dbforge->add_field([
			'id_user' => [
				'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
			],
            'username' => [
				'type' => 'VARCHAR',
				'constraint' => '50'
            ],
            'password' => [
				'type' => 'VARCHAR',
				'constraint' => '100',   
            ],
            'is_active' => [
				'type' => 'CHAR',
				'constraint' => '1',   
            ],
            'level_access' => [
				'type' => 'INT',
				'constraint' => '3',
				'unsigned' => TRUE  
            ],
            'created_at' => [
				'type' => 'TIMESTAMP'
            ],
            'updated_at' => [
				'type' => 'TIMESTAMP',
				'null' => true
            ],
            'deleted_at' => [
				'type' => 'TIMESTAMP',
				'null' => true
            ]
		]);

		$this->dbforge->add_key('id_user', TRUE);
        $this->dbforge->create_table($tables1);
        $this->db->query(
	    	add_foreign_key(
	    		$tables1, 
	    		'level_access', 
	    		$tables2.'(id)', 
	    		'CASCADE', 
	    		'RESTRICT')
        );
    }

    private function tables_level_access($tables)
    {
    	$this->dbforge->add_field([
			'id' => [
				'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
			],
            'name' => [
				'type' => 'VARCHAR',
				'constraint' => '50'
            ],
            'access' => [
				'type' => 'LONGTEXT'   
            ],
            'created_at' => [
				'type' => 'TIMESTAMP'
            ],
            'updated_at' => [
				'type' => 'TIMESTAMP',
				'null' => true
            ],
            'deleted_at' => [
				'type' => 'TIMESTAMP',
				'null' => true
            ]
		]);

		$this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($tables);
    }

    private function tables_modules($tables)
    {
    	$this->dbforge->add_field([
			'module_id' => [
				'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
			],
			'parent_id' => [
				'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'default' => 0
			],
            'name_module' => [
				'type' => 'VARCHAR',
				'constraint' => '200'
            ],
            'link' => [
				'type' => 'VARCHAR',
				'constraint' => '150'   
            ],
            'view' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'N',   
				'null' => 'FALSE'   
            ],
            'add' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'N',   
				'null' => 'FALSE'    
            ],
            'update' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'N',   
				'null' => 'FALSE'    
            ],
            'delete' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'N',   
				'null' => 'FALSE'    
            ],
            'upload' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'N',   
				'null' => 'FALSE'    
            ],
            'download' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'N',   
				'null' => 'FALSE'    
            ],
            'search' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'N',   
				'null' => 'FALSE'    
            ],
            'print' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'N',   
				'null' => 'FALSE'    
            ],
            'icon' => [
				'type' => 'VARCHAR',
				'constraint' => '100'   
            ],
            'atribut' => [
				'type' => 'TEXT'  
            ],
            'position' => [
				'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE
			],
			'status' => [
				'type' => 'ENUM("Y","N")',
				'default' => 'Y',   
				'null' => 'FALSE'    
            ],
            'created_at' => [
				'type' => 'TIMESTAMP'
            ],
            'updated_at' => [
				'type' => 'TIMESTAMP',
				'null' => true
            ],
            'deleted_at' => [
				'type' => 'TIMESTAMP',
				'null' => true
            ]
		]);

		$this->dbforge->add_key('module_id', TRUE);
        $this->dbforge->create_table($tables);
    }

    private function tables_routes($tables)
    {
    	$this->dbforge->add_field([
			'id' => [
				'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
			],
            'controller' => [
				'type' => 'VARCHAR',
				'constraint' => '150'
            ],
            'routes' => [
				'type' => 'VARCHAR',
				'constraint' => '250'   
            ],
            'position' => [
				'type' => 'INT',
                'constraint' => 5
			],
            'created_at' => [
				'type' => 'TIMESTAMP'
            ],
            'updated_at' => [
				'type' => 'TIMESTAMP',
				'null' => true
            ],
            'deleted_at' => [
				'type' => 'TIMESTAMP',
				'null' => true
            ]
		]);

		$this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($tables);
    }

    private function insert_routes($tables)
    {
    	$datas = [
    		1 => [
    			'controller' => 'Auth',
    			'routes' => 'default_controller',
    			'position' => 1
    		]
    	];

    	$this->db->insert_batch($tables, $datas); 
    }

}

