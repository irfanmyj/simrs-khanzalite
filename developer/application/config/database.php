<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['dsn']      The full DSN string describe a connection to the database.
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database driver. e.g.: mysqli.
|			Currently supported:
|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Query Builder class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['encrypt']  Whether or not to use an encrypted connection.
|
|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
|
|				'ssl_key'    - Path to the private key file
|				'ssl_cert'   - Path to the public key certificate file
|				'ssl_ca'     - Path to the certificate authority file
|				'ssl_capath' - Path to a directory containing trusted CA certificates in PEM format
|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not
|
|	['compress'] Whether or not to use client compression (MySQL only)
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
|	['failover'] array - A array with 0 or more data for connections if the main should fail.
|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
| 				NOTE: Disabling this will also effectively disable both
| 				$this->db->last_query() and profiling of DB queries.
| 				When you run a query, with this setting set to TRUE (default),
| 				CodeIgniter will store the SQL statement for debugging purposes.
| 				However, this may cause high memory usage, especially if you run
| 				a lot of SQL queries ... disable this to avoid that problem.
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $query_builder variables lets you determine whether or not to load
| the query builder class.
*/
global $Cf;
$active_group = $Cf->_group1;
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => $Cf->decrypt_aes_256_cbc($Cf->_hostname1),
	'username' => $Cf->decrypt_aes_256_cbc($Cf->_username1),
	'password' => $Cf->decrypt_aes_256_cbc($Cf->_password1),
	'database' => $Cf->decrypt_aes_256_cbc($Cf->_database1),
	'dbdriver' => $Cf->_dbdriver1,
	'dbprefix' => $Cf->_dbprefix1,
	'pconnect' => $Cf->_pconnect1,
	'db_debug' => $Cf->_db_debug1,
	'cache_on' => $Cf->_cache_on1,
	'cachedir' => $Cf->_cachedir1,
	'char_set' => $Cf->_char_set1,
	'dbcollat' => $Cf->_dbcollat1, //'utf8_general_ci',
	'swap_pre' => $Cf->_swap_pre1,
	'encrypt' => $Cf->_encrypt1,
	'compress' => $Cf->_compress1,
	'stricton' => $Cf->_stricton1,
	'failover' => $Cf->_failover1,
	'save_queries' => $Cf->_save_queries1,
	'port' => $Cf->_port1
);

$db['dbtwo'] = array(
	'dsn'	=> '',
	'hostname' => $Cf->decrypt_aes_256_cbc($Cf->_hostname2),
	'username' => $Cf->decrypt_aes_256_cbc($Cf->_username2),
	'password' => $Cf->decrypt_aes_256_cbc($Cf->_password2),
	'database' => $Cf->decrypt_aes_256_cbc($Cf->_database2),
	'dbdriver' => $Cf->_dbdriver2,
	'dbprefix' => $Cf->_dbprefix2,
	'pconnect' => $Cf->_pconnect2,
	'db_debug' => $Cf->_db_debug2,
	'cache_on' => $Cf->_cache_on2,
	'cachedir' => $Cf->_cachedir2,
	'char_set' => $Cf->_char_set2,
	'dbcollat' => $Cf->_dbcollat2, //'utf8_general_ci',
	'swap_pre' => $Cf->_swap_pre2,
	'encrypt' => $Cf->_encrypt2,
	'compress' => $Cf->_compress2,
	'stricton' => $Cf->_stricton2,
	'failover' => $Cf->_failover2,
	'save_queries' => $Cf->_save_queries2,
	'port' => $Cf->_port2
);

