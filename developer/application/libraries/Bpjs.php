<?php

class Bpjs {
	private $refrensi;
	private $peserta;
	private $rujukan;
	private $sep;
	private $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->refrensi = new Nsulistiyawan\Bpjs\VClaim\Referensi($this->CI->_vclam);
		$this->peserta = new Nsulistiyawan\Bpjs\VClaim\Peserta($this->CI->_vclam);
		$this->rujukan = new Nsulistiyawan\Bpjs\VClaim\Rujukan($this->CI->_vclamtes);
		$this->sep = new Nsulistiyawan\Bpjs\VClaim\Sep($this->CI->_vclamtes);
		$this->aplicare = new Nsulistiyawan\Bpjs\Aplicare\KetersediaanKamar($this->CI->_aplicare);
	}

	/*
	Refrensi
	*/
	public function diagnosa($keyword='')
	{
		if($keyword){
			$values = $this->refrensi->diagnosa($keyword);	
		}
		
		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function poli($keyword=''){
		if($keyword){
			$values = $this->refrensi->poli($keyword);	
		}
		
		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function dokterdpjp($jnsPelayanan='', $tglPelayanan='', $spesialis=''){
		if(!empty($jnsPelayanan) && !empty($tglPelayanan) && !empty($spesialis)){
			$values = $this->refrensi->dokterDpjp($jnsPelayanan,$tglPelayanan,$spesialis);	
		}
		
		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function Peserta($no='',$tgl='',$cat=''){
		
		if ($cat=='kartu') {
			$values = $this->peserta->getByNoKartu($no,$tgl);
		}
		else
		{
			$values = $this->peserta->getByNIK($no,$tgl);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function cariByNoRujukan($searchBy,$keyword)
	{
		if($keyword){
			$values = $this->rujukan->cariByNoRujukan($searchBy, $keyword);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function propinsi(){
		$res = $this->refrensi->propinsi();
		
		if($res){
			$errno = 'Connected';
			$datas = $res; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function kabupaten($kdprop=''){
		if($kdprop){
			$values = $this->refrensi->kabupaten($kdprop);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function kecamatan($kdkab=''){
		if($kdkab){
			$values = $this->refrensi->kecamatan($kdkab);
		}
		
		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}


	// Aplicare
	public function RefKelas()
	{
		$res = $this->aplicare->refKelas();
		return $res;
	}

	public function BedGet($id,$start,$limit){
		$res = $this->aplicare->bedGet($id,$start,$limit);
		return $res;
	}

	public function BedUpdate($id,$data){
		$res = $this->aplicare->bedUpdate($id,$data);
		return $res;
	}

	public function BedCreate($id,$data){
		$res = $this->aplicare->bedCreate($id,$data);
		return $res;
	}

	public function BedDelete($id,$data){
		$res = $this->aplicare->bedDelete($id,$data);
		return $res;
	}

	/*
	Vclaim SEP
	*/
	public function insertSEP($data = []){
		if(is_array($data)){
			$values = $this->sep->insertSEP($data);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function updateSEP($data = []){
		if(is_array($data)){
			$values = $this->sep->updateSEP($data);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function deleteSEP($data = []){
		if(is_array($data)){
			$values = $this->sep->deleteSEP($data);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}
	
	public function updateTglPlg($data = []){
		if(is_array($data)){
			$values = $this->sep->updateTglPlg($data);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function cariSEP($data=''){
		if($data){
			$values = $this->sep->cariSEP($keyword);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function inacbgSEP($data=''){
		if($data){
			$values = $this->sep->inacbgSEP($keyword);
		}

		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

}