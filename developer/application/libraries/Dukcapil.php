<?php

class Dukcapil {
	
	private $datas;

	public function __construct()
	{
		$this->datas = new Nsulistiyawan\Bpjs\Data\Penduduk();
	}

	public function get($data=[],$msg=''){

		$values = $this->datas->getByNIK(json_encode($data));
		if($values){
			$errno = 'Connected';
			$datas = $values; 
		}else{
			$errno = 'NotConnected';
			$datas = 'Tidak bisa terhubung dengan server.';
		}
		
		$res = ['error'=>$errno,'data'=>$datas];
		return $res;
	}

	public function pingDomain($domain){
	    $starttime = microtime(true);
	    $file      = @fsockopen($domain,-1, $errno, $errstr, 1);
	    $stoptime  = microtime(true);
	    $status    = '';

	    if (!$file){
	    	$status =  'putus';  // Site is down
	    }else{
	        fclose($file);
	        $status = 'terhubung';
	    }

	    return $status;
	}
}