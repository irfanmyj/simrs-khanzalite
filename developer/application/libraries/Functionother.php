<?php 

class Functionother {
	
	private $CI;

	function __construct() {
		$this->CI =& get_instance();
	}

	/*
	| Ambil Cara Bayar
	*/
	public function pay()
    {
    	$this->CI->load->model(['Pay_model']);
		$where = ['deleted_at'=>NULL]; 
		$res = $this->CI->Pay_model->get('','id,name',$where)->result_array();
		$split = splitRecursiveArray($res,'id','name'); 
		return $split;
    }

    public function rekening_coa(){
        $this->CI->load->model(['Rekening_model']);
        $res = $this->CI->Rekening_model->get('','kd_rek,nm_rek')->result_array();
        $split = splitRecursiveArray($res,'kd_rek','nm_rek'); 
        return $split;
    }

    public function sukubangsa($split=false){
        $this->CI->load->model(['Sukubangsa_model']);
        $res = $this->CI->Sukubangsa_model->get('','*')->result_array();
        if($split==true){
            $data = splitRecursiveArray($res,'id','nama_suku_bangsa');
        }else{
            $data = $res;
        }
        
        return $data;
    }

    public function bahasapasien($split=false){
        $this->CI->load->model(['Bahasapasien_model']);
        $res = $this->CI->Bahasapasien_model->get('','*')->result_array();
        if($split==true){
            $data = splitRecursiveArray($res,'id','nama_bahasa');
        }else{
            $data = $res;
        }
        
        return $data;
    }

    public function cacatfisik($split=false){
        $this->CI->load->model(['Cacatfisik_model']);
        $res = $this->CI->Cacatfisik_model->get('','*')->result_array();
        if($split==true){
            $data = splitRecursiveArray($res,'id','nama_cacat');
        }else{
            $data = $res;
        }
        
        return $data;
    }

    public function penjab($split=false){
        $this->CI->load->model(['Penjab_model']);
        $res = $this->CI->Penjab_model->get('','kd_pj,png_jawab')->result_array();
        if($split==TRUE){
            $data = splitRecursiveArray($res,'kd_pj','png_jawab'); 
        }else{
            $data = $res;
        }
        
        return $data;
    }

    /*
    | Ambil Biaya Pendafataran
    */
    public function Admin_cost()
    {
        $this->CI->load->model(['Administrativecosts_model']);
        $where = ['deleted_at'=>NULL,'status'=>'Y']; 
        $res = $this->CI->Administrativecosts_model->get('','id',$where,'','','id')->result();
        return $res[0]->id;
    }

    /*
	| Ambil Data Poliklinik
    */
    public function get_polyclinic()
    {
    	$this->CI->load->model('Doctorsschedule_model');
		$filed = '
			polyclinic.id,
			polyclinic.name,
		';

		$where = ['doctors_schedule.deleted_at'=>NULL,'days_schedule.name'=>ucwords(indoDays(date('Y-m-d')))];
		$res = $this->CI->Doctorsschedule_model->getSchedule($where,$filed)->result_array();
		return $res;
    }

    public function get_poliklinik($where=''){
        $this->CI->load->model('Poliklinik_model');
        $filed = '
            kd_poli,
            nm_poli
        ';

        if($where){
            $where = $where;
        }
        
        $res = $this->CI->Poliklinik_model->get('',$filed,$where)->result_array();
        return $res;
    }

    /*
	| Ambil Golongan Darah
    */
	public function get_bloodtype()
    {
    	$this->CI->load->model('Bloodtype_model');
    	$res = $this->CI->Bloodtype_model->get('','id,name',['deleted_at'=>NULL])->result_array();
    	return $res;
    }

    /*
	| Ambil Data Agama
    */
    public function get_religion()
    {
    	$this->CI->load->model('Religion_model');
    	$res = $this->CI->Religion_model->get('','id,name',['deleted_at'=>NULL])->result_array();
    	return $res;
    }

    /*
    | Ambil Data Pekerjaan
    */
    public function get_professional()
    {
        $this->CI->load->model('Professional_model');
        $res = $this->CI->Professional_model->get('','id,name',['deleted_at'=>NULL])->result_array();
        return $res;
    }

    /*
	| Ambil Data Sekolah
    */
    public function get_school()
    {
        $this->CI->load->model('School_model');
        $res = $this->CI->School_model->get('','id,name',['deleted_at'=>NULL])->result_array();
        return $res;
    }

    /*
	| Ambil Data Bahasa
    */
    public function get_language()
    {
        $this->CI->load->model('Language_model');
        $res = $this->CI->Language_model->get('','id,name',['deleted_at'=>NULL])->result_array();
        return $res;
    }

    /*
	| Ambil Nomor Rekam Medis Terakhir
    */
    public function getnormmedic()
    {
    	$this->CI->load->model('Pasien_model');
    	$res = $this->CI->Pasien_model->get('','no_rkm_medis','','','','tgl_daftar DESC',1)->result();
    	$no_rkm_medis = count_last_norm($res[0]->no_rkm_medis);
    	return $no_rkm_medis;
    }

    /*
    | Ambil No Rawat terkahir berdasarkan tanggal_periksa
    */
    public function getnorawatlast($tanggal_periksa=''){
        $this->CI->load->model('Gppendaftaran_model');
        $tgl_registrasi = date('Y/m/d', strtotime($tanggal_periksa));
        // Get No Rawat Paling Terbesar
        $getNoRawat = $this->CI->Gppendaftaran_model->get('','max(no_rawat) as no_rawat',array('tgl_registrasi'=>$tanggal_periksa),'','','',1)->result();
        
        $no_urut_rawat = ($getNoRawat[0]->no_rawat) ? substr($getNoRawat[0]->no_rawat, 11, 6) : 0 ;
        $no_rawat = $tgl_registrasi.'/'.sprintf('%06s', ($no_urut_rawat + 1));
        return $no_rawat;
    }

    /*
	|
    */
	public function getphysician()
    {
    	$this->CI->load->model('Doctorsschedule_model');
		$post = $this->CI->input->post();
		$filed = '
			users.id_user,
			detail_users.full_name
		';

		$where = ['doctors_schedule.deleted_at'=>NULL,'days_schedule.name'=>ucwords(indoDays(date('Y-m-d'))),'doctors_schedule.id_polyclinic'=>$post['id_polyclinic']];
		$res = $this->CI->Doctorsschedule_model->getSchedule($where,$filed)->result_array();
		$physician = splitRecursiveArray($res,'id_user','full_name');
		return htmlSelectFromArray($physician,'name="id_physician" class="form-control id_physician1" style="width:100%;"',TRUE);
		 
    }

    public function get_dokter($where='',$select=false)
    {
        $this->CI->load->model('Dokter_model');
        $filed = '
            kd_dokter,
            nm_dokter
        ';

        if($where){
            $where = $where;
        }

        $res = $this->CI->Dokter_model->get('',$filed,$where)->result_array();

        if($select==true){
            $physician = splitRecursiveArray($res,'kd_dokter','nm_dokter');
            return htmlSelectFromArray($physician,'name="kd_dokter" class="form-control kd_dokter" style="width:100%;"',TRUE);
        }else{
            return $res;
        }       
    }

    /*
	|
    */
	public function getparent()
    {
    	$this->CI->load->model('Patientdatamaster_model');
    	$post = $this->CI->input->post();
    	$where = ['parent'=>$post['norm_medic'],'family'=>'Istri'];
    	if($post['responsible_relationship']==3)
    	{
    		$where = ['norm_medic'=>$post['norm_medic'],'family'=>'Suami'];
    	}
    	
    	$res = $this->CI->Patientdatamaster_model->get('','full_name,addres',$where)->result_array();
    	return $res;
    }

    public function getpatient(){
    	$this->CI->load->model('Patientdatamaster_model');
    	$post = $this->CI->input->post();
    	$where = 'patient_data_master.deleted_at IS NULL and norm_medic like \'%'.$post['norm_medic'].'%\' || nik like \'%'.$post['norm_medic'].'%\' || full_name like \'%'.$post['norm_medic'].'%\'';//['patient_data_master.deleted_at'=>NULL,'norm_medic'=>$post['norm_medic']];
    	$res = $this->CI->Patientdatamaster_model->getData($where);
    	return $res;
    }

    public function get_state(){
    	$this->CI->load->model('State_model');
    	
    	$where = ['deleted_at'=>NULL];//['patient_data_master.deleted_at'=>NULL,'norm_medic'=>$post['norm_medic']];
    	$res = $this->CI->State_model->get('','id,name',$where)->result_array();
    	return $res;
    }

    public function get_checking_nik($data=[]){
        if(is_array($data))
        {
            $this->CI->load->model('Patientdatamaster_model');
            $res = $this->CI->Patientdatamaster_model->get('','*',$data)->result_array();
            return $res;
        }
    }

    public function get_tb_pasien($where=''){
        $this->CI->load->model('Pasien_model');
        $res = $this->CI->Pasien_model->get('','no_rkm_medis,nm_pasien,tgl_lahir,jk,keluarga,namakeluarga,kd_pj,alamatpj',$where,'','','',10)->result();
        return $res;
    }

    /*
    Get Kode
    */
    public function getCodeJnsperawatan(){
        $this->CI->load->model(['Jnsperawatan_model']);
        $res = $this->CI->Jnsperawatan_model->get('','MAX(kd_jenis_prw) as kd_jenis_prw','','','','kd_jenis_prw DESC',1)->result_array();
        return $res;
    }

    public function getCodeJenis(){
        $this->CI->load->model(['Jenis_model']);
        $res = $this->CI->Jenis_model->get('','MAX(kdjns) as kdjns','','','','kdjns DESC',1)->result_array();
        return $res;
    }

    public function getCodeIndustrifarmasi(){
        $this->CI->load->model(['Industrifarmasi_model']);
        $res = $this->CI->Industrifarmasi_model->get('','MAX(kode_industri) as kode_industri','','','','kode_industri DESC',1)->result_array();
        return $res;
    }

    public function getJenisWilayah(){
        $this->CI->load->model(['Jeniswilayah_model']);
        $res = $this->CI->Jeniswilayah_model->get('','*','','','','id_jenis DESC')->result_array();
        return $res;
    }

    public function getNegara(){
        $this->CI->load->model(['Negara_model']);
        $res = $this->CI->Negara_model->get('','id,nama','','','','id DESC')->result_array();
        return $res;
    }

    public function getProvinsi($split=false){
        $this->CI->load->model(['Provinsi_model']);
        $res = $this->CI->Provinsi_model->get('','kd_prop,nm_prop','','','','kd_prop DESC')->result_array();
        if($split==true){
            $data = splitRecursiveArray($res,'kd_prop','nm_prop');
        }else{
            $data = $res;
        }
        return $data;
    }

    public function getProvinsiBpjs($split=false){
        $this->CI->load->model(['Provinsi_model']);
        $res = $this->CI->Provinsi_model->get('','kd_prop_bpjs,nm_prop','','','','kd_prop_bpjs DESC')->result_array();
        if($split==true){
            $data = splitRecursiveArray($res,'kd_prop_bpjs','nm_prop');
        }else{
            $data = $res;
        }
        return $data;
    }

    public function getKabupaten(){
        $this->CI->load->model(['Kabupaten_model']);
        $res = $this->CI->Kabupaten_model->get('','kd_kab,nm_kab','','','','kd_kab DESC')->result_array();
        return $res;
    }

    public function getKecamatan($where=''){
        $this->CI->load->model(['Kecamatan_model']);

        if($where){
            $res = $this->CI->Kecamatan_model->getSearch($where,10)->result_array();
        }else{
            $res = $this->CI->Kecamatan_model->get('','kd_kec,nm_kec as nama','','','','kd_kec DESC')->result_array();
        }

        
        return $res;
    }
}