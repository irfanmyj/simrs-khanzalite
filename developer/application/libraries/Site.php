<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Site{

	public $themes;
	public $themes_error;
	public $template;
	public $template_setting;

	function view($pages,$data=NULL)
	{
		$CI =& get_instance();

		$data ?
		$CI->load->view($this->themes.'/'.$this->template.'/'.$pages,$data)
			:
				$CI->load->view($this->themes.'/'.$this->template.'/'.$pages);
	}

	function view_error($pages,$data = NULL){
		$CI =& get_instance();

		$data ? 
		$CI->load->view($this->themes_error.'/'.$this->template_error.'/'.$pages,$data)
			:
				$CI->load->view($this->themes_error.'/'.$this->template_error.'/'.$pages);
	}

	public function get_template($view){
		return $this->view($view);
	}

	function is_logged_in(){
		$_this =& get_instance();
				
		if($this->themes == 'lucida')
		{
			if($_this->session->userdata('logged_in')!=TRUE && $_this->session->userdata('id_level')=='' && $_this->session->userdata('status')!='Y')
			{
				$url = base_url('login');
				redirect($url);
			}
		}
		else
		{
			$url = base_url('login');
			redirect($url);
		}
	}

	function is_access($data,$key)
	{
		if($data[$key] != 'Y')
		{
			$url = base_url('logout');
			redirect($url);
		}
	}

}