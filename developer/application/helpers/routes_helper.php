<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function routes()
{
	$route = 
	[
	'default_controller' => 'Auth',
	'login' => 'Auth/index',
	'proses' => 'Auth/checking',
	'logout' => 'Auth/logout',

	//Dashboard Route
	'dashboard/(:any)' => 'Dashboard/index',
	'dashboard/(:any)/(:any)' => 'Dashboard/index',
	'dashboard/(:any)/(:any)/(:any)' => 'Dashboard/index',
	'dashboard/(:any)/(:any)/(:any)/(:any)' => 'Dashboard/index',

	//Rawatinap Route
	'rawatinap/(:any)' => 'Rawatinap/index',
	'rawatinap/(:any)/(:any)' => 'Rawatinap/index',
	'rawatinap/(:any)/(:any)/(:any)/' => 'Rawatinap/index',
	'rawatinap/(:any)/(:any)/(:any)/(:any)' => 'Rawatinap/index',

	//Bangsal Route
	'nursinginpatient/(:any)' => 'Nursinginpatient/index',
	'nursinginpatient/(:any)/(:any)' => 'Nursinginpatient/index',
	'nursinginpatient/(:any)/(:any)/(:any)' => 'Nursinginpatient/index',
	'nursinginpatient/(:any)/(:any)/(:any)/(:any)' => 'Nursinginpatient/index',
	'nursinginpatient/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Nursinginpatient/destroy',
	'nursinginpatient/(:any)/(:any)/(:any)/(:any)/store' => 'Nursinginpatient/store',
	'nursinginpatient/(:any)/(:any)/(:any)/(:any)/update' => 'Nursinginpatient/update',

	//Bangsal Route
	'ward/(:any)' => 'Ward/index',
	'ward/(:any)/(:any)' => 'Ward/index',
	'ward/(:any)/(:any)/(:any)' => 'Ward/index',
	'ward/(:any)/(:any)/(:any)/(:any)' => 'Ward/index',
	'ward/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Ward/destroy',
	'ward/(:any)/(:any)/(:any)/(:any)/store' => 'Ward/store',
	'ward/(:any)/(:any)/(:any)/(:any)/update' => 'Ward/update',

	//Classroom Route
	'classroom/(:any)' => 'Classroom/index',
	'classroom/(:any)/(:any)' => 'Classroom/index',
	'classroom/(:any)/(:any)/(:any)' => 'Classroom/index',
	'classroom/(:any)/(:any)/(:any)/(:any)' => 'Classroom/index',
	'classroom/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Classroom/destroy',
	'classroom/(:any)/(:any)/(:any)/(:any)/store' => 'Classroom/store',
	'classroom/(:any)/(:any)/(:any)/(:any)/update' => 'Classroom/update',

	//Classroom Route
	'roominpatient/(:any)' => 'Roominpatient/index',
	'roominpatient/(:any)/(:any)' => 'Roominpatient/index',
	'roominpatient/(:any)/(:any)/(:any)' => 'Roominpatient/index',
	'roominpatient/(:any)/(:any)/(:any)/(:any)' => 'Roominpatient/index',
	'roominpatient/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Roominpatient/destroy',
	'roominpatient/(:any)/(:any)/(:any)/(:any)/store' => 'Roominpatient/store',
	'roominpatient/(:any)/(:any)/(:any)/(:any)/update' => 'Roominpatient/update',

	//Cost Room Route
	'costroom/(:any)' => 'Costroom/index',
	'costroom/(:any)/(:any)' => 'Costroom/index',
	'costroom/(:any)/(:any)/(:any)' => 'Costroom/index',
	'costroom/(:any)/(:any)/(:any)/(:any)' => 'Costroom/index',
	'costroom/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Costroom/destroy',
	'costroom/(:any)/(:any)/(:any)/(:any)/store' => 'Costroom/store',
	'costroom/(:any)/(:any)/(:any)/(:any)/update' => 'Costroom/update',

	//Pasien Route
	'patient/(:any)' => 'Pasien/index',
	'patient/(:any)/(:any)' => 'Pasien/index',
	'patient/(:any)/(:any)/(:any)' => 'Pasien/index',
	'patient/(:any)/(:any)/(:any)/(:any)' => 'Pasien/index',
	'patient/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Pasien/destroy',
	'patient/(:any)/(:any)/(:any)/(:any)/store' => 'Pasien/store',
	'patient/(:any)/(:any)/(:any)/(:any)/update' => 'Pasien/update',

	//Bloodtype Route
	'bloodtype/(:any)' => 'Bloodtype/index',
	'bloodtype/(:any)/(:any)' => 'Bloodtype/index',
	'bloodtype/(:any)/(:any)/(:any)' => 'Bloodtype/index',
	'bloodtype/(:any)/(:any)/(:any)/(:any)' => 'Bloodtype/index',
	'bloodtype/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Bloodtype/destroy',
	'bloodtype/(:any)/(:any)/(:any)/(:any)/store' => 'Bloodtype/store',
	'bloodtype/(:any)/(:any)/(:any)/(:any)/update' => 'Bloodtype/update',

	//Category Allergy Route
	'categoryallergy/(:any)' => 'Categoryallergy/index',
	'categoryallergy/(:any)/(:any)' => 'Categoryallergy/index',
	'categoryallergy/(:any)/(:any)/(:any)' => 'Categoryallergy/index',
	'categoryallergy/(:any)/(:any)/(:any)/(:any)' => 'Categoryallergy/index',
	'categoryallergy/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Categoryallergy/destroy',
	'categoryallergy/(:any)/(:any)/(:any)/(:any)/store' => 'Categoryallergy/store',
	'categoryallergy/(:any)/(:any)/(:any)/(:any)/update' => 'Categoryallergy/update',

	//Allergy Route
	'allergy/(:any)' => 'Allergy/index',
	'allergy/(:any)/(:any)' => 'Allergy/index',
	'allergy/(:any)/(:any)/(:any)' => 'Allergy/index',
	'allergy/(:any)/(:any)/(:any)/(:any)' => 'Allergy/index',
	'allergy/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Allergy/destroy',
	'allergy/(:any)/(:any)/(:any)/(:any)/store' => 'Allergy/store',
	'allergy/(:any)/(:any)/(:any)/(:any)/update' => 'Allergy/update',

	//Allergy Route
	'allergy/(:any)' => 'Allergy/index',
	'allergy/(:any)/(:any)' => 'Allergy/index',
	'allergy/(:any)/(:any)/(:any)' => 'Allergy/index',
	'allergy/(:any)/(:any)/(:any)/(:any)' => 'Allergy/index',
	'allergy/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Allergy/destroy',
	'allergy/(:any)/(:any)/(:any)/(:any)/store' => 'Allergy/store',
	'allergy/(:any)/(:any)/(:any)/(:any)/update' => 'Allergy/update',

	//Master Pay Route
	'masterpay/(:any)' => 'Masterpay/index',
	'masterpay/(:any)/(:any)' => 'Masterpay/index',
	'masterpay/(:any)/(:any)/(:any)' => 'Masterpay/index',
	'masterpay/(:any)/(:any)/(:any)/(:any)' => 'Masterpay/index',
	'masterpay/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Masterpay/destroy',
	'masterpay/(:any)/(:any)/(:any)/(:any)/store' => 'Masterpay/store',
	'masterpay/(:any)/(:any)/(:any)/(:any)/update' => 'Masterpay/update',

	//Diagnosis Class
	'nursingdiagnosisclass/(:any)' => 'Nursingdiagnosisclass/index',
	'nursingdiagnosisclass/(:any)/(:any)' => 'Nursingdiagnosisclass/index',
	'nursingdiagnosisclass/(:any)/(:any)/(:any)' => 'Nursingdiagnosisclass/index',
	'nursingdiagnosisclass/(:any)/(:any)/(:any)/(:any)' => 'Nursingdiagnosisclass/index',
	'nursingdiagnosisclass/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Diagnosisclass/destroy',
	'nursingdiagnosisclass/(:any)/(:any)/(:any)/(:any)/store' => 'Nursingdiagnosisclass/store',
	'nursingdiagnosisclass/(:any)/(:any)/(:any)/(:any)/update' => 'Nursingdiagnosisclass/update',

	//Diagnosis Domain
	'nursingdiagnosisdomain/(:any)' => 'Nursingdiagnosisdomain/index',
	'nursingdiagnosisdomain/(:any)/(:any)' => 'Nursingdiagnosisdomain/index',
	'nursingdiagnosisdomain/(:any)/(:any)/(:any)' => 'Nursingdiagnosisdomain/index',
	'nursingdiagnosisdomain/(:any)/(:any)/(:any)/(:any)' => 'Nursingdiagnosisdomain/index',
	'nursingdiagnosisdomain/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Nursingdiagnosisdomain/destroy',
	'nursingdiagnosisdomain/(:any)/(:any)/(:any)/(:any)/store' => 'Nursingdiagnosisdomain/store',
	'nursingdiagnosisdomain/(:any)/(:any)/(:any)/(:any)/update' => 'Nursingdiagnosisdomain/update',

	//Diagnosis Master Induk
	'nursingdiagnosis/(:any)' => 'Nursingdiagnosis/index',
	'nursingdiagnosis/(:any)/(:any)' => 'Nursingdiagnosis/index',
	'nursingdiagnosis/(:any)/(:any)/(:any)' => 'Nursingdiagnosis/index',
	'nursingdiagnosis/(:any)/(:any)/(:any)/(:any)' => 'Nursingdiagnosis/index',
	'nursingdiagnosis/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Nursingdiagnosis/destroy',
	'nursingdiagnosis/(:any)/(:any)/(:any)/(:any)/store' => 'Nursingdiagnosis/store',
	'nursingdiagnosis/(:any)/(:any)/(:any)/(:any)/update' => 'Nursingdiagnosis/update',

	//Intervention Master Induk
	'nursingintervention/(:any)' => 'Nursingintervention/index',
	'nursingintervention/(:any)/(:any)' => 'Nursingintervention/index',
	'nursingintervention/(:any)/(:any)/(:any)' => 'Nursingintervention/index',
	'nursingintervention/(:any)/(:any)/(:any)/(:any)' => 'Nursingintervention/index',
	'nursingintervention/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Intervention/destroy',
	'nursingintervention/(:any)/(:any)/(:any)/(:any)/store' => 'Nursingintervention/store',
	'nursingintervention/(:any)/(:any)/(:any)/(:any)/update' => 'Nursingintervention/update',

	//Criteria Master Induk
	'nursingcriteria/(:any)' => 'Nursingcriteria/index',
	'nursingcriteria/(:any)/(:any)' => 'Nursingcriteria/index',
	'nursingcriteria/(:any)/(:any)/(:any)' => 'Nursingcriteria/index',
	'nursingcriteria/(:any)/(:any)/(:any)/(:any)' => 'Nursingcriteria/index',
	'nursingcriteria/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Criteria/destroy',
	'nursingcriteria/(:any)/(:any)/(:any)/(:any)/store' => 'Nursingcriteria/store',
	'nursingcriteria/(:any)/(:any)/(:any)/(:any)/update' => 'Nursingcriteria/update',

	//Purpose Master Induk
	'nursingpurpose/(:any)' => 'Nursingpurpose/index',
	'nursingpurpose/(:any)/(:any)' => 'Nursingpurpose/index',
	'nursingpurpose/(:any)/(:any)/(:any)' => 'Nursingpurpose/index',
	'nursingpurpose/(:any)/(:any)/(:any)/(:any)' => 'Nursingpurpose/index',
	'nursingpurpose/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Nursingpurpose/destroy',
	'nursingpurpose/(:any)/(:any)/(:any)/(:any)/store' => 'Nursingpurpose/store',
	'nursingpurpose/(:any)/(:any)/(:any)/(:any)/update' => 'Nursingpurpose/update',

	//Master Modules
	'modules/(:any)' => 'Modules/index',
	'modules/(:any)/(:any)' => 'Modules/index',
	'modules/(:any)/(:any)/(:any)' => 'Modules/index',
	'modules/(:any)/(:any)/(:any)/(:any)' => 'Modules/index',
	'modules/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Modules/destroy',
	'modules/(:any)/(:any)/(:any)/(:any)/store' => 'Modules/store',
	'modules/(:any)/(:any)/(:any)/(:any)/update' => 'Modules/update',
	'modules/(:any)/(:any)/(:any)/(:any)/position' => 'Modules/updateposition',

	//Master Levelaccess
	'levelaccess/(:any)' => 'Levelaccess/index',
	'levelaccess/(:any)/(:any)' => 'Levelaccess/index',
	'levelaccess/(:any)/(:any)/(:any)' => 'Levelaccess/index',
	'levelaccess/(:any)/(:any)/(:any)/(:any)' => 'Levelaccess/index',
	'levelaccess/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Levelaccess/destroy',
	'levelaccess/(:any)/(:any)/(:any)/(:any)/store' => 'Levelaccess/store',
	'levelaccess/(:any)/(:any)/(:any)/(:any)/update' => 'Levelaccess/update',
	'levelaccess/(:any)/(:any)/(:any)/(:any)/edit' => 'Levelaccess/edit',

	//Master Villages
	'villages/(:any)' => 'Villages/index',
	'villages/(:any)/(:any)' => 'Villages/index',
	'villages/(:any)/(:any)/(:any)' => 'Villages/index',
	'villages/(:any)/(:any)/(:any)/(:any)' => 'Villages/index',
	'villages/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Villages/destroy',
	'villages/(:any)/(:any)/(:any)/(:any)/store' => 'Villages/store',
	'villages/(:any)/(:any)/(:any)/(:any)/update' => 'Villages/update',

	//Master Employee
	'useremployee/(:any)' => 'Useremployee/index',
	'useremployee/(:any)/(:any)' => 'Useremployee/index',
	'useremployee/(:any)/(:any)/(:any)' => 'Useremployee/index',
	'useremployee/(:any)/(:any)/(:any)/(:any)' => 'Useremployee/index',
	'useremployee/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Useremployee/destroy',
	'useremployee/(:any)/(:any)/(:any)/(:any)/store' => 'Useremployee/store',
	'useremployee/(:any)/(:any)/(:any)/(:any)/update' => 'Useremployee/update',

	//Master Register Patient
	'registerpatient/(:any)' => 'Registerpatient/index',
	'registerpatient/(:any)/(:any)' => 'Registerpatient/index',
	'registerpatient/(:any)/(:any)/(:any)' => 'Registerpatient/index',
	'registerpatient/(:any)/(:any)/(:any)/(:any)' => 'Registerpatient/index',
	'registerpatient/(:any)/(:any)/(:any)/(:any)/destroy/(:any)' => 'Registerpatient/destroy',
	'registerpatient/(:any)/(:any)/(:any)/(:any)/store' => 'Registerpatient/store',
	'registerpatient/(:any)/(:any)/(:any)/(:any)/update' => 'Registerpatient/update',
	
	'404_override' => '',
	'translate_uri_dashes' => FALSE
	];
	return $route;
}