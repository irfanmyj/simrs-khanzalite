<?php defined('BASEPATH') OR exit('No direct script access allowed');

	function menu()
	{
		global $Cf;
		$CI =& get_instance();
		
		$CI->load->model(['Modules_model','Levelaccess_model']);
		$where = array('status'=>'Y','position_menu'=>'L');
		$get = $CI->Modules_model->get('','*',$where,'','','position ASC','','')->result_array();
		$result = buildTree($get,0);

		$akses = $CI->session->userdata('access');

		$active = '';
		$uri = $CI->uri->segment(1);
		
	
		if($uri == 'dashboard')
		{
			$active = 'active';
		}

		$access_level = array(
			'view' => 'view',
			'add' => 'add',
			'edit' => 'edit',
			'search' => 'search',
			'update' => 'update',
			'delete' => 'delete',
			'upload' => 'upload',
			'download' => 'download',
			'print' => 'print'
		);
		
		$html  = '';
		$html .= '<ul class="sidebar-menu" data-widget="tree">';
		$html .= '<li class="header">MAIN NAVIGATION</li>';
		foreach($result as $k => $v)
		{
			if($v['view']=='Y')
			{
				
				if(empty($v['children']))
				{
					if(@$akses[$v['module_id']]['view']==$v['view'])
					{
						$html .= class_active($uri,$v['link']);						
						 	$html .= '<a href="'.base_url($v['link'].'/'.$v['module_id'].'/search=/'.$Cf->default_limit.'/1').'">';
					            $html .= '<i class="'.$v['icon'].'"></i> <span>'.$v['name_module'].'</span>';
					            $html .= $v['atribut'];
				            $html .= '</a>';
				        $html .= '</li>';
					}
					
				}
				else
				{
					if(@$akses[$v['module_id']]['view']==$v['view'])
					{
						$html .= class_active_induk($uri,$v['link']);
							$html .= '<a href="#">';
					            $html .= '<i class="'.$v['icon'].'"></i> <span>'.$v['name_module'].'</span>';
					            $html .= '<span class="pull-right-container">';
					            	$html .= '<i class="fa fa-angle-left pull-right"></i>';
					            $html .= '</span>';
					        $html .= '</a>';
					        $html .= '<ul class="treeview-menu">';
							foreach ($v['children'] as $k1 => $v1) {
								if(empty($v1['children']))
								{
									if(@$akses[$v1['module_id']]['view']==$v1['view'])
									{
										$html .= class_active($uri,$v1['link']);
										 	$html .= '<a href="'.base_url($v1['link'].'/'.$v1['module_id'].'/search=/'.$Cf->default_limit.'/1').'">';
									            $html .= '<i class="'.$v1['icon'].'"></i> <span>'.$v1['name_module'].'</span>';
									            $html .= $v1['atribut'];
								            $html .= '</a>';
								        $html .= '</li>';
								    }
								}
								else
								{
									if(@$akses[$v1['module_id']]['view']==$v1['view'])
									{

										$html .= class_active($uri,$v1['link']);
										 	$html .= '<a href="#">';
									            $html .= '<i class="'.$v1['icon'].'"></i> <span>'.$v1['name_module'].'</span>';
									            $html .= $v1['atribut'];
								            $html .= '</a>';
								            $html .= '<ul class="treeview-menu">';
								            foreach ($v1['children'] as $k2 => $v2)
								            {
								            	if(empty(@$v2['children']))
								            	{
								            		if(@$akses[$v2['module_id']]['view']==$v2	['view'])
													{
									            		$html .= class_active($uri,$v2['link']);
														 	$html .= '<a href="'.base_url($v2['link'].'/'.$v2['module_id'].'/search=/'.$Cf->default_limit.'/1').'">';
													            $html .= '<i class="'.$v2['icon'].'"></i> <span>'.$v2['name_module'].'</span>';
													            $html .= $v2['atribut'];
												            $html .= '</a>';
												        $html .= '</li>';
											    	}
								            	}
								            }
								            $html .= '</ul>';
								        $html .= '</li>';
								    }
								}
							}
							$html .= '</ul>';
						$html .= '</li>';
					}
				}
			}
						
		}
		$html .= '</ul>';
		return $html;
	}

	function buildTree($elements=array(), $parentId = 0) {
	    $branch = array();

	    foreach ($elements as $element) {
	    	//print_r($element);
	        if ($element['parent_id'] == $parentId) {
	            $children = buildTree($elements, $element['module_id']);
	            if ($children) {
	                $element['children'] = $children;
	            }
	            $branch[] = $element;
	        }
	    }

	    return $branch;
	}

	function acl($id='')
	{
		$CI =& get_instance();
		//$CI->config->load('menu');
		if($id)
		{
			$acl = isset($CI->session->userdata('access')[$id]) ? $CI->session->userdata('access')[$id] : array('view' => 'N');
		}
		else
		{
			$acl = array('view' => 'N');
		}
		return $acl;
	}

	function class_active($uri,$link)
	{
		if($uri == $link)
		{
			$html = '<li class="active">';
		}else
		{
			$html = '<li class="">';
		}

		return $html;
	}

	function class_active_tree($uri,$link)
	{
		if($uri == $link)
		{
			$html = '<li class="treeview active">';
		}else
		{
			$html = '<li class="">';
		}

		return $html;
	}

	function class_active_induk($uri,$link)
	{
		$html = '<li class="treeview">';

		switch ($uri) {
			case 'ward': // Group ruangan
			case 'classroom':
			case 'roominpatient':
			case 'costroom':
				if($link == '#groupruangan')
				{
					$html = '<li class="treeview active">';
				}
				break;
			case 'registerpatient': // Group Pasien
			case 'bloodtype':
			case 'categoryallergy':
			case 'allergy':
			case 'masterpay':
			case 'masterlanguage':
			case 'categoryprofesi':
			case 'masterprofesi':
				if($link == '#grouppasien')
				{
					$html = '<li class="treeview active">';
				}
				break;
			case 'levelaccess': // Group SuperAdmin
			case 'modules':
			case 'master_api':
				if($link == '#groupsuperadmin')
				{
					$html = '<li class="treeview active">';
				}
				break;
			case 'villages': // Group Wilayah
			case 'provinces':
			case 'regencies':
			case 'districts':
				if($link == '#groupwilayah')
				{
					$html = '<li class="treeview active">';
				}
				break;
			case 'nursingdiagnosis': // Group Master Keperawatan
			case 'nursingdiagnosisclass':
			case 'nursingdiagnosisdomain':
			case 'nursingintervention':
			case 'nursingcriteria':
			case 'nursingpurpose':
			case 'assessment_mapping':
				if($link == '#groupmasternursing')
				{
					$html = '<li class="treeview active">';
				}
				break;
			case 'useremployee': // Group employee
			case 'userprofile': 
			case 'position': 
				if($link == '#groupemployee')
				{
					$html = '<li class="treeview active">';
				}
				break;
			case 'nursingigd': // Group Asuhan Keperawatan
			case 'nursingpoli':
			case 'nursingranap':
			case 'nursingicu':
			case 'nursingperina':
				if($link == '#askep')
				{
					$html = '<li class="treeview active">';
				}
				break;
			case 'building': // Group Asuhan Keperawatan
			case 'floor':
			case 'roominstallation':
			case 'safetyofficer':
			case 'safetyofficerschedule':
				if($link == '#timk3')
				{
					$html = '<li class="treeview active">';
				}
				break;
			case 'casemix': // Group Asuhan Keperawatan
			case 'casemixupload':
				if($link == '#casemix')
				{
					$html = '<li class="treeview active">';
				}
				break;
		}

		return $html;
	}