<?php defined('BASEPATH') OR exit('No direct script access allowed');

	if(! function_exists('count_last_number'))
	{
		/*
		* Fungsi ini digunakan untuk mendapatkan no rekam medis terakhir dari database.
		* 
		*/
		function count_last_number($data='')
		{
			$strlen = !empty($data) ? strlen($data) : 3;
			$convert= substr($data, 0, $strlen);

			switch (true) {
				case $strlen >= 10:
					$val = $strlen;
					break;
				
				default:
					$val = '0'.$strlen;
					break;
			}
			$res = sprintf('%'.$val.'s', (($convert) ? $convert + 1 : 1));
			return $res;
		}
	}

	if(! function_exists('count_last_norm'))
	{
		/*
		* Fungsi ini digunakan untuk mendapatkan no rekam medis terakhir dari database.
		* 
		*/
		function count_last_norm($data='')
		{
			$strlen = !empty($data) ? strlen($data) : 6;
			$convert= substr($data, 0, $strlen);

			switch (true) {
				case $strlen >= 10:
					$val = $strlen;
					break;
				
				default:
					$val = '0'.$strlen;
					break;
			}
			$res = sprintf('%'.$val.'s', (($convert) ? $convert + 1 : 1));
			return $res;
		}
	}

	if(! function_exists('count_last_novisit'))
	{
		/*
		* Fungsi ini digunakan untuk mendapatkan no rekam medis terakhir dari database.
		* 
		*/
		function count_last_novisit($data='')
		{
			$strlen = !empty($data) ? strlen($data)-11 : 6;
			$convert= substr($data, 11, $strlen);

			switch (true) {
				case $strlen >= 10:
					$val = $strlen;
					break;
				
				default:
					$val = '0'.$strlen;
					break;
			}
			$res = sprintf('%'.$val.'s', (($convert) ? $convert + 1 : 1));
			return $res;
		}
	}

	if(!function_exists('getAge'))
	{
		function getAge($d1, $d2, $shows=array())
	    {
	        $d1 = (is_string($d1) ? strtotime($d1) : $d1); // tanggal saat ini
	        $d2 = (is_string($d2) ? strtotime($d2) : $d2); // tanggal lahir
	        $diff_secs = abs($d1 - $d2);
	        $base_year = min(date("Y", $d1), date("Y", $d2));
	        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
	        
	        $show['year']      = date("Y", $diff) - $base_year;
	        $show['month']     = date("n", $diff) - 1;
	        $show['day']       = date("j", $diff) - 1;
	        $show['hour']      = date("G", $diff);
	        $show['minute']    = (int) date("i", $diff);

	        $res = [];
	        if(is_array($shows))
	        {
	            foreach ($shows as $k) {
	                $res[$k] = $show[$k];
	            }
	        }
	        return $res;
	    }
	}

    if(!function_exists('_log')){
    	function _log($user_id='',$controller='',$method='',$description=''){
	        global $Cf;
	        $CI =& get_instance();
	        $CI->load->model(['Log_model']);

	        $data['user_id']        = isset($user_id) ? $user_id : '';
	        $data['controller']     = isset($controller) ? $controller : '';
	        $data['method']         = isset($method) ? $method : '';
	        $data['description']    = isset($description) ? $description : '';

	        $CI->Log_model->insert('',$data,FALSE);
	    }
    }

    if(!function_exists('_patient_bracelet')){
    	function _patient_bracelet($data=[]){
	        $CI =& get_instance();
	        $CI->load->model(['Patientbracelet_model']);

	        $where = [
    			'no_visit' => $data['no_visit'],
    			'category' => 2
    		];

    		if(!empty(@$data['alergi_obat']) || !empty(@$data['alergi_makanan']) || !empty(@$data['alergi_lain_lain']) || !empty(@$data['gelang_merah'])){
    			$insert['red'] = 'Y';
    		}

    		if(!empty(@$data['jumlah_skala_jatuh_anak']) || !empty(@$data['jumlah_skala_jatuh_dewasa'])){
    			$insert['yellow'] = 'Y';
    		}

    		if(!empty(@$data['kondisi_khusus_pasien_sida_hiv'])){
    			$insert['orange'] = 'Y';
    		}

    		if(!empty(@$data['kondisi_khusus_pasien_dnr'])){
    			$insert['purple'] = 'Y';
    		}

    		$insert['no_visit']		= $data['no_visit'];
	        $insert['category']     = 2;


	        $res = $CI->Patientbracelet_model->get('','*',$where)->result();
	        if($res){
	        	$CI->Patientbracelet_model->update('',$insert,$where);
	        }else{
	        	$CI->Patientbracelet_model->insert('',$insert,FALSE);
	        }
	    }
    }

    if(!function_exists('daily_assessment_report')){
    	function daily_assessment_report($data=[])
    	{
    		$CI =& get_instance();
    		$CI->load->model('Dailyassessmentreport_model');
    		
    		$where = [
    			'no_visit' => $data['no_visit'],
    			'user_id' => $CI->session->userdata('id_user'),
    			'category' => 2
    		];

    		$res_report =  $CI->Dailyassessmentreport_model->getData($where,1);

    		if(strtotime($data['tanggal_periksa']) >= time()-(60*60*8)){
	            $nilai = 1;
	            $kajian_status = 1;
	        }else{
	            $nilai = 1;
	            $kajian_status = 0;
	        }

	        if(!empty($data['status_nyeri_poli_lokasi']) || 
				!empty($data['status_nyeri_poli_kapan_mulai']) || 
				!empty($data['status_nyeri_poli_penanganan']) || 
				!empty($data['status_nyeri_poli_tingkatan_nyeri']) || 
				!empty($data['nyeri_neonatus_a_total']) || 
				!empty($data['nyeri_neonatus_b_total'])){
				$report['nyeri'] = $nilai;
			}

			if(!empty($data['jumlah_skala_jatuh_anak']) || 
				!empty($data['jumlah_skala_jatuh_dewasa'])){
				$report['jatuh'] = $nilai;
			}

			
			if(!empty($data['budaya'])){
				$report['nilai_budaya'] = $nilai;
			}

			if(!empty($data['masalah_sosial'])){
				$report['sosial'] = $nilai;
			}

			if(!empty($data['status_psikologi'])){
				$report['psikologi'] = $nilai;
			}

			if(!empty($data['jumlah_dekubitus'])){
				$report['dekubitus'] = $nilai;
			}

			if(!empty($data['spiritual_beribadah_dibantu']) || 
				!empty($data['spiritual_konsul_rohaniawan']) ||
				!empty($data['spiritual_fase_dying']) ||
				!empty($data['spiritual_lain_lain'])){
				$report['spiritual'] = $nilai;
			}

			if(!empty($data['alergi_obat']) || 
				!empty($data['alergi_makanan']) ||
				!empty($data['alergi_lain_lain']) ||
				!empty($data['gelang_merah'])){
				$report['riwayat_alergi'] = $nilai;
			}

			if(!empty($data['total_skor']) || 
				!empty($data['skrining_mna_total']) ||
				!empty($data['strong_kids_total'])){
				$report['skrining_gizi'] = $nilai;
			}

			$report['no_visit'] = $data['no_visit'];
			$report['user_id'] = $CI->session->userdata('id_user');
			$report['category'] = 2;
			$report['kajian_status'] = $kajian_status;

    		if(empty($res_report))
    		{
    			$res_insert = $CI->Dailyassessmentreport_model->insert('',$report,FALSE);
    			if($res_insert){
    				$res['info'] = 'Success insert';
    				_log($CI->session->userdata('id_user'),'nursingpoli','save_assessment','insert daily assessment report Success ID '. $res_insert);
    			}else{
    				$res['info'] = 'Wrong insert';
    				_log($CI->session->userdata('id_user'),'nursingpoli','save_assessment','insert daily assessment report Wrong');
    			}
    			
    		}else{
    			if($CI->Dailyassessmentreport_model->update('',$report,['no_visit'=>$data['no_visit']])){
    				$res['info'] = 'Success update';
    				_log($CI->session->userdata('id_user'),'nursingpoli','save_assessment','update daily assessment report Success No Visit '. $data['no_visit']);
    			}else{
    				$res['info'] = 'Wrong update';
    				_log($CI->session->userdata('id_user'),'nursingpoli','save_assessment','update daily assessment report Wrong');
    			}
    		}

    		return $res;
    	}
    }

    if(!function_exists('active_bracelet')){
    	function active_bracelet($gelang=[]){
    		if(@$gelang[0]->red == 'Y' || @$gelang[0]->yellow == 'Y' || @$gelang[0]->orange == 'Y' || @$gelang[0]->purple == 'Y'){
    			return 'style="display: block;"';
    		}else{
    			return 'style="display: none;"';
    		}
    	}
    } 

    if(!function_exists('print_data')){
    	function print_data($data=''){
    		global $Cf;
    		if($data){
    			echo '<b>'.$data.'</b>';
    		}else{
    			echo $Cf->data_empty;
    		}
    	}
    }

    function encrypt_aes($string=''){
        /*$key    = hash('sha256', '2018201920202021');
        // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
        $iv     = substr(hash('sha256', 'APPSSECURITYSISY'), 0, 16);
        //do the encryption given text/string/number
        $result = openssl_encrypt($string, 'aes-256-cbc', $key, 0, $iv);
        $output = base64_encode($result);
        return $output;*/
        global $Cf;
    	return $Cf->encrypt_aes_256_cbc($string);
    }

    function decrypt_aes($enc=''){
    	global $Cf;
    	return $Cf->decrypt_aes_256_cbc($enc);
    }

    function bCrypt($pass='',$cost='')
	{
	    $chars='./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

	    // Build the beginning of the salt
	    $salt=sprintf('$2a$%02d$',$cost);

	    // Seed the random generator
	    mt_srand();

	    // Generate a random salt
	    for($i=0;$i<22;$i++) $salt.=$chars[mt_rand(0,63)];

	    // return the hash
	    return crypt($pass,$salt);
	}

    if(!function_exists('getNoReg'))
	{
		function getNoReg($noreg1='',$noreg2='')
		{
			/*
			$noreg1 = booking_registrasi
			$noreg2 = reg_periksa
			*/
			if(empty($noreg2) && empty($noreg1))
			{
				$NoReg = 000;
			}
			else if($noreg2 >= $noreg1)
			{
				$NoReg = $noreg2;
			}
			else if($noreg2 <= $noreg1)
			{
				$NoReg = $noreg1;
			}
			else if($noreg2 == $noreg1){
				$NoReg = $noreg2;
			}
			else
			{
				$NoReg = $noreg1;
			}

			return $NoReg;
		}
	}


	if(!function_exists('getKdPoli')){
		function getKdPoli($data,$time)
	    {
	    	$seskdpoli = $data;
	    	if(count($data) == 1)
	    	{
	    		if($time > 06 && $time <= 13)
				{
					$kdpoli = $seskdpoli[0]->kd_poli;
				}
				else if($time >= 13 && $time < 24)
				{
					$kdpoli = @$seskdpoli[0]->kd_poli;
				}
				else
				{
					$kdpoli = @$seskdpoli[1]->kd_poli;
				}
			}
			else if(count($data) > 1)
			{
				$kdpoli = $seskdpoli[0]->kd_poli;
			}
	    	else
	    	{
	    		$kdpoli = '';
	    	}
	    	//print_r($kdpoli);
			return $kdpoli;
	    }
	}
