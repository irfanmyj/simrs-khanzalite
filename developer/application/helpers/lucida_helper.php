<?php defined('BASEPATH') OR exit('No direct script access allowed');

	function menu()
	{
		global $Cf;
		$CI =& get_instance();
		$CI->load->model(['Modules_model','Levelaccess_model']);
		$where = array('status'=>'Y','position_menu'=>'L','categori_menu'=>'Default');
		$get = $CI->Modules_model->get('','*',$where,'','','position ASC','','')->result_array();
		$result = buildTree($get,0);
		$res_access = $CI->Levelaccess_model->get('','access',['id'=>$CI->session->userdata('level_access')])->result();
		$akses = unserialize($res_access[0]->access);

		$active = '';
		$uri = $CI->uri->segment(1);
		
	
		if($uri == 'dashboard')
		{
			$active = 'active';
		}

		$access_level = array(
			'view' => 'view',
			'add' => 'add',
			'edit' => 'edit',
			'search' => 'search',
			'update' => 'update',
			'delete' => 'delete',
			'upload' => 'upload',
			'download' => 'download',
			'print' => 'print'
		);
		
		/*echo '<pre>';
		print_r($akses);
		exit();*/
		$html  = '';
		//$html .= '<ul class="main-menu metismenu">';
		foreach($result as $k => $v)
		{
			if($v['view']=='Y')
			{
				
				if(empty($v['children']))
				{
					if(@$akses[$v['module_id']]['view']==$v['view'])
					{
						$html .= class_active($uri,$v['link']);						
						 	$html .= '<a href="'.base_url($v['link'].'/'.encrypt_aes($v['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
					            $html .= '<i class="'.$v['icon'].'"></i> <span>'.$v['name_module'].'</span>';
					            //$html .= $v['atribut'];
				            $html .= '</a>';
				        $html .= '</li>';
					}
					
				}
				else
				{
					if(@$akses[$v['module_id']]['view']==$v['view'])
					{
						$html .= class_active_induk($uri,$v['link']);
							$html .= '<a href="#">';
					            $html .= '<i class="'.$v['icon'].'"></i> <span>'.$v['name_module'].'</span>';
					            $html .= '<span class="pull-right-container">';
					            	$html .= '<i class="fa fa-angle-left pull-right"></i>';
					            $html .= '</span>';
					        $html .= '</a>';
					        $html .= '<ul>';
							foreach ($v['children'] as $k1 => $v1) {
								if(empty($v1['children']))
								{
									if(@$akses[$v1['module_id']]['view']==$v1['view'])
									{
										$html .= class_active($uri,$v1['link']);
										 	$html .= '<a href="'.base_url($v1['link'].'/'.encrypt_aes($v1['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
									            $html .= $v1['name_module'];
								            $html .= '</a>';
								        $html .= '</li>';
								    }
								}
								else
								{
									if(@$akses[$v1['module_id']]['view']==$v1['view'])
									{

										$html .= class_active($uri,$v1['link']);
										 	$html .= '<a href="#">';
									            $html .= '<i class="'.$v1['icon'].'"></i> <span>'.$v1['name_module'].'</span>';
								            $html .= '</a>';
								            $html .= '<ul>';
								            foreach ($v1['children'] as $k2 => $v2)
								            {
								            	if(empty(@$v2['children']))
								            	{
								            		if(@$akses[$v2['module_id']]['view']==$v2	['view'])
													{
									            		$html .= class_active($uri,$v2['link']);
														 	$html .= '<a href="'.base_url($v2['link'].'/'.encrypt_aes($v2['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
													            $html .= $v2['name_module'];
													            //$html .= $v2['atribut'];
												            $html .= '</a>';
												        $html .= '</li>';
											    	}
								            	}
								            }
								            $html .= '</ul>';
								        $html .= '</li>';
								    }
								}
							}
							$html .= '</ul>';
						$html .= '</li>';
					}
				}
			}
						
		}
		//$html .= '</ul>';
		return $html;
	}

	function menu_second()
	{
		global $Cf;
		$CI =& get_instance();
		$CI->load->model(['Modules_model','Levelaccess_model']);
		$where = array('status'=>'Y','position_menu'=>'L','categori_menu'=>'Second');
		$get = $CI->Modules_model->get('','*',$where,'','','position ASC','','')->result_array();
		$result = buildTree($get,0);
		$res_access = $CI->Levelaccess_model->get('','access',['id'=>$CI->session->userdata('level_access')])->result();
		$akses = unserialize($res_access[0]->access);
		
		$active = '';
		$uri = $CI->uri->segment(1);
		
	
		if($uri == 'dashboard')
		{
			$active = 'active';
		}

		$access_level = array(
			'view' => 'view',
			'add' => 'add',
			'edit' => 'edit',
			'update' => 'update',
			'delete' => 'delete',
			'upload' => 'upload',
			'download' => 'download'
		);
		
		/*echo '<pre>';
		print_r($akses);
		exit();*/
		$html  = '';
		//$html .= '<ul class="main-menu metismenu">';
		foreach($result as $k => $v)
		{
			if($v['view']=='Y')
			{
				
				if(empty($v['children']))
				{
					if(@$akses[$v['module_id']]['view']==$v['view'])
					{
						$html .= class_active($uri,$v['link']);						
						 	$html .= '<a href="'.base_url($v['link'].'/'.encrypt_aes($v['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
					            $html .= '<i class="'.$v['icon'].'"></i> <span>'.$v['name_module'].'</span>';
					            //$html .= $v['atribut'];
				            $html .= '</a>';
				        $html .= '</li>';
					}
					
				}
				else
				{
					if(@$akses[$v['module_id']]['view']==$v['view'])
					{
						$html .= class_active_induk($uri,$v['link']);
							$html .= '<a href="#">';
					            $html .= '<i class="'.$v['icon'].'"></i> <span>'.$v['name_module'].'</span>';
					            $html .= '<span class="pull-right-container">';
					            	$html .= '<i class="fa fa-angle-left pull-right"></i>';
					            $html .= '</span>';
					        $html .= '</a>';
					        $html .= '<ul>';
							foreach ($v['children'] as $k1 => $v1) {
								if(empty($v1['children']))
								{
									if(@$akses[$v1['module_id']]['view']==$v1['view'])
									{
										$html .= class_active($uri,$v1['link']);
										 	$html .= '<a href="'.base_url($v1['link'].'/'.encrypt_aes($v1['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
									            $html .= $v1['name_module'];
								            $html .= '</a>';
								        $html .= '</li>';
								    }
								}
								else
								{
									if(@$akses[$v1['module_id']]['view']==$v1['view'])
									{

										$html .= class_active($uri,$v1['link']);
										 	$html .= '<a href="#">';
									            $html .= '<i class="'.$v1['icon'].'"></i> <span>'.$v1['name_module'].'</span>';
								            $html .= '</a>';
								            $html .= '<ul>';
								            foreach ($v1['children'] as $k2 => $v2)
								            {
								            	if(empty(@$v2['children']))
								            	{
								            		if(@$akses[$v2['module_id']]['view']==$v2	['view'])
													{
									            		$html .= class_active($uri,$v2['link']);
														 	$html .= '<a href="'.base_url($v2['link'].'/'.encrypt_aes($v2['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
													            $html .= $v2['name_module'];
													            //$html .= $v2['atribut'];
												            $html .= '</a>';
												        $html .= '</li>';
											    	}
								            	}
								            }
								            $html .= '</ul>';
								        $html .= '</li>';
								    }
								}
							}
							$html .= '</ul>';
						$html .= '</li>';
					}
				}
			}
						
		}
		//$html .= '</ul>';
		return $html;
	}

	function menu_third()
	{
		global $Cf;
		$CI =& get_instance();
		$CI->load->model(['Modules_model','Levelaccess_model']);
		$where = array('status'=>'Y','position_menu'=>'L','categori_menu'=>'Third');
		$get = $CI->Modules_model->get('','*',$where,'','','position ASC','','')->result_array();
		$result = buildTree($get,0);
		$res_access = $CI->Levelaccess_model->get('','access',['id'=>$CI->session->userdata('level_access')])->result();
		$akses = unserialize($res_access[0]->access);
		
		$active = '';
		$uri = $CI->uri->segment(1);
		
	
		if($uri == 'dashboard')
		{
			$active = 'active';
		}

		$access_level = array(
			'view' => 'view',
			'add' => 'add',
			'edit' => 'edit',
			'update' => 'update',
			'delete' => 'delete',
			'upload' => 'upload',
			'download' => 'download'
		);
		
		/*echo '<pre>';
		print_r($akses);
		exit();*/
		$html  = '';
		//$html .= '<ul class="main-menu metismenu">';
		foreach($result as $k => $v)
		{
			if($v['view']=='Y')
			{
				
				if(empty($v['children']))
				{
					if(@$akses[$v['module_id']]['view']==$v['view'])
					{
						$html .= class_active($uri,$v['link']);						
						 	$html .= '<a href="'.base_url($v['link'].'/'.encrypt_aes($v['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
					            $html .= '<i class="'.$v['icon'].'"></i> <span>'.$v['name_module'].'</span>';
					            //$html .= $v['atribut'];
				            $html .= '</a>';
				        $html .= '</li>';
					}
					
				}
				else
				{
					if(@$akses[$v['module_id']]['view']==$v['view'])
					{
						$html .= class_active_induk($uri,$v['link']);
							$html .= '<a href="#">';
					            $html .= '<i class="'.$v['icon'].'"></i> <span>'.$v['name_module'].'</span>';
					            $html .= '<span class="pull-right-container">';
					            	$html .= '<i class="fa fa-angle-left pull-right"></i>';
					            $html .= '</span>';
					        $html .= '</a>';
					        $html .= '<ul>';
							foreach ($v['children'] as $k1 => $v1) {
								if(empty($v1['children']))
								{
									if(@$akses[$v1['module_id']]['view']==$v1['view'])
									{
										$html .= class_active($uri,$v1['link']);
										 	$html .= '<a href="'.base_url($v1['link'].'/'.encrypt_aes($v1['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
									            $html .= $v1['name_module'];
								            $html .= '</a>';
								        $html .= '</li>';
								    }
								}
								else
								{
									if(@$akses[$v1['module_id']]['view']==$v1['view'])
									{

										$html .= class_active($uri,$v1['link']);
										 	$html .= '<a href="#">';
									            $html .= '<i class="'.$v1['icon'].'"></i> <span>'.$v1['name_module'].'</span>';
								            $html .= '</a>';
								            $html .= '<ul>';
								            foreach ($v1['children'] as $k2 => $v2)
								            {
								            	if(empty(@$v2['children']))
								            	{
								            		if(@$akses[$v2['module_id']]['view']==$v2	['view'])
													{
									            		$html .= class_active($uri,$v2['link']);
														 	$html .= '<a href="'.base_url($v2['link'].'/'.encrypt_aes($v2['module_id']).'/search=/'.$Cf->default_limit.'/1').'">';
													            $html .= $v2['name_module'];
													            //$html .= $v2['atribut'];
												            $html .= '</a>';
												        $html .= '</li>';
											    	}
								            	}
								            }
								            $html .= '</ul>';
								        $html .= '</li>';
								    }
								}
							}
							$html .= '</ul>';
						$html .= '</li>';
					}
				}
			}
						
		}
		//$html .= '</ul>';
		return $html;
	}

	function buildTree($elements=array(), $parentId = 0) {
	    $branch = array();

	    foreach ($elements as $element) {
	    	//print_r($element);
	        if ($element['parent_id'] == $parentId) {
	            $children = buildTree($elements, $element['module_id']);
	            if ($children) {
	                $element['children'] = $children;
	            }
	            $branch[] = $element;
	        }
	    }

	    return $branch;
	}

	function acl($id='')
	{
		$CI =& get_instance();
		//$CI->config->load('menu');
		if($id)
		{
			$acl = isset($CI->session->userdata('access')[$id]) ? $CI->session->userdata('access')[$id] : array('view' => 'N');
		}
		else
		{
			$acl = array('view' => 'N');
		}
		return $acl;
	}

	function class_active($uri,$link)
	{
		if($uri == $link)
		{
			$html = '<li class="active">';
		}else
		{
			$html = '<li class="">';
		}

		return $html;
	}

	function class_active_tree($uri,$link)
	{
		if($uri == $link)
		{
			$html = '<li class="active">';
		}else
		{
			$html = '<li class="">';
		}

		return $html;
	}

	function class_active_induk($uri,$link)
	{
		$html = '<li>';

		switch ($uri) {
			case 'levelaccess': // Group SuperAdmin
			case 'modules':
			case 'masterapi':
			case 'masterroutes':
			case 'logactvityapplication':
			case 'settingsapi':
				if($link == '#groupsuperadmin')
				{
					$html = '<li class="active">';
				}
				break;
			case 'kelurahan': // Group Wilayah
			case 'kecamatan':
			case 'kabupaten':
			case 'provinsi':
			case 'negara':
			case 'jeniswilayah':
				if($link == '#groupwilayah')
				{
					$html = '<li class="active">';
				}
				break;
			case 'useremployee': // Group employee
			case 'userprofile': 
			case 'position': 
			case 'golonganpns': 
				if($link == '#groupemployee')
				{
					$html = '<li class="active">';
				}
				break;
			case 'holidayscalendar': 
			case 'leavecalendar': 
			case 'gppendaftaran':
			case 'masterpasien':
			case 'patientbooking': 
			case 'cancelbooking': 
			case 'jadwaldokter': 
			case 'limitsettings': 
			case 'bridgingsep': 
			case 'masterkamarinap': 
				if($link == '#grouppendaftaran')
				{
					$html = '<li class="active">';
				}
				break;
			case 'masterpenyakit':
			case 'akunbayar':
			case 'rekening':  
			case 'masterbank': 
			case 'closingkasir': 
			case 'departemen': 
			case 'diet': 
			case 'icdnine': 
			case 'kategoriperawatan': 
			case 'jnsperawatan': 
			case 'kodesatuan': 
			case 'jenis': 
			case 'industrifarmasi': 
			case 'kategoribarang': 
			case 'golonganbarang':  
			case 'bahasapasien':  
			case 'penjab':  
				if($link == '#groupmastermedis')
				{
					$html = '<li class="active">';
				}
				break;
			case 'masterkaryawan': 
			case 'mastergolongan': 
				if($link == '#groupkepegawaian')
				{
					$html = '<li class="active">';
				}
				break;
			case 'identity': 
			case 'dailyunitcost': 
			case 'identityfunctionary': 
				if($link == '#groupdiklat')
				{
					$html = '<li class="active">';
				}
				break;
			case 'nursingigd': 
			case 'nursingpoli': 
			case 'nursingrawatinap': 
				if($link == '#asuhankeperawatan')
				{
					$html = '<li class="active">';
				}
				break;
			case 'statistikbooking': 
				if($link == '#reportpendaftaran')
				{
					$html = '<li class="active">';
				}
				break;
			case 'ermigd': 
			case 'ermpoliklinik': 
			case 'ermrawatinap': 
				if($link == '#doktererm')
				{
					$html = '<li class="active">';
				}
				break;
			case 'nursingdiagnosis': 
			case 'nursingdiagnosisclass': 
			case 'nursingdiagnosisdomain': 
			case 'assessmentmapping': 
			case 'nursingintervention': 
			case 'nursingcriteria': 
			case 'nursingpurpose': 
				if($link == '#masteraskep')
				{
					$html = '<li class="active">';
				}
				break;
			case 'bangsal':
			case 'kamar':
			case 'poliklinik': 
				if($link == '#masterruangan')
				{
					$html = '<li class="active">';
				}
				break;
			case 'category_group':
			case 'usersgroup':
				if($link == '#appspesan')
				{
					$html = '<li class="active">';
				}
				break;
		}

		return $html;
	}

	function class_active_tabs($uri){
		switch ($uri) {
			case 'dashboard':
			case 'dokterpoliklinik':
			case 'levelaccess':
			case 'modules':
			case 'masterapi':
			case 'masterroutes':
			case 'logactvityapplication':
			case 'settingsapi':
			case 'useremployee': 
			case 'userprofile': 
			case 'position': 
			case 'golonganpns': 
			case 'holidayscalendar':
			case 'leavecalendar':
			case 'gppendaftaran':
			case 'masterpasien':
			case 'patientbooking':
			case 'cancelbooking':
			case 'jadwaldokter':
			case 'limitsettings':
			case 'bridgingsep':
			case 'masterkamarinap':
			case 'masterkaryawan': 
			case 'identity': 
			case 'dailyunitcost': 
			case 'identityfunctionary': 
			case 'nursingigd': 
			case 'nursingpoli': 
			case 'nursingrawatinap': 
			case 'ermigd': 
			case 'ermpoliklinik': 
			case 'ermrawatinap':  
				$tabs = 'Default';
				break;
			case 'statistikbooking':
				$tabs = 'Second';
			break;
			case 'nursingdiagnosis': 
			case 'nursingdiagnosisclass': 
			case 'nursingdiagnosisdomain': 
			case 'assessmentmapping': 
			case 'nursingintervention': 
			case 'nursingcriteria': 
			case 'nursingpurpose':
			case 'kelurahan': 
			case 'kecamatan':
			case 'kabupaten':
			case 'provinsi':
			case 'negara':
			case 'jeniswilayah': 
			case 'bangsal':
			case 'kamar':
			case 'poliklinik':
			case 'masterpenyakit':
			case 'akunbayar':
			case 'rekening':  
			case 'masterbank': 
			case 'closingkasir': 
			case 'departemen': 
			case 'diet': 
			case 'icdnine': 
			case 'kategoriperawatan': 
			case 'jnsperawatan': 
			case 'kodesatuan': 
			case 'jenis': 
			case 'industrifarmasi': 
			case 'kategoribarang': 
			case 'golonganbarang':  
			case 'bahasapasien': 
			case 'penjab':
			case 'category_group':
			case 'usersgroup':
				$tabs = 'Third';
			break;
		}

		return $tabs;
	}