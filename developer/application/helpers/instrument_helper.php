<?php defined('BASEPATH') OR exit('No direct script access allowed');

	function show_profiling($status='',$config='')
	{
		$CI =& get_instance();
		if($status == 'N') { 
			$CI->output->enable_profiler(FALSE);
		}
		else
		{		
			foreach (json_decode($config) as $key => $value) {
				$datas[$key] = ($value == 'Y') ? TRUE : FALSE; 
			}
			
			$CI->output->set_profiler_sections($datas);
			$CI->output->enable_profiler(TRUE);
		}
	}

	function pagination($url='',$limit='',$per_page='',$count='')
    {
        $jumlah_page = ceil($count/$limit);
        $jumlah_number = 3; 
        $start_number = ($per_page > $jumlah_number)? $per_page - $jumlah_number : 1; 
        $end_number = ($per_page < ($jumlah_page - $jumlah_number))? $per_page + $jumlah_number : $jumlah_page;
       
        $link = '<ul class="pagination pagination-sm no-margin pull-right">';
        if($per_page == 1){
            $link .= '<li class="disabled"><a href="#">First</a></li>';
            $link .= '<li class="disabled"><a href="#">&laquo;</a></li>';
        }else{
            $link_prev = ($per_page > 1) ? $per_page - 1 : 1;
            $link .= '<li><a href="'.$url.$limit.'/'.'1">First</a></li>';
            $link .= '<li><a href="'.$url.$limit.'/'.$link_prev.'">&laquo;</a></li>';
        }

        for($i = $start_number; $i <= $end_number; $i++){
            $link_active = ($per_page == $i)? ' class="active"' : '';
            //$link .= '<a href="'.$url.$limit.'/'.$i.'">'.$i.'</a>';
            $link .= '<li';
            $link .= $link_active.'><a href="'.$url.$limit.'/'.$i.'">'.$i.'</a></li>';

        }

        if($per_page == $jumlah_page){
            $link .= '<li class="disabled"><a href="#">&raquo;</a></li>';
            $link .= '<li class="disabled"><a href="#">Last</a></li>';
        }else{
            $link_next = ($per_page < $jumlah_page)? $per_page + 1 : $jumlah_page;
            $link .= '<li><a href="'.$url.$limit.'/'.$link_next.'">&raquo;</a></li>';
            $link .= '<li><a href="'.$url.$limit.'/'.$jumlah_page.'">Last</a></li>';
        }

        $link .= '</ul>';
        return $link;
    }

    function lucida_pagination($url='',$limit='',$per_page='',$count='',$method='')
    {
        $jumlah_page = ceil($count/$limit);
        $jumlah_number = 3; 
        $start_number = ($per_page > $jumlah_number)? $per_page - $jumlah_number : 1; 
        $end_number = ($per_page < ($jumlah_page - $jumlah_number))? $per_page + $jumlah_number : $jumlah_page;

        $mt = '';
        if($method){
            $mt = $method;
        }
       
        $link = '<ul class="pagination pagination-sm no-margin pull-right">';
        if($per_page == 1){
            $link .= '<li class="disabled page-item"><a href="#" class="page-link">First</a></li>';
            $link .= '<li class="disabled page-item"><a href="#" class="page-link">&laquo;</a></li>';
        }else{
            $link_prev = ($per_page > 1) ? $per_page - 1 : 1;
            $link .= '<li class="page-item"><a class="page-link" href="'.$url.$limit.'/'.'1">First</a></li>';
            $link .= '<li class="page-item"><a class="page-link" href="'.$url.$limit.'/'.$link_prev.'">&laquo;</a></li>';
        }

        for($i = $start_number; $i <= $end_number; $i++){
            $link_active = ($per_page == $i)? ' active' : '';
            //$link .= '<a href="'.$url.$limit.'/'.$i.'">'.$i.'</a>';
            $link .= '<li class="page-item ';
            $link .= $link_active.'"><a class="page-link" href="'.$url.$limit.'/'.$i.'">'.$i.'</a></li>';

        }

        if($per_page == $jumlah_page){
            $link .= '<li class="disabled page-item"><a class="page-link" href="#">&raquo;</a></li>';
            $link .= '<li class="disabled page-item"><a class="page-link" href="#">Last</a></li>';
        }else{
            $link_next = ($per_page < $jumlah_page)? $per_page + 1 : $jumlah_page;
            $link .= '<li class="page-item"><a class="page-link" href="'.$url.$limit.'/'.$link_next.'">&raquo;</a></li>';
            $link .= '<li class="page-item"><a class="page-link" href="'.$url.$limit.'/'.$jumlah_page.'">Last</a></li>';
        }

        $link .= '</ul>';
        return $link;
    }

    function lucida_pagination_search($url='',$limit='',$per_page='',$count='',$method='')
    {
        $jumlah_page = ceil($count/$limit);
        $jumlah_number = 3; 
        $start_number = ($per_page > $jumlah_number)? $per_page - $jumlah_number : 1; 
        $end_number = ($per_page < ($jumlah_page - $jumlah_number))? $per_page + $jumlah_number : $jumlah_page;

        $mt = '';
        if($method){
            $mt = '/'.$method;
        }
       
        $link = '<ul class="pagination pagination-sm no-margin pull-right">';
        if($per_page == 1){
            $link .= '<li class="disabled page-item"><a href="#" class="page-link">First</a></li>';
            $link .= '<li class="disabled page-item"><a href="#" class="page-link">&laquo;</a></li>';
        }else{
            $link_prev = ($per_page > 1) ? $per_page - 1 : 1;
            $link .= '<li class="page-item"><a class="page-link" href="'.$url.$limit.'/1'.$mt.'">First</a></li>';
            $link .= '<li class="page-item"><a class="page-link" href="'.$url.$limit.'/'.$link_prev.$mt.'">&laquo;</a></li>';
        }

        for($i = $start_number; $i <= $end_number; $i++){
            $link_active = ($per_page == $i)? ' active' : '';
            //$link .= '<a href="'.$url.$limit.'/'.$i.'">'.$i.'</a>';
            $link .= '<li class="page-item ';
            $link .= $link_active.'"><a class="page-link" href="'.$url.$limit.'/'.$i.$mt.'">'.$i.'</a></li>';

        }

        if($per_page == $jumlah_page){
            $link .= '<li class="disabled page-item"><a class="page-link" href="#">&raquo;</a></li>';
            $link .= '<li class="disabled page-item"><a class="page-link" href="#">Last</a></li>';
        }else{
            $link_next = ($per_page < $jumlah_page)? $per_page + 1 : $jumlah_page;
            $link .= '<li class="page-item"><a class="page-link" href="'.$url.$limit.'/'.$link_next.$mt.'">&raquo;</a></li>';
            $link .= '<li class="page-item"><a class="page-link" href="'.$url.$limit.'/'.$jumlah_page.$mt.'">Last</a></li>';
        }

        $link .= '</ul>';
        return $link;
    }


	function showValRec($ar) 
	{
	    $ret = array();
	    if (@count($ar) > 0) {
	        foreach ($ar as $k => $v) {
	            $ret[] = 'data-' . $k . '="' . $v . '"';
	        }
	    }

	    return implode(' ', $ret);
	}

	function generateReturn($location, $json = '', $returnJson = false) {
	    if ($returnJson) {
	    	//$this->isMobile() || 
	        //header('content-type:json/text');
	        echo $json ? json_encode($json) : json_encode($_SESSION);
	        exit();
	    } else {
	        header('location:' . $location);
	    }
	}

	function datasToArray($data='',$key1='',$key2='',$key3='',$key4='',$key5='')
	{
	    $ls = array();
	    if($data)
	    {
            if(!empty($key1) && !empty($key2) && !empty($key3) && !empty($key4) && !empty($key5))
            {
                foreach ($data as $key => $value) {
                    $ls[$value->$key1] = trim($value->$key2) .'  ('. trim($value->$key3).' - '. trim($value->$key4) . '-'. trim($value->$key5) .')';
                }
            }
	        else if(!empty($key1) && !empty($key2) && !empty($key3) && !empty($key4))
	        {
	            foreach ($data as $key => $value) {
	                $ls[$value->$key1] = trim($value->$key2) .'  ('. trim($value->$key3).' - '. trim($value->$key4) .')';
	            }
	        }
	        else if(!empty($key1) && !empty($key2) && !empty($key3))
	        {
	            foreach ($data as $key => $value) {
	                $ls[$value->$key1] = trim($value->$key2) .'  ('. trim($value->$key3).')';
	            }
	        }
	        else if(!empty($key1) && !empty($key2))
	        {
	            foreach ($data as $key => $value) {
	                $ls[$value->$key1] = trim($value->$key2);
	            }
	        }else if(!empty($key1))
	        {
	            foreach ($data as $key => $value) {
	                $ls[] = trim($value->$key1);
	            }
	        }
	    }

	    return $ls;
	}

    function conversiobjtoarr($data=''){
        $datas = [];
        if(is_array($data)){
            foreach ($data as $k => $v) {
                $datas[] = ['kd_kamar'=>$v->kd_kamar,'nm_bangsal'=>$v->nm_bangsal];
            }
        } 
        return $datas;
    }

	function createDateRangeArray($strDateFrom, $strDateTo) {
        $aryRange = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo   = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    function getWeekDay($week, $bulan, $tahun) {
        $ret_day = array();
        $wk      = 1;

        for ($dt = 1; $dt <= date('t', strtotime($tahun . '-' . $bulan . '-01')); $dt++) {
            $dte = strtotime($tahun . '-' . $bulan . '-' . $dt);

            if ($week == $wk) {//jika week cocok
                $ret_day[] = date('d-m-Y', $dte);
            }

            if (date("N", $dte) == 7) {//if sunday
                $wk++;
            }
        }

        if (count($ret_day) < 7) {
            $max = count($ret_day);
            for ($ikd = 1; $ikd <= 7 - $max; $ikd++) {
                $ret_day[] = '';
            }
        }

        return $ret_day;

    }

    /**
     * Returns the amount of weeks into the month a date is
     * @param $date a YYYY-MM-DD formatted date
     * @param $rollover The day on which the week rolls over
     */
    function getWeeks($date, $rollover) {
        $cut    = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first     = strtotime($cut . "00");
        $elapsed   = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++) {
            $dayfind      = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if ($day == strtolower($rollover))
                $weeks ++;
        }

        return $weeks;
    }

    function splitRecursiveArray($array = array(), $key = '', $val = '') {
        $newArray = array();
        if ($key != '' || $val != '') {
            $keys = $key ? explode(',', $key) : array();
            $jumk = count($keys);
            $vals = $val ? explode(',', $val) : array();
            $jumv = count($vals);

            if (is_array($array)) {
                foreach ($array as $vk) {
                    $value = array();
                    if ($jumv > 1) {
                        foreach ($vals as $vv) {
                            $value[(string) $vv] = $vk[(string) $vv];
                        }
                    } elseif ($jumv == 1) {
                        $value = $vk[(string) $vals[0]];
                    } else {
                        $value = $vk;
                    }
                    switch ($jumk) {
                        case 1:
                            $newArray[$vk[(string) $keys[0]]] = $value;
                            break;
                        case 2:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]] = $value;
                            break;
                        case 3:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]] = $value;
                            break;
                        case 4:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]] = $value;
                            break;
                        case 5:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]] = $value;
                            break;
                        case 6:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]] = $value;
                            break;
                        case 7:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]][$vk[(string) $keys[6]]] = $value;
                            break;
                        case 8:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]][$vk[(string) $keys[6]]][$vk[(string) $keys[7]]] = $value;
                            break;
                        case 9:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]][$vk[(string) $keys[6]]][$vk[(string) $keys[7]]][$vk[(string) $keys[8]]] = $value;
                            break;
                        case 10:
                            $newArray[$vk[(string) $keys[0]]][$vk[(string) $keys[1]]][$vk[(string) $keys[2]]][$vk[(string) $keys[3]]][$vk[(string) $keys[4]]][$vk[(string) $keys[5]]][$vk[(string) $keys[6]]][$vk[(string) $keys[7]]][$vk[(string) $keys[8]]][$vk[(string) $keys[9]]] = $value;
                            break;
                        default:
                            $newArray[] = $value;
                            break;
                    }
                }
            }
        }             

        return $newArray;
    }

    function select($array, $attribute, $blank = true, $selected = '', $showKey = false, $arrayNonActive = '') {
        //$html  = "\r\n";
        $html = '<select ' . $attribute . '>';
        if (!empty($blank)) {
            if ($blank === true) {
                $html .= '<option value="">-- -- --</option>';
            } else {
                $html .= '<option value="">' . $blank . '</option>';
            }
        }
        if (is_array($array)) {
            if (count($array) > 0) {
                foreach ($array as $k => $v) {
                    $html .= '<option value="' . $k . '"';
                    if (is_array($selected)) {
                        if (in_array($k, $selected)) {
                            $html .= ' selected="selected"';
                        }
                    } else {
                        if ((string) $k == $selected && strlen($k) == strlen($selected)) {
                            $html .= ' selected="selected"';
                        }
                    }
                    if (is_array($arrayNonActive)) {
                        if (in_array($k, $arrayNonActive)) {
                            $html .= ' style="background-color:#eeeeee;color:#ff0000;"';
                        }
                    }
                    $html .= '>';
                    if ($showKey == true) {
                        $html .= $k . ' - ';
                    }
                    $html .= $v . '</option>';
                }
            } else {
                $html .= '<option value="">-- --</option>';
            }
        } else {
            $html .= '<option value="">-- --</option>';
        }
        $html .= '</select>';
        return $html;
    }

    function _entity($datas)
    {
        if(is_array($datas) && count($datas) > 0)
        {
            $retArray = array();
            foreach ($datas as $key => $value) {
                $retArray[$key] = ($value) ? htmlentities($value,ENT_QUOTES) : ''; 
            }
        }

        return $retArray;
    }

    function _post($request='', $entity= true)
    {
        $CI =& get_instance();
        $datas = $CI->input->post();
        $retArray = array();

        if(is_array($request) && count($request) > 0)
        {
            foreach ($request as $key) {
                if (!$entity) {
                    $retArray[$key] = isset($datas[$key]) ? $datas[$key] : '';
                } else {
                    $retArray[$key] = isset($datas[$key]) ? ($entity ? htmlentities($datas[$key], ENT_QUOTES) : $datas[$key]) : '';
                }
            }
        }
        else
        {
            foreach ($datas as $key => $value) {
                $retArray[$key] = isset($value) ? htmlentities($value, ENT_QUOTES) : '';
            }
        }

        return $retArray;
    }

    function AddDate($date='',$numDay,$time='')
    {
        $tNow   = date_create($date);
        date_add($tNow, date_interval_create_from_date_string($numDay .' '. $time));
        $res  = date_format($tNow, 'Y-m-d');
        return $res;
    }

    function countDate($dNow,$dNext)
    {
        $convert_totime     = abs(strtotime($dNow) - strtotime($dNext));
        $res_day            = $convert_totime/86400;
        return $res_day;
    }

    function indoDays($date){
        $getDays=date('D',strtotime($date));
        $day = array(
            'Sun' => 'AKHAD',
            'Mon' => 'SENIN',
            'Tue' => 'SELASA',
            'Wed' => 'RABU',
            'Thu' => 'KAMIS',
            'Fri' => 'JUMAT',
            'Sat' => 'SABTU'
        );
        return $showDay=$day[$getDays];
    }

    if(!function_exists('tanggal_indo'))
{
    function tanggal_indo($tanggal, $cetak_hari = false)
    {
        $hari = array ( 1 =>    'Senin',
                    'Selasa',
                    'Rabu',
                    'Kamis',
                    'Jumat',
                    'Sabtu',
                    'Minggu'
                );
                
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $split    = explode('-', $tanggal);
        
        $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
        
        if ($cetak_hari) {
            $num = date('N', strtotime($tanggal));
            return $hari[$num] . ', ' . $tgl_indo;
        }
        return $tgl_indo;
    }
}

    function cutText($text, $length, $mode = 1)
    {
        if ($mode != 1)
        {
            $char = $text{$length - 1};
            switch($mode)
            {
                case 2: 
                    while($char != ' ') {
                        $char = $text{--$length};
                    }
                case 3:
                    while($char != ' ') {
                        $char = $text{++$length};
                    }
            }
        }
        return substr($text, 0, $length);
    }

    function cutName($data)
    {
        if($data)
        {
            $nm = explode(' ', $data);
        }

        return $nm[0];
    }

    function array_counts($array = array(),$deep){
        $return = 0;
        switch($deep){
            case 1:
                count($array);
            break;
            case 2:
                foreach($array as $k1=>$v1){
                    $return += count($array[$k1]);
                }
            break;
            case 3:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        $return += count($array[$k1][$k2]);
                    }
                }
            break;
            case 4:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        foreach($v2 as $k3=>$v3){
                            $return += count($array[$k1][$k2][$k3]);
                        }
                    }
                }
            break;
            case 5:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        foreach($v2 as $k3=>$v3){
                            foreach($v3 as $k4=>$v4){
                                $return += count($array[$k1][$k2][$k3][$k4]);
                            }
                        }
                    }
                }
            break;
        }
        
        return $return;
    }

    function htmlSelectFromArray($array, $attribute, $blank = true, $selected = '', $showKey = false, $arrayNonActive = '') {
        //$html  = "\r\n";
        $html = '<select ' . $attribute . '>';
        if (!empty($blank)) {
            if ($blank === true) {
                $html .= '<option value="">-- -- --</option>';
            } else {
                $html .= '<option value="">' . $blank . '</option>';
            }
        }
        if (is_array($array)) {
            if (count($array) > 0) {
                foreach ($array as $k => $v) {
                    $html .= '<option value="' . $k . '"';
                    if (is_array($selected)) {
                        if (in_array($k, $selected)) {
                            $html .= ' selected="selected"';
                        }
                    } else {
                        if ((string) $k == $selected && strlen($k) == strlen($selected)) {
                            $html .= ' selected="selected"';
                        }
                    }
                    if (is_array($arrayNonActive)) {
                        if (in_array($k, $arrayNonActive)) {
                            $html .= ' style="background-color:#eeeeee;color:#ff0000;"';
                        }
                    }
                    $html .= '>';
                    if ($showKey == true) {
                        $html .= $k . ' - ';
                    }
                    $html .= $v . '</option>';
                }
            } else {
                $html .= '<option value="">-- --</option>';
            }
        } else {
            $html .= '<option value="">-- --</option>';
        }
        $html .= '</select>';
        return $html;
    }

    function generates($size='16')
    {
        $CI =& get_instance();
        $CI->load->helper('string');
        return random_string('alnum', $size);
    }

    function calrt() {
        $msg = '';
        if (@$_SESSION['msg']) {
            $msg = @$_SESSION['msg'];
            $err = @$_SESSION['error'];

            if ((int) $err == 0) {
                return '<div class="alert alert-success alert-dismissible" role="alert" onclick="$(this).slideUp(\'fast\',function(){$(this).remove()});"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>' . $msg . '</div>';
            } elseif ($err == 'info') {
                return '<div class="alert alert-info" role="alert" onclick="$(this).slideUp(\'fast\',function(){$(this).remove()});">' . $msg . '</div>';
            } else {
                return '<div class="alert alert-danger alert-dismissible" role="alert" onclick="$(this).slideUp(\'fast\',function(){$(this).remove()});"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-ban"></i> Alert!</h4>' . $msg . '</div>';
            }
        }
    }

    function decodeUrl($string)
    {
        $search     = substr($string,(strpos($string,"=")+1));
        $res  = str_replace('lbesard','>=',
                str_replace('lbesar','>',
                str_replace('ldkecil','<=',
                str_replace('lkecil','<',
                str_replace('persen','%',
                str_replace('kakhir',')',
                str_replace('kawal','(',
                str_replace('andd','&',
                str_replace('coma',',',
                str_replace('smdg','=',
                str_replace('ddot',':',
                str_replace('sps',' ',
                str_replace('bcks','/',$search)))))))))))));        
        return $res;      
    }

    function array_count($array = array(),$deep){
        $return = 0;
        switch($deep){
            case 1:
                count($array);
            break;
            case 2:
                foreach($array as $k1=>$v1){
                    $return += count($array[$k1]);
                }
            break;
            case 3:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        $return += count($array[$k1][$k2]);
                    }
                }
            break;
            case 4:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        foreach($v2 as $k3=>$v3){
                            $return += count($array[$k1][$k2][$k3]);
                        }
                    }
                }
            break;
            case 5:
                foreach($array as $k1=>$v1){
                    foreach($v1 as $k2=>$v2){
                        foreach($v2 as $k3=>$v3){
                            foreach($v3 as $k4=>$v4){
                                $return += count($array[$k1][$k2][$k3][$k4]);
                            }
                        }
                    }
                }
            break;
        }
        
        return $return;
    }

    function getNumberLab($data,$tgl='',$sts='')
    {
        if($sts == 'labs')
        {
            $lastNumber = substr($data[0]->noorder, 0, 4);
            $get_next_number = sprintf('%04s', ($lastNumber + 1));
            $get_date = str_replace('-', '',$tgl);
            $next_no_order = 'PL'.$get_date.''.$get_next_number;
            return $next_no_order;
        }
        else if($sts == 'obat')
        {
            $lastNumber = substr($data[0]->no_resep, 0, 4);
            $next_no_resep = sprintf('%04s', ($lastNumber + 1));
            $get_date = str_replace('-', '',$tgl);
            return $get_date.$next_no_resep;
        }
        else if($sts == 'rad')
        {
            $lastNumber = substr($data[0]->noorder, 0, 4);
            $get_next_number = sprintf('%04s', ($lastNumber + 1));
            $get_date = str_replace('-', '',$tgl);
            $next_no_order = 'PR'.$get_date.''.$get_next_number;
            return $next_no_order;
        }
        
    }

    function cekAturanPakai($data1='',$data2='')
    {
        if(!empty($data1) && !empty($data2))
        {
            $aturan = $data1 .', '. $data2;
        }else if(!empty($data1) && empty($data2))
        {
            $aturan = $data1;
        }else if(empty($data1) && !empty($data2))
        {
            $aturan = $data2;
        }else
        {
            $aturan = '';
        }

        return $aturan;
    }

    function getKdPoli($data,$time)
    {
        if(count($data) > 1)
        {
            if($time > 06 && $time < 12)
            {
                $seskdpoli = $data;
                $kdpoli = $seskdpoli[0]->kd_poli;
            }
            else if($time > 12 && $time < 20)
            {
                $seskdpoli = $data;
                $kdpoli = $seskdpoli[1]->kd_poli;
            }
        }
        else
        {
            $seskdpoli = $data;
            $kdpoli = $seskdpoli[0]->kd_poli;
        }

        return $kdpoli;
    }

    function testing()
    {
        $CI =& get_instance();
        $CI->config->load('testing');
        $res = $CI->config->item('tes');
        echo '<pre>';
        print_r($res);
    }

    function ping($host, $port, $timeout) {
        $tB = microtime(true);
        $fP = @fSockOpen($host, $port, $errno, $errstr, $timeout);
        if(!$fP) 
        { 
            return "down"; 
        }

        $tA = microtime(true);
        return round((($tA - $tB)*10), 0)." ms";
        //Perintah Echo akan menampilkan ping jika host terhubung dan jika tidak maka akan ada pesan "down"
    }

    function _unset($data=[]){
        unset($data['nilaiDek']);
        //unset($data['jumlah_dekubitus']);
        unset($data['skala_jatuh_dewasa_nilai']);
        unset($data['skala_jatuh_dewasa_ket']);
        //unset($data['jumlah_skala_jatuh_dewasa']);
        unset($data['skala_jatuh_anak_nilai']);
        unset($data['skala_jatuh_anak_ket']);
        //unset($data['jumlah_skala_jatuh_anak']);
        unset($data['nyeri_neonatus_a']);
        unset($data['nyeri_neonatus_b']);
        //unset($data['nyeri_neonatus_a_total']);
        //unset($data['nyeri_neonatus_b_total']);
        unset($data['param_a']);
        unset($data['param_b']);
        unset($data['param_c']);
        unset($data['param_d']);
        unset($data['param_e']);
        unset($data['param_f1']);
        unset($data['param_f2']);
        //unset($data['skrining_mna_total']);
        unset($data['total_skor']);
        //unset($data['strong_kids_total']);
        unset($data['strong_kids_a']);
        unset($data['strong_kids_b']);
        unset($data['strong_kids_c']);
        unset($data['strong_kids_d']);
        unset($data['skrining_mst_a']);
        unset($data['skrining_mst_b']);
        unset($data['skrining_mst_c']);
        unset($data['timer_pengkajian_poliklinik']);
        unset($data['url']);
        unset($data['no_reg']);
        unset($data['tanggal_periksa']);
        unset($data['norm_medic']);
        $data['kardiovaskuler_alat_bantu_jantung'] = ($data['kardiovaskuler_alat_bantu_jantung']=='------') ? '' : $data['kardiovaskuler_alat_bantu_jantung'];
        $data['kardiovaskuler_irama_jantung'] = ($data['kardiovaskuler_irama_jantung']=='------') ? '' : $data['kardiovaskuler_irama_jantung'];
        $data['kardiovaskuler_suara_jantung'] = ($data['kardiovaskuler_suara_jantung']=='------') ? '' : $data['kardiovaskuler_suara_jantung'];

        return $data;
    }

    function formattelp($nohp) {
         // kadang ada penulisan no hp 0811 239 345
         $nohp = str_replace(" ","",$nohp);
         // kadang ada penulisan no hp (0274) 778787
         $nohp = str_replace("(","",$nohp);
         // kadang ada penulisan no hp (0274) 778787
         $nohp = str_replace(")","",$nohp);
         // kadang ada penulisan no hp 0811.239.345
         $nohp = str_replace(".","",$nohp);
 
        // cek apakah no hp mengandung karakter + dan 0-9
        if(!preg_match('/[^+0-9]/',trim($nohp))){
         // cek apakah no hp karakter 1-3 adalah +62
         if(substr(trim($nohp), 0, 3)=='+62'){
             $hp = trim($nohp);
         }
         // cek apakah no hp karakter 1 adalah 0
         elseif(substr(trim($nohp), 0, 1)=='0'){
             $hp = '+62'.substr(trim($nohp), 1);
         }
        }
        return $hp;
    }

    function isMobile($ua = '') {
        if ($ua == '') {
            $ua = $_SERVER["HTTP_USER_AGENT"];
        }
        if (isset($_REQUEST['mobile']) || isset($_SERVER['mobile']) || preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $ua)) {
            return true;
        } else {
            return false;
        }
    }