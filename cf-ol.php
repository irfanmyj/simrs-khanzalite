<?php 
class Cf{
	
	/*
	| -------------------------------------------------------------------
	| DATABASE CONNECTIVITY SETTINGS
	| -------------------------------------------------------------------
	| This file will contain the settings needed to access your database.
	|
	| For complete instructions please consult the 'Database Connection'
	| page of the User Guide.
	|
	| -------------------------------------------------------------------
	| EXPLANATION OF VARIABLES
	| -------------------------------------------------------------------
	|
	|	['dsn']      The full DSN string describe a connection to the database.
	|	['hostname'] The hostname of your database server.
	|	['username'] The username used to connect to the database
	|	['password'] The password used to connect to the database
	|	['database'] The name of the database you want to connect to
	|	['dbdriver'] The database driver. e.g.: mysqli.
	|			Currently supported:
	|				 cubrid, ibase, mssql, mysql, mysqli, oci8,
	|				 odbc, pdo, postgre, sqlite, sqlite3, sqlsrv
	|	['dbprefix'] You can add an optional prefix, which will be added
	|				 to the table name when using the  Query Builder class
	|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
	|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
	|	['cache_on'] TRUE/FALSE - Enables/disables query caching
	|	['cachedir'] The path to the folder where cache files should be stored
	|	['char_set'] The character set used in communicating with the database
	|	['dbcollat'] The character collation used in communicating with the database
	|				 NOTE: For MySQL and MySQLi databases, this setting is only used
	| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
	|				 (and in table creation queries made with DB Forge).
	| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
	| 				 can make your site vulnerable to SQL injection if you are using a
	| 				 multi-byte character set and are running versions lower than these.
	| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
	|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
	|	['encrypt']  Whether or not to use an encrypted connection.
	|
	|			'mysql' (deprecated), 'sqlsrv' and 'pdo/sqlsrv' drivers accept TRUE/FALSE
	|			'mysqli' and 'pdo/mysql' drivers accept an array with the following options:
	|
	|				'ssl_key'    - Path to the private key file
	|				'ssl_cert'   - Path to the public key certificate file
	|				'ssl_ca'     - Path to the certificate authority file
	|				'ssl_capath' - Path to a directory containing trusted CA certificates in PEM format
	|				'ssl_cipher' - List of *allowed* ciphers to be used for the encryption, separated by colons (':')
	|				'ssl_verify' - TRUE/FALSE; Whether verify the server certificate or not
	|
	|	['compress'] Whether or not to use client compression (MySQL only)
	|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
	|							- good for ensuring strict SQL while developing
	|	['ssl_options']	Used to set various SSL options that can be used when making SSL connections.
	|	['failover'] array - A array with 0 or more data for connections if the main should fail.
	|	['save_queries'] TRUE/FALSE - Whether to "save" all executed queries.
	| 				NOTE: Disabling this will also effectively disable both
	| 				$this->db->last_query() and profiling of DB queries.
	| 				When you run a query, with this setting set to TRUE (default),
	| 				CodeIgniter will store the SQL statement for debugging purposes.
	| 				However, this may cause high memory usage, especially if you run
	| 				a lot of SQL queries ... disable this to avoid that problem.
	|
	| The $active_group variable lets you choose which connection group to
	| make active.  By default there is only one group (the 'default' group).
	|
	| The $query_builder variables lets you determine whether or not to load
	| the query builder class.
	*/

	public $_hostname1		= 'b2Joejc4d1N6aHNsb0JTMCs5bkJVQT09'; 
	public $_database1		= 'ZkRGMWNmejg4TlhNZnQwMFFsYlFxLzVHckJHMm9wUklRcVZ5dkFiSkZwbz0=';
	public $_username1		= 'L1FNeWh1dEJIYnZwM1hlcTdudTZSQT09'; 
	public $_password1		= 'WkJVTERLYmVCSVpRZXQwMVRITWh0QT09'; 
	public $_dbprefix1		= ''; 
	public $_dbdriver1		= 'mysqli'; 
	public $_pconnect1		= FALSE; 
	public $_db_debug1		= (ENVIRONMENT !== 'production');
	public $_cache_on1		= FALSE;
	public $_cachedir1		= '';
	public $_char_set1		= 'utf8';
	public $_dbcollat1		= 'utf8_unicode_ci';
	public $_swap_pre1		= ''; 
	public $_encrypt1		= FALSE; 
	public $_compress1		= FALSE; 
	public $_stricton1		= FALSE; 
	public $_failover1		= array(); 
	public $_save_queries1	= TRUE; 
	public $_port1			= ''; 
	public $_group1 		= 'default';

	public $_hostname2		= 'b2Joejc4d1N6aHNsb0JTMCs5bkJVQT09'; 
	public $_database2		= 'RUl1YlVDd1k5eXo1Nm1DdDdGdXJCQT09';
	public $_username2		= 'L1FNeWh1dEJIYnZwM1hlcTdudTZSQT09'; 
	public $_password2		= 'WkJVTERLYmVCSVpRZXQwMVRITWh0QT09'; 
	public $_dbprefix2		= ''; 
	public $_dbdriver2		= 'mysqli'; 
	public $_pconnect2		= FALSE; 
	public $_db_debug2		= (ENVIRONMENT !== 'production');
	public $_cache_on2		= FALSE;
	public $_cachedir2		= '';
	public $_char_set2		= 'utf8';
	public $_dbcollat2		= 'utf8_unicode_ci';
	public $_swap_pre2		= ''; 
	public $_encrypt2		= FALSE; 
	public $_compress2		= FALSE; 
	public $_stricton2		= FALSE; 
	public $_failover2		= array(); 
	public $_save_queries2	= TRUE; 
	public $_port2			= '';  

	/*
	public $_hostname1		= 'localhost'; // localhost
	public $_database1		= 'postgres'; // nama database
	public $_username1		= 'postgres'; // username database
	public $_password1		= 'bismillah'; // password database
	public $_dbprefix1		= 'bt_'; // prefix tabel database
	public $_dbdriver1		= 'postgre'; // prefix tabel database
	public $_port1			= 5432; // prefix tabel database
	public $_group1 		= 'default'; // nama database group
	*/


	/*
	---------------------------------------------------------------------------
	|##########################################################################
	| AUTO-LOADER -> dev/application/config/autoload.php
	|##########################################################################
	---------------------------------------------------------------------------
	*/

	/*
	| -------------------------------------------------------------------
	| AUTO-LOADER
	| -------------------------------------------------------------------
	| This file specifies which systems should be loaded by default.
	|
	| In order to keep the framework as light-weight as possible only the
	| absolute minimal resources are loaded by default. For example,
	| the database is not connected to automatically since no assumption
	| is made regarding whether you intend to use it.  This file lets
	| you globally define which systems you would like loaded with every
	| request.
	|
	| -------------------------------------------------------------------
	| Instructions
	| -------------------------------------------------------------------
	|
	| These are the things you can load automatically:
	|
	| 1. Packages
	| 2. Libraries
	| 3. Drivers
	| 4. Helper files
	| 5. Custom config files
	| 6. Language files
	| 7. Models
	|
	*/
	
	/*
	| -------------------------------------------------------------------
	|  Auto-load Packages
	| -------------------------------------------------------------------
	| Prototype:
	|
	|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
	|
	*/
	public $packages 	= array();

	/*
	| -------------------------------------------------------------------
	|  Auto-load Libraries
	| -------------------------------------------------------------------
	| These are the classes located in system/libraries/ or your
	| application/libraries/ directory, with the addition of the
	| 'database' library, which is somewhat of a special case.
	|
	| Prototype:
	|
	|	$autoload['libraries'] = array('database', 'email', 'session');
	|
	| You can also supply an alternative library name to be assigned
	| in the controller:
	|
	|	$autoload['libraries'] = array('user_agent' => 'ua');
	*/
	public $libraries 	= array('database');

	/*
	| -------------------------------------------------------------------
	|  Auto-load Drivers
	| -------------------------------------------------------------------
	| These classes are located in system/libraries/ or in your
	| application/libraries/ directory, but are also placed inside their
	| own subdirectory and they extend the CI_Driver_Library class. They
	| offer multiple interchangeable driver options.
	|
	| Prototype:
	|
	|	$autoload['drivers'] = array('cache');
	|
	| You can also supply an alternative property name to be assigned in
	| the controller:
	|
	|	$autoload['drivers'] = array('cache' => 'cch');
	|
	*/
	public $drivers 	= array();

	/*
	| -------------------------------------------------------------------
	|  Auto-load Helper Files
	| -------------------------------------------------------------------
	| Prototype:
	|
	|	$autoload['helper'] = array('url', 'file');
	*/
	public $helper 		= array('url');

	/*
	| -------------------------------------------------------------------
	|  Auto-load Config files
	| -------------------------------------------------------------------
	| Prototype:
	|
	|	$autoload['config'] = array('config1', 'config2');
	|
	| NOTE: This item is intended for use ONLY if you have created custom
	| config files.  Otherwise, leave it blank.
	|
	*/
	public $config 		= array();

	/*
	| -------------------------------------------------------------------
	|  Auto-load Language files
	| -------------------------------------------------------------------
	| Prototype:
	|
	|	$autoload['language'] = array('lang1', 'lang2');
	|
	| NOTE: Do not include the "_lang" part of your file.  For example
	| "codeigniter_lang.php" would be referenced as array('codeigniter');
	|
	*/
	public $language 	= array();

	/*
	| -------------------------------------------------------------------
	|  Auto-load Models
	| -------------------------------------------------------------------
	| Prototype:
	|
	|	$autoload['model'] = array('first_model', 'second_model');
	|
	| You can also supply an alternative model name to be assigned
	| in the controller:
	|
	|	$autoload['model'] = array('first_model' => 'first');
	*/
	public $model 		= array();

	/*
	---------------------------------------------------------------------------
	|##########################################################################
	| CONFIG -> dev/application/config/config.php
	|##########################################################################
	---------------------------------------------------------------------------
	*/

	/*
	|--------------------------------------------------------------------------
	| Index File
	|--------------------------------------------------------------------------
	|
	| Typically this will be your index.php file, unless you've renamed it to
	| something else. If you are using mod_rewrite to remove the page set this
	| variable so that it is blank.
	|
	*/
	public $index_page		= '';

	/*
	|--------------------------------------------------------------------------
	| URI PROTOCOL
	|--------------------------------------------------------------------------
	|
	| This item determines which server global should be used to retrieve the
	| URI string.  The default setting of 'REQUEST_URI' works for most servers.
	| If your links do not seem to work, try one of the other delicious flavors:
	|
	| 'REQUEST_URI'    Uses $_SERVER['REQUEST_URI']
	| 'QUERY_STRING'   Uses $_SERVER['QUERY_STRING']
	| 'PATH_INFO'      Uses $_SERVER['PATH_INFO']
	|
	| WARNING: If you set this to 'PATH_INFO', URIs will always be URL-decoded!
	*/
	public $uri_protocol	= 'REQUEST_URI';

	/*
	|--------------------------------------------------------------------------
	| URL suffix
	|--------------------------------------------------------------------------
	|
	| This option allows you to add a suffix to all URLs generated by CodeIgniter.
	| For more information please see the user guide:
	|
	| https://codeigniter.com/user_guide/general/urls.html
	*/
	public $url_suffix	= 'REQUEST_URI';

	/*
	|--------------------------------------------------------------------------
	| Default Language
	|--------------------------------------------------------------------------
	|
	| This determines which set of language files should be used. Make sure
	| there is an available translation if you intend to use something other
	| than english.
	|
	*/
	public $languagec	= 'bahasa';

	/*
	|--------------------------------------------------------------------------
	| Default Character Set
	|--------------------------------------------------------------------------
	|
	| This determines which character set is used by default in various methods
	| that require a character set to be provided.
	|
	| See http://php.net/htmlspecialchars for a list of supported charsets.
	|
	*/
	public $charset = 'UTF-8';

	/*
	|--------------------------------------------------------------------------
	| Enable/Disable System Hooks
	|--------------------------------------------------------------------------
	|
	| If you would like to use the 'hooks' feature you must enable it by
	| setting this variable to TRUE (boolean).  See the user guide for details.
	|
	*/
	public $enable_hooks = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Class Extension Prefix
	|--------------------------------------------------------------------------
	|
	| This item allows you to set the filename/classname prefix when extending
	| native libraries.  For more information please see the user guide:
	|
	| https://codeigniter.com/user_guide/general/core_classes.html
	| https://codeigniter.com/user_guide/general/creating_libraries.html
	|
	*/
	public $subclass_prefix = 'MY_';

	/*
	|--------------------------------------------------------------------------
	| Composer auto-loading
	|--------------------------------------------------------------------------
	|
	| Enabling this setting will tell CodeIgniter to look for a Composer
	| package auto-loader script in application/vendor/autoload.php.
	|
	|	$config['composer_autoload'] = TRUE;
	|
	| Or if you have your vendor/ directory located somewhere else, you
	| can opt to set a specific path as well:
	|
	|	$config['composer_autoload'] = '/path/to/vendor/autoload.php';
	|
	| For more information about Composer, please visit http://getcomposer.org/
	|
	| Note: This will NOT disable or override the CodeIgniter-specific
	|	autoloading (application/config/autoload.php)
	*/
	public $composer_autoload = 'vendor/autoload.php';

	/*
	|--------------------------------------------------------------------------
	| Allowed URL Characters
	|--------------------------------------------------------------------------
	|
	| This lets you specify which characters are permitted within your URLs.
	| When someone tries to submit a URL with disallowed characters they will
	| get a warning message.
	|
	| As a security measure you are STRONGLY encouraged to restrict URLs to
	| as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
	|
	| Leave blank to allow all characters -- but only if you are insane.
	|
	| The configured value is actually a regular expression character group
	| and it will be executed as: ! preg_match('/^[<permitted_uri_chars>]+$/i
	|
	| DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
	| 'a-z 0-9~%.:_\-@\='; 'a-z 0-9~%.:_\-';
	*/
	public $permitted_uri_chars  = 'a-z 0-9~%.:_\+-@\=';

	/*
	|--------------------------------------------------------------------------
	| Enable Query Strings
	|--------------------------------------------------------------------------
	|
	| By default CodeIgniter uses search-engine friendly segment based URLs:
	| example.com/who/what/where/
	|
	| You can optionally enable standard query string based URLs:
	| example.com?who=me&what=something&where=here
	|
	| Options are: TRUE or FALSE (boolean)
	|
	| The other items let you set the query string 'words' that will
	| invoke your controllers and its functions:
	| example.com/index.php?c=controller&m=function
	|
	| Please note that some of the helpers won't work as expected when
	| this feature is enabled, since CodeIgniter is designed primarily to
	| use segment based URLs.
	|
	*/
	public $enable_query_strings = FALSE;
	public $controller_trigger = 'c';
	public $function_trigger = 'm';
	public $directory_trigger = 'd';

	/*
	|--------------------------------------------------------------------------
	| Allow $_GET array
	|--------------------------------------------------------------------------
	|
	| By default CodeIgniter enables access to the $_GET array.  If for some
	| reason you would like to disable it, set 'allow_get_array' to FALSE.
	|
	| WARNING: This feature is DEPRECATED and currently available only
	|          for backwards compatibility purposes!
	|
	*/
	public $allow_get_array = TRUE;

	/*
	|--------------------------------------------------------------------------
	| Error Logging Threshold
	|--------------------------------------------------------------------------
	|
	| You can enable error logging by setting a threshold over zero. The
	| threshold determines what gets logged. Threshold options are:
	|
	|	0 = Disables logging, Error logging TURNED OFF
	|	1 = Error Messages (including PHP errors)
	|	2 = Debug Messages
	|	3 = Informational Messages
	|	4 = All Messages
	|
	| You can also pass an array with threshold levels to show individual error types
	|
	| 	array(2) = Debug Messages, without Error Messages
	|
	| For a live site you'll usually only enable Errors (1) to be logged otherwise
	| your log files will fill up very fast.
	|
	*/
	public $log_threshold = array(1);

	/*
	|--------------------------------------------------------------------------
	| Error Logging Directory Path
	|--------------------------------------------------------------------------
	|
	| Leave this BLANK unless you would like to set something other than the default
	| application/logs/ directory. Use a full server path with trailing slash.
	|
	*/
	public $log_path = '';

	/*
	|--------------------------------------------------------------------------
	| Log File Extension
	|--------------------------------------------------------------------------
	|
	| The default filename extension for log files. The default 'php' allows for
	| protecting the log files via basic scripting, when they are to be stored
	| under a publicly accessible directory.
	|
	| Note: Leaving it blank will default to 'php'.
	|
	*/
	public $log_file_extension = 'txt';

	/*
	|--------------------------------------------------------------------------
	| Log File Permissions
	|--------------------------------------------------------------------------
	|
	| The file system permissions to be applied on newly created log files.
	|
	| IMPORTANT: This MUST be an integer (no quotes) and you MUST use octal
	|            integer notation (i.e. 0700, 0644, etc.)
	*/
	public $log_file_permissions = 0644;

	/*
	|--------------------------------------------------------------------------
	| Date Format for Logs
	|--------------------------------------------------------------------------
	|
	| Each item that is logged has an associated date. You can use PHP date
	| codes to set your own date formatting
	|
	*/
	public $log_date_format = 'Y-m-d H:i:s';

	/*
	|--------------------------------------------------------------------------
	| Error Views Directory Path
	|--------------------------------------------------------------------------
	|
	| Leave this BLANK unless you would like to set something other than the default
	| application/views/errors/ directory.  Use a full server path with trailing slash.
	|
	*/
	public $error_views_path = '';

	/*
	|--------------------------------------------------------------------------
	| Cache Directory Path
	|--------------------------------------------------------------------------
	|
	| Leave this BLANK unless you would like to set something other than the default
	| application/cache/ directory.  Use a full server path with trailing slash.
	|
	*/
	public $cache_path = '';

	/*
	|--------------------------------------------------------------------------
	| Cache Include Query String
	|--------------------------------------------------------------------------
	|
	| Whether to take the URL query string into consideration when generating
	| output cache files. Valid options are:
	|
	|	FALSE      = Disabled
	|	TRUE       = Enabled, take all query parameters into account.
	|	             Please be aware that this may result in numerous cache
	|	             files generated for the same page over and over again.
	|	array('q') = Enabled, but only take into account the specified list
	|	             of query parameters.
	|
	*/
	public $cache_query_string = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| If you use the Encryption class, you must set an encryption key.
	| See the user guide for more info.
	|
	| https://codeigniter.com/user_guide/libraries/encryption.html
	|
	*/
	public $encryption_key = 'mencoba#membuat~karya2019';

	/*
	|--------------------------------------------------------------------------
	| Session Variables
	|--------------------------------------------------------------------------
	|
	| 'sess_driver'
	|
	|	The storage driver to use: files, database, redis, memcached
	|
	| 'sess_cookie_name'
	|
	|	The session cookie name, must contain only [0-9a-z_-] characters
	|
	| 'sess_expiration'
	|
	|	The number of SECONDS you want the session to last.
	|	Setting to 0 (zero) means expire when the browser is closed.
	|
	| 'sess_save_path'
	|
	|	The location to save sessions to, driver dependent.
	|
	|	For the 'files' driver, it's a path to a writable directory.
	|	WARNING: Only absolute paths are supported!
	|
	|	For the 'database' driver, it's a table name.
	|	Please read up the manual for the format with other session drivers.
	|
	|	IMPORTANT: You are REQUIRED to set a valid save path!
	|
	| 'sess_match_ip'
	|
	|	Whether to match the user's IP address when reading the session data.
	|
	|	WARNING: If you're using the database driver, don't forget to update
	|	         your session table's PRIMARY KEY when changing this setting.
	|
	| 'sess_time_to_update'
	|
	|	How many seconds between CI regenerating the session ID.
	|
	| 'sess_regenerate_destroy'
	|
	|	Whether to destroy session data associated with the old session ID
	|	when auto-regenerating the session ID. When set to FALSE, the data
	|	will be later deleted by the garbage collector.
	|
	| Other session cookie settings are shared with the rest of the application,
	| except for 'cookie_prefix' and 'cookie_httponly', which are ignored here.
	|
	*/
	public $sess_driver = 'files';
	public $sess_cookie_name = '';
	public $sess_expiration = 7200;
	public $sess_save_path = '/opt/lampp/temp/';
	public $sess_match_ip = FALSE;
	public $sess_time_to_update = 300;
	public $sess_regenerate_destroy = FALSE;
	
	/*
	|--------------------------------------------------------------------------
	| Cookie Related Variables
	|--------------------------------------------------------------------------
	|
	| 'cookie_prefix'   = Set a cookie name prefix if you need to avoid collisions
	| 'cookie_domain'   = Set to .your-domain.com for site-wide cookies
	| 'cookie_path'     = Typically will be a forward slash
	| 'cookie_secure'   = Cookie will only be set if a secure HTTPS connection exists.
	| 'cookie_httponly' = Cookie will only be accessible via HTTP(S) (no javascript)
	|
	| Note: These settings (with the exception of 'cookie_prefix' and
	|       'cookie_httponly') will also affect sessions.
	|
	*/
	public $cookie_prefix = '';
	public $cookie_domain = '';
	public $cookie_path = '/';
	public $cookie_secure = FALSE;
	public $cookie_httponly = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Standardize newlines
	|--------------------------------------------------------------------------
	|
	| Determines whether to standardize newline characters in input data,
	| meaning to replace \r\n, \r, \n occurrences with the PHP_EOL value.
	|
	| WARNING: This feature is DEPRECATED and currently available only
	|          for backwards compatibility purposes!
	|
	*/
	public $standardize_newlines = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Global XSS Filtering
	|--------------------------------------------------------------------------
	|
	| Determines whether the XSS filter is always active when GET, POST or
	| COOKIE data is encountered
	|
	| WARNING: This feature is DEPRECATED and currently available only
	|          for backwards compatibility purposes!
	|
	*/
	public $global_xss_filtering = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Cross Site Request Forgery
	|--------------------------------------------------------------------------
	| Enables a CSRF cookie token to be set. When set to TRUE, token will be
	| checked on a submitted form. If you are accepting user data, it is strongly
	| recommended CSRF protection be enabled.
	|
	| 'csrf_token_name' = The token name
	| 'csrf_cookie_name' = The cookie name
	| 'csrf_expire' = The number in seconds the token should expire.
	| 'csrf_regenerate' = Regenerate token on every submission
	| 'csrf_exclude_uris' = Array of URIs which ignore CSRF checks
	*/
	public $csrf_protection = FALSE;
	public $csrf_token_name = 'csrf_sysmessage_oke';
	public $csrf_cookie_name = 'csrf_cookie_name';
	public $csrf_expire = 7200;
	public $csrf_regenerate = TRUE;
	public $csrf_exclude_uris = array();

	/*
	|--------------------------------------------------------------------------
	| Output Compression
	|--------------------------------------------------------------------------
	|
	| Enables Gzip output compression for faster page loads.  When enabled,
	| the output class will test whether your server supports Gzip.
	| Even if it does, however, not all browsers support compression
	| so enable only if you are reasonably sure your visitors can handle it.
	|
	| Only used if zlib.output_compression is turned off in your php.ini.
	| Please do not use it together with httpd-level output compression.
	|
	| VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
	| means you are prematurely outputting something to your browser. It could
	| even be a line of whitespace at the end of one of your scripts.  For
	| compression to work, nothing can be sent before the output buffer is called
	| by the output class.  Do not 'echo' any values with compression enabled.
	|
	*/
	public $compress_output = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Master Time Reference
	|--------------------------------------------------------------------------
	|
	| Options are 'local' or any PHP supported timezone. This preference tells
	| the system whether to use your server's local time as the master 'now'
	| reference, or convert it to the configured one timezone. See the 'date
	| helper' page of the user guide for information regarding date handling.
	|
	*/
	public $time_reference = 'local';

	/*
	|--------------------------------------------------------------------------
	| Rewrite PHP Short Tags
	|--------------------------------------------------------------------------
	|
	| If your PHP installation does not have short tag support enabled CI
	| can rewrite the tags on-the-fly, enabling you to utilize that syntax
	| in your view files.  Options are TRUE or FALSE (boolean)
	|
	| Note: You need to have eval() enabled for this to work.
	|
	*/
	public $rewrite_short_tags = FALSE;

	/*
	|--------------------------------------------------------------------------
	| Reverse Proxy IPs
	|--------------------------------------------------------------------------
	|
	| If your server is behind a reverse proxy, you must whitelist the proxy
	| IP addresses from which CodeIgniter should trust headers such as
	| HTTP_X_FORWARDED_FOR and HTTP_CLIENT_IP in order to properly identify
	| the visitor's IP address.
	|
	| You can use both an array or a comma-separated list of proxy addresses,
	| as well as specifying whole subnets. Here are a few examples:
	|
	| Comma-separated:	'10.0.1.200,192.168.5.0/24'
	| Array:		array('10.0.1.200', '192.168.5.0/24')
	*/
	public $proxy_ips = '';

	/*
	|--------------------------------------------------------------------------
	| LOG VIEWR
	|--------------------------------------------------------------------------
	| konfigurasi log
	*/
	/*public $clv_log_folder_path = "logs";
	public $clv_log_file_pattern = "log-*.txt";*/

	/*
	KONFIGURASI
	*/
	public $port 			= '';
	
	public $default_limit	= 10; //Limit minimal jumlah data tabel yang dimunculkan 
	
	public $limit_rows		= [5 => 5,10 => 10,25 => 25,50 => 50,100 => 100,500 => 500,1000 => 1000,10000 => 10000]; // Pilihan jumlah limit

	public $access_level	= ['View','Add','search','filter','Update','Delete','Download','Upload','print']; // Akses level setiap module/controller

	public $status			= ['Y' => 'Aktif','N' => 'Tidak Aktif'];

	public $dayShow			= 7;
	var $_jadwal_cuti   	= 10;
	var $_hari_daftar		= '07';
	var $_limit_jam			= '13:00:00';

	public $configEmail	= [
		'protocol' => 'smtp',
		'smtp_host' => 'ssl://smtp.googlemail.com',
		'smtp_port' => 465,
		'smtp_timeout' => '7',
		'smtp_user' => 'rsuddepokinfo@gmail.com',
		'smtp_pass' => 'rsud99!@',
		'charset' => 'utf-8',
		'newline' => "\r\n",
		'mailtype' => 'html'
	];

	public $size_length_token = [
		'AKHAD' => 33,
        'SENIN' => 27,
        'SELASA' => 39,
        'RABU' => 35,
        'KAMIS' => 37,
        'JUMAT' => 41,
        'SABTU' => 43
	];

	public $gender = [
		'L' => 'Laki-laki',
		'P' => 'Perempuan'
	];

	public $_golongan_darah = [
		'-' => '-',
		'A' => 'A',
		'B' => 'B',
		'O' => 'O',
		'AB' => 'AB'
	];

	public $_pendidikan = [
		'TS' => 'TS',
		'TK' => 'TK',
		'SD' => 'SD',
		'SMP' => 'SMP',
		'SMA' => 'SMA',
		'SLTA' => 'SLTA/SEDERAJAT',
		'D1' => 'D1',
		'D2' => 'D2',
		'D3' => 'D3',
		'S1' => 'S1',
		'S2' => 'S2',
		'S3' => 'S3'
	];

	public $_agama = [
		'ISLAM' 		=> 'ISLAM',
		'KRISTEN' 		=> 'KRISTEN',
		'KATOLIK' 		=> 'KATOLIK',
		'HINDU' 		=> 'HINDU',
		'BUDHA' 		=> 'BUDHA',
		'KONG HU CHU' 	=> 'KONG HU CHU'
	];

	/*
	* data master visit pasien
	*/
	public $status_field = [
		1 => 'Aktiv',
		0 => 'Tidak Aktiv'
	];

	public $status_familys = [
		'Ayah' => 'Ayah',
		'Ibu' => 'Ibu',
		'Suami' => 'Suami',
		'Istri' => 'Istri',
		'Saudara' => 'Saudara',
		'Keluaraga' => 'Keluaraga',
		'Teman' => 'Teman'
	];

	public $status_family = [
		'Ayah' => 'Ayah',
		'Ibu' => 'Ibu',
		'Suami' => 'Suami',
		'Istri' => 'Istri',
		'Saudara' => 'Saudara',
		'Keluaraga' => 'Keluaraga'
	];

	public $status_marital = [
		'belum menikah' => 'Belum Menikah',
		'menikah' => 'Menikah',
		'janda' => 'Janda',
		'duda' => 'Duda',
		'single' => 'Single'
	];

	public $_sts_nikah = [
		'MENIKAH' => 'MENIKAH',
		'BELUM MENIKAH' => 'BELUM MENIKAH',
		'JANDA' => 'JANDA',
		'DUDHA' => 'DUDHA',
		'JOMBLO' => 'JOMBLO'
	];

	public $patient_status = [
		1 => 'Belum',
		2 => 'Sudah',
		3 => 'Batal',
		4 => 'Berkas Diterima',
		5 => 'Dirujuk',
		6 => 'Meninggal',
		7 => 'Dirawat',
		8 => 'Pulang Paksa'
	];

	public $status_reg = [
		1 => 'Lama',
		2 => 'Baru'
	];

	public $next_status = [
		1 => 'Ralan',
		2 => 'Ranap'
	];

	public $pay_status = [
		1 => 'Belum Bayar',
		2 => 'Sudah Bayar'
	];

	public $status_polyclinic = [
		1 => 'Lama',
		2 => 'Baru'
	];

	public $doctors_schedule = [
		1 => 'Senin',
		2 => 'Selasa',
		3 => 'Rabu',
		4 => 'Kamis',
		5 => 'Jumat',
		6 => 'Sabtu',
		7 => 'Akhad'
	];

	public $doctors_schedule_khanza = [
		1 => 'Senin',
		2 => 'Selasa',
		3 => 'Rabu',
		4 => 'Kamis',
		5 => 'Jumat',
		6 => 'Sabtu',
		7 => 'Akhad'
	];

	public $shift_doctors = [
		1 => 'Pagi 08:00:00 - 12:00:00',
		2 => 'Sore 14:00:00 - 17:00:00',
		3 => 'Malam 18:00:00 - 21:00:00'
	];

	public $shift_hours = [
		1 => '08:00:00 - 12:00:00',
		2 => '14:00:00 - 17:00:00',
		3 => '18:00:00 - 21:00:00'
	];

	public $dashbord_files = [
		1 => 'superadmin',
		2 => 'administrator',
		3 => 'perawatranap',
		4 => 'dokterpoliklinik',
		5 => 'admink3',
		6 => 'perawatranap'
	];

	public $themes_login = 'lucida';
	public $themes_folder = [
		'SuperAdministrator' => 'lucida',
		'DokterPoliklinik' => 'lucida'
	];

	public $themes_folder_default = [
		'SuperAdministrator' => 'lucida'
	];

	public $status_roominpatient = [
		'Kosong' => 'Kosong',
		'Isi' => 'Isi',
		'Dibersihkan' => 'Dibersihkan',
		'Dibooking' => 'Dibooking'
	];

	/*
	MASTER SUPER ADMIN
	*/
	public $modules = [
		'view' => ['Y','N'],
		'add' => ['Y','N'],
		'search' => ['Y','N'],
		'filter' => ['Y','N'],
		'update' => ['Y','N'],
		'delete' => ['Y','N'],
		'upload' => ['Y','N'],
		'download' => ['Y','N'],
		'print' => ['Y','N']
	];

	public $acl = array(
		'view' => 'view',
		'add' => 'add',
		'edit' => 'edit',
		'search' => 'search',
		'filter' => 'filter',
		'update' => 'update',
		'delete' => 'delete',
		'upload' => 'upload',
		'download' => 'download',
		'print' => 'print'
	);

	public $statusdata = [
		'Y' => 'Active',
		'N' => 'Non Active'
	];

	/*
	NURSING MASTER
	*/
	public $intervention_group = [
		1 => 'Intervensi Mandiri',
		2 => 'Intervensi Kolaborasi / Mandiri Ahli'
	];

	public $nursing_type = [
		'POLI'=>'Poliklinik',
        'IGD'=>'IGD',
        'OK'=>'Kamar Operasi',
        'ICU'=>'ICU/ICCU',
        'RANAP'=>'Rawat Inap'
	];

	/*
	SAFETY
	*/
	public $safety_officer_code = [
		1 => 'white',
		2 => 'red',
		3 => 'yellow',
		4 => 'blue',
		5 => 'pink',
		6 => 'brown',
		7 => 'green',
		8 => 'black'
	];

	public $safety_officer_code_desc = [
		1 => 'Kordinator',
		2 => 'Pemadam kebakaran',
		3 => 'Kuning',
		4 => 'Biru',
		5 => 'Merah Jambu',
		6 => 'Cokelat',
		7 => 'Hijau',
		8 => 'Hitam'
	];

	public $dayschedule = [
		'Senin' => 'Senin',
		'Selasa' => 'Selasa',
		'Rabu' => 'Rabu',
		'Kamis' => 'Kamis',
		'Jumat' => 'Jumat',
		'Sabtu' => 'Sabtu',
		'Akhad' => 'Akhad'
	];

	// Configuration Encrypt
	var $_key_encypt = '2018201920202021';
	var $_iv = 'APPSSECURITYSISY';
	var $_encryption_mechanism = 'aes-256-cbc';
	var $_hash = 'sha256';

    public function decrypt_aes_256_cbc($string='')
    {
        $output = false;

        // hash
        $key    = hash($this->_hash, $this->_key_encypt);
        // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
        $iv = substr(hash($this->_hash,  $this->_iv), 0, 16);
        //do the decryption given text/string/number
        $output = openssl_decrypt(base64_decode($string), $this->_encryption_mechanism, $key, 0, $iv);
        return $output;
    }

    public function encrypt_aes_256_cbc($string='')
    {
        $output = false;

        // hash
        $key    = hash($this->_hash, $this->_key_encypt);
        // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
        $iv = substr(hash($this->_hash,  $this->_iv), 0, 16);
        //do the decryption given text/string/number
        $result = openssl_encrypt($string, 'aes-256-cbc', $key, 0, $iv);
        $output = base64_encode($result);
        return $output;
    }

    /*
	Resiko Dekubitus
    */
    public $_RESIKO_DEKUBITUS = [
    	'nilaiDek_a' => 'nilaiDek_a',
    	'nilaiDek_b' => 'nilaiDek_b',
    	'nilaiDek_c' => 'nilaiDek_c',
    	'nilaiDek_d' => 'nilaiDek_d',
    	'nilaiDek_e' => 'nilaiDek_e',
    	'nilaiDek_f' => 'nilaiDek_f',
    	'nilaiDek_g' => 'nilaiDek_g'
    ];

    /*
	Data Sakal Jatuh Dewasa (Morse)
    */
    public $_JATUH_VAR1  = [
        'D'=> [
            'D1'=>'Riwayat Jatuh',
            'D2'=>'Diagnosa Sekunder',
            'D3'=>'Alat Bantu Jalan',
            'D4'=>'Terapi Intravena',
            'D5'=>'Cara Berjalan',
            'D6'=>'Status Mental'],
        'G'=> [
            'G1'=>'Gangguan gaya berjalan (diseret, menghentak, berayun)',
            'G2'=>'Pusing / pingsan pada posisi tegak',
            'G3'=>'Kebingungan setiap saat',
            'G4'=>'Nokturia / Inkontinesia',
            'G5'=>'Kebingungan intermiten',
            'G6'=>'Kelemahan umum',
            'G7'=>'Obat - obatan beresiko tinggi (duretik, narkotik, sedatif, antipsikotik, laksatif, vasodinator, antioritma, antihipertensi, obat hipo... anti depresan, neurolepok, NSAID)',
            'G8'=>'Riwayat jatuh dalam 12 bulan sebelumnya',
            'G9'=>'Osteoporosis',
            'G10'=>'Gangguan pendengaran atau penglihatan',
            'G11'=>'Usia 60 tahun ke atas'],
        'A'=> [
            'A1'=>'Umur',
            'A2'=>'Jenis kelamin',
            'A3'=>'Diagnosa',
            'A4'=>'Faktor lingkungan',
            'A5'=>'Respon terhadap ...',
            'A6'=>'Penggunaan obat',
            'A7'=>'Gangguan Kognitif']
        ];
                                
	public $_JATUH_VAR2 = [
        'D1'=> [
            'D11'=>'Ya, pernah jatuh dalam 3 bulan terakhir',
            'D12'=>'Tidak ada'],
        'D2'=> [
            'D21'=>'Memiliki lebih dari satu diagnosa medis',
            'D22'=>'Hanya memiliki satu diagnose medis'],
        'D3'=> [
            'D31'=>'Berpegangan pada benda-benda disekitar',
            'D32'=>'Kruk, tongkat atau walker',
            'D33'=>'Bed Rest atau dibantu keluarga atau perawat'],
        'D4'=> [
            'D41'=>'Terpasang IV line dan atau heparin',
            'D42'=>'Tidak terpasang IV line dan atau heparin'],
        'D5'=> [
            'D51'=>'Ada gangguan seperti pincang atau tertatih',
            'D52'=>'Lemah, tidak bertenaga',
            'D53'=>'Normal, bedrest atau imobilisasi (tidak dapat bergerak sendiri'],
        'D6'=> [
            'D61'=>'Tidak memahami keterbatasan yang dimiliki',
            'D62'=>'Memahami keterbatasan yang dimiliki'],
        'A1'=> [
            'A11'=>'Dibawah 3 tahun',
            'A12'=>'3 - 7 tahun',
            'A13'=>'7 - 13 tahun',
            'A14'=>'> 13 tahun'],
        'A2'=> [
            'A21'=>'Laki - laki',
            'A22'=>'Perempuan'],
        'A3'=> [
            'A31'=>'Kelainan neurologis',
            'A32'=>'Perubahan dalam oksigenisasi (masalah haluaran nafas, ....)',
            'A33'=>'Kelainan psikis / perilaku',
            'A34'=>'Diagnosis lain'],
        'A4'=> [
            'A41'=>'Riwayat jatuh dari tempat tidur saat bayi anak',
            'A42'=>'Pasien yang meunggunakan obat',
            'A43'=>'Pasien berada di tempat tidur',
            'A44'=>'Di luar ruang rawat'],
        'A5'=> [
            'A51'=>'Dalam 24 jam',
            'A52'=>'Dalam 48 jam',
        'A53'=>'> 48 jam'],
        'A6'=> [
            'A61'=>'Bermacam - macam obat yang di gunakan obat sedatif...',
            'A62'=>'Salah satu dari pengobatan di atas',
            'A63'=>'Penggunaan lain'],
         'A7'=> [
           'A71'=>'Tidak sadar keterbatasan',
           'A72'=>'Lupa keterbatasan',
           'A73'=>'Mengetahui kemampuan diri'
        ]
    ];
                                
	public $_JATUH_VAR3 = [
        'D11'=>25,
        'D12'=>0,
        'D21'=>15,
        'D22'=>0,
        'D31'=>30,
        'D32'=>15,
        'D33'=>0,
        'D41'=>20,
        'D42'=>0,
        'D51'=>20,
        'D52'=>10,
        'D53'=>0,
        'D61'=>15,
        'D62'=>0,
        'A11'=>4,
        'A12'=>3,
        'A13'=>2,
        'A14'=>1,
        'A21'=>2,
        'A22'=>1,
        'A31'=>4,
        'A32'=>3,
        'A33'=>2,
        'A34'=>1,
        'A41'=>'',
        'A42'=>3,
        'A43'=>2,
        'A44'=>1,
        'A51'=>'',
        'A52'=>3,
        'A53'=>2,
        'A61'=>'',
        'A62'=>3,
        'A63'=>2,
        'A71'=>3,
        'A72'=>2,
        'A73'=>1,
        'G1'=>4,
        'G2'=>3,
        'G3'=>3,
        'G4'=>3,
        'G5'=>2,
        'G6'=>2,
        'G7'=>2,
        'G8'=>2,
        'G9'=>1,
        'G10'=>1,
        'G11'=>1];

    /*
	Data Skrining Strong Kids
    */
    public $_SKRINING_MALNUTRISI_ANAK = [
		'strong_kids_a'=>'Apakah pasien tampak kurus?',
		'strong_kids_b'=>'Apakah terdapat penurunan berat badan selama 1 bulan terakhir? <br>(berdasarkan penilaian objektif data BB bila ada / penilaian subjektif dari orang tua',
		'strong_kids_c'=>'Apakah terdapat salah satu dari kondisi berikut ? <br>* Diare > 5 kali/hari dan atau muntah > 3  kali/hari dalam seminggu terakhir <br>* Asupan makan berkurang selama 1 minggu terakhir',
		'strong_kids_d'=>'Apakah terdapat penyakit atau keadaan yang mengakibatkan pasien berisiko mengalami malnutrisi ? ( lihat tabel dibawah )'
	];

	public $_SKRINING_MALNUTRISI_ANAK_CHILD = [
		'strong_kids_a'=> [
			'A1'=>'a. Tidak',
			'A2'=>'b. Ya'
		],
		'strong_kids_b'=> [
			'A1'=>'a. Tidak',
                                            'A2'=>'b. Ya'
		],
		'strong_kids_c'=> [
			'A1'=>'a. Tidak',
                                            'A2'=>'b. Ya'
		],
		'strong_kids_d'=> [
			'A1'=>'a. Tidak',
                                            'A2'=>'b. Ya'
		]
	];

	public $_SKRINING_MALNUTRISI_PENYAKIT = [
		'strong_kids_a'=>[
			'A1'=>'Diare kronik (lebih dari 2 minggu)',
			'A2'=>'(Tersangka) Penyakit Jantung Bawaaan',
			'A3'=>'(Tersangka) HIV',
			'A4'=>'(Tersangka) Kanker'
		],
		'strong_kids_b'=>[
			'B1'=>'Penyakit Hati Kronik',
			'B2'=>'Penyakit Ginjal Kronik',
			'B3'=>'TB Paru',
			'B4'=>'Luka Bakar Luas'
		],
		'strong_kids_c'=>[
			'C1'=>'Lain-lain (berdasarkan pertimbangan dokter )',
			'C2'=>'Kelainan anatomi daerah mulut yang menyebabkan kesulitan makan  (misalnya bibir sumbing )',
			'C3'=>'Trauma',
			'C4'=>'Kelainan metabolik bawaan'
		],
		'strong_kids_d'=>[
			'D1'=>'Retardasi mental',
			'D2'=>'Keterlambatan perkembangan',
			'D3'=>'Rencana/ pasca operasi mayor (misal laparotomi, torakotomi)',
			'D4'=>'Terpasang Stoma'
		]
	];
						
	public $_SKOR_SKRINING_ANAK	= [
        'strong_kids_a' => [
         1 => 0,
         2 => 1
        ],
        'strong_kids_b' => [
         1 => 0,
         2 => 1
        ],
        'strong_kids_c' => [
         1 => 0,
         2 => 1
        ],
		'strong_kids_d' => [
         1 => 0,
         2 => 1
        ]
    ];

    public $_SKRINING_STRONGKIDS = [
    	'strong_kids_a' => 'strong_kids_a',
    	'strong_kids_b' => 'strong_kids_b',
    	'strong_kids_c' => 'strong_kids_c',
    	'strong_kids_d' => 'strong_kids_d',
    	'strong_kids_total' => 'strong_kids_total'
    ];

    public $_SKRINING_MST = [
    	'skrining_mst_a' => 'skrining_mst_a',
    	'skrining_mst_b' => 'skrining_mst_b',
    	'skrining_mst_c' => 'skrining_mst_c'
    ];

    public $_SKRINING_MNA = [
    	'param_a' => 'param_a',
    	'param_b' => 'param_b',
    	'param_c' => 'param_c',
    	'param_d' => 'param_d',
    	'param_e' => 'param_e',
    	'param_f1' => 'param_f1',
    	'param_f2' => 'param_f2',
    	'skrining_mna_total' => 'skrining_mna_total'
    ];

    // unset data post pengkajian
    public $_UNSET_ASSESSMENT = [
    	'nilaiDek',
    	'skala_jatuh_dewasa_nilai',
    	'skala_jatuh_dewasa_ket',
    	'jumlah_skala_jatuh_dewasa',
    	'skala_jatuh_anak_nilai',
    	'skala_jatuh_anak_ket',
    	'jumlah_skala_jatuh_anak',
    	'nyeri_neonatus_a',
    	'nyeri_neonatus_b',
    	'nyeri_neonatus_a_total',
    	'nyeri_neonatus_b_total',
    	'param_a',
    	'param_b',
    	'param_c',
    	'param_d',
    	'param_e',
    	'param_f1',
    	'param_f2',
    	'skrining_mna_total',
    	'total_skor',
    	'strong_kids_total',
    	'strong_kids_a',
    	'strong_kids_b',
    	'strong_kids_c',
    	'strong_kids_d',
    	'skrining_mst_a',
    	'skrining_mst_b',
    	'skrining_mst_c',
    	'timer_pengkajian_poliklinik',
    	'url',
    	'no_reg',
    	'tanggal_periksa',
    	'norm_medic'
    ];

    public $_daily_reporting_assessment = [
    	'nyeri' => [
	    	'status_nyeri_poli_lokasi',
	    	'status_nyeri_poli_kapan_mulai',
	    	'status_nyeri_poli_penanganan',
	    	'status_nyeri_poli_tingkatan_nyeri',
	    	'nyeri_neonatus_a_total',
	    	'nyeri_neonatus_b_total',
    	],
    	'jatuh' => [
    		'jumlah_skala_jatuh_anak',
    		'jumlah_skala_jatuh_dewasa',
    	],
    	'gizi' => [
    		'total_skor',
    		'skrining_mna_total',
    		'strong_kids_total',
    	],
    	'budaya',
    	'masalah_sosial',
    	'spiritual_beribadah_dibantu',
    	'spiritual_konsul_rohaniawan',
    	'spiritual_fase_dying',
    	'spiritual_lain_lain',
    	'status_psikologi',
    	'alergi_obat',
    	'alergi_makanan',
    	'alergi_lain_lain',
    	'gelang_merah',
    	'jumlah_dekubitus'
    ];

    /*
	Pengkajian Mapping
    */
	public $_DES_INISIAL = [
		'IGD' => 'Instalasi Gawat Darurat',
		'POLI' => 'Poliklinik',
		'RANAP' => 'Rawatinap'
	];

	public $_assessment_mapping = [
		'poli' => [
			'riwayat_kesehatan_keluarga' => 'riwayat_kesehatan_keluarga',
			'status_psikologi' => 'status_psikologi',
			'status_mental' => 'status_mental',
			'masalah_sosial' => 'masalah_sosial',
			'budaya' => 'budaya',
			'pengkajian_keluarga' => 'pengkajian_keluarga',
			'alergi_obat' => 'riwayat_alergi',
			'alergi_makanan' => 'riwayat_alergi',
			'alergi_lain_lain' => 'riwayat_alergi',
			'jumlah_skala_jatuh_dewasa' => 'resiko_jatuh',
			'jumlah_skala_jatuh_anak' => 'resiko_jatuh',
			'status_nyeri_poli_lokasi' => 'status_nyeri',
			'status_nyeri_poli_kapan_mulai' => 'status_nyeri',
			'status_nyeri_poli_penanganan' => 'status_nyeri',
			'status_nyeri_poli_tingkatan_nyeri' => 'status_nyeri',
			'nyeri_neonatus_a_total' => 'status_nyeri',
			'nyeri_neonatus_b_total' => 'status_nyeri',
			'status_intoksikasi_makanan' => 'status_intoksikasi',
			'status_intoksikasi_obat' => 'status_intoksikasi',
			'status_intoksikasi_gas' => 'status_intoksikasi',
			'status_intoksikasi_binatang' => 'status_intoksikasi',
			'status_intoksikasi_kimia' => 'status_intoksikasi',
			'status_intoksikasi_lain_lain' => 'status_intoksikasi',
			'spiritual_beribadah_dibantu' => 'spiritual',
			'spiritual_konsul_rohaniawan' => 'spiritual',
			'spiritual_fase_dying' => 'spiritual',
			'spiritual_lain_lain' => 'spiritual',
			'kondisi_khusus_pasien_restrain' => 'kondisi_khusus_pasien',
			'kondisi_khusus_pasien_kegananasan' => 'kondisi_khusus_pasien',
			'kondisi_khusus_pasien_sida_hiv' => 'kondisi_khusus_pasien',
			'kondisi_khusus_pasien_kongenital' => 'kondisi_khusus_pasien',
			'kondisi_khusus_pasien_dnr' => 'kondisi_khusus_pasien',
			'kondisi_khusus_pasien_lain_lain' => 'kondisi_khusus_pasien',
			'tekanan_darah' => 'tekanan_darah',
			'nadi' => 'nadi',
			'pernafasan_fisik' => 'pernafasan_fisik',
			'suhu' => 'suhu',
			'berat_badan' => 'berat_badan',
			'tinggi_badan' => 'tinggi_badan',
			'lingkar_kepala' => 'lingkar_kepala',
			'lingkar_perut' => 'lingkar_perut',
			'termoregulasi_hipotiroid' => 'termoregulasi',
			'termoregulasi_hipertermi' => 'termoregulasi',
			'termoregulasi_menggigil' => 'termoregulasi',
			'termoregulasi_akral_dingin' => 'termoregulasi',
			'termoregulasi_pucat' => 'termoregulasi',
			'termoregulasi_akral_hangat' => 'termoregulasi',
			'termoregulasi_kulit_dingin' => 'termoregulasi',
			'termoregulasi_keringat_berlebih' => 'termoregulasi',
			'termoregulasi_sianosis' => 'termoregulasi',
			'termoregulasi_capilary_refill_melambat' => 'termoregulasi',
			'termoregulasi_lain_lain' => 'termoregulasi',
			'pernafasan_kelainan_bentuk_dada' => 'pernafasan',
			'pernafasan_pola_nafas' => 'pernafasan',
			'pernafasan_irama_nafas' => 'pernafasan',
			'pernafasan_pengembangan_paru' => 'pernafasan',
			'pernafasan_lain_lain' => 'pernafasan',
			'pernafasan_lain_lain1' => 'pernafasan',
			'kesadaran_persyarafan_gcs' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_e' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_m' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_v' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_kompos_mentis' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_kompos_tik' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_kompos_pupil' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_ukuran_pupil' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_muka_tidak_simetris' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_kesulitan_menelan' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_lidah' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_reflek' => 'kesadaran_persyarafan',
			'kesadaran_persyarafan_lain_lain' => 'kesadaran_persyarafan',
			'muskuloskeletal_kekuatan_otot' => 'muskuloskeletal',
			'muskuloskeletal_lokasi_kelemahan' => 'muskuloskeletal',
			'muskuloskeletal_perubahan_motorik' => 'muskuloskeletal',
			'muskuloskeletal_perubahan_sensorik' => 'muskuloskeletal',
			'muskuloskeletal_perubahan_bentuk' => 'muskuloskeletal',
			'muskuloskeletal_kelainan_kongenital' => 'muskuloskeletal',
			'muskuloskeletal_kesulitan_aktivitas' => 'muskuloskeletal',
			'muskuloskeletal_plegi' => 'muskuloskeletal',
			'muskuloskeletal_parese' => 'muskuloskeletal',
			'muskuloskeletal_tremor' => 'muskuloskeletal',
			'muskuloskeletal_fraktur' => 'muskuloskeletal',
			'muskuloskeletal_diskolasi' => 'muskuloskeletal',
			'muskuloskeletal_luksasio' => 'muskuloskeletal',
			'muskuloskeletal_krepitasi' => 'muskuloskeletal',
			'muskuloskeletal_luka' => 'muskuloskeletal',
			'muskuloskeletal_pengguna_alat_bantu' => 'muskuloskeletal',
			'muskuloskeletal_kesemutan' => 'muskuloskeletal',
			'muskuloskeletal_lain_lain' => 'muskuloskeletal',
			'kardiovaskuler_suara_jantung' => 'kardiovaskuler',
			'kardiovaskuler_suara_jantung_kelainan' => 'kardiovaskuler',
			'kardiovaskuler_irama_jantung' => 'kardiovaskuler',
			'kardiovaskuler_irama_jantung_kelainan' => 'kardiovaskuler',
			'kardiovaskuler_palpitasi' => 'kardiovaskuler',
			'kardiovaskuler_tachikard' => 'kardiovaskuler',
			'kardiovaskuler_bradikardi' => 'kardiovaskuler',
			'kardiovaskuler_alat_bantu_jantung' => 'kardiovaskuler',
			'kardiovaskuler_alat_bantu_jantung_lain' => 'kardiovaskuler',
			'kardiovaskuler_lain_lain' => 'kardiovaskuler'
		]
	];

	public $_PENGKAJIAN_MAPPING = [
		'IGD' => [
			'' => ''
		],
		'POLI' => [
			'keluhan_utama' => 'Keluhan Utama',
			'riwayat_kesehatan_keluarga' => 'Riwayat Kesehatan Keluarga',
			'berat_badan' => 'Riwayat Kesehatan Keluarga',
		],
		'RANAP' => [
			'' => ''
		]
	];

	public $data_empty = '<b>Tidak ada masalah.</b>';

	public $_filename = [
		'index.php',
		'addForm.php',
		'js.php'
	];

	/*
	DIklat
	*/

	public $_identity = [
		'tahun_anggaran' => 'Tahun Anggaran',
		'kode_spd' => 'Kode SPD',
		'akun_spd' => 'Akun SPD',
		'lembaga' => 'Kementerian/Lembaga',
		'unit_organisasi' => 'Unit Organisasi',
		'provinsi' => 'Propinsi', 
		'kabupaten' => 'Kabupaten/Kota', 
		'satun_kerja' => 'Satuan Kerja', 
		'unit_kerja' => 'Unit Kerja', 
		'alamat' => 'Alamat', 
		'kppn' => 'Kppn', 
		'jenis_dokument_anggaran' => 'Jenis Dokumen Anggaran', 
		'nomor_jenis_dokument' => 'Nomor Dokumen', 
		'tanggal_dipa' => 'Tanggal/Bulan/Tahun', 
		'kop_spd' => 'Referensi Peraturan (KOP SPD)'
	];

	public $_idtype_data = [
		'tahun_anggaran' => 'date',
		'kode_spd' => 'text',
		'akun_spd' => 'text',
		'lembaga' => 'text',
		'unit_organisasi' => 'text',
		'provinsi' => 'text', 
		'kabupaten' => 'text', 
		'satun_kerja' => 'text', 
		'unit_kerja' => 'text', 
		'alamat' => 'text', 
		'kppn' => 'text', 
		'jenis_dokument_anggaran' => 'text', 
		'nomor_jenis_dokument' => 'text', 
		'tanggal_dipa' => 'date', 
		'kop_spd' => 'text'
	];

	public $_jenis_dokument_anggaran = [
		'DIPA' => 'DIPA',
		'SKPA' => 'SKPA',
		'LAINNYA' => 'LAINNYA'
	];

	/*
	BPJS
	*/
	public $_kodeppk = '1027R007';

	public $_bpjs = [
		'kodeppk' => '1027R007',
		'katarak' => [
			'0' => '0. Tidak',
			'1' => '1. Ya'
		],
		'cob' => [
			'0' => '0. Tidak',
			'1' => '1. Ya'
		],
		'asal_rujukan' => [
			'1' => '1. Faskes 1',
			'2' => '2. Faskes 2(RS)'
		],
		'laka_lantas' => [
			'0' => '0. Tidak',
			'1' => '1. Ya'
		],
		'jnspelayanan' => [
			'1' => '1. r.inap',
			'2' => '2. r.jalan'
		],
		'klsrawat' => [
			"1" => 'Kelas 1',
			"2" => 'Kelas 2',
			"3" => 'Kelas 3'
		],
		'eksekutif' => [
			'0' => '0. Tidak',
			'1' => '1. Ya'
		],
		'penjamin_lakalantas' => [
			"1" => "Jasa raharja PT",
			"2" => "BPJS Ketenagakerjaan",
			"3" => "TASPEN PT",
			"4" => "ASABRI PT"
		],
		'suplesi' => [
			'0' => '0. Tidak',
			'1' => '1. Ya'
		]
	];

}