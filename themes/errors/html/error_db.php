<?php include_once('head.php'); ?>
<body class="theme-cyan">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle auth-main">
				<div class="auth-box">
                    <div class="top">
                        <!-- <img src="../assets/images/logo-white.svg" alt="Lucid"> -->
                    </div>
					<div class="card">
                        <div class="header">
                            <h3>
                                <span class="clearfix title">
                                    <span class="number left"><?php echo $heading;?></span> <span class="text">Oops!</span>
                                </span>
                            </h3>
                        </div>
                        <div class="body">
                            <p><?php echo $message; ?> <a href="javascript:void(0);">contact us</a> to report this issue.</p>
                            <div class="margin-top-30">
                                <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> <span>Go Back</span></a>
                                <a href="index.html" class="btn btn-primary"><i class="fa fa-home"></i> <span>Home</span></a>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>
</html>