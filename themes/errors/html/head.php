<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?php echo config_item('base_url');?>vendor/lucida/assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>vendor/lucida/assets/vendor/font-awesome/css/font-awesome.min.css">

<!-- MAIN CSS -->
<link rel="stylesheet" href="<?php echo config_item('base_url');?>vendor/lucida/assets/css/main.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>vendor/lucida/assets/css/color_skins.css">
</head>