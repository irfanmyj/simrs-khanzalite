<?php global $Cf; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo isset($title) ? $title : ''; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('vendor/adminLte/');?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('vendor/adminLte/');?>bower_components/font-awesome/css/font-awesome.min.css">


  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
  @font-face {
    font-family: myFirstFont;
    src: url('<?php echo base_url('uploads/ufonts.com_square721-bt-roman.ttf');?>');
  }

  p, u, td{
    font-family: myFirstFont;
  }

  @media print
  {
    table { page-break-after:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    td    { page-break-inside:avoid; page-break-after:auto }
    th    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tbody { display:table-footer-group }
    tfoot { display:table-footer-group }
    pre, blockquote {page-break-inside: avoid;}
  }  
</style>
</head>
<body > <!-- onload="window.print();" -->
<div class="wrapper">
  <!-- Main content -->
  <?php  
  $this->load->view('header_report');
  ?>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered ">
        <tr>
          <td colspan="3" class="text-center" style="background: #337ab7; color: #ffffff;"><b>DATA PENGKAJIAN POLIKLINIK</b></td>
        </tr>
        <tr>
          <td width="250"><b>Keluhan Utama</b></td>
          <td width="10">:</td>
          <td><?php print_data($r[0]->keluhan_utama); ?></td>
        </tr>
        <tr>
          <td><b>Riwayat Kesehatan Keluarga</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->riwayat_kesehatan_keluarga); ?></td>
        </tr>
        <tr>
          <td><b>Status Psikologi</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->status_psikologi); ?></td>
        </tr>
        <tr>
          <td><b>Status Mental</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->status_mental); ?></td>
        </tr>
        <tr>
          <td><b>Masalah Sosial</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->masalah_sosial); ?></td>
        </tr>
        <tr>
          <td><b>Budaya</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->budaya); ?></td>
        </tr>
        <tr>
          <td><b>Pengkajian Keluarga</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->pengkajian_keluarga); ?></td>
        </tr>
        <tr>
          <td colspan="3"><b>Status Kesehatan Masa Lalu</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <table class="table table-bordered">
              <tr>
                <td width="250"><b>Kapan</b></td>
                <td width="10">:</td>
                <td><?php print_data($r[0]->stsml_kapan); ?></td>
              </tr>
              <tr>
                <td><b>Dimana</b></td>
                <td width="10">:</td>
                <td><?php print_data($r[0]->stsml_dimana); ?></td>
              </tr>
              <tr>
                <td><b>Diagnosis</b></td>
                <td width="10">:</td>
                <td><?php print_data($r[0]->stsml_diagnosa); ?></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="3"><b>Riwayat Alergi</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <table class="table table-bordered">
              <tr>
                <td width="250"><b>Alergi Obat</b></td>
                <td width="10">:</td>
                <td><?php print_data($r[0]->alergi_obat); ?></td>
              </tr>
              <tr>
                <td><b>Alergi Makanan</b></td>
                <td width="10">:</td>
                <td><?php print_data($r[0]->alergi_makanan); ?></td>
              </tr>
              <tr>
                <td><b>Alergi Lain-lain</b></td>
                <td width="10">:</td>
                <td><?php print_data($r[0]->stsml_diagnosa); ?></td>
              </tr>
              <tr>
                <td><b>Gelang Merah</b></td>
                <td width="10">:</td>
                <td><?php print_data($r[0]->stsml_diagnosa); ?></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="3"><b>Resiko Cedera/Jatuh</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment1/resiko_cedera_jatuh');
            ?>
          </td>
        </tr>
        <tr>
          <td><b>Resiko Dekubitus Pasien</b></td>
          <td width="10">:</td>
          <td>
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment1/resiko_dekubitus');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3"><b>Status Nyeri</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment1/status_nyeri');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3" ><b>Status Intoksikasi</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment1/status_intoksikasi');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3" ><b>Spiritual</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment1/spiritual');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3" ><b>Kondisi Khusus Pasien</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment1/kondisi_khusus_pasien');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3" ><b>Gizi</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment1/gizi');
            ?>
          </td>
        </tr>
        <tr>
          <td width="250"><b>Tekanan Darah</b></td>
          <td width="10">:</td>
          <td><?php print_data($r[0]->tekanan_darah); ?></td>
        </tr>
        <tr>
          <td><b>Nadi</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->nadi); ?></td>
        </tr>
        <tr>
          <td><b>Pernafasan</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->pernafasan_fisik); ?></td>
        </tr>
        <tr>
          <td><b>Suhu</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->suhu); ?></td>
        </tr>
        <tr>
          <td><b>Berat Badan</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->berat_badan); ?></td>
        </tr>
        <tr>
          <td><b>Tinggi Badan</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->tinggi_badan); ?></td>
        </tr><tr>
          <td><b>Golongan Darah</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->golongan_darah); ?></td>
        </tr><tr>
          <td><b>Lingkar Kepala</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->lingkar_kepala); ?></td>
        </tr><tr>
          <td><b>lingkar Perut</b></td>
          <td>:</td>
          <td><?php print_data($r[0]->lingkar_perut); ?></td>
        </tr>
        <tr>
          <td colspan="3"><b>Termoregulasi</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment2/termoregulasi');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3"><b>Pernafasan</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment2/pernafasan');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3"><b>Kesadaran/Persyarafan</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment2/kesadaran_persyarafan');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3"><b>Muskuloskeletal</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment2/muskuloskeletal');
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="3"><b>Kardiovaskuler</b></td>
        </tr>
        <tr>
          <td colspan="3" style="margin: 0px; padding: 0px;">
            <?php  
            $this->load->view('adminLte/Nursingpoli/Assessment/view/assessment2/kardiovaskuler');
            ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
