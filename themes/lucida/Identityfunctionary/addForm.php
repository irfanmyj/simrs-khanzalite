<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Username</label>
              <?php  
              echo htmlSelectFromArray($user, 'name="id_user" id="id_user" class="form-control select2" style="width:100%;"', true);
              ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Kategori Group</label>
              <?php  
              echo htmlSelectFromArray(['bpp'=>'Bpp','ppk'=>'Ppk','pejabat_tandatangan'=>'Pejabat Tanda Tangan'], 'name="category" id="category" class="form-control select2" style="width:100%;"', true);
              ?>
            </div>
          </div>    
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->