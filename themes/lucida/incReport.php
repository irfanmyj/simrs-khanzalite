<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
	$directory 	= !empty($folder) ? $folder : 'coreTheme';
	$files 		= !empty($file) ? $file : 'content';

	// Memanggil file inc.head.html yang berada di folder inc 
	$this->site->get_template('coreTheme/inc.head.report.html');

	$this->site->get_template($directory .'/'. $files);

	$this->site->get_template('coreTheme/inc.footer.end.html');
	
