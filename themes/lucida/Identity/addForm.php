<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xxl fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="row">
          <div class="col-md-12">
            <table class="table table-hover table-striped">
              <thead class="l-blush text-white">
                <tr>
                  <th style="width:10px;">No</th>
                  <th style="width: 150px;">Parameter</th>
                  <th style="width: 5px;"></th>
                  <th>Nilai</th>
                </tr>
              </thead>
                <tbody>
                  <?php $no=1; foreach ($identity as $k => $v) { ?>
                  <tr>
                    <td><?php echo ($no++); ?></td>
                    <td><?php echo $v; ?></td>
                    <td>:</td>
                    <td><input type="<?php echo $idtype_data[$k]; ?>" id="<?php echo $k; ?>" name="<?php echo $k; ?>" class="form-control"></td>
                  </tr>                  
                  <?php } ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->