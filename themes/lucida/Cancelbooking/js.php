<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		$('#kd_dokter').select2();
		$('#kd_poli').select2();
		
		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
			$('#modalAddForm :input').val('');
            $('#modalAddForm [name="id"]').val(tr.data('id'));
            $('#modalAddForm [name="kd_dokter"]').val(tr.data('kd_dokter'));
            $('#modalAddForm [name="kd_dokter"]').select2({width:'100%'}).trigger('change');
            $('#modalAddForm [name="tgl_periksa"]').val(tr.data('tgl_periksa'));
            $('#modalAddForm [name="judul"]').val(tr.data('judul'));
            $('#modalAddForm [name="keterangan"]').val(tr.data('keterangan'));

            if(tr.data('kd_dokter')){
            	$.ajax({
		            type : 'post',
		            url : '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>/get_poli',
		            data : 'kd_dokter='+tr.data('kd_dokter'),
		            success : function(res)
		            { 
						$('#select2_kdpoli').html(res);
						$('#modalAddForm [name="kd_poli"]').val(tr.data('kd_poli'));
						$('#modalAddForm [name="kd_poli"]').select2({width:'100%'}).trigger('change');

						$.ajax({
					        type : 'post',
					        url : '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>/get_pasien_booking',
					        data : 'kd_poli='+tr.data('kd_poli')+'&kd_dokter='+tr.data('kd_dokter'),
					        success : function(res)
					        {
					          	$('#pilih_tanggal_periksa').html(res);
					        }
					    });
		            }
	          	});
            }
            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

	});

	//
	function formAdd()
	{
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		$('#modalAddForm :input').val('');
        
        // Ambil data poli berdasarkan kode dokter
		$('#kd_dokter').change(function(){
			$.ajax({
	            type : 'post',
	            url : '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>/get_poli',
	            data : 'kd_dokter='+$(this).val(),
	            success : function(res)
	            { 
	              $('#select2_kdpoli').html(res);
	              $('#kd_poli').select2();
	            }
	          });
		});

		$('#select2_kdpoli').on('change','#kd_poli',function(){
			var kd_dokter = $('#kd_dokter').val();
			$.ajax({
		        type : 'post',
		        url : '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>/get_pasien_booking',
		        data : 'kd_poli='+$(this).val()+'&kd_dokter='+kd_dokter,
		        success : function(res)
		        {
		          	$('#pilih_tanggal_periksa').html(res);
		        }
		    });
		});

        $('#modalAddForm').modal();
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }
</script>