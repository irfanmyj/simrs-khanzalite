<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id">
        <input type="hidden" name="tgl_periksa">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Nama Dokter</label>
              <?php  
              echo htmlSelectFromArray($dokter, 'name="kd_dokter" id="kd_dokter" style="width:100%;" class="form-control"', true);
              ?>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Nama Poliklinik</label>
              <div id="select2_kdpoli">
                <?php  
                echo htmlSelectFromArray([], 'name="kd_poli" id="kd_poli" style="width:100%;" class="form-control"', true);
                ?>
              </div>
            </div>
          </div>

          <div class="col-md-12" id="pilih_tanggal_periksa"></div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Tanggal Awal Cuti</label>
              <input type="date" name="tgl_awal" value="<?php echo date('Y-m-d'); ?>" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Tanggal Akhir Cuti</label>
              <input type="date" name="tgl_akhir" value="<?php echo date('Y-m-d'); ?>" class="form-control">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Judul Pesan</label>
              <input type="text" name="judul" id="judul" class="form-control"> 
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Keterangan</label>
              <textarea class="form-control" id="keterangan" rows="4" name="keterangan"></textarea> 
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->