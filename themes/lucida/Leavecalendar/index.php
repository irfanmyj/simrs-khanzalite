  <div id="main-content">
    <div class="container-fluid">
      <!-- Block Header -->
      <?php $this->site->get_template('coreTheme/block-header.php'); ?>
      <!-- End Block Header -->

      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="card">
            <div class="header">
              <h2><?php echo isset($title) ? $title : ''; ?></h2>
            </div>

            <div class="body">
              <?php 
                echo calrt();
                $this->session->unset_userdata('msg');
                $this->session->unset_userdata('error');
                if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y') {
                  $addClass = 'tr_mod';
                }
              ?>

              <?php $this->site->get_template('coreTheme/btnAkses'); ?>

              <table class="table table-hover m-b-0 c_list" style="margin-top: 5px;">
                <thead class="l-blush text-white">
                  <tr>
                    <th style="width: 1rem;" class="text-center">#</th>
                    <th style="width: 30px;" class="text-center">No</th>
                    <th>Nama Dokter</th>
                    <th>Tanggal Awal</th>
                    <th>Tanggal AKhir</th>
                    <th>Status</th>
                    <th style="width: 200px;">Dibuat Tanggal</th>
                    <th style="width: 100px;">Aksi</th>
                  </tr>
                </thead>  
                <tbody>
                <?php $no=1; foreach ($data as $key => $val) { ?>
                  <tr style="cursor:pointer" <?php echo showValRec($val);?>>
                    <td class="text-center chk"><input type="checkbox" name="id[]" value="<?php echo $val->id;?>"></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_dokter;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->tgl_awal;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->tgl_akhir;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>">
                      <?php if($val->id_bc_info_cuti){ ?>
                        <span class="text-info">Pembatalan</span>
                      <?php }else{ ?>
                        <span class="text-info">Cuti</span>
                      <?php } ?>
                    </td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->created_at;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>">
                      <?php if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y'){ ?>
                        <button type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                      <?php } ?>
                    </td>
                  </tr>
                <?php } ?>
                <?php 
                if (count($data) == 0) {
                    echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
                }
                ?>
                </tbody>          
              </table>

              <div class="row">
                <div class="col-md-7 pull-left">
                  <div class="row">
                    <div class="col-md-2">
                      <?php echo select($limit_rows, 'name="limit" style="width:100px;" class="form-control"', true, $limit); ?>
                    </div>
                    <div class="col-md-10">
                      <div style="margin-top: 7px; margin-left: 5px;">
                        Memunculkan halaman <?php echo $offset; ?> sampai <?php echo $limit; ?> dari <?php echo $res_count; ?> data. 
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="col-md-5">
                  <?php echo $pagination;?>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/addForm'); ?>
    </div>
  </div>
