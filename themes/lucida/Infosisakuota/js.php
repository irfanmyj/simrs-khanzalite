<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		$('#room_poli').select2();

		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var id_room = $('#modalAddForm [name="id_room[]"]').map(function(){
				return $(this).val();
			}).get();
			
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url+'&id_room='+id_room,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
			$('#modalAddForm :input').val('');
            $('#modalAddForm [name="id"]').val(tr.data('id_user'));
            $('#modalAddForm [name="username"]').val(tr.data('username'));
            $('#modalAddForm [name="username"]').attr('readonly',true);
            $('#modalAddForm [name="is_active"]').val(tr.data('is_active'));
            $('#modalAddForm [name="level_access"]').val(tr.data('level_access'));
            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		$( '#menu-sidebar a' ).on( 'click', function () {
			//$( '#menu-sidebar .sidebar-menu' ).find( 'li.active' ).removeClass( 'active' );
			$( '#menu-sidebar .sidebar-menu' ).find( 'li.active' ).removeClass( 'active' );
			$( this ).parent( 'ul' ).parent( 'li.treeview' ).addClass( 'active' );
			$( this ).parent( 'li' ).addClass( 'active' );
		});

		$('#level_access').change(function(){
			var id_level = $(this).val();
			if(id_level==4 || id_level==6){
				$('#poli_show').css('display','block');
			}else{
				$('#poli_show').css('display','none');
			}
		});

	});

	//
	function formAdd()
	{
		var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		$('#modalAddForm :input').val('');
		notSpace('modalAddForm','username');
		notSpace('modalAddForm','password');
		$('#modalAddForm [name="username"]').change(function(){
			checking_username(url,$(this).val());
		});
        $('#modalAddForm').modal();
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }

    function checking_username(url,username)
    {
    	$.ajax({
    		type : 'post',
    		url : url+'/checking_username',
    		data : 'username='+username,
    		success : function(res)
    		{
    			var dt = JSON.parse(res);
    			if(dt.error == 'Ada')
    			{
    				$('#modalAddForm #msg-username').html(dt.msg);
    				$('#modalAddForm #msg-username').css('color','red');
    				$('#modalAddForm [name="username"]').css('background','red');
    				$('#modalAddForm [name="username"]').css('color','white');
    			}
    			else
    			{
    				$('#modalAddForm #msg-username').html('');
    				$('#modalAddForm #msg-username').removeAttr('style');
    				$('#modalAddForm [name="username"]').removeAttr('style');
    			}
    		}
    	});
    }
</script>
