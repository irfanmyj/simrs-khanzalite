<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm">
  <div class="modal-dialog modal-lg fade-left">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Nama Jabatan</label>
              <input type="text" name="name" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Parent Jabatan</label>
              <?php echo htmlSelectFromArray($position, 'name="parent_id" id="parent_id" class="form-control"', true);?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn l-blush text-white pull-right" id="save">Save changes</button>
        <button type="button" class="btn l-blush text-white pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->