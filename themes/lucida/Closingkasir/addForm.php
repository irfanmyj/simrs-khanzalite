<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label>Shift</label>
              <?php  
              echo htmlSelectFromArray(['Pagi'=>'Pagi','Siang'=>'Siang','Sore'=>'Sore','Malam'=>'Malam'], 'name="shift" id="shift" style="width:100%;" class="form-control"', true);
              ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Jam Masuk</label>
              <input type="time" name="jam_masuk" value="<?php echo date('H:i:s'); ?>" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Jam Pulang</label>
             <input type="time" name="jam_pulang" value="<?php echo date('H:i:s'); ?>" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->