<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Kode Rek</label>
              <input type="text" name="kd_rek" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Nama Rek</label>
              <input type="text" name="nm_rek" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Tipe</label>
              <?php  
              echo htmlSelectFromArray(['N'=>'N','M'=>'M','R'=>'R'], 'name="tipe" id="tipe" class="form-control"', true);
              ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Balance</label>
              <?php  
              echo htmlSelectFromArray(['D'=>'D','K'=>'K'], 'name="balance" id="balance" class="form-control"', true);
              ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Level</label>
              <?php  
              echo htmlSelectFromArray([0,1], 'name="level" id="level" class="form-control"', true);
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->