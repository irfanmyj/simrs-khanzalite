  <?php global $Cf; ?>
    <style type="text/css">
    
    .ui-autocomplete-input {
      border: none; 
      border: 1px solid #DDD !important;
      z-index: 1511;
      position: relative;
    }
    .ui-menu .ui-menu-item a {
      font-size: 12px;
    }
    .ui-autocomplete {
      position: absolute;
      top: 0;
      left: 0;
      z-index: 1510 !important;
      float: left;
      display: none;
      padding: 4px 4px;
      margin: 2px 0 0 0;
      list-style: none;
      background-color: #ffffff;
      border-color: #ccc;
      border-color: rgba(0, 0, 0, 0.2);
      border-style: solid;
      border-width: 1px;
      -webkit-border-radius: 2px;
      -moz-border-radius: 2px;
      border-radius: 2px;
      -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
      -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
      box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
      -webkit-background-clip: padding-box;
      -moz-background-clip: padding;
      background-clip: padding-box;
      *border-right-width: 2px;
      *border-bottom-width: 2px;
    }
    .ui-menu-item > a.ui-corner-all {
      display: block;
      padding: 3px 15px;
      clear: both;
      font-weight: normal;
      line-height: 18px;
      color: #555555;
      white-space: nowrap;
      text-decoration: none;
    }
    .ui-state-hover, .ui-state-active {
      color: #000000;
      text-decoration: none;
      border-radius: 0px;
      -webkit-border-radius: 0px;
      -moz-border-radius: 0px;
      background-image: none;
    }

    #modalAddForm ::-webkit-input-placeholder { /* Edge */
      color: white;
    }
  </style>
  <div id="main-content">
    <div class="container-fluid">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-6 col-md-8 col-sm-12">
              <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo isset($title) ? $title : ''; ?></h2>
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item active"><?php echo $this->uri->segment(1); ?></li>
              </ul>
          </div>
        </div>
      </div>

      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="card">
            <div class="header">
              <h2><?php echo isset($title) ? $title : ''; ?></h2>
            </div>

            <div class="body">
              <?php 
                echo calrt();
                $this->session->unset_userdata('msg');
                $this->session->unset_userdata('error');
                if(@acl($Cf->decrypt_aes_256_cbc($this->uri->segment(2)))['update']=='Y') {
                  $addClass = 'tr_mod';
                }
              ?>

              <?php $this->site->get_template('coreTheme/btnAkses'); ?>

              <div class="row clearfix">
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-hover m-b-0 c_list" style="margin-top: 5px;">
                      <thead class="l-blush text-white">
                        <tr>
                          <th style="width: 1rem;" class="text-center">#</th>
                          <th style="width: 30px;" class="text-center">No</th>
                          <th>Provinsi</th>
                          <th>Kabupaten</th>
                          <th>Kecamatan</th>
                          <th>kelurahan</th>
                          <th style="width: 50px;">Aksi</th>
                        </tr>
                      </thead>  
                      <tbody>
                      <?php $no=1; foreach ($data as $key => $val) { ?>
                        <tr style="cursor:pointer" <?php echo showValRec($val);?>>
                          <td class="text-center chk"><input type="checkbox" name="id[]" value="<?php echo $val->kd_kel;?>"></td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_provinsi;?></td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_kabupaten;?></td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_kecamatan;?></td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nama;?></td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>">
                            <?php if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y'){ ?>
                              <span class="text btn l-blush text-white"><i class="fa fa-edit"></i> Edit</span>
                            <?php } ?>
                          </td>
                        </tr>
                      <?php } ?>
                      <?php 
                      if (count($data) == 0) {
                          echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
                      }
                      ?>
                      </tbody>          
                    </table>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-7 pull-left">
                  <div class="row">
                    <div class="col-md-2">
                      <?php echo select($limit_rows, 'name="limit" style="width:100px;" class="form-control"', true, $limit); ?>
                    </div>
                    <div class="col-md-10">
                      <div style="margin-top: 7px; margin-left: 5px;">
                        Memunculkan halaman <?php echo $offset; ?> sampai <?php echo $limit; ?> dari <?php echo $res_count; ?> data. 
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="col-md-5">
                  <?php echo $pagination;?>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/addForm'); ?>
    </div>
  </div>
