<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm">
  <div class="modal-dialog modal-lg fade-left">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id">
        <div class="form-group">
    		  <label>Nama Profesi</label>
    		  <input type="text" name="name" class="form-control" placeholder="Masukan disini.">
    		</div>

        <div class="form-group">
          <label>Kategori Profesi</label>
          <?php echo select($category,'name="id_category_professional" class="form-control"',TRUE); ?>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->