  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo isset($title) ? $title : ''; ?>
        <small>Selamat berkativtas, jangan lupa berdoa. <span style="font-size: 25px;">&#9786;</span></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard/1'); ?>"><i class="fa fa-dashboard"></i> Halaman Utama</a></li>
        <li><a href="#">Rawat Inap</a></li>
        <li class="active">Kaji Pasien</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php 
        echo calrt();
        $this->session->unset_userdata('msg');
        $this->session->unset_userdata('error');
        if(@acl($this->uri->segment(2))['update']=='Y') {
          $addClass = 'tr_mod';
        }
      ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo isset($title) ? $title : ''; ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <?php $this->site->get_template('coreTheme/btnAkses'); ?>
          
          <table class="table table-bordered table-hover" style="margin-top: 5px;">
            <thead>
              <tr>
                <th style="width: 1rem;" class="text-center">#</th>
                <th style="width: 30px;" class="text-center">No</th>
                <th>Kategori</th>
                <th>Nama</th>
                <th style="width: 200px;">Dibuat Tanggal</th>
              </tr>
            </thead>  
            <tbody>
            <?php $no=1; foreach ($data as $key => $val) { ?>
              <tr style="cursor:pointer" <?php echo showValRec($val);?>>
                <td class="text-center chk"><input type="checkbox" name="id[]" value="<?php echo $val->id;?>"></td>
                <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
                <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->category;?></td>
                <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->name;?></td>
                <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->created_at;?></td>
              </tr>
            <?php } ?>
            <?php 
            if (count($data) == 0) {
                echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
            }
            ?>
            </tbody>          
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <div class="row">
            <div class="col-md-7 pull-left">
              <div class="row">
                <div class="col-md-2">
                  <?php echo select($limit_rows, 'name="limit" style="width:100px;" class="form-control"', true, $limit); ?>
                </div>
                <div class="col-md-10">
                  <div style="margin-top: 7px; margin-left: 5px;">
                    Memunculkan halaman <?php echo $offset; ?> sampai <?php echo $limit; ?> dari <?php echo $res_count; ?> data. 
                  </div>
                </div>
              </div> 
            </div>
            <div class="col-md-5">
              <?php echo $pagination;?>
            </div>
          </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>

    <!-- Modal -->
    <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/addForm'); ?>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
