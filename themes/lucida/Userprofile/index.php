<?php  
if(@acl($this->uri->segment(2))['update']=='Y') {
  $editSchool = '&nbsp;<a id="formEditSchool"><i class="fa fa-pencil"></i> Edit</a>';
  $editAddress = '&nbsp;<a id="formEditAddress"><i class="fa fa-pencil"></i> Edit</a>';
  $editSkill = '&nbsp;<a id="formEditSkill"><i class="fa fa-pencil"></i> Edit</a>';
  $editNote = '&nbsp;<a id="formEditNote"><i class="fa fa-pencil"></i> Edit</a>';
}

if(isset($data[0]->upload_image))
{
  $image = base_url('uploads/'.$this->session->userdata('username').'/img_profile/'.$data[0]->upload_image);
}else
{
  $image = base_url('vendor/adminLte/dist/img/avatar5.png');
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ($title) ? $title : ''; ?>
        <small>Selamat berkativitas, jangan lupa berdoa. <span style="font-size: 25px;">&#9786;</span></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php 
        echo calrt();
        $this->session->unset_userdata('msg');
        $this->session->unset_userdata('error');
      ?>
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo $image; ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo isset($data[0]->full_name) ? $data[0]->full_name : ''; ?></h3>

              <p class="text-muted text-center"><?php echo $this->session->userdata('name_levelaccess'); ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Tanggal Lahir</b> <a class="pull-right"><?php echo isset($data[0]->birthday) ? date('d M Y',strtotime($data[0]->birthday)) : ''; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Tempat Lahir</b> <a class="pull-right"><?php echo isset($data[0]->place_of_birth) ? $data[0]->place_of_birth : ''; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Jabatan</b> <a class="pull-right"><?php echo isset($data[0]->id_position) ? $position[$data[0]->id_position] : ''; ?></a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tentang Saya</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Pendidikan <?php echo $editSchool; ?></strong>

              <p class="text-muted">
              <?php echo isset($school) ? $school : 'Data tidak tersedia.'; ?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat <?php echo $editAddress; ?></strong>

              <p class="text-muted">
                <?php echo isset($data[0]->state) ? ucwords($data[0]->state) : 'Data tidak tersedia.'; ?>,
                <?php echo isset($data[0]->provinces) ? ucwords($data[0]->provinces) : 'Data tidak tersedia.'; ?>,
                <?php echo isset($data[0]->regencies) ? ucwords($data[0]->regencies) : 'Data tidak tersedia.'; ?>,
                <?php echo isset($data[0]->districts) ? ucwords($data[0]->districts) : 'Data tidak tersedia.'; ?>,
                <?php echo isset($data[0]->villages) ? ucwords($data[0]->villages) : 'Data tidak tersedia.'; ?>,
                <?php echo isset($data[0]->address) ? ucwords($data[0]->address) : 'Data tidak tersedia.'; ?>
              </p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Keahlian <?php echo $editSkill; ?></strong>

              <p>
                <?php echo isset($skill) ? $skill : 'Data tidak tersedia.'; ?>
                <!-- <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span> -->
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Catatan <?php echo $editNote; ?></strong>

              <p>
                <?php echo isset($note) ? $note : 'Data tidak tersedia.'; ?>
              </p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <?php 
              $this->site->get_template(ucwords($this->uri->segment(1)).'/nav-tabs'); 
            ?>           
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/settingForm'); ?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

    <!-- Modal -->
    <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/editFormAddress'); ?>
    <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/editFormSchool'); ?>

  </div>
  <!-- /.content-wrapper -->