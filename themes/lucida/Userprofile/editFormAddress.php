<!-- Modal Add Form -->
<div class="modal fade" id="modalEditAddress">
  <div class="modal-dialog modal-lg fade-left">
    <div class="modal-content">
    <form method="post" action="<?php echo $url.'/1/10/update'; ?>" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id" value="<?php echo isset($data[0]->id_detail_user) ? $data[0]->id_detail_user : ''; ?>">
        <div class="form-group">
    		  <label for="exampleInputEmail1">Warga Negara</label>
          <?php echo htmlSelectFromArray($state,'name="state" class="form-control select2" style="width:100%;" id="state"',TRUE,isset($data[0]->idstate) ? $data[0]->idstate : ''); ?>
    		</div>
        <div class="form-group">
          <label for="exampleInputEmail1">Provinsi</label>
          <div id="get_provinces">
            <?php echo htmlSelectFromArray(array(),'name="province" class="form-control select2" style="width:100%;" id="province"',TRUE); ?>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Kabupaten</label>
          <div id="get_regencies">
            <?php echo htmlSelectFromArray(array(),'name="regencies" class="form-control select2" style="width:100%;" id="regencies"',TRUE); ?>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Kecamatan</label>
          <div id="get_districts">
            <?php echo htmlSelectFromArray(array(),'name="districts" class="form-control select2" style="width:100%;" id="districts"',TRUE); ?>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Kelurahan/Desa</label>
          <div id="get_villages">
            <?php echo htmlSelectFromArray(array(),'name="villages" class="form-control select2" style="width:100%;" id="villages"',TRUE); ?>
          </div>
        </div>
        <div class="form-group">
          <label>Alamat Rumah</label>
          <textarea class="form-control" name="address" id="address"></textarea> 
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->