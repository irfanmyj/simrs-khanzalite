<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		
		$('#searchProfesi').select2();

		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
			$('#modalAddForm :input').val('');
            $('#modalAddForm [name="id"]').val(tr.data('id'));
            $('#modalAddForm [name="name"]').val(tr.data('name'));
            $('#modalAddForm [name="id_category_allergy"]').val(tr.data('id_category_allergy'));
            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit School
		$('#formEditAddress').click(function(){
			$('#modalEditAddress').modal();
		});

		$('#state').change(function(){
			var state = $(this).val();
			$.ajax({
				type : 'post',
				data : 'id_state='+state,
				url : '<?php echo base_url('provinces/24/search=/1/10/get_provinces');?>',
				success : function(res)
				{
					$('#get_provinces').html(res);
					$('.province').select2();
					$('#province').change(function(){
						var province = $(this).val();
						$.ajax({
							type : 'post',
							data : 'province_id='+province,
							url : '<?php echo base_url('regencies/25/search=/1/10/get_regencies');?>',
							success : function(res)
							{
								$('#get_regencies').html(res);
								$('.regencies').select2();
								$('#regencies').change(function(){
									var regencies = $(this).val();
									$.ajax({
										type : 'post',
										data : 'regency_id='+regencies,
										url : '<?php echo base_url('districts/26/search=/1/10/get_districts');?>',
										success : function(res)
										{
											$('#get_districts').html(res);
											$('.districts').select2();
											$('#districts').change(function(){
												var districts = $(this).val();
												$.ajax({
													type : 'post',
													data : 'district_id='+districts,
													url : '<?php echo base_url('villages/27/search=/1/10/get_villages');?>',
													success : function(res)
													{
														$('#get_villages').html(res);
														$('.villages').select2();
													}
												});
											});
										}
									});
								});
							}
						});
					});
				}
			});
		});

		if($('#state').val())
		{
			var state = $('#state').val();
				$.ajax({
					type : 'post',
					data : 'id_state='+state,
					url : '<?php echo base_url('provinces/24/search=/1/10/get_provinces');?>',
					success : function(res)
					{
						$('#get_provinces').html(res);
						$('.province').select2();
						$('#province').change(function(){
							var province = $(this).val();
							$.ajax({
								type : 'post',
								data : 'province_id='+province,
								url : '<?php echo base_url('regencies/25/search=/1/10/get_regencies');?>',
								success : function(res)
								{
									$('#get_regencies').html(res);
									$('.regencies').select2();
									$('#regencies').change(function(){
										var regencies = $(this).val();
										$.ajax({
											type : 'post',
											data : 'regency_id='+regencies,
											url : '<?php echo base_url('districts/26/search=/1/10/get_districts');?>',
											success : function(res)
											{
												$('#get_districts').html(res);
												$('.districts').select2();
												$('#districts').change(function(){
													var districts = $(this).val();
													$.ajax({
														type : 'post',
														data : 'district_id='+districts,
														url : '<?php echo base_url('villages/27/search=/1/10/get_villages');?>',
														success : function(res)
														{
															$('#get_villages').html(res);
															$('.villages').select2();
														}
													});
												});
											}
										});
									});
								}
							});
						});
					}
				});
		}

	});

	//
	function formAdd()
	{
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		$('#modalAddForm :input').val('');
        //$('.sm-war-kosong').hide();
        $('#modalAddForm').modal();
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }
</script>