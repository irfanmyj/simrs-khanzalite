<form class="form-horizontal" method="post" action="<?php echo $url.'/1/10/update'; ?>" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?php echo isset($data[0]->id_detail_user) ? $data[0]->id_detail_user : ''; ?>">
  <div class="form-group">
    <label for="inputName" class="col-sm-3 control-label">Gelar Depan</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="title_first" name="title_first" placeholder="" value="<?php echo isset($data[0]->title_first) ? $data[0]->title_first : ''; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-3 control-label">Nama Lengkap</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Nama Lengkap" value="<?php echo isset($data[0]->full_name) ? $data[0]->full_name : ''; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-3 control-label">Gelar Akhir</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="title_last" name="title_last" placeholder="" value="<?php echo isset($data[0]->title_last) ? $data[0]->title_last : ''; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputName" class="col-sm-3 control-label">Nama Panggilan</label>

    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputName" placeholder="Name" value="<?php echo isset($data[0]->nickname) ? $data[0]->nickname : ''; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail" class="col-sm-3 control-label">Email</label>
    <div class="col-sm-9">
      <input type="email" class="form-control" id="email" placeholder="Email" value="<?php echo isset($data[0]->email) ? $data[0]->email : ''; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputExperience" class="col-sm-3 control-label">Profesi</label>
    <div class="col-sm-9">
      <?php echo htmlSelectFromArray($professional,'name="professional" class="form-control" id="searchProfesi" style="width:100%;"',TRUE,isset($data[0]->professional) ? $data[0]->professional : ''); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="inputSkills" class="col-sm-3 control-label">Jenis Kelamin</label>
    <div class="col-sm-9">
      <?php echo htmlSelectFromArray($gender,'name="gender" class="form-control" id="gender"',TRUE,isset($data[0]->gender) ? $data[0]->gender : ''); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="inputSkills" class="col-sm-3 control-label">Agama</label>
    <div class="col-sm-9">
      <?php echo htmlSelectFromArray($religion,'name="religion" class="form-control" id="religion"',TRUE,isset($data[0]->religion) ? $data[0]->religion : ''); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail" class="col-sm-3 control-label">Tempat Lahir</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="place_of_birth" name="place_of_birth" value="<?php echo isset($data[0]->place_of_birth) ? $data[0]->place_of_birth : ''; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail" class="col-sm-3 control-label">Tanggal Lahir</label>
    <div class="col-sm-9">
      <input type="date" class="form-control" id="birthday" name="birthday" value="<?php echo isset($data[0]->birthday) ? $data[0]->birthday : ''; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputSkills" class="col-sm-3 control-label">Golongan Darah</label>
    <div class="col-sm-9">
      <?php echo htmlSelectFromArray($bloodtype,'name="blood_type" class="form-control" id="blood_type"',TRUE,isset($data[0]->blood_type) ? $data[0]->blood_type : ''); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="inputSkills" class="col-sm-3 control-label">Status Pernikahan</label>
    <div class="col-sm-9">
      <?php echo htmlSelectFromArray($status_marital,'name="marital_status" class="form-control" id="status_marital"',TRUE,isset($data[0]->marital_status) ? $data[0]->marital_status : ''); ?>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail" class="col-sm-3 control-label">Handphone</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="mobile1" name="mobile1" placeholder="Nomor Handphone 1" value="<?php echo isset($data[0]->mobile1) ? $data[0]->mobile1 : ''; ?>">
      <br>
      <input type="text" class="form-control" id="mobile2" name="mobile2" placeholder="Nomor Handphone 2" value="<?php echo isset($data[0]->mobile2) ? $data[0]->mobile2 : ''; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail" class="col-sm-3 control-label">Upload Foto</label>
    <div class="col-sm-9">
      <input type="file" class="form-control" id="upload_image" name="upload_image">
      <br>
      <span class="image">
        <img class="profile-user-img" src="<?php echo base_url('uploads/'.$this->session->userdata('username').'/img_profile/'.$data[0]->upload_image); ?>" alt="User profile picture">
      </span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-danger">Submit</button>
    </div>
  </div>
</form>