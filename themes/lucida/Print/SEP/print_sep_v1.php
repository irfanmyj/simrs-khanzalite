<style type="text/css">
  @media print {
    body {
      margin-top: 0mm; 
      margin-bottom: 0mm; 
      margin-left: 0mm; 
      margin-right: 0mm
      }
    table,tr,td,img,p {
      margin: 0px;
      padding: 0px;
    }
  }
</style>
  <div class="padding">
      <div class="row" id="printarea">
          <div class="col-sm-12 col-lg-12">
              <div class="col-md-10 col-sm-12 col-xs-12" style="margin-top: 0px;">
                <div class="box">
                  <table>
                    <tr>
                      <td width="250"><img src="<?php echo base_url('uploads/logo_bpjs.jpeg');?>" width="250"></td>
                      <td width="700" align="center">
                        <table>
                          <tr>
                            <td style="padding: 5px 0 0 0;"><h5>SURAT ELEGIBILITAS PESERTA</h5></td>
                          </tr>
                          <tr>
                            <td align="center">RSUD Kota Depok</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <table>
                          <tr>
                            <td width="500">
                              <table width="500">
                                <tr>
                                  <td width="130">&nbsp;&nbsp;No. SEP</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->no_sep) ? $r[0]->no_sep : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Tgl. SEP</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->tglsep) ? date('d/m/Y',strtotime($r[0]->tglsep)) : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;No. Kartu</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->no_kartu) ? $r[0]->no_kartu .'&nbsp; (MR : '.$r[0]->nomr.')' : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Nama Peserta</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->nama_pasien) ? $r[0]->nama_pasien : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Tgl. Lahir</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->tanggal_lahir) ? date('d/m/Y',strtotime($r[0]->tanggal_lahir)) : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;No. Telp</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->notelep) ? $r[0]->notelep : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Sub/Spesialis</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->nmpolitujuan) ? $r[0]->nmpolitujuan : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Faskes Perujuk</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->nmppkrujukan) ? $r[0]->nmppkrujukan : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Diagnosa Awal</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->diagawal) ? $r[0]->diagawal : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Catatan</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->catatan) ? $r[0]->catatan : ''; ?></td>
                                </tr>
                              </table>
                            </td>
                            <td width="500">
                              <table width="500">
                                <tr>
                                  <td colspan="3">
                                    <img src="<?php echo $url.'/10/0/getbarcode/'.$r[0]->no_sep; ?>" style="padding: 0 0 5px 0;">
                                  </td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;No. Rawat</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->no_rawat) ? $r[0]->no_rawat : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;No. Reg</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->no_reg) ? $r[0]->no_reg : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Peserta</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->peserta) ? $r[0]->peserta : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Jns. Rawat</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo ($r[0]->jnspelayanan==2) ? 'Rawat Jalan' : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Kls. Rawat</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->klsrawat) ? $this->_cf->_bpjs['klsrawat'][$r[0]->klsrawat] : ''; ?></td>
                                </tr>
                                <tr>
                                  <td width="130">&nbsp;&nbsp;Penjamin</td>
                                  <td>:</td>
                                  <td>&nbsp;&nbsp;<?php echo isset($r[0]->penjamin) ? $r[0]->penjamin : ''; ?></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" width="1000">
                        <table width="1000">
                          <tr>
                            <td width="500">
                              <table>
                                <tr>
                                  <td>
                                    <p style="font-size: 10px;"><b><i>&nbsp;&nbsp;*Saya menyetujui BPJSKesehatan menggunakan informasi Medis Pasien jika diperlukan</i></b><br><b><i>&nbsp;&nbsp;*Sep bukan sebagai bukti penjaminan peserta</i></b></p>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td align="center" width="500">
                              <table>
                                <tr>
                                  <td>
                                    <p>&nbsp;&nbsp;Pasien/Keluarga Pasien</p>
                                    <br>
                                    <br>
                                    <br>
                                    -----------------------------
                                    <div class="box-divider m-0 text-center" style="background-color: #000000;"></div>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
          </div>
      </div>
  </div>
<script type="text/javascript">
  /*$(function(){
    window.print();
    window.close();
  });*/
  /*function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}*/
</script>