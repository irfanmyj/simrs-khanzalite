<script src="<?php echo base_url('vendor/lucida/');?>assets/vendor/toastr/toastr.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		$("#tanggal_periksa").datepicker().datepicker("setDate", new Date());
		

		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			//alert(data_post);			
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	if(dt.error==1){
		        		toastr_msg(dt.msg);
		        	}else{
		        		window.location = dt.url;
		        		toastr_info(dt.msg);
		        	}
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$('#modalEditForm #update').css('display','block');
			$('#modalEditForm #save').css('display','none');
			$('#modalEditForm :input').val('');
            $('#modalEditForm [name="id"]').val(tr.data('no_rkm_medis'));
            $('#modalEditForm [name="no_rkm_medis"]').val(tr.data('no_rkm_medis'));
            $('#modalEditForm #getpasien_edit').val(tr.data('no_rkm_medis'));
            $('#modalEditForm #getpasien_edit').attr('disabled',true);
            $('#modalEditForm [name="tanggal_periksa"]').val(tr.data('tanggal_periksa'));
            if(tr.data('tanggal_periksa')){
	            //$('#tanggal_periksa').change(function(){
				var tanggal_periksa = tr.data('tanggal_periksa');
				var no_rkm_medis = tr.data('no_rkm_medis');
				$('#modalEditForm #kd_dokter').val('');
				$('#modalEditForm #kd_dokter').select2({width:'100%'});
				$('#modalEditForm #kd_poli').val('');
				$('#modalEditForm #kd_poli').select2({width:'100%'});
				$('#modalEditForm #kd_pj').val('');
				$('#modalEditForm #kd_pj').select2({width:'100%'});

				if(no_rkm_medis!=''){
					$.ajax({
						type : 'post',
						url : url+'/get_validasi',
						data : 'no_rkm_medis='+no_rkm_medis+'&tanggal_periksa='+tanggal_periksa,
						success : function(res){
							var dt = JSON.parse(res);
							if(dt.error==1){
								toastr_msg(dt.msg);
							}else{
								$('#modalEditForm #list_poliklinik').html(dt.msg);
								$('#modalEditForm #kd_poli').select2({width:'100%'});
							}
						}
					});
				}else{
					toastr_msg('Formulir nama pasien tidak boleh kosong.');
					$('#modalEditForm #tanggal_periksa').val("");
				}
				//});
        	}
			
			$("#list_poliklinik").on('change','#kd_poli',function(){
				var kd_poli = $(this).val();
				var no_rkm_medis = $('#id_no_rkm_medis').val();
				var tanggal_periksa = $('#tanggal_periksa').val();
				if(kd_poli!='' && tanggal_periksa!=''){
					$.ajax({
						type : 'post',
						url : url+'/get_dokter',
						data : 'kd_poli='+kd_poli+'&tanggal_periksa='+tanggal_periksa+'&no_rkm_medis='+no_rkm_medis,
						success : function(res){
							$('#list_dokter_booking').html(res);
							$('#modalAddForm #kd_dokter').select2({width:'100%'});
						}
					});
				}
			});

			$('#list_dokter_booking').on('change','#kd_dokter',function(){
				var no_rkm_medis = $('#getpasien').val();
				var tanggal_periksa = $('#tanggal_periksa').val();
				var kd_poli = $('#kd_poli').val();
				var kd_dokter = $(this).val();
				var datas = 'no_rkm_medis='+no_rkm_medis+'&tanggal_periksa='+tanggal_periksa+'&kd_poli='+kd_poli+'&kd_dokter='+kd_dokter;
				if(no_rkm_medis!='' && tanggal_periksa !='' && kd_poli!='' && kd_dokter!=''){
					$.ajax({
						type : 'post',
						url : url+'/cek_info_dokter',
						data : datas,
						success : function(res){
							var dt = JSON.parse(res);
							if(dt.sts==1){
								toastr_msg(dt.msg);
							}else{
								toastr_info(dt.msg);
							}
							
						}
					});
				}else{
					toastr_msg('Nama pasien, tanggal kunjungan, poliklinik dan dokter wajib di isi');
				}
			});

			$('#kd_pj').change(function(){
				var no_rkm_medis = $('#getpasien').val();
				var tanggal_periksa = $('#tanggal_periksa').val();
				var kd_poli = $('#kd_poli').val();
				var kd_dokter = $('#kd_dokter').val();
				var kd_pj = $(this).val();
				var datas = 'no_rkm_medis='+no_rkm_medis+'&tanggal_periksa='+tanggal_periksa+'&kd_poli='+kd_poli+'&kd_dokter='+kd_dokter+'&kd_pj='+kd_pj;

				$.ajax({
					type : 'post',
					url : url+'/cek_info_carabayar',
					data : datas,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.sts==1){
							toastr_msg(dt.msg);
							$('#save').css('display','none');
						}else{

							if(kd_pj=='BPJ'){
								$('#no_rujukan_bpjs').css('display','block');
								$('#asal_rujukan').attr('disabled',false);
								$('#no_rujukan').attr('disabled',false);
								$('#no_skdp').attr('disabled',false);
								$('#save').css('display','none');
							}else{
								$('#no_rujukan_bpjs').css('display','none');
								$('#asal_rujukan').attr('disabled',true);
								$('#no_rujukan').attr('disabled',true);
								$('#no_skdp').attr('disabled',true);
								$('#save').css('display','block');
							}

							toastr_info(dt.msg);
						}
					}
				});
			});

			$('#no_rujukan_bpjs').on('click','#cek_rujukan',function(){
				var asal_rujukan 	= $('#asal_rujukan').val();
				var no_rujukan 		= $('#no_rujukan').val();
				var no_skdp 		= $('#no_skdp').val();

				$.ajax({
					type : 'post',
					url : url+'/cek_rujukan_bpjs',
					data : 'asal_rujukan='+asal_rujukan+'&no_rujukan='+no_rujukan+'&no_skdp='+no_skdp,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.error==1){
							toastr_msg(dt.msg);
							$('#save').css('display','none');
						}else{
							toastr_info(dt.msg);
							$('#save').css('display','block');
						}
					}
				});
			});

            $('#modalEditForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalEditForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		$("#getpasien").autocomplete({
	        source: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getpasien';?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	event.preventDefault();
	        	$('#id_no_rkm_medis').val(ui.item.id); //$('#voicetotextsdiag').val()
	        	$('#modalAddForm #tanggal_periksa').val("");
				$('#modalAddForm #kd_dokter').val('');
				$('#modalAddForm #kd_dokter').select2({width:'100%'});
				$('#modalAddForm #kd_poli').val('');
				$('#modalAddForm #kd_poli').select2({width:'100%'});
				$('#modalAddForm #kd_pj').val('');
				$('#modalAddForm #kd_pj').select2({width:'100%'});
	        }
	    });

	    /*
		Filter
	    */
	    $('[name="limit_search"]').change(function(){
			var limit = $('[name="limit_search"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset+'/search';
			}
		});
	});

	//
	function formAdd()
	{
		var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		$('#modalAddForm #kd_poli').select2({width:'100%'});
		$('#modalAddForm #kd_pj').select2({width:'100%'});
		$('#modalAddForm #kd_dokter').select2({width:'100%'});
		$('#no_rujukan_bpjs').css('display','none');
		
		$('#getpasien').change(function(){
			var no_rkm_medis = $(this).val();

			if(no_rkm_medis==''){
				toastr_msg('Formulir nama pasien tidak boleh kosong..');
				$('#tanggal_periksa').attr('disabled',true);
			}else{
				$('#modalAddForm #tanggal_periksa').attr('disabled',false);
			}
		});

		$('#tanggal_periksa').change(function(){
			var tanggal_periksa = $(this).val();
			var no_rkm_medis = $('#getpasien').val();
			$('#modalAddForm #kd_dokter').val('');
			$('#modalAddForm #kd_dokter').select2({width:'100%'});
			$('#modalAddForm #kd_poli').val('');
			$('#modalAddForm #kd_poli').select2({width:'100%'});
			$('#modalAddForm #kd_pj').val('');
			$('#modalAddForm #kd_pj').select2({width:'100%'});

			if(no_rkm_medis!=''){
				$.ajax({
					type : 'post',
					url : url+'/get_validasi',
					data : 'no_rkm_medis='+no_rkm_medis+'&tanggal_periksa='+tanggal_periksa,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.error==1){
							toastr_msg(dt.msg);
						}else{
							$('#list_poliklinik').html(dt.msg);
							$('#kd_poli').select2({width:'100%'});
						}
					}
				});
			}else{
				toastr_msg('Formulir nama pasien tidak boleh kosong.');
				$('#modalAddForm #tanggal_periksa').val("");
			}
		});
		

		$("#list_poliklinik").on('change','#kd_poli',function(){
			var kd_poli = $(this).val();
			var no_rkm_medis = $('#id_no_rkm_medis').val();
			var tanggal_periksa = $('#tanggal_periksa').val();
			if(kd_poli!='' && tanggal_periksa!=''){
				$.ajax({
					type : 'post',
					url : url+'/get_dokter',
					data : 'kd_poli='+kd_poli+'&tanggal_periksa='+tanggal_periksa+'&no_rkm_medis='+no_rkm_medis,
					success : function(res){
						$('#list_dokter_booking').html(res);
						$('#modalAddForm #kd_dokter').select2({width:'100%'});
					}
				});
			}
		});

		$('#list_dokter_booking').on('change','#kd_dokter',function(){
			var no_rkm_medis = $('#getpasien').val();
			var tanggal_periksa = $('#tanggal_periksa').val();
			var kd_poli = $('#kd_poli').val();
			var kd_dokter = $(this).val();
			var datas = 'no_rkm_medis='+no_rkm_medis+'&tanggal_periksa='+tanggal_periksa+'&kd_poli='+kd_poli+'&kd_dokter='+kd_dokter;
			if(no_rkm_medis!='' && tanggal_periksa !='' && kd_poli!='' && kd_dokter!=''){
				$.ajax({
					type : 'post',
					url : url+'/cek_info_dokter',
					data : datas,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.sts==1){
							toastr_msg(dt.msg);
						}else{
							toastr_info(dt.msg);
						}
						
					}
				});
			}else{
				toastr_msg('Nama pasien, tanggal kunjungan, poliklinik dan dokter wajib di isi');
			}
		});

		$('#kd_pj').change(function(){
			var no_rkm_medis = $('#getpasien').val();
			var tanggal_periksa = $('#tanggal_periksa').val();
			var kd_poli = $('#kd_poli').val();
			var kd_dokter = $('#kd_dokter').val();
			var kd_pj = $(this).val();
			var datas = 'no_rkm_medis='+no_rkm_medis+'&tanggal_periksa='+tanggal_periksa+'&kd_poli='+kd_poli+'&kd_dokter='+kd_dokter+'&kd_pj='+kd_pj;

			$.ajax({
				type : 'post',
				url : url+'/cek_info_carabayar',
				data : datas,
				success : function(res){
					var dt = JSON.parse(res);
					if(dt.sts==1){
						toastr_msg(dt.msg);
						$('#save').css('display','none');
					}else{

						if(kd_pj=='BPJ'){
							$('#no_rujukan_bpjs').css('display','block');
							$('#asal_rujukan').attr('disabled',false);
							$('#no_rujukan').attr('disabled',false);
							$('#no_skdp').attr('disabled',false);
							$('#save').css('display','none');
						}else{
							$('#no_rujukan_bpjs').css('display','none');
							$('#asal_rujukan').attr('disabled',true);
							$('#no_rujukan').attr('disabled',true);
							$('#no_skdp').attr('disabled',true);
							$('#save').css('display','block');
						}

						toastr_info(dt.msg);
					}
				}
			});
		});

		$('#no_rujukan_bpjs').on('click','#cek_rujukan',function(){
			var asal_rujukan 	= $('#asal_rujukan').val();
			var no_rujukan 		= $('#no_rujukan').val();
			var no_skdp 		= $('#no_skdp').val();

			$.ajax({
				type : 'post',
				url : url+'/cek_rujukan_bpjs',
				data : 'asal_rujukan='+asal_rujukan+'&no_rujukan='+no_rujukan+'&no_skdp='+no_skdp,
				success : function(res){
					var dt = JSON.parse(res);
					if(dt.error==1){
						toastr_msg(dt.msg);
						$('#save').css('display','none');
					}else{
						toastr_info(dt.msg);
						$('#save').css('display','block');
					}
				}
			});

		});

		$('#modalAddForm #update').css('display','none');
		$('#modalAddForm :input').val('');
        $('#modalAddForm').modal();
	}

	function toastr_msg(msg,time='2000'){
		toastr.options.fadeOut = time;
		$context = 'error';
        $message = msg;
        $position = 'top-full-width';
        $positionClass = 'toast-' + $position;
        if(msg!=''){
        	toastr.remove();
	        toastr[$context]($message, '', {
	            positionClass: $positionClass
	        });
        }else{
        	toastr.remove();
        }   
	}

	function toastr_info(msg,time='2500'){
		toastr.options.fadeOut = time;
		$context = 'success';
        $message = msg;
        $position = 'top-full-width';
        $positionClass = 'toast-' + $position;
        if(msg!=''){
        	toastr.remove();
	        toastr[$context]($message, '', {
	            positionClass: $positionClass
	        });
        }else{
        	toastr.remove();
        }   
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }

    function formUpdate(){

    }
</script>
