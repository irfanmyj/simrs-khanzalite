<form method="post" action="<?php echo $url.'/10/0/download'; ?>">
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="collapse multi-collapse l-amber rounded" id="btnDownload">
      <div class="body" style="margin-top: 3px;">
        <div class="row clearfix">
            <div class="col-sm-4">
                <div class="form-group">
                  <label class="text-white">Pilih Poliklinik</label>
                  <?php echo htmlSelectFromArray($poliklinik, 'name="poliklinik" id="download_poliklinik" style="width:100%;" class="form-control input-lg"', true);?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="ajax_download_dokter">
                  <label class="text-white">Nama Dokter</label>
                  <?php echo htmlSelectFromArray($dokter, 'name="dokter" id="download_dokter" style="width:100%;" class="form-control"', true);?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group" id="ajax_download_cara_bayar">
                  <label class="text-white">Cara Bayar</label>
                    <?php echo htmlSelectFromArray(['UMU'=>'UMUM','BPJ'=>'JAMKESNANS'], 'name="cara_bayar" id="download_cara_bayar" style="width:100%;" class="form-control"', true);?>
                </div>
            </div> 
            
            <div class="col-sm-4">
                <div class="form-group">
                  <label class="text-white">Tanggal Awal</label>
                  <input type="date" name="tgl_awal" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Tanggal Registrasi" value="">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                  <label class="text-white">Tanggal Akhir</label>
                  <input type="date" name="tgl_akhir" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Tanggal Registrasi" value="">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                  <label class="text-white">Jenis Kelamin</label>
                  <select class="form-control show-tick" name="jk">
                      <option value="">- Jenis Kelamin -</option>
                      <option value="L">Laki-laki</option>
                      <option value="P">Perempuan</option>
                  </select>
                </div>
            </div>
            <div class="col-sm-12">
                <button type="submit" class="btn l-blush text-white">Proses</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
