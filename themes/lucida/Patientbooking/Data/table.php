<?php  
if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y') {
  $addClass = 'tr_mod';
}
?>
<table class="table table-bordered table-hover" style="margin-top: 3px;">
  <thead class="l-blush text-white">
    <tr>
      <th style="width: 1rem;" class="text-center">#</th>
      <th class="text-center">Aksi</th>
      <th style="width: 30px;" class="text-center">No</th>
      <th style="width: 30px;" class="text-center">No Reg</th>
      <th>No RM</th>
      <th>Nama Pasien</th>
      <th>JK</th>
      <th>Dokter</th>
      <th>Poliklinik</th>
      <th>Cara Bayar</th>
      <th style="width: 200px;">Tanggal Periksa</th>
      <th style="width: 200px;">Dibuat Tanggal</th>
    </tr>
  </thead>  
  <tbody>
  <?php $no=1; foreach ($data as $key => $val) { ?>
    <tr style="cursor:pointer" <?php echo showValRec($val);?>>
      <td class="text-center chk">
        <div class="fancy-checkbox" style="margin: 0 0 0 10px;">
            <label><input type="checkbox" name="id[]" value="<?php echo $val->no_rkm_medis.'mer'.$val->tanggal_periksa.'mer'.$val->kd_poli.'mer'.$val->kd_dokter.'mer'.$val->kd_pj.'mer'.$val->no_rujukan.'mer'.$val->no_sep;?>"><span></span></label>
        </div>
      </td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>">
        <?php if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y'){ ?>
          <span class="text-white btn l-blush "><i class="fa fa-edit"></i></span>
        <?php } ?>
        <?php if(@acl(decrypt_aes($this->uri->segment(2)))['print']=='Y'){ ?>
          <a href="<?php echo $url.'/10/0/print/'.$val->no_rkm_medis.'mer'.$val->tanggal_periksa.'mer'.$val->kd_poli.'mer'.$val->kd_dokter.'mer'.$val->kd_pj; ?>" target="_blank" class="text-white btn l-amber"><i class="fa fa-print"></i></a>
        <?php } ?>
      </td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->no_reg;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->no_rkm_medis;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_pasien;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->jk;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_dokter;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_poli;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->png_jawab;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo tanggal_indo($val->tanggal_periksa);?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo tanggal_indo($val->tanggal_booking);?></td>
    </tr>
  <?php } ?>
  <?php 
  if (count($data) == 0) {
      echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
  }
  ?>
  </tbody>          
</table>