<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">

        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>No RM/Nama Pasien</label>
              <input type="hidden" id="id_no_rkm_medis" name="no_rkm_medis">
              <input type="text" id="getpasien" class="form-control mdb-autocomplete l-blush text-white" placeholder="Pencarian minimal 3 karakter. By No RM/Nama Pasien" />
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Tanggal Kunjungan</label>
              <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                  <input type="text" class="form-control" name="tanggal_periksa" id="tanggal_periksa">
                  <div class="input-group-append">                                            
                      <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                  </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Poliklinik</label>
              <div id="list_poliklinik">
                <?php  
                echo htmlSelectFromArray([], 'name="kd_poli" id="kd_poli" class="form-control"', true);
                ?>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Dokter</label>
              <div id="list_dokter_booking">
                <?php  
                echo htmlSelectFromArray([], 'name="kd_dokter" id="kd_dokter" class="form-control input-lg"', true);
                ?>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Cara Bayar</label>
              <div id="list_cara_bayar">
                <?php  
                echo htmlSelectFromArray(['UMU'=>'UMUM','BPJ'=>'JAMKESNAS'], 'name="kd_pj" id="kd_pj" class="form-control"', true);
                ?>
              </div>
            </div>
          </div>
        </div>
        
        <div id="no_rujukan_bpjs" style="display: none;"> 
          <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label>Asala Rujukan</label>
                <?php  
                echo htmlSelectFromArray(['faskes1'=>'1. Faskes 1 (PKM/KLINIK PRATAMA)','faskes2'=>'2. Faskes 2 (RS)'], 'name="asal_rujukan" id="asal_rujukan" class="form-control"', true);
                ?>   
              </div>           
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>No Rujukan</label>
                <input type="text" name="no_rujukan" id="no_rujukan" class="form-control">  
              </div>    
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>No Surat SKDP</label>
                <input type="number" min="1" max="6" minlength="6" name="no_skdp" id="no_skdp" class="form-control">  
              </div>    
            </div>

            <div class="col-md-1">
              <div class="form-group">
                <label>Cek</label>
                <button type="button" class="btn l-blush text-white" id="cek_rujukan"><i class="icon-check"></i></button>
              </div>
            </div>
          </div>          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn l-blue text-black pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save" style="display: none;">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->

<!-- Modal Edit Form -->
<div class="modal fade" id="modalEditForm" role="dialog" aria-labelledby="myModalLabelEdit" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabelEdit">Formulir Edit <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">

        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>No RM/Nama Pasien</label>
              <input type="hidden" id="id_no_rkm_medis" name="no_rkm_medis">
              <input type="text" id="getpasien_edit" class="form-control mdb-autocomplete" placeholder="Pencarian minimal 3 karakter. By No RM/Nama Pasien" />
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Tanggal Kunjungan</label>
              <input type="date" name="tanggal_periksa" id="tanggal_periksa" class="form-control form-control-sm" value="" style="height: 28px;">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Poliklinik</label>
              <div id="list_poliklinik">
                <?php  
                echo htmlSelectFromArray([], 'name="kd_poli" id="kd_poli" class="form-control"', true);
                ?>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Dokter</label>
              <div id="list_dokter_booking">
                <?php  
                echo htmlSelectFromArray([], 'name="kd_dokter" id="kd_dokter" class="form-control input-lg"', true);
                ?>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Cara Bayar</label>
              <div id="list_cara_bayar">
                <?php  
                echo htmlSelectFromArray(['UMU'=>'UMUM','BPJ'=>'JAMKESNAS'], 'name="kd_pj" id="kd_pj" class="form-control"', true);
                ?>
              </div>
            </div>
          </div>
        </div>
        
        <div id="no_rujukan_bpjs" style="display: none;"> 
          <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label>Asala Rujukan</label>
                <?php  
                echo htmlSelectFromArray(['faskes1'=>'1. Faskes 1 (PKM/KLINIK PRATAMA)','faskes2'=>'2. Faskes 2 (RS)'], 'name="asal_rujukan" id="asal_rujukan" class="form-control"', true);
                ?>   
              </div>           
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>No Rujukan</label>
                <input type="text" name="no_rujukan" id="no_rujukan_edit" class="form-control">  
              </div>    
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>No Surat SKDP</label>
                <input type="number" min="1" max="6" minlength="6" name="no_skdp" id="no_skdp" class="form-control">  
              </div>    
            </div>

            <div class="col-md-1">
              <div class="form-group">
                <label>Cek</label>
                <button type="button" class="btn l-blush text-white" id="cek_rujukan"><i class="icon-check"></i></button>
              </div>
            </div>
          </div>          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn l-blue text-black pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save" style="display: none;">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->