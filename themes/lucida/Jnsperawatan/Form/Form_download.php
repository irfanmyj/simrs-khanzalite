<form method="post" action="<?php echo $url.'/10/0/download'; ?>">
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="collapse multi-collapse l-amber rounded" id="btnDownload">
      <div class="body" style="margin-top: 3px;">
        <div class="row clearfix">
            <div class="col-sm-4">
                <div class="form-group">
                  <label>Pilih Poliklinik</label>
                  <?php echo htmlSelectFromArray($poliklinik, 'name="kd_poli" id="download_kd_poli" style="width:100%;" class="form-control input-lg"', true,@$this->session->userdata('kd_poli'));?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group" id="ajax_filter_dokter">
                  <label>Nama Kategori</label>
                  <?php echo htmlSelectFromArray($kategori, 'name="kd_kategori" id="download_kd_kategori" style="width:100%;" class="form-control"', true,@$this->session->userdata('kd_kategori'));?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                  <label>Cara Bayar</label>
                    <?php echo htmlSelectFromArray($penjab, 'name="kd_pj" id="download_kd_pj" style="width:100%;" class="form-control"', true,@$this->session->userdata('kd_pj'));?>
                </div>
            </div>
            
            <div class="col-sm-12">
                <button type="submit" class="btn l-blush text-white">Proses</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
