<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		$('#kd_kategori').select2();
		$('#kd_poli').select2();
		$('#kd_pj').select2();

		$('#filter_kd_poli').select2();
		$('#filter_kd_kategori').select2();
		$('#filter_kd_pj').select2();

		$('#download_kd_poli').select2();
		$('#download_kd_kategori').select2();
		$('#download_kd_pj').select2();

		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
						
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
			$('#modalAddForm :input').val('');
            $('#modalAddForm [name="id"]').val(tr.data('kd_jenis_prw'));
            $('#modalAddForm [name="kd_jenis_prw"]').val(tr.data('kd_jenis_prw'));
            $('#modalAddForm [name="kd_jenis_prw"]').attr('disabled',true);
            $('#modalAddForm [name="nm_perawatan"]').val(tr.data('nm_perawatan'));
            $('#modalAddForm [name="kd_kategori"]').val(tr.data('kd_kategori'));
            $('#modalAddForm [name="kd_kategori"]').select2({width:'100%'}).trigger('change');
            $('#modalAddForm [name="kd_poli"]').val(tr.data('kd_poli'));
            $('#modalAddForm [name="kd_poli"]').select2({width:'100%'}).trigger('change');
            $('#modalAddForm [name="kd_pj"]').val(tr.data('kd_pj'));
            $('#modalAddForm [name="kd_pj"]').select2({width:'100%'}).trigger('change');
            $('#modalAddForm [name="material"]').val(tr.data('material'));
            $('#modalAddForm [name="bhp"]').val(tr.data('bhp'));
            $('#modalAddForm [name="tarif_tindakandr"]').val(tr.data('tarif_tindakandr'));
            $('#modalAddForm [name="tarif_tindakanpr"]').val(tr.data('tarif_tindakanpr'));
            $('#modalAddForm [name="kso"]').val(tr.data('kso'));
            $('#modalAddForm [name="menejemen"]').val(tr.data('menejemen'));
            $('#modalAddForm [name="total_byrdr"]').val(tr.data('total_byrdr'));
            $('#modalAddForm [name="total_byrpr"]').val(tr.data('total_byrpr'));
            $('#modalAddForm [name="total_byrdrpr"]').val(tr.data('total_byrdrpr'));
            $('#modalAddForm [name="status"]').val(tr.data('status'));
            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

	});

	//
	function formAdd()
	{
		var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		$('#modalAddForm :input').val('');
		getCode();
		$('#modalAddForm [name="kd_jenis_prw"]').attr('readonly',true);
		//$('#modalAddForm [name="username"]').attr('disabled',false);
        $('#modalAddForm').modal();
	}

	function getCode(){
		var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		$.ajax({
			type : 'GET',
			url : url+'/getcode',
			success : function(res){
				$('#modalAddForm [name="kd_jenis_prw"]').val(res);
			}
		});
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }
</script>
