<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="row clearfix">
          <div class="col-md-6">
            <div class="form-group">
              <label>Kode</label>
              <input type="text" name="kd_jenis_prw" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Nama Perawatan</label>
              <input type="text" name="nm_perawatan" class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Kategori</label>
              <?php  
              echo htmlSelectFromArray($kategori, 'name="kd_kategori" id="kd_kategori" class="form-control" style="width:100%;"', true);
              ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Poliklinik</label>
              <?php  
              echo htmlSelectFromArray($poliklinik, 'name="kd_poli" id="kd_poli" class="form-control" style="width:100%;"', true);
              ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Cara Bayar</label>
              <?php  
              echo htmlSelectFromArray($penjab, 'name="kd_pj" id="kd_pj" class="form-control" style="width:100%;"', true);
              ?>
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>Material</label>
              <input type="text" name="material" class="form-control">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Bhp</label>
              <input type="text" name="bhp" class="form-control">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Tarif Tindakan Dr</label>
              <input type="text" name="tarif_tindakandr" class="form-control">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Tarif Tindakan Pr</label>
              <input type="text" name="tarif_tindakanpr" class="form-control">
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>Kso</label>
              <input type="text" name="kso" class="form-control">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Menejemen</label>
              <input type="text" name="menejemen" class="form-control">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Tarif Bayar Dr</label>
              <input type="text" name="total_byrdr" class="form-control">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Tarif Bayar Pr</label>
              <input type="text" name="total_byrpr" class="form-control">
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-6">
            <div class="form-group">
              <label>Tarif Bayar Dr/Pr</label>
              <input type="text" name="total_byrdrpr" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Status</label>
              <?php  
              echo htmlSelectFromArray($status, 'name="status" id="status" class="form-control" style="width:100%;"', true);
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->