<?php  
if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y') {
  $addClass = 'tr_mod';
}
?>
<table class="table table-hover m-b-0 c_list" style="margin-top: 5px;">
  <thead class="l-blush text-white">
    <tr>
      <th style="width: 1rem;" class="text-center">#</th>
      <th style="width: 30px;" class="text-center">No</th>
      <th style="width: 50px;">Aksi</th>
      <th style="width: 100px;">Kode</th>
      <th>Nama Perawatan</th>
      <th>Nama Kategori</th>
      <th>Poliklinik</th>
      <th>Cara Bayar</th>
      <th>Material</th>
      <th>BHP</th>
      <th>Tarif Tindakan Dr</th>
      <th>Tarif Tindakan Pr</th>
      <th>Kso</th>
      <th>Menejemen</th>
      <th>Total Bayar Dr</th>
      <th>Total Bayar Pr</th>
      <th>Total Bayar Dr/Pr</th>
    </tr>
  </thead>  
  <tbody>
  <?php $no=1; foreach ($data as $key => $val) { ?>
    <tr style="cursor:pointer" <?php echo showValRec($val);?>>
      <td class="text-center chk"><input type="checkbox" name="id[]" value="<?php echo $val->kd_jenis_prw;?>"></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>">
        <?php if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y'){ ?>
          <span class="text btn l-blush text-white"><i class="fa fa-edit"></i></span>
        <?php } ?>
      </td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->kd_jenis_prw;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_perawatan;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_kategori;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_poli;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->png_jawab;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->material;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->bhp;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->tarif_tindakandr;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->tarif_tindakanpr;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->kso;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->menejemen;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->total_byrdr;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->total_byrpr;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->total_byrdrpr;?></td>
    </tr>
  <?php } ?>
  <?php 
  if (count($data) == 0) {
      echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
  }
  ?>
  </tbody>          
</table>