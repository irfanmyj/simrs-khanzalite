  <?php global $Cf; ?>
  <div id="main-content">
    <div class="container-fluid">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-6 col-md-8 col-sm-12">
              <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo isset($title) ? $title : ''; ?></h2>
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                  <li class="breadcrumb-item active"><?php echo $this->uri->segment(1); ?></li>
              </ul>
          </div>
        </div>
      </div>

      <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="card">
            <div class="header">
              <h2><?php echo isset($title) ? $title : ''; ?></h2>
            </div>

            <div class="body">
              <?php 
                echo calrt();
                $this->session->unset_userdata('msg');
                $this->session->unset_userdata('error');
                if(@acl($Cf->decrypt_aes_256_cbc($this->uri->segment(2)))['update']=='Y') {
                  $addClass = 'tr_mod';
                }
              ?>

              <?php $this->site->get_template('coreTheme/btnAkses'); ?>

              <table class="table table-responsive table-hover m-b-0 c_list" style="margin-top: 5px;">
                <thead class="l-blush text-white">
                  <tr>
                    <th style="width: 1rem;" class="text-center">#</th>
                    <th style="width: 30px;" class="text-center">No</th>
                    <th>Nama Dokter</th>
                    <th>Poliklinik</th>
                    <th>Hari</th>
                    <th>LPO</th>
                    <th>LBB</th>
                    <th>LBU</th>
                    <th style="width: 200px;">Dibuat Tanggal</th>
                    <th>Aksi</th>
                  </tr>
                </thead>  
                <tbody>
                <?php $no=1; foreach ($data as $key => $val) { ?>
                  <tr style="cursor:pointer" <?php echo showValRec($val);?>>
                    <td class="text-center chk">
                      <div class="fancy-checkbox" style="margin: 0 0 0 10px;">
                          <label><input type="checkbox" name="id[]" value="<?php echo $val->id_limit_pasien;?>"><span></span></label>
                      </div>
                    </td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_dokter;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_poli;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->hari_kerja;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->limit_reg;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->bpjs;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->umum;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->masuk_tgl;?></td>
                    <td class="<?php echo isset($addClass) ? $addClass : ''; ?>">
                      <?php if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y'){ ?>
                        <span class="text btn l-blush text-white"><i class="fa fa-edit"></i> Edit</span>
                      <?php } ?>
                    </td>
                  </tr>
                <?php } ?>
                <?php 
                if (count($data) == 0) {
                    echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
                }
                ?>
                </tbody>          
              </table>

              <div class="row">
                <div class="col-md-7 pull-left">
                  <div class="row">
                    <div class="col-md-2">
                      <?php echo select($limit_rows, 'name="limit" style="width:100px;" class="form-control"', true, $limit); ?>
                    </div>
                    <div class="col-md-10">
                      <div style="margin-top: 7px; margin-left: 5px;">
                        Memunculkan halaman <?php echo $offset; ?> sampai <?php echo $limit; ?> dari <?php echo $res_count; ?> data. 
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="col-md-5">
                  <?php echo $pagination;?>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/addForm'); ?>
    </div>
  </div>
