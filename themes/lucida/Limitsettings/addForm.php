<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">

        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Pilih Poliklinik</label>
              <?php echo select($poli,'name="kd_poli" id="kd_poli" class="form-control" style="width:100%;"',TRUE); ?>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label>Pilih Dokter</label>
              <div id="list_dokter">
                <?php echo select([],'name="kd_dokter" id="kd_dokter" class="form-control"  style="width:100%;"',TRUE); ?>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div id="list_form_limit">
              
            </div>

            <div>
              <blockquote>
                <h5>Keterangan : </h5>
                <p class="blockquote blockquote-info l-blush text-white">
                1. LPO : Limit Pendaftaran Online <br>
                2. LBB : Limit Cara Bayar BPJS <br>
                3. LBU : Limit Cara Bayar UMUM <br>
                4. LPL : Limit Pendaftaran Online Lansia <br>
                5. LI  : Limit Interval Kedatangan Pasien <br>
                6. JPP : Jam Pengurangan Awal Prakter Dokter 
                </p>
              </blockquote>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form --><!-- Modal Add Form -->