<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			//alert(tr.data('kd_dokter'));
			var kd_poli = tr.data('kd_poli');
			var kd_dokter = tr.data('kd_dokter');
			var hari_kerja = tr.data('hari_kerja');
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
			$('#modalAddForm :input').val('');
            $('#modalAddForm [name="id"]').val(tr.data('id_limit_pasien'));
            $('#modalAddForm [name="kd_poli"]').val(tr.data('kd_poli'));
            $('#modalAddForm [name="kd_poli"]').select2({width:'100%'});

            var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
        	$.ajax({
				type : 'post',
				url : url+'/get_dokter',
				data : 'kd_poli='+kd_poli+'&kd_dokter='+kd_dokter,
				success : function(res){
					$('#list_dokter').html(res);
					$('#modalAddForm [name="kd_dokter"]').val(tr.data('kd_dokter'));
            		$('#modalAddForm [name="kd_dokter"]').attr("disabled", true);
            		$('#modalAddForm [name="kd_poli"]').attr("disabled", true);

					$.ajax({
						type : 'post',
						url : url+'/edit',
						data : 'kd_poli='+kd_poli+'&kd_dokter='+kd_dokter+'&hari_kerja='+hari_kerja,
						success : function(res1){
							$('#list_form_limit').html(res1);
						}
					});
				}
			});

            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

	});

	//
	function formAdd()
	{
		$('#kd_poli').select2();
		$('#modalAddForm :input').val('');
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		
        // Ambil jadwal dokter yang sudah terinput
		$('#kd_poli').change(function(){
			var kd_poli = $(this).val();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';

			$.ajax({
				type : 'post',
				url : url+'/get_dokter',
				data : 'kd_poli='+kd_poli,
				success : function(res){
					$('#list_dokter').html(res);
					$('#kd_dokter').select2();
					$('#list_form_limit').html("");
				}
			});

			$('#list_dokter').on('change','#kd_dokter',function(){
				var kd_poli = $('#kd_poli').val();
				var kd_dokter = $(this).val();
				$.ajax({
					type : 'post',
					url : url+'/get_limit',
					data : 'kd_poli='+kd_poli+'&kd_dokter='+kd_dokter,
					success : function(res1){
						$('#list_form_limit').html(res1);
					}
				});
			});
			
		});
		
        $('#modalAddForm').modal();
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }
</script>