<div class="modal fade" id="modalUpdateLamaInap" role="dialog" aria-labelledby="myModalUpdateLamaInap" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalUpdateLamaInap">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label>Hari</label>
              <input type="number" name="lama" id="lama_inap" class="form-control">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Biaya</label>
              <input type="number" name="trf_kamar" id="trf_kamar_inap" class="form-control">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Biaya</label>
              <input type="number" name="ttl_biaya" id="ttl_biaya_inap" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>