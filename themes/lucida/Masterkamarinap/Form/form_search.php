<form method="post" action="<?php echo $url.'/10/0/search'; ?>">
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="collapse multi-collapse l-blush rounded" id="btnSearch">
      <div class="body" style="margin-top: 3px;">
        <div class="row clearfix">
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Nomor Rekam Medis</label>
                  <input type="text" name="no_rkm_medis" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Nomor Rekam Medis" value="<?php echo @$this->session->userdata('no_rkm_medis'); ?>">
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group" id="ajax_filter_cara_bayar">
                  <label>Ruangan</label>
                    <?php echo htmlSelectFromArray($kamar, 'name="kd_kamar" id="kd_kamar" style="width:100%;" class="form-control select2"', true,@$this->session->userdata('kd_kamar'));?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group" id="ajax_filter_cara_bayar">
                  <label>Cara Bayar</label>
                    <?php echo htmlSelectFromArray(['UMU'=>'UMUM','BPJ'=>'JAMKESNANS'], 'name="cara_bayar" id="filter_cara_bayar" style="width:100%;" class="form-control select2"', true,@$this->session->userdata('cara_bayar'));?>
                </div>
            </div>
        </div>

        <div class="row clearfix">
          <div class="col-sm-3">
              <div class="form-group">
                <label>Tanggal Masuk Awal</label>
                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                    <input type="text" class="form-control" name="tgl_masuk_awal" id="tgl_masuk_awal" value="<?php echo @$this->session->userdata('tgl_masuk_awal'); ?>">
                    <div class="input-group-append">                                            
                        <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Tanggal Masuk Akhir</label>
                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                    <input type="text" class="form-control" name="tgl_masuk_akhir" id="tgl_masuk_akhir" value="<?php echo @$this->session->userdata('tgl_masuk_akhir'); ?>">
                    <div class="input-group-append">                                            
                        <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                    </div>
                </div>
              </div>
            </div>

            <div class="col-sm-3">
              <div class="form-group">
                <label>Tanggal Keluar Awal</label>
                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                    <input type="text" class="form-control" name="tgl_keluar_awal" id="tgl_keluar_awal" value="<?php echo @$this->session->userdata('tgl_keluar_awal'); ?>">
                    <div class="input-group-append">                                            
                        <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Tanggal Keluar Akhir</label>
                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                    <input type="text" class="form-control" name="tgl_keluar_akhir" id="tgl_keluar_akhir" value="<?php echo @$this->session->userdata('tgl_keluar_akhir'); ?>">
                    <div class="input-group-append">                                            
                        <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                    </div>
                </div>
              </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control show-tick" name="jk">
                      <option value="" <?php echo (@$this->session->userdata('jk')=='')  ?' selected="selected"' : ''; ?>>- Jenis Kelamin -</option>
                      <option value="L" <?php echo (@$this->session->userdata('jk')=='L') ? ' selected="selected"' : ''; ?>>Laki-laki</option>
                      <option value="P" <?php echo (@$this->session->userdata('jk')=='P') ? ' selected="selected"' : ''; ?>>Perempuan</option>
                  </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="ajax_filter_cara_bayar">
                  <label>Status Pulang</label>
                    <?php echo htmlSelectFromArray($status_pulang, 'name="stts_pulang" id="stts_pulang" style="width:100%;" class="form-control select2"', true,@$this->session->userdata('stts_pulang'));?>
                </div>
            </div>
            <div class="col-sm-12">
                <button type="submit" class="btn l-blush text-white">Proses</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
