                  <table class="table table-hover m-b-0 c_list" style="margin-top: 5px;">
                      <thead class="l-blush text-white">
                        <tr>
                          <th style="width: 1rem;" class="text-center">#</th>
                          <th style="width: 30px;" class="text-center">No</th>
                          <th style="width: 50px;">Aksi</th>
                          <th>No Rawat</th>
                          <th>No RM</th>
                          <th>Nama Pasien</th>
                          <th>JK</th>
                          <th>Tgl lahir</th>
                          <th>Usia</th>
                          <th>Nama Bangsal</th>
                          <th>Tarif</th>
                          <th>Lama</th>
                          <th>Total</th>
                          <th>Tgl Masuk</th>
                          <th>Jam Masuk</th>
                          <th>Tgl Keluar</th>
                          <th>Jam Keluar</th>
                          <th>Status Pulang</th>
                        </tr>
                      </thead>  
                      <tbody>
                      <?php $no=1; foreach ($data as $key => $val) { ?>
                        <tr style="cursor:pointer" <?php echo showValRec($val);?>>
                          <td class="text-center chk"><input type="checkbox" name="id[]" value="<?php echo $val->no_rawat;?>"></td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
                           <td>
                            <?php if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y'){ ?>
                              <span class="text btn l-blush text-white <?php echo isset($addClass) ? $addClass : ''; ?>"><i class="fa fa-edit"></i></span>
                              <button class="btn l-blush text-white updatelamainap"><i class="icon-calendar"></i></button>
                            <?php } ?>
                          </td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->no_rawat;?></td>
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->no_rkm_medis;?></td>                          
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_pasien;?></td>                          
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->jk;?></td>                          
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->tgl_lahir;?></td>                          
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->umurdaftar . $val->sttsumur;?></td>   
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_bangsal;?></td>                        
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->trf_kamar;?></td>                        
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->lama;?></td>                        
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->ttl_biaya;?></td>                        
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->tgl_masuk;?></td>                        
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->jam_masuk;?></td>                        
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->tgl_keluar;?></td>                        
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->jam_keluar;?></td>                        
                          <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->stts_pulang;?></td>                        
                        </tr>
                      <?php } ?>
                      <?php 
                      if (count($data) == 0) {
                          echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
                      }
                      ?>
                      </tbody>          
                  </table>