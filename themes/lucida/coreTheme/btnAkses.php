<div class="row">
  <div class="col-md-6">
    <div class="btn-group" role="group" aria-label="Basic example">
      <?php if(@acl(decrypt_aes($this->uri->segment(2)))['add']=='Y') { ?>
        <button class="btn l-salmon text-white" onclick="formAdd()"><i class="fa fa-plus"></i></button>
      <?php } ?>
      <?php 
        if($this->uri->segment(1)=='gppendaftaran'){
          if(@acl(131)['add']=='Y') { 
          ?>
          <button class="btn l-blue text-dark" onclick="formRegPeriksa()"><i class="icon-user-follow"></i> Poli</button>
          <?php 
          }
          if(@acl(131)['add']=='Y') { 
          ?>
          <button class="btn l-blue text-dark" onclick="formRegIgd()"><i class="icon-user-follow"></i> Igd</button>
          <?php 
          }
        } 
      ?>
      <?php if(@acl(decrypt_aes($this->uri->segment(2)))['filter']=='Y') { ?>
        <button class="btn l-blush text-white" data-toggle="collapse" data-target="#btnSearch" aria-expanded="false" aria-controls="btnSearch" type="button"><i class="fa fa-filter"></i></button>
      <?php } ?>
      <?php if(@acl(decrypt_aes($this->uri->segment(2)))['download']=='Y') { ?>
        <button class="btn l-amber text-white" data-toggle="collapse" data-target="#btnDownload" aria-expanded="false" aria-controls="btnDownload" type="button"><i class="fa fa-download"></i></button>
      <?php } ?>
      <?php if(@acl(decrypt_aes($this->uri->segment(2)))['upload']=='Y') { ?>
        <button class="btn l-green text-dark" onclick="formAdd()"><i class="fa fa-upload"></i></button>
      <?php } ?>
      <?php if(@acl(decrypt_aes($this->uri->segment(2)))['delete']=='Y') { ?>
        <button class="btn l-coral text-dark" disabled="true" id="btnDelete" onclick="formDelete()"><i class="fa fa-trash-o"></i></button>
      <?php } ?>
    </div>
  </div>
  <div class="col-md-3"></div>
  <div class="col-md-3">
    <?php if(@acl(decrypt_aes($this->uri->segment(2)))['search']=='Y') { ?>
      <input type="text" name="table_search" class="form-control" placeholder="Search" value="<?php echo isset($search) ? $search : ''; ?>" onkeyup="event.keyCode == 13 ? window.location = '<?php echo $url; ?>' + this.value.replace(/ /g,'-') +'/'+'<?php echo $limit.'/1'; ?>' : '';">
    <?php } ?>
  </div>
</div>