<div class="block-header">
  <div class="row">
    <div class="col-lg-6 col-md-8 col-sm-12">
        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo isset($title) ? $title : ''; ?></h2>
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
            <li class="breadcrumb-item active"><?php echo $this->uri->segment(1); ?></li>
        </ul>
    </div>
  </div>
</div>