<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		$('#kd_dokter').select2();
		$('#kd_poli').select2();
		//$('#hari_kerja').select2();

		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			//alert(tr.data('kd_dokter'));
			var kd_poli = tr.data('kd_poli');
			var kd_dokter = tr.data('kd_dokter');
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
			$('#modalAddForm :input').val('');
            $('#modalAddForm [name="id"]').val(tr.data('kd_dokter'));
            $('#modalAddForm [name="kd_poli"]').val(tr.data('kd_poli'));
            $('#modalAddForm [name="kd_poli"]').select2({width:'100%'}).trigger('change');
            $('#modalAddForm [name="kd_dokter"]').val(tr.data('kd_dokter'));
            $('#modalAddForm [name="kd_dokter"]').select2({width:'100%'}).trigger('change');

            if(kd_poli!='' && kd_dokter!=''){
            	var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';

				$.ajax({
					type : 'post',
					url : url+'/edit_jadwal',
					data : 'kd_poli='+kd_poli+'&kd_dokter='+kd_dokter,
					success : function(res){
						$('#list_jadwal_aktiv').html(res);
						$('.kd_dokter').select2();
						$('.kd_poli').select2();
					}
				});
            }

            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

	});

	//
	function formAdd()
	{
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		
        
        // Ambil jadwal dokter yang sudah terinput
		$('#kd_dokter').change(function(){
			var kd_poli = $('#kd_poli').val();
			var kd_dokter = $(this).val();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';

			$.ajax({
				type : 'post',
				url : url+'/get_jadwal',
				data : 'kd_poli='+kd_poli+'&kd_dokter='+kd_dokter,
				success : function(res){
					$('#list_jadwal_aktiv').html(res);
					$('.kd_dokter').select2();
					$('.kd_poli').select2();
				}
			});
		});
		$('#modalAddForm :input').val('');
		$("#modalAddForm #kd_dokter").val("");
		$('#modalAddForm [name="kd_dokter"]').select2({width:'100%'});
		$("#modalAddForm #kd_poli").val("");
		$('#modalAddForm [name="kd_poli"]').select2({width:'100%'});
        $('#modalAddForm').modal();
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }
</script>