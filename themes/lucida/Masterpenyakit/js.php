<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		//$('#room_poli').select2(); // fungsi untuk mengatikan pencarian pada form select2

		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
						
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
			$('#modalAddForm :input').val('');
            $('#modalAddForm [name="id"]').val(tr.data('kd_penyakit'));
            $('#modalAddForm [name="kd_penyakit"]').val(tr.data('kd_penyakit'));
            $('#modalAddForm [name="kd_penyakit"]').attr('disabled',true);
            $('#modalAddForm [name="nm_penyakit"]').val(tr.data('nm_penyakit'));
            $('#modalAddForm [name="ciri_ciri"]').val(tr.data('ciri_ciri'));
            $('#modalAddForm [name="keterangan"]').val(tr.data('keterangan'));
            $('#modalAddForm [name="kd_ktg"]').val(tr.data('kd_ktg'));
            $('#modalAddForm [name="status"]').val(tr.data('status'));
            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

	});

	//
	function formAdd()
	{
		var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		$('#modalAddForm :input').val('');
        $('#modalAddForm').modal();
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }
</script>
