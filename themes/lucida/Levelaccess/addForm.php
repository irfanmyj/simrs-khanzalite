<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-xxl fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id">
        <div class="form-group">
    		  <label>Nama Akses Level</label>
    		  <input type="text" name="name" class="form-control" placeholder="Masukan kategori Level">
    		</div>

        <div id="edit">
          <?php
          /*echo '<pre>';
          print_r($modules);*/  
          foreach ($modules as $k => $v) {
             echo '<div class="form-group row">';
             echo '<label class="col-sm-3"><strong>'.$v['name_module'].'</strong></label>';
              if($v['view'] == 'Y')
                {
                  echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][view]" value="Y" data-val="Y"> &nbsp;</span>';
                }

                if($v['add'] == 'Y')
                {
                  echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][add]" value="Y" data-val="Y"> </span>';
                }

                if($v['update'] == 'Y')
                {
                  echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][update]" value="Y" data-val="Y"> </span>';
                }

                if($v['delete'] == 'Y')
                {
                  echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][delete]" value="Y" data-val="Y"> </span>';
                }

                if($v['search'] == 'Y')
                {
                  echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][search]" value="Y" data-val="Y"> </span>';
                }

                if($v['filter'] == 'Y')
                {
                  echo '<span>Filter &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][filter]" value="Y" data-val="Y"> </span>';
                }

                if($v['upload'] == 'Y')
                {
                  echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][upload]" value="Y" data-val="Y"> </span>';
                }

                if($v['download'] == 'Y')
                {
                  echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][download]" value="Y" data-val="Y"> </span>';
                }

                if($v['print'] == 'Y')
                {
                  echo '<span>Print &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][print]" value="Y" data-val="Y"> </span>';
                }

             echo '<div class="col-sm-9">';
              if(@$v['children'])
              {
                foreach (@$v['children'] as $k1 => $v1) {
                    echo '<label><strong>'.$v1['name_module'].'</strong></label><br>';
                    if(@$v2['children'])
                    {
                      foreach (@$v1['children'] as $k2 => $v2) {
                        if($v2['view'] == 'Y')
                        {
                          echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][view]" value="Y" data-val="Y"> &nbsp;</span>';
                        }

                        if($v2['add'] == 'Y')
                        {
                          echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][add]" value="Y" data-val="Y"> </span>';
                        }

                        if($v2['update'] == 'Y')
                        {
                          echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][update]" value="Y" data-val="Y"> </span>';
                        }

                        if($v2['delete'] == 'Y')
                        {
                          echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][delete]" value="Y" data-val="Y"> </span>';
                        }

                        if($v2['search'] == 'Y')
                        {
                          echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][search]" value="Y" data-val="Y"> </span>';
                        }

                        if($v2['filter'] == 'Y')
                        {
                          echo '<span>Filter &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][filter]" value="Y" data-val="Y"> </span>';
                        }

                        if($v2['upload'] == 'Y')
                        {
                          echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][upload]" value="Y" data-val="Y"> </span>';
                        }

                        if($v2['download'] == 'Y')
                        {
                          echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][download]" value="Y" data-val="Y"> </span>';
                        } 

                        if($v2['print'] == 'Y')
                        {
                          echo '<span>Print &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][print]" value="Y" data-val="Y"> </span>';
                        }
                        echo '<br>';                   
                      }
                      
                    }
                    else
                    {
                      if($v1['view'] == 'Y')
                      {
                        echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][view]" value="Y" data-val="Y"> &nbsp;</span>';
                      }

                      if($v1['add'] == 'Y')
                      {
                        echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][add]" value="Y" data-val="Y"> </span>';
                      }

                      if($v1['update'] == 'Y')
                      {
                        echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][update]" value="Y" data-val="Y"> </span>';
                      }

                      if($v1['delete'] == 'Y')
                      {
                        echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][delete]" value="Y" data-val="Y"> </span>';
                      }

                      if($v1['search'] == 'Y')
                      {
                        echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][search]" value="Y" data-val="Y"> </span>';
                      }

                      if($v1['filter'] == 'Y')
                      {
                        echo '<span>Filter &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][filter]" value="Y" data-val="Y"> </span>';
                      }

                      if($v1['upload'] == 'Y')
                      {
                        echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][upload]" value="Y" data-val="Y"> </span>';
                      }

                      if($v1['download'] == 'Y')
                      {
                        echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][download]" value="Y" data-val="Y"> </span>';
                      }

                      if($v1['print'] == 'Y')
                      {
                        echo '<span>Print &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][print]" value="Y" data-val="Y"> </span>';
                      }
                      echo '<br>';
                    }                 
                  echo '<br>';
                }
              }
             echo '</div>';
             echo '</div>';
          }
          ?>  
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn l-blush text-white pull-right" id="save">Save changes</button>
        <button type="button" class="btn l-blush text-white pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->