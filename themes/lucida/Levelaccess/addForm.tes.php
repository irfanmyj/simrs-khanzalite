<!-- Modal Add Form -->
<form method="post" action="<?php echo $url.'/'.$limit.'/'.$offset; ?>/store">
<div class="modal fade" id="modalAddForm">
  <div class="modal-dialog modal-lg fade-left">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id">
        <div class="form-group">
    		  <label>Nama Akses Level</label>
    		  <input type="text" name="name" class="form-control" placeholder="Masukan kategori alergi.">
    		</div>

        <?php  
        foreach ($modules as $k => $v) {
           echo '<div class="form-group row">';
           echo '<label class="col-sm-3"><strong>'.$v['name_module'].'</strong></label>';
           echo '<div class="col-sm-9">';
            if(@$v['children'])
            {
              foreach (@$v['children'] as $k1 => $v1) {
                  echo '<label><strong>'.$v1['name_module'].'</strong></label><br>';
                  if(@$v2['children'])
                  {
                    foreach (@$v1['children'] as $k2 => $v2) {
                      if($v2['view'] == 'Y')
                      {
                        echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][view]" value="Y" data-val="Y"> &nbsp;</span>';
                      }

                      if($v2['add'] == 'Y')
                      {
                        echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][add]" value="Y" data-val="Y"> </span>';
                      }

                      if($v2['update'] == 'Y')
                      {
                        echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][update]" value="Y" data-val="Y"> </span>';
                      }

                      if($v2['delete'] == 'Y')
                      {
                        echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][delete]" value="Y" data-val="Y"> </span>';
                      }

                      if($v2['search'] == 'Y')
                      {
                        echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][search]" value="Y" data-val="Y"> </span>';
                      }

                      if($v2['upload'] == 'Y')
                      {
                        echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][upload]" value="Y" data-val="Y"> </span>';
                      }

                      if($v2['download'] == 'Y')
                      {
                        echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v2['module_id'].'][download]" value="Y" data-val="Y"> </span>';
                      }  
                      echo '<br>';                   
                    }
                    
                  }
                  else
                  {
                    if($v1['view'] == 'Y')
                    {
                      echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][view]" value="Y" data-val="Y"> &nbsp;</span>';
                    }

                    if($v1['add'] == 'Y')
                    {
                      echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][add]" value="Y" data-val="Y"> </span>';
                    }

                    if($v1['update'] == 'Y')
                    {
                      echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][update]" value="Y" data-val="Y"> </span>';
                    }

                    if($v1['delete'] == 'Y')
                    {
                      echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][delete]" value="Y" data-val="Y"> </span>';
                    }

                    if($v1['search'] == 'Y')
                    {
                      echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][search]" value="Y" data-val="Y"> </span>';
                    }

                    if($v1['upload'] == 'Y')
                    {
                      echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][upload]" value="Y" data-val="Y"> </span>';
                    }

                    if($v1['download'] == 'Y')
                    {
                      echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v1['module_id'].'][download]" value="Y" data-val="Y"> </span>';
                    }
                    echo '<br>';
                  }                 
                echo '<br>';
              }
            }
            else
            {
              if($v['view'] == 'Y')
              {
                echo '<span>View &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][view]" value="Y" data-val="Y"> &nbsp;</span>';
              }

              if($v['add'] == 'Y')
              {
                echo '<span>Add &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][add]" value="Y" data-val="Y"> </span>';
              }

              if($v['update'] == 'Y')
              {
                echo '<span>Update &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][update]" value="Y" data-val="Y"> </span>';
              }

              if($v['delete'] == 'Y')
              {
                echo '<span>Delete &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][delete]" value="Y" data-val="Y"> </span>';
              }

              if($v['search'] == 'Y')
              {
                echo '<span>Search &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][search]" value="Y" data-val="Y"> </span>';
              }

              if($v['upload'] == 'Y')
              {
                echo '<span>Upload &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][upload]" value="Y" data-val="Y"> </span>';
              }

              if($v['download'] == 'Y')
              {
                echo '<span>Download &nbsp; <input type="checkbox" name="akses['.$v['module_id'].'][download]" value="Y" data-val="Y"> </span>';
              }
              
            }
           echo '</div>';
           echo '</div>';
        }
        ?>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</form>
<!-- /.Modal Form -->