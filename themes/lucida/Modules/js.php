<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(5);?>';
		var limit = '<?php echo $this->uri->segment(4);?>';
		var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		
		//$('#parent_id').select2();

		// Untuk mengaktifkan tombol hapus.
		$('[name="module_id[]"]').click(function () {
            if ($('[name="module_id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			//var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
            $('#modalAddForm [name="module_id"]').val(tr.data('module_id'));
            $('#modalAddForm [name="name_module"]').val(tr.data('name_module'));
            $('#modalAddForm [name="status"]').val(tr.data('status'));
            $('#modalAddForm [name="categori_menu"]').val(tr.data('categori_menu'));
            $('#modalAddForm [name="position_menu"]').val(tr.data('position_menu'));
            $('#modalAddForm [name="link"]').val(tr.data('link'));
            $('#modalAddForm [name="icon"]').val(tr.data('icon'));
            $('#modalAddForm [name="atribut"]').val(tr.data('atribut'));
            $('#modalAddForm [name="view"][value="'+tr.data('view')+'"]').prop('checked',true);
            $('#modalAddForm [name="add"][value="'+tr.data('add')+'"]').prop('checked',true);
            $('#modalAddForm [name="update"][value="'+tr.data('update')+'"]').prop('checked',true);
            $('#modalAddForm [name="delete"][value="'+tr.data('delete')+'"]').prop('checked',true);
            $('#modalAddForm [name="search"][value="'+tr.data('search')+'"]').prop('checked',true);
            $('#modalAddForm [name="filter"][value="'+tr.data('filter')+'"]').prop('checked',true);
            $('#modalAddForm [name="download"][value="'+tr.data('download')+'"]').prop('checked',true);
            $('#modalAddForm [name="upload"][value="'+tr.data('upload')+'"]').prop('checked',true);
            $('#modalAddForm [name="print"][value="'+tr.data('print')+'"]').prop('checked',true);
            //$('#modalAddForm #parent_id').select2('val',tr.data('parent_id'));
            $('#modalAddForm [name="parent_id"]').val(tr.data('parent_id'));
    		$('#modalAddForm [name="parent_id"]').select2({width:'100%'}).trigger('change');
            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			//var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		$( ".tablemodules" ).sortable({
	        delay: 150,
	        stop: function() {
	            var selectedData = new Array();
	            $('.tablemodules>tr').each(function() {
	                selectedData.push($(this).attr("id"));
	            });
	            updateOrder(selectedData,'<?php echo $url.$search.'/';?>'+limit+'/1');
	        }
	    });

		$(function(){
		    $( "#list-datamodules" ).sortable();
		    $( "#list-datamodules" ).disableSelection();
	  	});

		sortTableModules(urls);
	});

	function sortTableModules(url)
    {
    	$( ".tablemodules" ).sortable({
	        delay: 150,
	        stop: function() {
	            var selectedData = new Array();
	            $('.tablemodules>tr').each(function() {
	                selectedData.push($(this).attr("module_id"));
	            });
	            //alert(selectedData +' - '+ url);
	            updateOrder(selectedData,url);
	        }
	    });
    }

    function updateOrder(data,url) {
        $.ajax({
            url:url+'/position',
            type:'post',
            data:'module_id='+data+'&url='+url,
            success:function(res){
            	var r = JSON.parse(res);
            	if(r.error=='')
            	{
            		//alert('perubahan Anda berhasil disimpan');
            		window.location = r.url;
            	}
            }
        })
    }

	//
	function formAdd()
	{
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		//$('#modalAddForm :input').val('');
        //$('.sm-war-kosong').hide();
        $('#modalAddForm').modal();
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="module_id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }
</script>