  <div id="main-content">
      <div class="container-fluid">
        <div class="block-header">
          <div class="row">
              <div class="col-lg-6 col-md-8 col-sm-12">
                  <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo isset($title) ? $title : ''; ?></h2>
                  <ul class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                      <li class="breadcrumb-item active"><?php echo $this->uri->segment(1); ?></li>
                  </ul>
              </div>
            </div>
        </div>

        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
              <div class="header">
                  <h2><?php echo isset($title) ? $title : ''; ?></h2>
              </div>

              <div class="body">
                <?php 
                  echo calrt();
                  $this->session->unset_userdata('msg');
                  $this->session->unset_userdata('error');
                  if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y') {
                    $addClass = 'tr_mod';
                  }
                ?>

                <?php $this->site->get_template('coreTheme/btnAkses'); ?>

                <table class="table table-hover m-b-0 c_list" id="list-datamodules" style="margin-top: 5px;">
                  <thead>
                    <tr>
                      <th style="width: 1rem;" class="text-center">
                        <label class="fancy-checkbox">
                            <input class="select-all" type="checkbox" name="checkbox">
                            <span></span>
                        </label>
                      </th>
                      <th style="width: 30px;" class="text-center">No</th>
                      <th style="width: 100px;" class="text-center">parent</th>
                      <th>Nama Module</th>
                      <th>Link</th>
                      <th style="width: 200px;">Dibuat Tanggal</th>
                    </tr>
                  </thead>  
                  <tbody class="tablemodules">
                  <?php $no=1; foreach ($data as $key => $val) { unset($val->icon); ?>
                    <tr style="cursor:pointer" <?php echo showValRec($val);?> id="<?php echo $val->module_id;?>">
                      <td class="text-center">
                        <label class="fancy-checkbox">
                          <input class="checkbox-tick" type="checkbox" name="module_id[]" value="<?php echo $val->module_id;?>">
                          <span></span>
                        </label>
                      </td>
                      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
                      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->parent_id;?></td>
                      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->name_module;?></td>
                      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><a href="<?php echo base_url($val->link.'/'.encrypt_aes($val->module_id).'/search=/5/1');?>"><?php echo $val->link;?></a></td>
                      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->created_at;?></td>
                    </tr>
                  <?php } ?>
                  <?php 
                  if (count($data) == 0) {
                      echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
                  }
                  ?>
                  </tbody>          
                </table>

                <div class="row">
                  <div class="col-md-7 pull-left">
                    <div class="row">
                      <div class="col-md-2">
                        <?php echo select($limit_rows, 'name="limit" style="width:100px;" class="form-control"', true, $limit); ?>
                      </div>
                      <div class="col-md-10">
                        <div style="margin-top: 7px; margin-left: 5px;">
                          Memunculkan halaman <?php echo $offset; ?> sampai <?php echo $limit; ?> dari <?php echo $res_count; ?> data. 
                        </div>
                      </div>
                    </div> 
                  </div>
                  <div class="col-md-5">
                    <?php echo $pagination;?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/addForm'); ?>

      </div>
    </div>

  