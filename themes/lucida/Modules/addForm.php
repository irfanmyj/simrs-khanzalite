<!--Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="module_id">
        <div class="form-group">
    		  <label>Nama Module</label>
    		  <input type="text" name="name_module" class="form-control" placeholder="Masukan nama module.">
    		</div>

        <div class="form-group">
          <label>Link</label>
          <input type="text" name="link" class="form-control" placeholder="Masukan nama module.">
        </div>

        <div class="form-group">
          <label>Parent</label>
          <?php echo htmlSelectFromArray($modules, 'name="parent_id" id="parent_id" style="width:100%;" class="form-control select2"', true);?>
        </div>

        <div class="form-group">
          <label>Icon</label>
          <input type="text" name="icon" class="form-control">
        </div>

        <div class="form-group">
          <label>Atribut</label>
          <input type="text" name="atribut" class="form-control">
        </div>

        <div class="form-group">
          <table class="table">
          <?php 
            foreach ($akses as $k => $v) {
              echo '<tr>';
              echo '<td width="100"><b>'.$k.'</b></td>';
              echo '<td width="10">:</td>';
              echo '<td alignt="left">';
              foreach ($v as $v1) {
                echo '<input type="radio" name="'.$k.'" value='.$v1.'> '. $v1 .' ';
              }
              echo '</td>';
              echo '</tr>';
            } 
          ?>
          </table>
        </div>

        <div class="form-group">
          <label>Posisi</label>
          <?php echo select(['L'=>'Show','H'=>'Hide'],'name="position_menu" class="form-control"',TRUE); ?>
        </div>

        <div class="form-group">
          <label>Kategori Menu</label>
          <?php echo select(['Default'=>'Pertama','Second'=>'Kedua','Third'=>'Ketiga','Fourth'=>'Keempat'],'name="categori_menu" class="form-control"',TRUE); ?>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form