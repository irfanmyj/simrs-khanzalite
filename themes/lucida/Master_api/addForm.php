<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm">
  <div class="modal-dialog modal-lg fade-left">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id">
        <div class="row">
          <div class="col-xs-4">
            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="name" class="form-control" placeholder="Masukan nama api.">
            </div>
          </div>

          <div class="col-xs-4">
            <div class="form-group">
              <label>ID</label>
              <input type="text" name="api_id" class="form-control">
            </div>
          </div>

          <div class="col-xs-4">
            <div class="form-group">
              <label>Password</label>
              <input type="text" name="password" class="form-control">
            </div>
          </div>

          <div class="col-xs-4">
            <div class="form-group">
              <label>Base URL</label>
              <input type="text" name="base_url" class="form-control">
            </div>
          </div>

          <div class="col-xs-4">
            <div class="form-group">
              <label>Serives Name</label>
              <input type="text" name="service_name" class="form-control">
            </div>
          </div>

          <div class="col-xs-4">
            <div class="form-group">
              <label>Status</label>
              <?php echo select($status,'name="status" class="form-control"',TRUE); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->