<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Judul</label>
              <input type="text" name="judul" class="form-control" placeholder="Masukan judul.">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Tanggal Libur Nasional</label>
              <input type="date" name="tanggal" value="<?php echo date('Y-m-d'); ?>" class="form-control" placeholder="Masukan judul.">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Keterangan</label>
              <textarea class="form-control" name="keterangan"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->