<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label>Nama Provinsi</label>
              <?php  
              echo htmlSelectFromArray($provinsi, 'name="kd_prop" id="kd_prop" class="form-control" style="width:100%;"', true);
              ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Kategori</label>
              <?php  
              echo htmlSelectFromArray($jenis_wilayah, 'name="id_jenis" id="id_jenis" class="form-control"', true);
              ?>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Kode Bpjs Kabupaten</label>
              <input type="text" name="kd_kab_bpjs" class="form-control">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Kabupaten</label>
              <input type="text" name="nm_kab" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->