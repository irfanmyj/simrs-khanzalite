<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" name="id">
        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>Email</label>
              <input type="email" name="username" id="username" class="form-control" placeholder="Masukan email.">
              <span id="msg-username"></span>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Level Akses</label>
              <?php echo select($access,'name="level_access" id="level_access" class="form-control"',TRUE); ?>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Status</label>
              <?php echo select($status,'name="is_active" class="form-control"',TRUE); ?>
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-12 dokter_poli" style="display: none;">
            <div class="form-group">
              <label>Kode Dokter</label>
              <?php echo htmlSelectFromArray($dokter,'name="code_doctor" id="code_doctor" class="form-control select2" style="width:100%;"',TRUE); ?>
            </div>
          </div>

          <div class="col-md-12 perawat_poli" style="display: none;">
            <div class="form-group">
              <label>Poliklinik</label>
              <?php echo htmlSelectFromArray($poliklinik,'name="id_room1[]" id="id_room1" class="form-control select2" multiple style="width:100%; height:20px;"'); ?>
            </div>
          </div>

          <div class="col-md-12 perawat_rawatinap" style="display: none;">
            <div class="form-group">
              <label>Rawatinap</label>
              <?php echo htmlSelectFromArray($ranap,'name="id_room2[]" id="id_room2" class="form-control select2" multiple style="width:100%; height:20px;"'); ?>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Akses Menu</label><br>
              <label class="fancy-checkbox"><input name="menu_utama" id="menu_utama" value="Y" type="checkbox"><span><i></i>Menu Utama</span></label>
              <label class="fancy-checkbox"><input name="menu_report" id="menu_report" value="Y" type="checkbox"><span><i></i>Menu Report</span></label>
              <label class="fancy-checkbox"><input name="menu_master" id="menu_master" value="Y" type="checkbox"><span><i></i>Menu Master</span></label>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary pull-right" id="save">Save changes</button>
        <button type="button" class="btn btn-primary pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->