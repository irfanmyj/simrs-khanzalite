<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $this->uri->segment(4);?>';
		//$('#room_poli').select2();

		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			if(limit)
			{
				offset = 1;
				window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;
			}
		});

		// Add datas to table database
		$('#save').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var id_room1 = $('#modalAddForm [name="id_room1[]"]').map(function(){
				return $(this).val();
			}).get();
			
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/store',
		        type: "post",
		        data: data_post+'&url='+url+'&id_room1='+id_room1,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		// Edit data
		$('.tr_mod').click(function(){
			var tr = $(this).parent();
			$('#modalAddForm #update').css('display','block');
			$('#modalAddForm #save').css('display','none');
			//$('#modalAddForm :input').val('');
            $('#modalAddForm [name="id"]').val(tr.data('id_user'));
            $('#modalAddForm [name="username"]').val(tr.data('username'));
            $('#modalAddForm [name="username"]').attr('readonly',true);
            $('#modalAddForm [name="is_active"]').val(tr.data('is_active'));
            $('#modalAddForm [name="level_access"]').val(tr.data('level_access'));

            if(tr.data('code_doctor')!='-'){
            	$('#modalAddForm .dokter_poli').css('display','block');
            	$('#modalAddForm [name="code_doctor"]').val(tr.data('code_doctor'));
            	$('#modalAddForm [name="code_doctor"]').select2({width:'100%'}).trigger('change');
            }

             if(tr.data('id_room2')!=''){
            	$('#modalAddForm .perawat_rawatinap').css('display','block');
            	$('#modalAddForm [name="id_room2"]').val(tr.data('id_room2'));
            	$('#modalAddForm [name="id_room2"]').select2({width:'100%'}).trigger('change');
            }

            if(tr.data('menu_utama')=='Y'){
            	$('#modalAddForm [name="menu_utama"]').val(tr.data('menu_utama')).prop('checked',true);
            }else{
            	$('#modalAddForm [name="menu_utama"]').prop('checked',false);
            }
            
            if(tr.data('menu_report')=='Y'){
            	$('#modalAddForm [name="menu_report"]').val(tr.data('menu_report')).prop('checked',true);
            }else{
            	$('#modalAddForm [name="menu_report"]').prop('checked',false);
            }
            
            if(tr.data('menu_master')=='Y'){
            	$('#modalAddForm [name="menu_master"]').val(tr.data('menu_master')).prop('checked',true);
            }else{
            	$('#modalAddForm [name="menu_master"]').prop('checked',false);
            }
            
            $('#modalAddForm').modal();
		});

		// Update
		$('#update').click(function(){
			var data_post = $('#modalAddForm :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/update',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	window.location = dt.url;
		        	//alert(response)
		           // You will get response from your PHP page (what you echo or print)
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		$( '#menu-sidebar a' ).on( 'click', function () {
			//$( '#menu-sidebar .sidebar-menu' ).find( 'li.active' ).removeClass( 'active' );
			$( '#menu-sidebar .sidebar-menu' ).find( 'li.active' ).removeClass( 'active' );
			$( this ).parent( 'ul' ).parent( 'li.treeview' ).addClass( 'active' );
			$( this ).parent( 'li' ).addClass( 'active' );
		});

		$('#level_access').change(function(){
			var id_level = $(this).val();
			if(id_level==4 || id_level==6 || id_level==7 || id_level==8){
				$('.dokter_poli').css('display','block');
				$('.perawat_rawatinap').css('display','none');
			}else if(id_level==3){
				$('.perawat_rawatinap').css('display','block');
				$('.dokter_poli').css('display','none');
			}else{
				$('.perawat_rawatinap').css('display','none');
				$('.dokter_poli').css('display','none');
			}
		});

	});

	//
	function formAdd()
	{
		var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		//$('#modalAddForm :input').val('');
		$('#modalAddForm [name="menu_utama"]').prop('checked',false);
        $('#modalAddForm [name="menu_report"]').prop('checked',false);
        $('#modalAddForm [name="menu_master"]').prop('checked',false);
		notSpace('modalAddForm','username');
		notSpace('modalAddForm','password');
		$('#modalAddForm [name="username"]').change(function(){
			checking_username(url,$(this).val());
		});
        $('#modalAddForm').modal();
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }

    function checking_username(url,username)
    {
    	$.ajax({
    		type : 'post',
    		url : url+'/checking_username',
    		data : 'username='+username,
    		success : function(res)
    		{
    			var dt = JSON.parse(res);
    			if(dt.error == 'Ada')
    			{
    				$('#modalAddForm #msg-username').html(dt.msg);
    				$('#modalAddForm #msg-username').css('color','red');
    				$('#modalAddForm [name="username"]').css('background','red');
    				$('#modalAddForm [name="username"]').css('color','white');
    			}
    			else
    			{
    				$('#modalAddForm #msg-username').html('');
    				$('#modalAddForm #msg-username').removeAttr('style');
    				$('#modalAddForm [name="username"]').removeAttr('style');
    			}
    		}
    	});
    }
</script>
