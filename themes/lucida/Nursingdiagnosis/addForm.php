<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg fade-left" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel">Formulir <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Nama Class</label>
              <?php echo select($dClass, 'name="code_class" id="code_class" style="width:100%;" class="form-control select2"', true); ?>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Nama Domain</label>
              <?php echo select($dDomain, 'name="code_domain" id="code_domain" style="width:100%;" class="form-control select2"', true); ?>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Nama Diagnosis Class</label>
              <input type="text" name="name" class="form-control" placeholder="Masukan Nama Diagnosis kelas.">
              <br>
            <p class="text-muted well well-sm no-shadow name-error mt-2" style="display: none;"></p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save">Save changes</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->