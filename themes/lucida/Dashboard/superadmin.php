    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-4 col-md-8 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Dashboard</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ul>
                    </div>            
                    <div class="col-lg-8 col-md-4 col-sm-12 text-right">
                        <div class="bh_chart hidden-xs">
                            <div class="float-left m-r-15">
                                <small>Visitors</small>
                                <h6 class="mb-0 mt-1"><i class="icon-user"></i> <?php echo $visitor_month[0]->visitor; ?></h6>
                            </div>
                            <span class="bh_visitors float-right"><?php echo implode(',', $visitor); ?></span>
                        </div>
                        <div class="bh_chart hidden-sm">
                            <div class="float-left m-r-15">
                                <small>Visits</small>
                                <h6 class="mb-0 mt-1"><i class="icon-globe"></i> 325</h6>
                            </div>
                            <span class="bh_visits float-right">10,8,9,3,5,8,5</span>
                        </div>
                        <div class="bh_chart hidden-sm">
                            <div class="float-left m-r-15">
                                <small>Chats</small>
                                <h6 class="mb-0 mt-1"><i class="icon-bubbles"></i> 13</h6>
                            </div>
                            <span class="bh_chats float-right">1,8,5,6,2,4,3,2</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2><?php echo isset($title_table1) ? $title_table1 : ''; ?></h2>
                        </div>

                        <div class="body">
                            <?php 
                                echo calrt();
                                $this->session->unset_userdata('msg');
                                $this->session->unset_userdata('error');
                            ?>

                            <table class="table table-hover m-b-0 c_list" style="margin-top: 5px;">
                              <thead>
                                <tr>
                                  <th style="width: 1rem;" class="text-center">
                                    <label class="fancy-checkbox">
                                        <input class="select-all" type="checkbox" name="checkbox">
                                        <span></span>
                                    </label>
                                  </th>
                                  <th style="width: 30px;" class="text-center">No</th>
                                  <th style="width: 100px;" class="text-center">parent</th>
                                  <th>Nama Module</th>
                                  <th>Link</th>
                                  <th style="width: 200px;">Dibuat Tanggal</th>
                                </tr>
                              </thead>  
                              <tbody>
                              
                              </tbody>          
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>