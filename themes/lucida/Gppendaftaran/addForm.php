<!-- Start Modal Add Form -->
  <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_reg_pasien'); ?>
<!-- End Modal Add Form -->

<!-- Modal Add Reg Periksa Form -->
  <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_reg_periksa'); ?>
<!-- /.End Reg Periksa Form -->

<!-- Modal Add Reg Periksa IGD Form -->
  <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_reg_periksa_igd'); ?>
<!-- /.End Reg Periksa IGD Form -->

<!-- Modal Edit Reg Periksa Form -->
  <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_edit_periksa'); ?>
<!-- /.End Edit Reg Periksa Form-->

<!-- Modal Create SEP -->
  <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_create_sep'); ?>
<!-- /.End Create SEP -->

<!-- Modal Update SEP -->
  <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_update_sep'); ?>
<!-- /.End Update SEP -->

<!-- Modal Update Tanggal SEP -->
  <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_update_tglsep'); ?>
<!-- /.End Update Tanggal SEP -->

<!-- Modal Transfer Ranap -->
  <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_transfer_ranap'); ?>
<!-- /.End Transfer Ranap -->