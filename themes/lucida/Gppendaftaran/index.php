  <style type="text/css">
    #modalAddRegPeriksa ::-webkit-input-placeholder { /* Edge */
      color: white;
    } 
    .ui-datepicker-today {
    background: #fcc !important;
    }
    .ui-datepicker-current-day {
        background: #999 !important;
    }
  </style>
  <div id="main-content">
      <div class="container-fluid">
        <div class="block-header">
          <div class="row">
              <div class="col-lg-6 col-md-8 col-sm-12">
                  <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo isset($title) ? $title : ''; ?></h2>
                  <ul class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                      <li class="breadcrumb-item active"><?php echo $this->uri->segment(1); ?></li>
                  </ul>
              </div>
            </div>
        </div>

        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
              <div class="header">
                  <h2><?php echo isset($title) ? $title : ''; ?></h2>
              </div>

              <div class="body" style="margin-top: -30px;">
                <?php 
                  echo calrt();
                  $this->session->unset_userdata('msg');
                  $this->session->unset_userdata('error');
                ?>

                <div class="row clearfix">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="body">
                        <?php $this->site->get_template('coreTheme/btnAkses'); ?>                

                        <div class="row clearfix">
                          <div class="col-md-12">
                            <div class="table-responsive">

                              <!-- Start Area Form Search -->
                              <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_search'); ?>
                              <!-- End Area Form Search -->

                              <!-- Start Area Form Download -->
                              <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Form/form_download'); ?>
                              <!-- End Area Form Download -->

                              <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/Data/Tables'); ?>
                            </div>
                          </div>  
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-7 pull-left">
                    <div class="row">
                      <div class="col-md-2">
                        <?php echo select($limit_rows, 'name="limit" style="width:100px;" class="form-control"', true, $limit); ?>
                      </div>
                      <div class="col-md-10">
                        <div style="margin-top: 7px; margin-left: 5px;">
                          Memunculkan halaman <?php echo $offset; ?> sampai <?php echo $limit; ?> dari <?php echo $res_count; ?> data. 
                        </div>
                      </div>
                    </div> 
                  </div>
                  <div class="col-md-5">
                    <?php echo $pagination;?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/addForm'); ?>

      </div>
    </div>