<div class="modal fade" id="modalEditRegPeriksa" role="dialog" aria-labelledby="myModalLabelEdit1" aria-hidden="true">
  <div class="fade-left modal-lg modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabelEdit1">Formulir Tambah Baru <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
            
        <div class="row clearfix">
          <div class="col-sm-12">
              <div class="form-group">
                 <label>No RM/Nama Pasien</label>
                  <input type="hidden" name="no_rkm_medis" id="no_rkm_medis_regped" class="form-control l-blush text-white" placeholder="Nomor Rekam Medis">
                  <input type="text" class="form-control mdb-autocomplete l-blush text-white getpasien" placeholder="Pencarian minimal 3 karakter. By No RM/Nama Pasien" />
              </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>Tanggal Kunjungan</label>
              <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                  <input type="text" class="form-control" name="tanggal_periksa" id="tanggal_periksa_reged">
                  <div class="input-group-append">                                            
                      <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                  </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Poliklinik</label>
              <div id="list_poliklinik_ed">
                <?php  
                echo htmlSelectFromArray([], 'name="kd_poli" id="kd_poli_reged" class="form-control select2" style="width:100%;"', true);
                ?>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Dokter</label>
              <div id="list_dokter_booking_ed">
                <?php  
                echo htmlSelectFromArray([], 'name="kd_dokter" id="kd_dokter_reged" class="form-control input-lg select2" style="width:100%;"', true);
                ?>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Cara Bayar</label>
              <div id="list_cara_bayar_ed">
                <?php  
                echo htmlSelectFromArray(['UMU'=>'UMUM','BPJ'=>'JAMKESNAS'], 'name="kd_pj" id="kd_pj" class="form-control"', true);
                ?>
              </div>
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-sm-3">
            <div class="form-group">
              <label>Hubungan</label>
              <input type="text" name="hubunganpj" id="hubunganpjed" class="form-control">
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label>Png. Jawab</label>
              <input type="text" name="p_jawab" id="p_jawabed" class="form-control">
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label>Status</label>
              <input type="text" name="stts_daftar" id="stts_daftared" class="form-control">
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label>Asala Rujukan</label>
              <input type="text" name="asal_rujukan" id="asal_rujukaned" class="form-control">
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group">
              <label>Alamat Png. Jawab</label>
              <textarea class="form-control" rows="1" name="almt_pj" id="almt_pjed"></textarea>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn l-blue text-black pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save" style="display: none;">Save</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>