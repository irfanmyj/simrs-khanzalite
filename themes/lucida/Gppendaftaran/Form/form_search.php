<form method="post" action="<?php echo $url.'/10/0/search'; ?>">
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="collapse multi-collapse l-blush rounded" id="btnSearch">
      <div class="body" style="margin-top: 3px;">
        <div class="row clearfix">
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Pilih Poliklinik</label>
                  <?php echo htmlSelectFromArray($poliklinik, 'name="poliklinik" id="filter_poliklinik" style="width:100%;" class="form-control input-lg select2"', true,@$this->session->userdata('poliklinik'));?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" id="ajax_filter_dokter">
                  <label>Nama Dokter</label>
                  <?php echo htmlSelectFromArray($dokter, 'name="dokter" id="filter_dokter" style="width:100%;" class="form-control select2"', true,@$this->session->userdata('dokter'));?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group" id="ajax_filter_cara_bayar">
                  <label>Cara Bayar</label>
                    <?php echo htmlSelectFromArray(['UMU'=>'UMUM','BPJ'=>'JAMKESNANS'], 'name="cara_bayar" id="filter_cara_bayar" style="width:100%;" class="form-control select2"', true,@$this->session->userdata('cara_bayar'));?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                  <label>Nomor Rekam Medis</label>
                  <input type="text" name="no_rkm_medis" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Nomor Rekam Medis" value="<?php echo @$this->session->userdata('no_rkm_medis'); ?>">
                </div>
            </div>
        </div>
        <div class="row clearfix">
            
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Nama Pasien</label>
                  <input type="text" name="nm_pasien" class="form-control" placeholder="Nama pasien" value="<?php echo @$this->session->userdata('nm_pasien'); ?>">
                </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Tanggal Awal</label>
                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                    <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?php echo @$this->session->userdata('tgl_awal'); ?>">
                    <div class="input-group-append">                                            
                        <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label>Tanggal Akhir</label>
                <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                    <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?php echo @$this->session->userdata('tgl_akhir'); ?>">
                    <div class="input-group-append">                                            
                        <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control show-tick" name="jk">
                      <option value="" <?php echo (@$this->session->userdata('jk')=='')  ?' selected="selected"' : ''; ?>>- Jenis Kelamin -</option>
                      <option value="L" <?php echo (@$this->session->userdata('jk')=='L') ? ' selected="selected"' : ''; ?>>Laki-laki</option>
                      <option value="P" <?php echo (@$this->session->userdata('jk')=='P') ? ' selected="selected"' : ''; ?>>Perempuan</option>
                  </select>
                </div>
            </div>
            <div class="col-sm-12">
                <button type="submit" class="btn l-blush text-white">Proses</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
