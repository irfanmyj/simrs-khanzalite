<div class="modal fade" id="modalTransferRanap" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="fade-left modal-lg modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabel1">Formulir Transfer Kamar Inap</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="stts_pulang" value="-">
        <input type="hidden" name="status_lanjut" value="Ranap">
        <div class="row clearfix">
          <div class="col-md-4">
            <div class="form-group">
              <label>No Rawat</label>
              <input type="text" name="no_rawat" class="form-control" id="no_rawat_transferranap">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>No Rekam Medis</label>
              <input type="text" name="no_rkm_medis" class="form-control" id="no_rkm_medis_transferranap">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Nama Pasien</label>
              <input type="text" name="nm_pasien" class="form-control" id="nm_pasien_transferranap">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Tanggal</label>
              <input type="datetime-local" name="tgl_masuk" class="form-control">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Kamar Inap</label>
              <input type="hidden" name="kd_kamar" id="kd_kamar_inap" required="">
              <input type="text" id="kamarinap_trf" class="form-control mdb-autocomplete" placeholder="Pencarian minimal 3 digit.">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Diagnosa</label>
              <input type="hidden" name="diagnosa_awal" id="diagnosa_awal" required="">
              <input type="text" id="kodediagnosa_kamarinap" class="form-control mdb-autocomplete" placeholder="Pencarian minimal 3 digit.">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Hari</label>
              <input type="text" name="lama" id="lama_inap" class="form-control">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Biaya</label>
              <input type="text" name="trf_kamar" id="trf_kamar_inap" class="form-control">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Biaya</label>
              <input type="text" name="ttl_biaya" id="ttl_biaya_inap" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn l-blue text-black pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save" style="display: block;">Save</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>