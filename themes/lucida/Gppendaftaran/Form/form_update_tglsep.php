<div class="modal fade" id="modalTglsepSep" role="dialog" aria-labelledby="myModalLabelTglsep" aria-hidden="true">
  <div class="fade-left modal-lg modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabelTglsep">Formulir Pembuatan SEP BPJS</h4>
      </div>
      <div class="modal-body">
        
        <div class="row clearfix">
          <div class="col-md-6">
            <div class="form-group">
              <label>No SEP</label>
              <input type="text" name="no_sep" id="noseppulang" value="" class="form-control l-blush text-white">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Tanggal Pulang</label>
              <div class="input-group">
                  <input type="datetime-local" class="form-control" name="tglpulang" id="tglpulang_sep">
                  <div class="input-group-append">                                            
                      <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn l-blue text-black pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save" style="display: none;">Save</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: block;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>