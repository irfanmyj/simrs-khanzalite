<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="collapse multi-collapse bg-info rounded border-success" id="btnAddNew">
      <div class="body" style="margin-top: 3px;">
        <div class="row clearfix">
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Nomor Kartu</label>
                  <input type="text" name="nik" id="nik" class="form-control" placeholder="Masukan Nomor Kartu">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Jenis Kartu</label>
                  <?php echo htmlSelectFromArray($api, 'name="getapi" id="getapi" class="form-control"', true);?>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                   <label>Nomor RM</label>
                    <input type="text" name="no_rkm_medis" id="no_rkm_medis" class="form-control" placeholder="Nomor Rekam Medis" readonly="">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                   <label>Nama Pasien</label>
                    <input type="text" name="nm_pasien" id="nm_pasien" class="form-control" placeholder="Masukan Nama Pasien">
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control show-tick" name="jenis_kelamin" id="jenis_kelamin">
                      <option value="">- Jenis Kelamin -</option>
                      <option value="L">Laki-laki</option>
                      <option value="P">Perempuan</option>
                  </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Gol. Darah</label>
                  <?php echo htmlSelectFromArray($golongan_darah, 'name="gol_darah" id="gol_darah" class="form-control"', true);?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Tmp. Lahir</label>
                  <input type="text" name="tmp_lahir" id="tmp_lahir" class="form-control">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Tgl. Lahir</label>
                  <input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control" placeholder="Tanggal Registrasi">
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-sm-2">
                <div class="form-group">
                  <label>Usia Tahun</label>
                  <input type="text" name="usia_tahun" id="usia_tahun" class="form-control">
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                  <label>Usia Bulan</label>
                  <input type="text" name="usia_tahun" id="usia_bulan" class="form-control">
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                  <label>Usia Hari</label>
                  <input type="text" name="usia_tahun" id="usia_hari" class="form-control">
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                  <label>Pendidikan</label>
                  <?php echo htmlSelectFromArray($pendidikan, 'name="pnd" id="pnd" class="form-control"', true);?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                  <label>Nama Ibu</label>
                  <input type="text" name="nm_ibu" id="nm_ibu" class="form-control">
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-sm-2">
                <div class="form-group">
                  <label>Agama</label>
                  <?php echo htmlSelectFromArray($agama, 'name="agama" id="agama" class="form-control"', true);?>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                  <label>Status Nikah</label>
                  <?php echo htmlSelectFromArray($sts_nikah, 'name="sts_nikah" id="sts_nikah" class="form-control"', true);?>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                  <label>Asuransi/Penjamin</label>
                  <?php echo htmlSelectFromArray($penjab, 'name="penjab" id="penjab" class="form-control"', true);?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                  <label>Telp</label>
                  <input type="text" name="no_tlp" id="no_tlp" class="form-control">
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" name="email" id="email" class="form-control">
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-sm-3">
                <div class="form-group">
                  <label>Pekerjaan</label>
                  <input type="text" name="pekerjaan" id="pekerjaan" class="form-control">
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                  <label>No Peserta</label>
                  <input type="text" name="no_peserta" id="no_peserta" class="form-control">
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                  <label>Suku Bangsa</label>
                  <?php echo htmlSelectFromArray($sukubangsa, 'name="suku_bangsa" id="suku_bangsa" class="form-control"', true);?>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                  <label>Bahasa Dipakai</label>
                  <?php echo htmlSelectFromArray($bahasapasien, 'name="bahasa_pasien" id="bahasa_pasien" class="form-control"', true);?>
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                  <label>Cacat Fisik</label>
                  <?php echo htmlSelectFromArray($cacatfisik, 'name="cacat_fisik" id="cacat_fisik" class="form-control"', true);?>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-sm-3">
                <div class="form-group" id="form_propinsi">
                  <label>Provinsi</label>
                  <?php echo htmlSelectFromArray($provinsi, 'name="propinsi" id="propinsi" class="form-control" style="width:100%;"', true);?>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group" id="form_kab">
                  <label>Kabupaten</label>
                  <?php echo htmlSelectFromArray($kabupaten, 'name="kd_kab" id="kd_kab" class="form-control" style="width:100%;"', true);?>
                </div>
            </div>
        </div>

        <div class="row clearfix">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-body">Proses</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>