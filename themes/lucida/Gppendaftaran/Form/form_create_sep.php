<div class="modal fade" id="modalCreateSep" role="dialog" aria-labelledby="myModalLabelCreateSep" aria-hidden="true">
  <div class="fade-left modal-lg modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabelCreateSep">Formulir Pembuatan SEP BPJS</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">
        <input type="hidden" name="kodeppk" value="<?php echo $bpjs['kodeppk']; ?>">
        
        <div class="row clearfix">
          <div class="col-md-12" id="infobooking" style="display: none;">
            <table class="table table-striped table-bordered">
              <thead class="l-blush text-white">
                <tr>
                  <th style="width: 10px;">No</th>
                  <th style="width: 150px;">Parameter</th>
                  <th>Nilai</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1.</td>
                  <td>Kategori</td>
                  <td id="kategori_data_booking"></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>No Rujukan</td>
                  <td id="nilai_rujukan_booking"></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>No Rawat</label>
              <input type="text" name="no_rawat" id="norawat_sep" class="form-control">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>No Rm</label>
              <input type="text" name="no_rkm_medis" id="norkmmedis_sep" class="form-control">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Nama Pasien</label>
              <input type="text" name="nm_pasien" id="nmpasien_sep" class="form-control">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Tanggal Lahir</label>
              <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                  <input type="text" class="form-control" name="tgl_lahir" id="tgllahir_sep">
                  <div class="input-group-append">                                            
                      <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                  </div>
              </div>
            </div>
          </div>

        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>Peserta</label>
              <input type="text" name="pekerjaan" id="pekerjaan_sep" class="form-control">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Jenis Kelamin</label>
              <?php echo htmlSelectFromArray(['L'=>'Laki-laki','P'=>'Perempuan'], 'name="jk" id="jk_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>No Kartu</label>
              <input type="text" name="no_peserta" id="nopeserta_sep" class="form-control">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Status</label>
              <input type="text" name="status_kartu" id="statuskartu_sep" class="form-control">
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>No Rujukan (<i class="fa fa-asterisk text-danger"></i>)</label>
              <input type="text" name="no_rujukan" id="norujukan_sep" class="form-control" required="">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Kode Diagnosa (<i class="fa fa-asterisk text-danger"></i>)</label>
              <input type="hidden" name="kodediagnosa" id="kodediagnosa_sep" required="">
              <input type="hidden" name="namadiagnosa" id="namadiagnosa_sep" required="">
              <input type="text" id="kodediagnosa" class="form-control mdb-autocomplete" placeholder="Pencarian minimal 3 digit.">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>No. SKDP (<i class="fa fa-asterisk text-danger"></i>)</label>
              <input type="text" name="no_skdp" id="noskdp_sep" class="form-control" required="">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Tanggal Sep</label>
              <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                  <input type="text" class="form-control" name="tgl_sep" id="tglsep_sep">
                  <div class="input-group-append">                                            
                      <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                  </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>Tanggal Rujukan</label>
              <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                  <input type="text" class="form-control" name="tgl_rujukan" id="tglrujukan_sep">
                  <div class="input-group-append">                                            
                      <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                  </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>No Telepon</label>
              <input type="text" name="no_telp" id="notelp_sep" class="form-control">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Eksekutif</label>
              <?php echo htmlSelectFromArray($bpjs['eksekutif'], 'name="eksekutif" id="eksekutif_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Cob</label>
              <?php echo htmlSelectFromArray($bpjs['cob'], 'name="cob" id="cob_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Katarak</label>
              <?php echo htmlSelectFromArray($bpjs['katarak'], 'name="katarak" id="katarak_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-2">
            <div class="form-group">
              <label>Asal Rujukan</label>
              <?php echo htmlSelectFromArray($bpjs['asal_rujukan'], 'name="asal_rujukan" id="asalrujukan_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Jenis Pelayanan</label>
              <?php echo htmlSelectFromArray($bpjs['jnspelayanan'], 'name="jnspelayanan" id="jnspelayanan_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Kelas Pelayanan</label>
              <?php echo htmlSelectFromArray($bpjs['klsrawat'], 'name="klsrawat" id="klsrawat_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>PPK Rujukan</label>
              <input type="text" name="kodeppkrujukan" id="kodeppk_sep" class="form-control">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Nama Rujukan</label>
              <input type="text" name="namappkrujukan" id="namappk_sep" class="form-control">
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-4">
            <div class="form-group">
              <label>Poli Tujuan (<i class="fa fa-asterisk text-danger"></i>)</label>
              <input type="hidden" name="politujuan" id="politujuan_sep" required="">
              <input type="hidden" name="namapolitujuan" id="namapolitujuan_sep" required="">
              <input type="text" id="politujuan" class="form-control mdb-autocomplete" placeholder="Pencarian minimal 3 digit.">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Kode Poli Spesialis (<i class="fa fa-asterisk text-danger"></i>)</label>
              <input type="hidden" name="dokterdpjp" id="dokterdpjp_sep" required="">
              <input type="hidden" name="namadokterdpjp" id="namadokterdpjp_sep" required="">
              <input type="text" id="dokterdpjp" class="form-control mdb-autocomplete" placeholder="Pencarian minimal 3 digit.">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Catatan</label>
              <input type="text" name="catatan" id="catatan_sep" class="form-control">
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>Laka Lantas</label>
              <?php echo htmlSelectFromArray($bpjs['laka_lantas'], 'name="laka_lantas" id="lakalantas_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Tanggal KLL</label>
              <div class="input-group date" data-date-autoclose="true" data-provide="datepicker">
                  <input type="text" class="form-control" name="tgl_kll" id="tglkll_sep">
                  <div class="input-group-append">                                            
                      <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                  </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Penjamin Laka</label>
              <?php echo htmlSelectFromArray($bpjs['penjamin_lakalantas'], 'name="penjamin_lakalantas" id="penjaminlakalantas_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Keterangan</label>
              <input type="text" name="keterangan" id="keterangan_sep" class="form-control">
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-3">
            <div class="form-group">
              <label>Suplesi</label>
              <?php echo htmlSelectFromArray($bpjs['suplesi'], 'name="suplesi" id="suplesi_sep" style="width:100%;" class="form-control input-lg"', true);?>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>SEP. Suplesi</label>
              <input type="text" name="nosepsuplesi" id="nosepsuplesi_sep" class="form-control">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Propinsi</label>
              <input type="hidden" name="nmpropinsi" id="nmpropinsi_sep">
              <?php echo htmlSelectFromArray($provinsibpjs, 'name="kdpropinsi" id="kdpropinsi_sep" style="width:100%;" class="form-control input-lg select2"', true);?>
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-6">
            <div class="form-group">
              <label>Kabupaten</label>
              <input type="hidden" name="nmkabupaten" id="nmkabupaten_sep">
              <div id="kabsepbpjs">
                <?php echo htmlSelectFromArray([], 'name="kdkabupaten" id="kdkabupaten_sep" style="width:100%;" class="form-control input-lg select2"', true);?>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label>Kecamatan</label>
              <input type="hidden" name="nmkecamatan" id="nmkecamatan_sep">
              <div id="kecsepbpjs">
                <?php echo htmlSelectFromArray([], 'name="kdkecamatan" id="kdkecamatan_sep" style="width:100%;" class="form-control input-lg select2"', true);?>
              </div>
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-md-12">
            <blockquote>
              <h5>Keterangan : </h5>
              <p class="blockquote blockquote-info l-blush text-white">
              1. (<i class="fa fa-asterisk text-danger"></i>) : Wajib DI Isi. <br>
              </p>
            </blockquote>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn l-blue text-black pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save" style="display: none;">Save</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>