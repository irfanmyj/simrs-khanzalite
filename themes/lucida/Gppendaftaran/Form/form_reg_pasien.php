<!-- Modal Add Form -->
<div class="modal fade" id="modalAddForm" role="dialog" aria-labelledby="myModalLabelAdd" aria-hidden="true">
  <div class="fade-left modal-xxl modal-dialog" role="document">
    <div class="modal-content">
      <div id="btn_cepat" style="position: fixed; z-index: 3; margin: 310px 0 0 1022px; height: 0px; width: 30px;">
        <button type="submit" class="btn l-khaki text-dark" style="border-radius: 0px;" id="simpan_pengkajian"><i class="fa fa-save"></i></button>
        <button type="button" class="btn l-amper text-black" style="border-radius: 0px;" id="reset"><i class="fa fa-trash"></i></button>
        <button type="button" class="btn l-coral text-black" style="border-radius: 0px;" data-dismiss="modal"><i class="fa fa-close"></i></button>
      </div>
      <div class="modal-header l-blush text-white">
        <h4 class="modal-title" id="myModalLabelAdd">Formulir Tambah Baru <?php echo ($title) ? $title : ''; ?></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id">

        <div class="row clearfix" id="info_bpjs" style="display: none;">
          <div class="col-sm-12">
            <table class="table table-striped">
              <thead class="l-blush text-white">
                <tr>
                  <th style="width: 5px;">No</th>
                  <th style="width: 100px;">Parameter</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1.</td>
                  <td>Nama</td>
                  <td id="bpj_nm"></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Status</td>
                  <td id="sts_keterangan"></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>Hak Kelas</td>
                  <td id="sts_hakKelas"></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-sm-2">
              <div class="form-group">
                 <label>Nomor RM</label>
                  <input type="text" name="no_rkm_medis" id="no_rkm_medis" class="form-control" placeholder="Nomor Rekam Medis" readonly="">
              </div>
          </div>

          <div class="col-sm-4" id="GetDataApi">
            <div class="form-group">
              <label>Nomor Kartu</label>
              <div class="input-group">
                  <div class="custom-file" id="addNik">
                      <input type="text" name="nik" id="nik" class="form-control" placeholder="Masukan Nomor Kartu" style="border-radius:0px;" required="">
                  </div>
                  <div class="input-group-append">
                      <button class="btn l-khaki" id="getdukcapil" type="button"><i class="fa fa-check-circle-o text-dark"></i></button>
                      <button class="btn l-khaki" id="getbpjs" type="button"><i class="fa fa-bold text-dark"></i></button>
                  </div>
              </div>
            </div>
          </div>
          
          <div class="col-sm-3">
              <div class="form-group">
                 <label>Nama Pasien</label>
                  <input type="text" name="nm_pasien" id="nm_pasien" class="form-control" placeholder="Masukan Nama Pasien" required="">
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group">
                <label>Tmp. Lahir</label>
                <input type="text" name="tmp_lahir" id="tmp_lahir" class="form-control" required="">
              </div>
          </div>
        </div>

        <div class="row clearfix">
          
          <div class="col-sm-2">
              <div class="form-group">
                <label>Tgl. Lahir</label>
                <input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control" placeholder="Tanggal Registrasi" required="">
              </div>
          </div>

          <div class="col-sm-2">
              <div class="form-group">
                <label>Jenis Kelamin</label>
                <select class="form-control show-tick" name="jk" id="jenis_kelamin" required="">
                    <option value="">- Jenis Kelamin -</option>
                    <option value="L">Laki-laki</option>
                    <option value="P">Perempuan</option>
                </select>
              </div>
          </div>
          <div class="col-sm-2">
              <div class="form-group">
                <label>Gol. Darah</label>
                <?php echo htmlSelectFromArray($golongan_darah, 'name="gol_darah" id="gol_darah" class="form-control"', true);?>
              </div>
          </div>

          <div class="col-sm-2">
              <div class="form-group">
                <label>Asuransi</label>
                <?php echo htmlSelectFromArray($penjab, 'name="kd_pj" id="kd_pj" class="form-control"', true);?>
              </div>
          </div>

          <div class="col-sm-4" id="GetDataAsuransi">
              <div class="form-group">
                <label>No Peserta</label>
                <div class="input-group">
                  <div class="custom-file" id="addNoPeserta">
                      <input type="text" name="no_peserta" id="no_peserta" class="form-control" placeholder="Masukan Peserta" style="border-radius:0px;">
                  </div>
                  <div class="input-group-append" id="displayGetVclaim">
                      <button class="btn l-khaki" id="getVclaim" type="button" style="display: none;"><i class="fa fa-check-circle-o text-dark"></i></button>
                  </div>
                </div>
              </div>
          </div>  
          
        </div>

        <div class="row clearfix">
          <div class="col-sm-3">
              <div class="form-group" id="form_propinsi">
                <label>Provinsi</label>
                <?php echo htmlSelectFromArray($provinsi, 'name="kd_prop" id="kd_prop" class="form-control select2" style="width:100%;" required=""', true);?>
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group" id="form_kab">
                <label>Kabupaten</label>
                <div id="addKab">
                  <?php 
                    echo htmlSelectFromArray([], 'name="kd_kab" id="kd_kab" class="form-control select2" style="width:100%;"', true);
                  ?>
                </div>
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group" id="form_kec">
                <label>Kecamatan</label>
                <div id="addKec">
                <?php echo htmlSelectFromArray([], 'name="kd_kec" id="kd_kec" class="form-control select2" style="width:100%;"', true);?>
                </div>
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group" id="form_kel">
                <label>Kelurahan</label>
                <div id="addKel">
                <?php echo htmlSelectFromArray([], 'name="kd_kel" id="kd_kel" class="form-control select2" style="width:100%;"', true);?>
                </div>
              </div>
          </div>
        </div>

        <div class="row clearfix">
        
          <div class="col-sm-3">
              <div class="form-group">
                <label>Nama Ibu</label>
                <input type="text" name="nm_ibu" id="nm_ibu" class="form-control" required="">
              </div>
          </div>
          
          <div class="col-sm-3">
              <div class="form-group">
                <label>Pekerjaan</label>
                <input type="text" name="pekerjaan" id="pekerjaan" class="form-control">
              </div>
          </div>

          <div class="col-sm-2">
              <div class="form-group">
                <label>Pendidikan</label>
                <?php echo htmlSelectFromArray($pendidikan, 'name="pnd" id="pnd" class="form-control"');?>
              </div>
          </div>

          <div class="col-sm-2">
              <div class="form-group">
                <label>Telp</label>
                <input type="text" name="no_tlp" id="no_tlp" class="form-control">
              </div>
          </div>

          <div class="col-sm-2">
              <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" id="email" class="form-control">
              </div>
          </div> 
        </div>

        <div class="row clearfix">
          <div class="col-sm-2">
              <div class="form-group">
                <label>Suku Bangsa</label>
                <?php echo htmlSelectFromArray($sukubangsa, 'name="suku_bangsa" id="suku_bangsa" class="form-control"');?>
              </div>
          </div>

          <div class="col-sm-2">
              <div class="form-group">
                <label>Bahasa</label>
                <?php echo htmlSelectFromArray($bahasapasien, 'name="bahasa_pasien" id="bahasa_pasien" class="form-control"');?>
              </div>
          </div>

          <div class="col-sm-2">
              <div class="form-group">
                <label>Cacat Fisik</label>
                <?php echo htmlSelectFromArray($cacatfisik, 'name="cacat_fisik" id="cacat_fisik" class="form-control"');?>
              </div>
          </div>

          <div class="col-sm-6">
              <div class="form-group">
                <label>Alamat</label>
                <textarea class="form-control" name="alamat" id="alamat" rows="1"></textarea>
              </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-sm-3">
              <div class="form-group">
                <label>Intansi Pasien</label>
                <?php echo htmlSelectFromArray(['-'=>'-'], 'name="perusahaan_pasien" id="perusahaan_pasien" class="form-control"');?>
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group">
                <label>NIP/NRP</label>
                <input type="text" name="nip" id="nip" class="form-control">
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group">
                <label>Kategori Penanggung Jawab</label>
                <?php echo htmlSelectFromArray($status_family, 'name="keluarga" id="keluarga" class="form-control"');?>
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group">
                <label>Penanggung Jawab</label>
                <input type="text" name="namakeluarga" id="namakeluarga" class="form-control">
              </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-sm-3">
              <div class="form-group" id="form_propinsi_png">
                <label>Provinsi P.J</label>
                <?php echo htmlSelectFromArray($provinsi, 'name="propinsipj" id="propinsipj" class="form-control select2" style="width:100%;"');?>
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group" id="form_kab_png">
                <label>Kabupaten P.J</label>
                <div id="addKab_png">
                  <?php 
                    echo htmlSelectFromArray([], 'name="kabupatenpj" id="kabupatenpj" class="form-control select2" style="width:100%;"', true);
                  ?>
                </div>
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group" id="form_kec_png">
                <label>Kecamatan P.J</label>
                <div id="addKec_png">
                <?php echo htmlSelectFromArray([], 'name="kecamatanpj" id="kecamatanpj" class="form-control select2" style="width:100%;"', true);?>
                </div>
              </div>
          </div>

          <div class="col-sm-3">
              <div class="form-group" id="form_kel_png">
                <label>Kelurahan P.J</label>
                <div id="addKel_png">
                <?php echo htmlSelectFromArray([], 'name="kelurahanpj" id="kelurahanpj" class="form-control select2" style="width:100%;"', true);?>
                </div>
              </div>
          </div>

          <div class="col-sm-12">
            <label>Alamat P.J</label>
            <textarea class="form-control" rows="1" name="alamatpj" id="alamatpj"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn l-amper text-black pull-left" id="reset">Reset</button>
        <button type="button" class="btn l-blue text-black pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white l-blush pull-right" id="save" style="display: none;">Save</button>
        <button type="button" class="btn text-white l-blush pull-right" id="update" style="display: none;">Update</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.Modal Form -->