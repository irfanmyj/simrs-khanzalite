  <div id="main-content">
      <div class="container-fluid">
        <div class="block-header">
          <div class="row">
              <div class="col-lg-6 col-md-8 col-sm-12">
                  <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> <?php echo isset($title) ? $title : ''; ?></h2>
                  <ul class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                      <li class="breadcrumb-item active"><?php echo $this->uri->segment(1); ?></li>
                  </ul>
              </div>
            </div>
        </div>

        <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
              <div class="header">
                  <h2><?php echo isset($title) ? $title : ''; ?></h2>
              </div>

              <div class="body">
                <?php 
                  echo calrt();
                  $this->session->unset_userdata('msg');
                  $this->session->unset_userdata('error');
                  if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y') {
                    $addClass = 'tr_mod';
                  }
                ?>

                <div class="row">
                  <div class="col-md-4">
                    <?php if(@acl(decrypt_aes($this->uri->segment(2)))['add']=='Y') { ?>
                      <button class="btn btn-info col-md-5" onclick="formAdd()"><i class="fa fa-plus"></i> Tambah Data</button>
                    <?php } ?>
                  
                    <?php if(@acl(decrypt_aes($this->uri->segment(2)))['delete']=='Y') { ?>
                      <button class="btn btn-danger col-md-5" disabled="true" id="btnDelete" onclick="formDelete()"><i class="fa fa-trash-o"></i> Hapus Data</button>
                    <?php } ?>
                  </div>
                  <div class="col-md-5"></div>
                  <div class="col-md-3">
                    
                  </div>
                </div>

                <div class="row clearfix">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="body">
                        <div class="table-responsive">
                          <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="table">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>No Rekam Medis</th>
                                <th>Nama Pasien</th>
                                <th>Dokter</th>
                                <th>Poliklinik</th>
                                <th>Cara Bayar</th>
                                <th>Dibuat Tanggal</th>
                                <th>Aksi</th>
                              </tr>
                            </thead> 
                            
                            <tbody>
                            
                            </tbody>          
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <?php $this->site->get_template(ucwords($this->uri->segment(1)).'/addForm'); ?>

      </div>
    </div>

  