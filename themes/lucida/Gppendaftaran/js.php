<script src="<?php echo base_url('vendor/lucida/');?>assets/vendor/toastr/toastr.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var offset	= '<?php echo $offset;?>';
		var limit = '<?php echo $limit;?>';
		var urls = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		
		$('.dropdown-submenu a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
		});

		$('#tanggal_periksa_reg').datepicker({
	        todayHighlight: true
	    });

		// Untuk mengaktifkan tombol hapus.
		$('[name="id[]"]').click(function () {
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

		// Fitur untuk menampilkan data berdasarkan limit.
		$('[name="limit"]').change(function(){
			var limit = $('[name="limit"]').val();
			var method = '<?php echo $this->uri->segment(6); ?>';
			if(limit)
			{
				offset = 1;
				if(method=='search'){
					window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset+'/'+method;
				}else{
					window.location = '<?php echo $url.$search.'/';?>'+limit+'/'+offset;	
				}
				
			}
		});

		// Add datas to table database
		$('#modalAddForm').on('click','#save',function(){
			var data_post = $('#modalAddForm :input').serialize();
			var kd_prop = $('#form_propinsi [name="kd_prop"]').val();
			var kd_kab = $('#form_kab [name="kd_kab"]').val();
			var kd_kec = $('#form_kec [name="kd_kec"]').val();
			var kd_kel = $('#form_kel [name="kd_kel"]').val();
			var datas = '&kd_prop='+kd_prop+'&kd_kab='+kd_kab+'&kd_kec='+kd_kec+'&kd_kel='+kd_kel;
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			if($('#modalAddForm [name="nik"]').val()!='' && $('#modalAddForm [name="tgl_lahir"]').val()!=''){
				$.ajax({
			        url: url+'/store',
			        type: "post",
			        data: data_post+'&url='+url+datas,
			        success: function (response) {
			        	var dt = JSON.parse(response);
			        	if(dt.error==''){
			        		toastr_info(dt.msg);
			        		formRegPeriksa();
			        		$('#modalAddForm').modal('toggle');
			        		$('div').remove('#btn_cepat');
			        		$('#modalAddRegPeriksa #info_pendaftaran').css('display','block');
			        		$('#modalAddRegPeriksa [name="no_rkm_medis"]').val(dt.no_rkm_medis);
			        		$('#modalAddRegPeriksa [name="no_rkm_medis"]').val(dt.no_rkm_medis);
			        		$('#modalAddRegPeriksa #hubunganpj').val(dt.keluarga);
			        		$('#modalAddRegPeriksa #p_jawab').val(dt.namakeluarga);
			        		$('#modalAddRegPeriksa #stts_daftar').val(dt.stts_daftar);
			        		$('#modalAddRegPeriksa #almt_pj').val(dt.alamatpj);
			        		$('#modalAddRegPeriksa [name="tgl_lahir"]').val(dt.tgl_lahir);
			        		$('#info_pendaftaran #nm_pasien_reg').html(dt.nm_pasien);
			        		$('#info_pendaftaran #norm_reg').html(dt.no_rkm_medis);
			        	}else{
			        		$('#modalAddRegPeriksa #info_pendaftaran').css('display','none');
			        		toastr_msg(dt.msg);
			        	}
			        	//window.location = dt.url;
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}else{
				toastr_msg('Silahkan isi no ktp anda.');
			}
		});

		$('#modalAddForm').on('click','#simpan_pengkajian',function(){
			var data_post = $('#modalAddForm :input').serialize();
			var kd_prop = $('#form_propinsi [name="kd_prop"]').val();
			var kd_kab = $('#form_kab [name="kd_kab"]').val();
			var kd_kec = $('#form_kec [name="kd_kec"]').val();
			var kd_kel = $('#form_kel [name="kd_kel"]').val();
			var datas = '&kd_prop='+kd_prop+'&kd_kab='+kd_kab+'&kd_kec='+kd_kec+'&kd_kel='+kd_kel;
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			if($('#modalAddForm [name="nik"]').val()!=''){
				$.ajax({
			        url: url+'/store',
			        type: "post",
			        data: data_post+'&url='+url+datas,
			        success: function (response) {
			        	var dt = JSON.parse(response);
			        	if(dt.error==''){
			        		toastr_info(dt.msg);
			        		formRegPeriksa();
			        		$('#modalAddForm').modal('toggle');
			        		$('div').remove('#btn_cepat');
			        		$('#modalAddRegPeriksa #info_pendaftaran').css('display','block');
			        		$('#modalAddRegPeriksa [name="no_rkm_medis"]').val(dt.no_rkm_medis);
			        		$('#modalAddRegPeriksa [name="no_rkm_medis"]').val(dt.no_rkm_medis);
			        		$('#modalAddRegPeriksa #hubunganpj').val(dt.keluarga);
			        		$('#modalAddRegPeriksa #p_jawab').val(dt.namakeluarga);
			        		$('#modalAddRegPeriksa #stts_daftar').val(dt.stts_daftar);
			        		$('#modalAddRegPeriksa #almt_pj').val(dt.alamatpj);
			        		$('#info_pendaftaran #nm_pasien_reg').html(dt.nm_pasien);
			        		$('#info_pendaftaran #norm_reg').html(dt.no_rkm_medis);
			        		$('#modalAddRegPeriksa [name="tgl_lahir"]').val(dt.tgl_lahir);
			        	}else{
			        		$('#modalAddRegPeriksa #info_pendaftaran').css('display','none');
			        		toastr_msg(dt.msg);
			        	}
			        	//window.location = dt.url;
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}else{
				toastr_msg('Silahkan isi no ktp anda.');
			}
		});

		$('#modalAddForm').on('click','#reset',function(){
			$('#modalAddForm :input').val('');
			$('#modalAddForm [name="kd_prop"]').val('');
			$('#modalAddForm [name="kd_prop"]').select2({width:'100%'});
			$('#modalAddForm [name="kd_kab"]').val('');
			$('#modalAddForm [name="kd_kab"]').select2({width:'100%'});
			$('#modalAddForm [name="kd_kec"]').val('');
			$('#modalAddForm [name="kd_kec"]').select2({width:'100%'});
			$('#modalAddForm [name="kd_kel"]').val('');
			$('#modalAddForm [name="kd_kel"]').select2({width:'100%'});
			$('#modalAddForm [name="propinsipj"]').val('');
			$('#modalAddForm [name="propinsipj"]').select2({width:'100%'});
			$('#modalAddForm [name="kabupatenpj"]').val('');
			$('#modalAddForm [name="kabupatenpj"]').select2({width:'100%'});
			$('#modalAddForm [name="kecamatanpj"]').val('');
			$('#modalAddForm [name="kecamatanpj"]').select2({width:'100%'});
			$('#modalAddForm [name="kelurahanpj"]').val('');
			$('#modalAddForm [name="kelurahanpj"]').select2({width:'100%'});
		});

		/*
		Area Modal Add Reg Pasien
		*/		
		$('#addNik').on('change','#nik',function(){
			var nik = $(this).val();
			$.ajax({
				type : 'post',
				url : urls+'/checkNik',
				data : 'nik='+nik,
				success : function(res){
					var dt = JSON.parse(res);
					if(dt.no_ktp!=''){
						$('#nik [name="nik"]').css('background','red');
						$('#save').css('display','none');
						toastr_msg('Nomor nik yang anda masukan sudah terdaftar.');
						
					}else{
						$('#nik [name="nik"]').css('background','white');
						$('#save').css('display','block');
						toastr_info('Nomor nik yang anda masukan belum terdaftar.');
					}
				}
			});
		});

		$('#GetDataApi').on('click','#getdukcapil',function(){
			var nik = $('#GetDataApi [name="nik"]').val();
			if(nik!=''){
				var getapi = 'dukcapil';
				$.ajax({
					type	: 'post',
					url 	: "<?php echo $url.'/10/1/getapi';?>",
					data 	: 'nik='+nik+'&getapi='+getapi,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.status!='expired'){
							$('#nm_pasien').val(dt.api['nm_pasien']);
							$('#jenis_kelamin').val(dt.api['jk']);
							$('#gol_darah').val(dt.api['gol_darah']);
							$('#tmp_lahir').val(dt.api['tmp_lahir']);
							$('#tgl_lahir').val(dt.api['tgl_lahir']);
							$('#sts_nikah').val(dt.api['sts_nikah']);
							$('#nm_ibu').val(dt.api['nm_ibu']);
							$('#agama').val(dt.api['agama']);
							$('#pekerjaan').val(dt.api['pekerjaan']);
							$('#alamat').val(dt.api['alamat']);
							$('#kd_prop').val(dt.api['provinsi']);
							$('#form_propinsi [name="kd_prop"]').select2({width:'100%'}).trigger('change');
							$('#kd_kab').html(dt.api['kab']);
							$('#form_kab [name="kd_kab"]').select2({width:'100%'}).trigger('change');
							$('#suku_bangsa').val(dt.api['suku_bangsa']);
							$('#bahasa_pasien').val(dt.api['bahasa_pasien']);
							$('#cacat_fisik').val(dt.api['cacat_fisik']);
							$('#penjab').val(dt.api['penjab']);
						}else{
							toastr_msg(dt.data);
						}
						
					}
				});
			}else{
				toastr_msg('Silahkan isi no kartu terlebih dahulu.');
			}
		});

		$('#GetDataApi').on('click','#getbpjs',function(){
			var nik = $('#GetDataApi [name="nik"]').val();
			if(nik!=''){
				var getapi = 'bpjs';
				$.ajax({
					type	: 'post',
					url 	: "<?php echo $url.'/10/1/getapi';?>",
					data 	: 'nik='+nik+'&getapi='+getapi,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.status!='expired'){
							$('#nm_pasien').val(dt.api['nm_pasien']);
							$('#no_peserta').val(dt.api['no_peserta']);
							$('#jenis_kelamin').val(dt.api['jk']);
							$('#no_telp').val(dt.api['no_telp']);
							$('#gol_darah').val(dt.api['gol_darah']);
							$('#tgl_lahir').val(dt.api['tgl_lahir']);
							$('#sts_nikah').val(dt.api['sts_nikah']);
							$('#pekerjaan').val(dt.api['pekerjaan']);
							$('#suku_bangsa').val(dt.api['suku_bangsa']);
							$('#bahasa_pasien').val(dt.api['bahasa_pasien']);
							$('#cacat_fisik').val(dt.api['cacat_fisik']);
							$('#penjab').val(dt.api['penjab']);
							$('#kd_pj').val(dt.api['kd_pj']);
							if(dt.api['nm_pasien']!=''){
								$('#info_bpjs').css('display','block');
								$('#info_bpjs #bpj_nm').html(dt.api['nm_pasien']);
								$('#info_bpjs #sts_hakKelas').html(dt.api['hakKelasKet']);
								if(dt.api['stskode']==27){
									$('#info_bpjs #sts_keterangan').css('background','red');
									$('#info_bpjs #sts_keterangan').css('color','white');
									$('#info_bpjs #sts_keterangan').html(dt.api['stsketerangan']);
									setTimeout(function() { $("#info_bpjs").hide(); }, 7000);
								}else{
									$('#info_bpjs #sts_keterangan').css('background','white');
									$('#info_bpjs #sts_keterangan').css('color','#5a5a5a');
									$('#info_bpjs #sts_keterangan').html(dt.api['stsketerangan']);
									setTimeout(function() { $("#info_bpjs").hide(); }, 7000);
								}
								
							}else{
								$('#info_bpjs').css('display','none');
							}
						}else{
							toastr_msg(dt.data);
						}
						
					}
				});
			}else{
				toastr_msg('Silahkan isi no kartu terlebih dahulu.');
			}
		});

		$('#kd_pj').change(function(){
			var kd_pj = $(this).val();
			if(kd_pj=='BPJ'){
				$('#GetDataAsuransi #getVclaim').css('display','block');
			}else{
				$('#GetDataAsuransi #getVclaim').css('display','none');
			}
		});

		$('#GetDataAsuransi').on('click','#getVclaim',function(){
			var kd_pj = $('#kd_pj').val();
			var no_peserta = $('#addNoPeserta [name="no_peserta"]').val();
			if(kd_pj=='BPJ' && no_peserta!=''){
				$.ajax({
					type : 'post',
					url : urls+'/getInfoKartu',
					data : 'kd_pj='+kd_pj+'&no_peserta='+no_peserta,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.stskode==27){
							toastr_info(dt.info);
						}else{
							toastr_msg(dt.info);
						}
					}
				});
			}else{
				toastr_msg('Silahkan pilih nama asuransi dan isi no peserta');
			}
		});

		$('#form_propinsi').on('change','#kd_prop',function(){
			var kd_prop = $(this).val();
			$.ajax({
				type : 'POST',
				url  : urls+'/get_kab',
				data : 'kd_prop='+kd_prop,
				success : function(res){
					var dt = JSON.parse(res);
					$('#addKab').html(dt.data);
					$('#kd_kab').select2();
				}
			});
		});

		$('#form_kab').on('change','#kd_kab',function(){
			var kd_kab = $(this).val();
			$.ajax({
				type : 'POST',
				url  : urls+'/get_kec',
				data : 'kd_kab='+kd_kab,
				success : function(res){
					var dt = JSON.parse(res);
					$('#addKec').html(dt.data);
					$('#kd_kec').select2();
				}
			});
		});

		$('#form_kec').on('change','#kd_kec',function(){
			var kd_kec = $(this).val();
			$.ajax({
				type : 'POST',
				url  : urls+'/get_kel',
				data : 'kd_kec='+kd_kec,
				success : function(res){
					var dt = JSON.parse(res);
					$('#addKel').html(dt.data);
					$('#kd_kel').select2();
				}
			});
		});

		$('#form_propinsi_png').on('change','#propinsipj',function(){
			var kd_prop = $(this).val();
			$.ajax({
				type : 'POST',
				url  : urls+'/get_kab',
				data : 'kd_prop='+kd_prop+'&pj=pj',
				success : function(res){
					var dt = JSON.parse(res);
					$('#addKab_png').html(dt.data);
					$('#kabupatenpj').select2();
				}
			});
		});

		$('#form_kab_png').on('change','#kabupatenpj',function(){
			var kd_kab = $(this).val();
			$.ajax({
				type : 'POST',
				url  : urls+'/get_kec',
				data : 'kd_kab='+kd_kab+'&pj=pj',
				success : function(res){
					var dt = JSON.parse(res);
					$('#addKec_png').html(dt.data);
					$('#kecamatanpj').select2();
				}
			});
		});

		$('#form_kec_png').on('change','#kecamatanpj',function(){
			var kd_kec = $(this).val();
			$.ajax({
				type : 'POST',
				url  : urls+'/get_kel',
				data : 'kd_kec='+kd_kec+'&pj=pj',
				success : function(res){
					var dt = JSON.parse(res);
					$('#addKel_png').html(dt.data);
					$('#kelurahanpj').select2();
				}
			});
		});

		/*
		Area Modal Add Reg Periksa
		*/

		$(".getpasien").autocomplete({
	        source: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getpasien';?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	event.preventDefault();
	        	$('#no_rkm_medis_regp').val(ui.item.id);
	        	$('#hubunganpj').val(ui.item.keluarga);
	        	$('#p_jawab').val(ui.item.namakeluarga);
	        	$('#almt_pj').val(ui.item.alamatpj);
	        	$('#stts_daftar').val(ui.item.stts_daftar);
	        	$('#modalAddRegPeriksa [name="tgl_lahir"]').val(ui.item.tgl_lahir);
	        	$('#modalAddRegPeriksa #kd_pj').val(ui.item.kd_pj);
				$('#modalAddRegPeriksa #kd_pj').select2({width:'100%'});
				$('#modalAddRegPeriksa #kd_dokter').val('');
				$('#modalAddRegPeriksa #kd_dokter').select2({width:'100%'});
				$('#modalAddRegPeriksa #kd_poli').val('');
				$('#modalAddRegPeriksa #kd_poli').select2({width:'100%'});

				/*
				IGD
				*/
				$('#no_rkm_medis_regigd').val(ui.item.id);
				$('#hubunganpjigd').val(ui.item.keluarga);
	        	$('#p_jawabigd').val(ui.item.namakeluarga);
	        	$('#almt_pjigd').val(ui.item.alamatpj);
	        	$('#stts_daftarigd').val(ui.item.stts_daftar);
				$('#modalAddRegPeriksaIgd [name="tgl_lahir"]').val(ui.item.tgl_lahir);
	        	$('#modalAddRegPeriksaIgd #kd_pj').val(ui.item.kd_pj);
				$('#modalAddRegPeriksaIgd #kd_pj').select2({width:'100%'});
	        }
	    });

	    $('#modalAddRegPeriksa').on('change','#tanggal_periksa_reg',function(){
	    	var tanggal_periksa_reg = $(this).val();
	    	var no_rkm_medis = $('#no_rkm_medis_regp').val();
	    	if(tanggal_periksa_reg!='' && no_rkm_medis!=''){
	    		$.ajax({
	    			type : 'post',
	    			data : 'tanggal_periksa='+tanggal_periksa_reg+'&no_rkm_medis='+no_rkm_medis,
	    			url : urls+'/get_validasi',
	    			success : function(res){
	    				var dt = JSON.parse(res);
	    				if(dt.error==2){
	    					$('#modalAddRegPeriksa #list_poliklinik').html(dt.msg);
	    					$('#modalAddRegPeriksa #kd_poli_reg').select2();

	    				}else{
	    					toastr_msg(dt.msg);
	    					$('#modalAddRegPeriksa #tanggal_periksa_reg').val('');
	    					$('#modalAddRegPeriksa #kd_poli_reg').val('');
	    					$('#modalAddRegPeriksa #kd_poli_reg').select2({width:'100%'});
	    				}
	    			}
	    		});
	    	}else if(no_rkm_medis==''){
	    		$('#modalAddRegPeriksa #tanggal_periksa_reg').val('');
	    		toastr_msg('Silahkan isi dan cari nama pasien di formulir no rekam media.');
	    	}else{
	    		$('#modalAddRegPeriksa #tanggal_periksa_reg').val('');
	    		toastr_msg('Tanggal kunjungan harus diisi.');
	    	}
	    });

	    $('#list_poliklinik').on('change','#kd_poli_reg',function(){
	    	var kd_poli = $('#kd_poli_reg').val();
	    	var tanggal_periksa = $('#tanggal_periksa_reg').val();
	    	if(kd_poli!='' & tanggal_periksa!=''){
	    		$.ajax({
	    			type : 'post',
	    			url : urls+'/get_dokter',
	    			data : 'kd_poli='+kd_poli+'&tanggal_periksa='+tanggal_periksa,
	    			success :function(res){
	    				var dt = JSON.parse(res);
	    				if(dt.status=='Tersedia'){
	    					$('#list_dokter_booking').html(dt.data);
	    					$('#list_dokter_booking [name="kd_dokter"]').select2();
	    					$('#modalAddRegPeriksa #save').css('display','block');
	    				}else{
	    					toastr_msg(dt.data);
	    				}
	    			}
	    		});
	    	}else{
	    		toastr_msg('Poliklinik dan Tanggal Periksa tidak boleh kosong.');
	    	}
	    });

	    $('#modalAddRegPeriksa').on('click','#save',function(){
	    	var tgl_lahir = $('#modalAddRegPeriksa [nama="tgl_lahir"]').val();
			var data_post = $('#modalAddRegPeriksa :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			if(tgl_lahir!=''){
				$.ajax({
			        url: url+'/storepasien',
			        type: "post",
			        data: data_post+'&url='+url,
			        success: function (response) {
			        	var dt = JSON.parse(response);
			        	if(dt.error==''){
			        		window.location = dt.url;
			        		toastr_info(dt.msg);
			        	}else{
			        		toastr_msg(dt.msg);
			        	}
			        	//
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}else{
				toastr_msg('Mohon maaf system tidak bisa menyimpan data pasien ini.');
			}
			
		});

	    $('.tr_mod_reg_periksa').click(function(){
	    	var dt = $(this).parent();
	    	var dateFormat = $.datepicker.formatDate('m/dd/yy', new Date(dt.data('tgl_registrasi')));
	    	$('#modalEditRegPeriksa #no_rkm_medis_regped').val(dt.data('no_rkm_medis'));
	    	$('#modalEditRegPeriksa .getpasien').val(dt.data('no_rkm_medis'));
	    	$('#modalEditRegPeriksa #tanggal_periksa_reged').val(dateFormat);
	    	$('#list_cara_bayar_ed [name="kd_pj"]').val(dt.data('kd_pj'));
	    	$('#modalEditRegPeriksa #hubunganpjed').val(dt.data('hubunganpj'));
	    	$('#modalEditRegPeriksa #p_jawabed').val(dt.data('p_jawab'));
	    	$('#modalEditRegPeriksa #stts_daftared').val(dt.data('stts_daftar'));
	    	$('#modalEditRegPeriksa #almt_pjed').val(dt.data('almt_pj'));

	    	$('#modalEditRegPeriksa #save').css('display','none');
	    	$('#modalEditRegPeriksa #update').css('display','block');

	    	if(dt.data('tgl_registrasi')){
	    		$.ajax({
	    			type : 'post',
	    			data : 'tanggal_periksa='+dt.data('tgl_registrasi'),
	    			url : urls+'/get_poli',
	    			success : function(res){
	    				var dts = JSON.parse(res);
	    				if(dts.status=='Tersedia'){
	    					$('#modalEditRegPeriksa #list_poliklinik_ed').html(dts.data);
	    					$('#modalEditRegPeriksa [name="id"]').val(dt.data('no_rawat'));
	    					//$('#modalAddRegPeriksa [name="id"]').val(dt.data('tgl_registrasi'));
	    					$('#modalEditRegPeriksa [name="kd_poli"]').val(dt.data('kd_poli'));
	    					$('#modalEditRegPeriksa #kd_poli_reg').select2({width:'100%'}).trigger('change');
	    					if(dt.data('kd_poli')){
	    						$.ajax({
					    			type : 'post',
					    			url : urls+'/get_dokter',
					    			data : 'kd_poli='+dt.data('kd_poli')+'&tanggal_periksa='+dt.data('tgl_registrasi'),
					    			success :function(res){
					    				var dta = JSON.parse(res);
					    				if(dta.status=='Tersedia'){
					    					$('#list_dokter_booking_ed').html(dta.data);
					    					$('#modalEditRegPeriksa [name="kd_dokter"]').val(dt.data('kd_dokter'));
	    									$('#modalEditRegPeriksa #kd_dokter').select2({width:'100%'}).trigger('change');
					    					//$('#modalAddRegPeriksa #save').css('display','block');
					    				}else{
					    					toastr_msg(dt.data);
					    				}
					    			}
					    		});
	    					}
	    					
	    				}else{
	    					toastr_msg(dts.msg);
	    					$('#modalEditRegPeriksa #tanggal_periksa_reg').val('');
	    					$('#modalEditRegPeriksa #kd_poli_reg').val('');
	    					$('#modalEditRegPeriksa #kd_poli_reg').select2({width:'100%'});
	    				}
	    			}
	    		});
	    	}
	    	$('#modalEditRegPeriksa').modal();
	    });

	    /*
		SEP
	    */
	    $('#modalCreateSep').on('change','#nopeserta_sep',function(){
	    	var no_peserta = $(this).val();
	    	$.ajax({
				type : 'post',
				url : urls+'/getInfoKartu',
				data : 'no_peserta='+no_peserta+'&kd_pj=BPJ',
				success : function(res){
					var stts = JSON.parse(res);
					$('#modalCreateSep #statuskartu_sep').val(stts.status);
				}
			});
	    });

	    $('#modalCreateSep').on('change','#norujukan_sep',function(){
	    	var no_rujukan = $(this).val();
	    	$.ajax({
				type : 'post',
				url : urls+'/cekRujukan',
				data : 'no_rujukan='+no_rujukan+'&kd_pj=BPJ',
				success : function(res){
					var dtrj = JSON.parse(res);
					if(dtrj.info=='Ok'){
						var tglrujukan = $.datepicker.formatDate('m/dd/yy', new Date(dtrj.tglkunjungan));
						$('#modalCreateSep #tglrujukan_sep').val(tglrujukan);
						$('#modalCreateSep #notelp_sep').val(dtrj.mr_notelp);
						$('#modalCreateSep #eksekutif_sep').val(0);
						$('#modalCreateSep #cob_sep').val(0);
						$('#modalCreateSep #katarak_sep').val(0);
						$('#modalCreateSep #asalrujukan_sep').val(1);
						$('#modalCreateSep #kodeppk_sep').val(dtrj.provperujuk_kode);
						$('#modalCreateSep #namappk_sep').val(dtrj.provperujuk_nama);
						$('#modalCreateSep #jnspelayanan_sep').val(dtrj.kode_pelayanan);
						$('#modalCreateSep #klsrawat_sep').val(dtrj.hakkelas_kode);
					}else{
						toastr_msg(dtrj.msg);
					}
				}
			});
	    });

	   	$("#modalCreateSep #kodediagnosa").autocomplete({
	        source: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getdiagnosabpjs';?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	//alert(ui.item.id);
	        	event.preventDefault();
	        	$('#modalCreateSep #kodediagnosa_sep').val(ui.item.id);
	        	$('#modalCreateSep #namadiagnosa_sep').val(ui.item.name);
	        }
	    });

	    $("#modalCreateSep #politujuan").autocomplete({
	        source: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getpolibpjs';?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	//alert(ui.item.id);
	        	event.preventDefault();
	        	$('#modalCreateSep #politujuan_sep').val(ui.item.id);
	        	$('#modalCreateSep #namapolitujuan_sep').val(ui.item.name);
	        }
	    });

	    // Single Select
	    $('#kdpropinsi_sep').on('select2:select', function (e) {
		    var data = e.params.data;
		   	$('#modalCreateSep #nmpropinsi_sep').val(data.text);
		});

		$("#modalCreateSep #dokterdpjp").autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getdokterdpjpbpjs';?>",
					type: 'post',
					dataType: "json",
					data: {
						search: request.term,
						jnspelayanan_sep : $('#modalCreateSep [name="jnspelayanan"]').val(),
						tglpelayanan_sep : $('#modalCreateSep [name="tgl_sep"]').val()
					},
					success: function(data) {
						response(data);
					}
				});
			},
				focus: function( event, ui ) {
		        	//alert(ui.item.id);
		        	event.preventDefault();
		        	$('#modalCreateSep #dokterdpjp_sep').val(ui.item.id);
		        	$('#modalCreateSep #namadokterdpjp_sep').val(ui.item.name);
		        }
		});

		$('#modalCreateSep').on('change','#kdpropinsi_sep',function(){
			var kd_prop = $(this).val();
			if(kd_prop!=''){
				$.ajax({
					type : 'post',
					url : urls+'/getKabBpjs',
					data : 'kd_prop='+kd_prop,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.status==''){
							$('#kabsepbpjs').html(dt.data);
							$('#modalCreateSep #kdkabupaten_sep').select2();
							$('#kdkabupaten_sep').on('select2:select', function (e) {
							    var data = e.params.data;
							   	$('#modalCreateSep #nmkabupaten_sep').val(data.text);
							});
						}else{
							toastr_msg(dt.data);
						}
					}
				});
			}
		});

		$('#modalCreateSep').on('change','#kdkabupaten_sep',function(){
			var kd_kab = $(this).val();
			if(kd_kab!=''){
				$.ajax({
					type : 'post',
					url : urls+'/getKecBpjs',
					data : 'kd_kab='+kd_kab,
					success : function(res){
						var dt = JSON.parse(res);
						if(dt.status==''){
							$('#kecsepbpjs').html(dt.data);
							$('#modalCreateSep #kdkecamatan_sep').select2();
							$('#kdkecamatan_sep').on('select2:select', function (e) {
							    var data = e.params.data;
							   	$('#modalCreateSep #nmkecamatan_sep').val(data.text);
							});
						}else{
							toastr_msg(dt.data);
						}
					}
				});
			}
		});

		$('#modalCreateSep').on('click','#save',function(){
	    	var tgl_lahir = $('#modalCreateSep [nama="tgl_lahir"]').val();
			var data_post = $('#modalCreateSep :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			if(tgl_lahir!=''){
				$.ajax({
			        url: url+'/createsep',
			        type: "post",
			        data: data_post+'&url='+url,
			        success: function (response) {
			        	var dt = JSON.parse(response);
			        	if(dt.error==''){
			        		window.location = dt.url;
			        		toastr_info(dt.msg);
			        	}else{
			        		toastr_msg(dt.msg);
			        	}
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}else{
				toastr_msg('Mohon maaf system tidak bisa menyimpan data pasien ini.');
			}
			
		});

		/*
		UPDATE SEP
		*/
		$('.formUpdatesep').click(function(){
			formUpdateSep();
			var dt = $(this);
			var tgllahir = $.datepicker.formatDate('m/dd/yy', new Date(dt.data('tgllahir')));

			$('#modalUpdateSep #nosep').val(dt.data('nosep'));
			$('#modalUpdateSep #norawat_sepup').val(dt.data('no_rawat'));
			$('#modalUpdateSep #norkmmedis_sepup').val(dt.data('no_rkm_medis'));
			$('#modalUpdateSep #nmpasien_sepup').val(dt.data('nm_pasien'));
			$('#modalUpdateSep #pekerjaan_sepup').val(dt.data('pekerjaan'));
			$('#modalUpdateSep #tgllahir_sepup').val(tgllahir);
			$('#modalUpdateSep #jk_sepup').val(dt.data('jk'));
			$('#modalUpdateSep #nopeserta_sepup').val(dt.data('no_peserta'));
			
			if(dt.data('nosep')!=''){
				$.ajax({
					type : 'post',
					url : urls+'/getBridgingSep',
					data : 'no_sep='+dt.data('nosep'),
					success : function(res){
						var stts = JSON.parse(res);
						
						$('#modalUpdateSep #statuskartu_sepup').val('AKTIF');
						var tglrujukan = $.datepicker.formatDate('m/dd/yy', new Date(stts.data[0].tglrujukan));
						var tglsep = $.datepicker.formatDate('m/dd/yy', new Date(stts.data[0].tglsep));

						if(stts.data[0].asal_rujukan=='1. Faskes 1'){
							var asal_rujukan = 1;
						}else{
							var asal_rujukan = 2;
						}

						//alert(asal_rujukan); cara memunculkan json multidimensi
						$('#modalUpdateSep #tglrujukan_sepup').val(tglrujukan);
						$('#modalUpdateSep #tglsep_sepup').val(tglsep);
						$('#modalUpdateSep #notelp_sepup').val(stts.data[0].notelep);
						$('#modalUpdateSep #eksekutif_sepup').val(stts.data[0].eksekutif);
						$('#modalUpdateSep #cob_sepup').val(stts.data[0].cob);
						$('#modalUpdateSep #katarak_sepup').val(stts.data[0].katarak);
						$('#modalUpdateSep #asalrujukan_sepup').val(asal_rujukan);
						$('#modalUpdateSep #kodeppk_sepup').val(stts.data[0].kdppkrujukan);
						$('#modalUpdateSep #namappk_sepup').val(stts.data[0].nmppkrujukan);
						$('#modalUpdateSep #jnspelayanan_sepup').val(stts.data[0].jnspelayanan);
						$('#modalUpdateSep #klsrawat_sepup').val(stts.data[0].klsrawat);
						$('#modalUpdateSep #norujukan_sepup').val(stts.data[0].no_rujukan);
						$('#modalUpdateSep #noskdp_sepup').val(stts.data[0].noskdp);
						$('#modalUpdateSep #catatan_sepup').val(stts.data[0].catatan);
						$('#modalUpdateSep #dokterdpjp_sepup').val(stts.data[0].kddpjp);
						$('#modalUpdateSep #namadokterdpjp_sepup').val(stts.data[0].nmdpdjp);
						$('#modalUpdateSep #politujuan_sepup').val(stts.data[0].kdpolitujuan);
						$('#modalUpdateSep #namapolitujuan_sepup').val(stts.data[0].nmpolitujuan);
						$('#modalUpdateSep #kodediagnosa_sepup').val(stts.data[0].diagawal);
						$('#modalUpdateSep #namadiagnosa_sepup').val(stts.data[0].nmdiagnosaawal);
					}
				});
			}
			$("#tglsep_sep").datepicker().datepicker("setDate", new Date());
			$("#tglkll_sep").datepicker().datepicker("setDate", new Date());
		});

		$('.formUpdateTglsep').click(function(){
			var dt = $(this).data('noseptglpulang');
			$('#modalTglsepSep #noseppulang').val(dt);
		});

		$("#modalUpdateSep #kodediagnosa_update").autocomplete({
	        source: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getdiagnosabpjs';?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	//alert(ui.item.id);
	        	event.preventDefault();
	        	$('#modalUpdateSep #kodediagnosa_sepup').val(ui.item.id);
	        	$('#modalUpdateSep #namadiagnosa_sepup').val(ui.item.name);
	        }
	    });

	    $("#modalUpdateSep #politujuan_update").autocomplete({
	        source: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getpolibpjs';?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	//alert(ui.item.id);
	        	event.preventDefault();
	        	$('#modalUpdateSep #politujuan_sepup').val(ui.item.id);
	        	$('#modalUpdateSep #namapolitujuan_sepup').val(ui.item.name);
	        }
	    });

	    $("#modalUpdateSep #dokterdpjp_update").autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getdokterdpjpbpjs';?>",
					type: 'post',
					dataType: "json",
					data: {
						search: request.term,
						jnspelayanan_sep : $('#modalUpdateSep [name="jnspelayanan"]').val(),
						tglpelayanan_sep : $('#modalUpdateSep [name="tgl_sep"]').val()
					},
					success: function(data) {
						response(data);
					}
				});
			},
				focus: function( event, ui ) {
		        	//alert(ui.item.id);
		        	event.preventDefault();
		        	$('#modalUpdateSep #dokterdpjp_seppu').val(ui.item.id);
		        	$('#modalUpdateSep #namadokterdpjp_sepup').val(ui.item.name);
		        }
		});

		$('#modalUpdateSep').on('click','#update',function(){
			var data_post = $('#modalUpdateSep :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/updatesep',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	if(dt.error==''){
		        		window.location = dt.url;
		        		toastr_info(dt.msg);
		        	}else{
		        		toastr_msg(dt.msg);
		        	}
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });	
		});

		$('#modalTglsepSep').on('click','#update',function(){
			var data_post = $('#modalTglsepSep :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/updatetglsep',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	if(dt.error==''){
		        		window.location = dt.url;
		        		toastr_info(dt.msg);
		        	}else{
		        		toastr_msg(dt.msg);
		        	}
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });	
		});

		$('.deletesep').click(function(){
			if(confirm('Apakah anda yakin ingin menghapus data ini?')){
				var dt = $(this).data('noseps');
				window.location = '<?php echo $url.$search.'/'.$limit.'/1/deletesep/';?>' + dt;
			}
		});

		/*
		Area Modal Add Reg Periksa IGD
		*/
		$('#modalAddRegPeriksaIgd').on('change','#tanggal_periksa_regigd',function(){
	    	var tanggal_periksa_reg = $(this).val();
	    	var no_rkm_medis = $('#no_rkm_medis_regigd').val();
	    	//alert(no_rkm_medis);
	    	if(tanggal_periksa_reg!='' && no_rkm_medis!=''){
	    		$.ajax({
	    			type : 'post',
	    			data : 'tanggal_periksa='+tanggal_periksa_reg+'&no_rkm_medis='+no_rkm_medis,
	    			url : urls+'/get_validasi',
	    			success : function(res){
	    				var dt = JSON.parse(res);
	    				if(dt.error==2){
	    					$('#modalAddRegPeriksaIgd #list_poliklinikigd').html(dt.msg);
	    					$('#modalAddRegPeriksaIgd #kd_poli_regigd').select2();

	    				}else{
	    					toastr_msg(dt.msg);
	    					$('#modalAddRegPeriksaIgd #tanggal_periksa_regigd').val('');
	    					$('#modalAddRegPeriksaIgd #kd_poli_regigd').val('');
	    					$('#modalAddRegPeriksaIgd #kd_poli_regigd').select2({width:'100%'});
	    				}
	    			}
	    		});
	    	}else if(no_rkm_medis==''){
	    		$('#modalAddRegPeriksaIgd #tanggal_periksa_regigd').val('');
	    		toastr_msg('Silahkan isi dan cari nama pasien di formulir no rekam media.');
	    	}else{
	    		$('#modalAddRegPeriksaIgd #tanggal_periksa_regigd').val('');
	    		toastr_msg('Tanggal kunjungan harus diisi.');
	    	}
	    });

	    $('#modalAddRegPeriksaIgd').on('click','#save',function(){
	    	var tgl_lahir = $('#modalAddRegPeriksaIgd [nama="tgl_lahir"]').val();
			var data_post = $('#modalAddRegPeriksaIgd :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			if(tgl_lahir!=''){
				$.ajax({
			        url: url+'/storepasienigd',
			        type: "post",
			        data: data_post+'&url='+url,
			        success: function (response) {
			        	var dt = JSON.parse(response);
			        	if(dt.error==''){
			        		window.location = dt.url;
			        		toastr_info(dt.msg);
			        	}else{
			        		toastr_msg(dt.msg);
			        	}
			        	//
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}else{
				toastr_msg('Mohon maaf system tidak bisa menyimpan data pasien ini.');
			}	
		});

		$('.formtransferranap').click(function(){
			formTransferRanap();
			var dt = $(this);
			//alert(dt.data('no_rawat')+'-'+dt.data('no_rkm_medis')+'-'+dt.data('nm_pasien'));
			$('#no_rawat_transferranap').val(dt.data('no_rawat'));
			$('#no_rkm_medis_transferranap').val(dt.data('no_rkm_medis'));
			$('#nm_pasien_transferranap').val(dt.data('nm_pasien'));
		});

		$('#kodediagnosa_kamarinap').autocomplete({
	        source: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/getdiagnosa';?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	//alert(ui.item.id);
	        	event.preventDefault();
	        	$('#diagnosa_awal').val(ui.item.value);
	        }
	    });

	    $('#kamarinap_trf').autocomplete({
	        source: "<?php echo $url.$search.'/'. $limit .'/'. $offset.'/kamarinap';?>",
	        minLength: 3,
	        focus: function( event, ui ) {
	        	//alert(ui.item.id);
	        	event.preventDefault();
	        	$('#kd_kamar_inap').val(ui.item.id);
	        	$('#lama_inap').val(1);
	        	$('#trf_kamar_inap').val(ui.item.nilai);
	        	$('#ttl_biaya_inap').val(ui.item.total);
	        }
	    });

	    $('#modalTransferRanap').on('click','#save',function(){
	    	var data_post = $('#modalTransferRanap :input').serialize();
			var url = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
			$.ajax({
		        url: url+'/stroretrftranap',
		        type: "post",
		        data: data_post+'&url='+url,
		        success: function (response) {
		        	var dt = JSON.parse(response);
		        	if(dt.error==''){
		        		window.location = dt.url;
		        		toastr_info(dt.msg);
		        	}else{
		        		toastr_msg(dt.msg);
		        	}
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
	    });
	});

	function getLiveNoRkmMedis() {
        setTimeout(function () {
            $.ajax({
                url : "<?php echo $url.'/10/1/get_normmedic';?>",
                type: 'get',
                success: function (res) {
                	var dt = JSON.parse(res);
                    $('#modalAddForm [name="no_rkm_medis').val(dt.no_rkm_medis);
                },
                complete: getLiveNoRkmMedis
            });
        }, 30000);
    }

    function getNoRkmMedis() {
        $.ajax({
	        url : "<?php echo $url.'/10/1/get_normmedic';?>",
	        type: 'get',
	        success: function (res) {
	        	var dt = JSON.parse(res);
	            $('#modalAddForm [name="no_rkm_medis"]').val(dt.no_rkm_medis);
	        }
	    });
    }

	function formAdd()
	{
		getNoRkmMedis();
		getLiveNoRkmMedis();
		$('#modalAddForm #save').css('display','block');
		$('#modalAddForm #update').css('display','none');
		/*$('#modalAddForm :input').val('');*/
        $('#modalAddForm').modal();
	}

	function formRegPeriksa(){
		//$('#modalAddRegPeriksa #save').css('display','block');
		$('#modalAddRegPeriksa #update').css('display','none');
		$('#modalAddRegPeriksa :input').val('');
        $('#modalAddRegPeriksa').modal();
	}

	function formRegIgd(){
		$('#modalAddRegPeriksaIgd #save').css('display','block');
		//$('#modalAddRegPeriksaIgd :input').val('');
		$('#modalAddRegPeriksaIgd').modal();
	}

	function formCreateSep(){
		$('#modalCreateSep :input').val('');
		var urls = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		var dt = $('#formsep');
		var tgllahir = $.datepicker.formatDate('m/dd/yy', new Date(dt.data('tgllahir')));
		
		$('#modalCreateSep #norawat_sep').val(dt.data('no_rawat'));
		$('#modalCreateSep #norkmmedis_sep').val(dt.data('no_rkm_medis'));
		$('#modalCreateSep #nmpasien_sep').val(dt.data('nm_pasien'));
		$('#modalCreateSep #pekerjaan_sep').val(dt.data('pekerjaan'));
		$('#modalCreateSep #tgllahir_sep').val(tgllahir);
		$('#modalCreateSep #jk_sep').val(dt.data('jk'));
		$('#modalCreateSep #nopeserta_sep').val(dt.data('no_peserta'));

		getDataBooking(dt.data('no_rkm_medis'),dt.data('kdpoli'),dt.data('kddokter'),dt.data('tglregistrasi'));
		
		if(dt.data('no_peserta')!=''){
			$.ajax({
				type : 'post',
				url : urls+'/getInfoKartu',
				data : 'no_peserta='+dt.data('no_peserta')+'&kd_pj=BPJ',
				success : function(res){
					var stts = JSON.parse(res);
					$('#modalCreateSep #statuskartu_sep').val(stts.status);
				}
			});
		}

		$("#tglsep_sep").datepicker().datepicker("setDate", new Date());
		$("#tglkll_sep").datepicker().datepicker("setDate", new Date());
		$('#modalCreateSep #save').css('display','block');
		$('#modalCreateSep #update').css('display','none');
		$('#modalCreateSep').modal();
	}

	function getDataBooking(no_rkm_medis='',kdpoli='',kddokter='',tglreg=''){
		var urls = '<?php echo $url.$search.'/'. $limit .'/'. $offset;?>';
		$.ajax({
			type : 'post',
			url : urls+'/getDataBooking',
			data : 'no_rkm_medis='+no_rkm_medis+'&kd_poli='+kdpoli+'&kd_dokter='+kddokter+'&tgl_registrasi='+tglreg,
			success : function(res){
				var dt = JSON.parse(res);
				if(dt.status=='Ok'){
					$('#modalCreateSep #nilai_rujukan_booking').html(dt.data);
					$('#modalCreateSep #kategori_data_booking').html(dt.info);
					$('#modalCreateSep #infobooking').css('display','block');
				}else{
					$('#modalCreateSep #infobooking').css('display','none');
				}
				
			}
		});
	}

	function formUpdateSep(){
		$('#modalUpdateSep :input').val('');
		$('#modalUpdateSep').modal();
	}

	function formUpdateTglPulang(){
		$('#modalTglsepSep').modal();
	}

	function formTransferRanap(){
		$('#modalTransferRanap').modal();
	}

	function getPoli(){
		var tanggal_periksa_reg = $('#modalAddRegPeriksa [name="tanggal_periksa"]').val();
    	$.ajax({
    		type : 'post',
    		url : urls+'/get_poli',
    		data : 'tanggal_periksa='+tanggal_periksa_reg,
    		success : function(res){
    			var dt = JSON.parse(res);
    			if(dt.status == 'Tersedia'){
    				$('#modalAddRegPeriksa #list_poliklinik').html(dt.data);
    				$('#modalAddRegPeriksa #kd_poli_reg').select2();
    			}else{
    				toastr_msg(dt.data);
    			}
    		}
    	});
	}

	function formDelete() {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            var pkC = $('[name="id[]"]:checked'), idC = [];

            for (var ip = 0; ip < pkC.length; ip++) {
                idC.push(pkC.eq(ip).val());
            }

            window.location = '<?php echo $url.$search.'/'.$limit.'/1/destroy/';?>' + (idC.join('del'));
        }
    }

    function toastr_msg(msg,time='2000'){
		toastr.options.fadeOut = time;
		$context = 'error';
        $message = msg;
        $position = 'top-full-width';
        $positionClass = 'toast-' + $position;
        if(msg!=''){
        	toastr.remove();
	        toastr[$context]($message, '', {
	            positionClass: $positionClass
	        });
        }else{
        	toastr.remove();
        }   
	}

	function toastr_info(msg,time='2000'){
		toastr.options.fadeOut = time;
		$context = 'success';
        $message = msg;
        $position = 'top-full-width';
        $positionClass = 'toast-' + $position;
        if(msg!=''){
        	toastr.remove();
	        toastr[$context]($message, '', {
	            positionClass: $positionClass
	        });
        }else{
        	toastr.remove();
        }   
	}
</script>