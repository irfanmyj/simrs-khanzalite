<?php  
if(@acl(decrypt_aes($this->uri->segment(2)))['update']=='Y') {
	$addClass = 'tr_mod_reg_periksa';
}
?>
<table class="table m-b-0 c_list table-hover" style="margin-top: 3px;">
  <thead class="l-blush text-white">
    <tr>
      <th style="width: 1rem;" class="text-center">#</th>
      <th style="width: 30px;" class="text-center">No</th>
      <th class="text-center">Aksi</th>
      <th>No Reg</th>
      <th>No Rekam Medis</th>
      <th>Nama Pasien</th>
      <th>Dokter</th>
      <th>Poliklinik</th>
      <th>Cara Bayar</th>
      <th style="width: 200px;">Dibuat Tanggal</th>
    </tr>
  </thead>  
  <tbody>
  <?php $no=1; foreach ($data as $key => $val) { ?>
    <tr style="cursor:pointer" <?php echo showValRec($val);?>>
      <td class="text-center chk"><input type="checkbox" name="id[]" value="<?php echo str_replace('/', '-', $val->no_rawat);?>"></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo ($no++);?></td>
      <td>
        <div class="btn-group" role="group">
            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Aksi
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
              <a href="#" onclick="formTransferRanap()" data-no_rawat="<?php echo $val->no_rawat; ?>" data-no_rkm_medis="<?php echo $val->no_rkm_medis; ?>" data-nm_pasien="<?php echo $val->nm_pasien; ?>" class="dropdown-item formtransferranap" title="Tombol Untuk Transfer Rawatinap">Transfer Rawatinap</a>
              <?php if(empty($val->no_sep) && $val->kd_pj=='BPJ'){ ?>
              <a href="#" onclick="formCreateSep()" id="formsep" data-no_rawat="<?php echo $val->no_rawat; ?>" data-no_rkm_medis="<?php echo $val->no_rkm_medis; ?>" data-nm_pasien="<?php echo $val->nm_pasien; ?>" data-jk="<?php echo $val->kode_jk; ?>" data-pekerjaan="<?php echo $val->pekerjaan; ?>" data-no_peserta="<?php echo $val->no_peserta; ?>" data-tgllahir="<?php echo $val->tgl_lahir; ?>" data-kddokter="<?php echo $val->kd_dokter; ?>" data-kdpoli="<?php echo $val->kd_poli; ?>" data-tglregistrasi="<?php echo $val->tgl_registrasi; ?>" class="dropdown-item" title="Tombol Buat SEP BPJS">Buat Sep</a>
              <?php } ?>
              <?php if(!empty($val->no_sep)){ ?>
                <a href="#" onclick="formUpdateSep()" data-no_rawat="<?php echo $val->no_rawat; ?>" data-no_rkm_medis="<?php echo $val->no_rkm_medis; ?>" data-nm_pasien="<?php echo $val->nm_pasien; ?>" data-jk="<?php echo $val->kode_jk; ?>" data-pekerjaan="<?php echo $val->pekerjaan; ?>" data-no_peserta="<?php echo $val->no_peserta; ?>" data-tgllahir="<?php echo $val->tgl_lahir; ?>" data-nosep="<?php echo $val->no_sep; ?>" class="dropdown-item formUpdatesep" title="Tombol Update SEP BPJS">Update Sep</a>
                <a href="#" onclick="formUpdateTglPulang()" data-noseptglpulang="<?php echo $val->no_sep; ?>" class="dropdown-item formUpdateTglsep" title="Tombol Update Tanggal Pulang">Update Tanggal Pulang Sep</a>
                <button data-noseps="<?php echo encrypt_aes($val->no_sep); ?>" class="dropdown-item deletesep" title="Tombol Delete SEP BPJS">Hapus Sep</button>
                <a href="<?php echo $url.'/10/0/cetaksep/'.encrypt_aes($val->no_sep.'-v1'); ?>" id="cetaksep" target="_blank" class="dropdown-item" title="CETAK SEP BPJS">Cetak Sep V.1</a>
                <a href="<?php echo $url.'/10/0/cetaksep/'.encrypt_aes($val->no_sep.'-v2'); ?>" id="cetaksep" target="_blank" class="dropdown-item" title="CETAK SEP BPJS">Cetak Sep V.2</a>
              <?php } ?>
            </div>
        </div>
      </td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->no_reg;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->no_rkm_medis;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_pasien;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_dokter;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->nm_poli;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->png_jawab;?></td>
      <td class="<?php echo isset($addClass) ? $addClass : ''; ?>"><?php echo $val->tgl_registrasi;?></td>
    </tr>
  <?php } ?>
  <?php 
  if (count($data) == 0) {
      echo '<tr><td colspan="100%" style="text-align:center">Tidak ada data tersimpan</td></tr>';
  }
  ?>
  </tbody>          
</table>